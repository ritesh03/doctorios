//
//  CommonClass.swift
//  tms
//
//  Created by Nitish Sharma on 29/12/17.
//  Copyright © 2017 Nitish Sharma. All rights reserved.
//

import UIKit

class CommonClass: NSObject {
    
    class func textFieldPadding() -> UIView {
        
        let indentView = UIView(frame: CGRect(x:0,y:0, width: 50, height :20))
        //        indentView.backgroundColor = UIColor.blue
        return indentView
        
    }
   
    
    class func showAlert(view: UIViewController, title: String, message: String, completion: @escaping (_ action: UIAlertAction) -> Void) {
        func localizeValue(key: String) -> String  {
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: localizeValue(key: "Ok"), style: UIAlertActionStyle.default, handler: { action in
            
            completion(action)
        }
        ))
        view.present(alert, animated: true, completion: nil)
    }
    
    class func showAlert(_ onController:UIViewController!, title:String?,message:String? = nil ,cancelButton:String = "OK",buttons:[String]? = nil,actions:((_ alertAction:UIAlertAction,_ index:Int)->())? = nil) {
        // make sure it would run on main queue
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: cancelButton, style: UIAlertAction.Style.cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        if let _buttons = buttons {
            for button in _buttons {
                let action = UIAlertAction(title: button, style: .default) { (action) in
                    let index = _buttons.index(of: action.title!)
                    actions?(action,index!)
                }
                alertController.addAction(action)
            }
        }
        onController.present(alertController, animated: true, completion: nil)
    }

    class func convertFormatOfDate(date: String, originalFormat: String, destinationFormat: String) -> String!
    {
        let dateOriginalFormat = DateFormatter()
        dateOriginalFormat.dateFormat = originalFormat
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.dateFormat = destinationFormat
        let dateFromString = dateOriginalFormat.date(from: date)
        let dateFormated = dateDestinationFormat.string(from: dateFromString!)
        return dateFormated
    }
    
    class func checkAndEditCountryCodeFor(countryCode number: String) -> String {
        var countryCode = number
        if countryCode.hasPrefix("+") {
        } else {
            countryCode.insert("+", at: number.startIndex)
        }
        return countryCode
    }

    class func convertStringToDate(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        let date = dateFormatter.date(from:date)!
        return date
    }
    
    class func getLabelHeight(label: UILabel, text: String, width: CGFloat, font: UIFont) -> CGFloat {
        label.frame.size.width = width
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.size.height
    }
    
    class func getTextFieldHeight(textField: UITextField, text: String, width: CGFloat, font: UIFont) -> CGFloat {
        textField.frame.size.width = width
        textField.font = font
        textField.text = text
        textField.sizeToFit()
        return textField.frame.size.height
    }

    class func checkAndEditPhoneNumberFor(phoneNumber number: String) -> String {
        var phoneNumber = number
        if phoneNumber.hasPrefix("0") {
            phoneNumber.remove(at: number.startIndex)
        }
        return phoneNumber
    }
    
//    class func isLiveServer() -> Bool {
//        if Config.baseUrl == "http://18.218.116.215/blog/public/api" {
//            return true
//        } else {
//            return false
//        }
//    }
    class func isLiveServer() -> Bool {
        if Config.baseUrl == "https://clinido.com/clinido/public/api" {
            return true
        } else {
            return false
        }
    }
}
//
