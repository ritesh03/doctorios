
//
//  Server.swift
//
//  Created by Praveen Sharma on 12/01/18.
//  Copyright © 2018 Praveen Sharma. All rights reserved.

import UIKit
import Alamofire
import Foundation

class Server: NSObject {

    // Post Request
    
    func performPostRequest(requestURL: String, params: [String: Any]?, completion: @escaping (_ result : Bool, _ data : NSDictionary?) -> Void) {
        
        Alamofire.request(requestURL, method:.get, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            // print(response)
            
            switch(response.result) {
            case .success(_):
                //print(response.result)
                if let json = response.result.value as? NSDictionary
                {
                    //print(json)
//                    let error = json["results"]!
                    
//                    print("errorFinal======",error)
                    
                    completion(true,json)
                    
                    
//                    if error == 200
//                    {
//                        completion(true, error, json)
//                    }
//                    else if error == 100 {
//                        completion(true,error,nil)
//                    }
//                    else {
//                        completion(false, error, json)
//                    }
                }
                
                break
                
            case .failure(_):
                //print(response.result.error!)
                completion(false, nil)
                break
                
            }
        }
    }
}
