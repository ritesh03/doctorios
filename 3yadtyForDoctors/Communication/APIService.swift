//
//  APIManager.swift
//  iOSArchitecture
//
//  Created by Amit on 23/02/18.
//  Copyright © 2018 smartData. All rights reserved.
//

import Foundation
import UIKit

private let sharedManager = APIService()

public enum Method: Int {
    case post,get,put,delete
}

extension Method {
    
    var name:String {
        switch self {
        case .post:
            return "POST"
        case .get:
            return "GET"
        case .put:
            return "PUT"
        default:
            return "DELETE"
        }
    }
}

extension String {
    var nsdata: Data {
        return self.data(using: String.Encoding.utf8)!
    }
}

struct File {
    let name: String!
    let image: UIImage!
    let type: Int!
    

    init(name: String, image: UIImage, type: Int) {
        self.name = name
        self.image = image
        self.type = type
    } 
}

enum Result <T>{
    case Success(T)
    case Error(String)
}

public class APIService: NSObject {
    
    class var shared : APIService {
        return sharedManager
        
    }
    
    func startService(with method:Method,baseURL:String,path:String,parameters:Dictionary<String,Any>?,files:Array<File>?, completion: @escaping (Result<Any>) -> Void) {
        var finalUrl = baseURL + path
    
        switch method {
        case .get:
            if let params = parameters,params.count > 0 {
                finalUrl = finalUrl + "&" + buildParams(parameters: params)
                if let token = AppInstance.shared.access_token,!token.isEmpty {
                    finalUrl = finalUrl + "&access_token=\(token)"
                }
            } else {
                if let token = AppInstance.shared.access_token,!token.isEmpty {
                    finalUrl = finalUrl + "?access_token=\(token)"
                }
            }
        
        case .delete:
            if let token = AppInstance.shared.access_token,!token.isEmpty {
                finalUrl = finalUrl + "?access_token=\(token)"
            }
            
        default:
           
            break
        }
        
        guard let url = URL(string:finalUrl) else { return completion(.Error("Invalid URL, we can't proceed.")) }
        print(url)
        let request = self.buildRequest(with: method, url: url, parameters: parameters, files: files)
        
//        if method == .post {
//            if let token = AppInstance.shared.user?.remembertoken,!token.isEmpty {
//                request.addValue("\(token)", forHTTPHeaderField: "token")
//            }
//        }
        
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in

            guard error == nil else { return completion(.Error(error!.localizedDescription)) }
                guard let data = data else { return completion(.Error(error?.localizedDescription ?? "Data not found."))
            }
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? Dictionary<String,AnyObject> {

                    if let error = json["error"] as? Dictionary<String,AnyObject> {
                        let msg = error["message"] as? String ?? ""
                        completion(.Error(msg))
                    } else {
                        completion(.Success(json))
                    }
                    
                } else if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [Dictionary<String,AnyObject>] {
                    completion(.Success(json))
                }
            } catch let error {
                completion(.Error(error.localizedDescription))
            }
        }
        task.resume()
    }
}

extension APIService {
    
    func buildRequest(with method:Method,url:URL,parameters:Dictionary<String,Any>?,files:Array<File>?) -> URLRequest {
        let boundary = "Boundary-\(UUID().uuidString)"
        let boundaryPrefix = "--\(boundary)\r\n"
        let boundarySuffix = "--\(boundary)--\r\n"
        print(url)
        var request:URLRequest? = nil
        switch method {
        case .get:
            request = URLRequest(url: url)
            request?.addValue("application/json", forHTTPHeaderField: "Accept")
            request?.addValue("application/json", forHTTPHeaderField: "Content-Type")
            if AppInstance.shared.user?.remembertoken != nil{
                request?.addValue(String(describing: AppInstance.shared.user!.remembertoken!), forHTTPHeaderField: "token")
            }
        case .post,.put:
            request = URLRequest(url: url)
            if AppInstance.shared.user?.remembertoken != nil{
                request?.addValue(String(describing: AppInstance.shared.user!.remembertoken!), forHTTPHeaderField: "token")
            }
            if let images = files,images.count > 0{
                request?.addValue("multipart/form-data; boundary=" + boundary, forHTTPHeaderField: "Content-Type")
                let data = NSMutableData()
                if let params = parameters,params.count > 0{
                    print(params)
                    for (key, value) in params {
                        data.append("--\(boundary)\r\n".nsdata)
                        data.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".nsdata)
                        data.append("\((value as AnyObject).description!)\r\n".nsdata)
                    }
                }
                for (_,file) in images.enumerated() {
                    data.append(boundaryPrefix.nsdata)
                    if file.type == 3{
                        data.append("Content-Disposition: form-data; name=\"image[]\"; filename=\"\(file.name!)\"\r\n\r\n".nsdata)
                    }else if file.type == 2{
                        data.append("Content-Disposition: form-data; name=\"dr_certificate\"; filename=\"\(file.name!)\"\r\n\r\n".nsdata)
                    }else if file.type == 1{
                         data.append("Content-Disposition: form-data; name=\"image\"; filename=\"\(file.name!)\"\r\n\r\n".nsdata)
                    }
            
                    if let image = file.image ,let a =  UIImageJPEGRepresentation(image,0.5){
                        data.append(a)
                        data.append("\r\n".nsdata)
                    }
                }
                data.append(boundarySuffix.nsdata)
                request?.httpBody = data as Data
                
            } else if let params = parameters,params.count > 0 {
                do {
                    let postString = buildParams(parameters: params)
                    request?.httpBody = postString.data(using: String.Encoding.utf8);
                }
            }
        default:
            request = URLRequest(url: url)
            break
        }
        
        request?.httpMethod = method.name
        
        return request!
    }
    
    func buildParams(parameters: Dictionary<String,Any>) -> String {
        var components: [(String, String)] = []
        for (key,value) in parameters {
            components += self.queryComponents(key, value)
        }
        return (components.map{"\($0)=\($1)"} as [String]).joined(separator: "&")
    }
    func queryComponents(_ key: String, _ value: Any) -> [(String, String)] {
        var components: [(String, String)] = []
        if let dictionary = value as? Dictionary<String,Any> {
            for (nestedKey, value) in dictionary {
                components += queryComponents("\(key)[\(nestedKey)]", value)
            }
        } else if let array = value as? [AnyObject] {
            for value in array {
                components += queryComponents("\(key)", value)
            }
        } else {
            components.append(contentsOf: [(escape(string: key), escape(string: "\(value)"))])
        }
        return components
    }
    func escape(string: String) -> String {
        if let encodedString = string.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) {
            return encodedString
        }
        return ""
    }
}
