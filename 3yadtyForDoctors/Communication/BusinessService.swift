//
//  BusinessService.swift
//  ThinkBeauty
//
//  Created by Amit on 02/05/18.
//  Copyright © 2018 Ritesh Chopra. All rights reserved.
//

import UIKit

// #show and hide loader in this class depending on your needs.
// #you can make service class according to module
// #show errors in this class, if you have any need where you want to show in controller class, then just by-pass error method.

public class BusinessService: APIService {
    
    // MARK: Dr mobileVarification
    func mobileVarification(with countrycode:String, mobile:String,path:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["countrycode":countrycode, "mobile":mobile]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: path, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            target?.logOut()
                            complition(false)
                        } else {
                            if dict["message"] as? String == "Mobile number already registered" {
                                target?.showAlert(with: self.localizeValue(key: "Mobile number already registered"))
                            }
                           
                            complition(false)
                        }
                    } else {
                        target?.showAlert(with: (((data as! Dictionary<String,Any>)["response"]!) as! NSDictionary)["message"]! as? String)
                        complition(false)
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    func resenOTP(with mobile:String, countryCode:String,endPoint:String, target:UIViewController?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["mobile":mobile, "countrycode":countryCode]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: endPoint, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    // MARK: Dr otpVarify
    func otpVarify(with mobile:String, otp:String, path:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["mobile":mobile, "otp":otp]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: path, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            
                            if dict["message"] as? String == "OTP doesn't match" ||  dict["message"] as? String == "OTP does not matched"  {
                                   target?.showAlert(with: LocalizationSystem.sharedInstance.localizedStringForKey(key: "OTP doesn't match", comment: ""))
                            }else if dict["message"] as? String == "Incorrect OTP" {
                                target?.showAlert(with: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Incorrect OTP", comment: ""))
                            }
                            
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    // MARK: Dr Signup
    func drSignup(with url: String,And param:[String : Any],image:Array<File>,
                  target:UIViewController?, complition:@escaping (User?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: url, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                     if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>, let userDict = dict["data"] as? Dictionary<String,Any>   {
                                AppInstance.shared.doctorSignUp = userDict
                                complition(User(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    func addClinic(with url: String,And param:[String : Any],image:Array<File>,
                  target:UIViewController?, complition:@escaping (AnyObject?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: url, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(dict as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    func uploadClinicImages(with url: String,And param:[String : Any],image:Array<File>,
                   target:UIViewController?, complition:@escaping (AnyObject?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: url, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let data = dict["data"] as? [String]   {
                                complition(data as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
        // MARK: LogIn
    func logIn(with mobile:String,
               password:String,devicetoken:String, path:String,
                      target:BaseViewControler?, complition:@escaping (User?) -> Void){
        let param:[String : Any] = ["mobile":mobile,"password":password,"devicetoken":devicetoken,"devicetype":"2"]
            target?.showLoader(with: "Loading...")
            super.startService(with: .post, baseURL: Config.baseUrl, path: path, parameters: param, files: []) { (result) in
                DispatchQueue.main.async {
                    target?.hideLoader()
                    switch result {
                    case .Success(let data):
                        // #parse response here
                        if let dict = data as? Dictionary<String,Any> {
                            let status = dict["return"] as? Int
                            if status == 1 {
                                if let dict = data as? Dictionary<String,Any>, let userDict = dict["data"] as? Dictionary<String,Any>   {
                                    AppInstance.shared.doctorSignUp = userDict
                                    complition(User(dictionary: dict as NSDictionary))
                                } else {
                                    //target?.showAlert(with: "Network Error! Please try after sometime")
                                    target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                    complition(nil)
                                }
                            }else if status == 2{
                                
                                target?.logOut()
                                complition(nil)
                            }else{
                                if CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: mobile)) {
                                    target?.showAlert(with: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mobile or Password does not matched", comment: ""))
                                    complition(nil)
                                }else{
                                   target?.showAlert(with: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Email or Password does not matched", comment: ""))
                                    complition(nil)
                                }

                                
                            }
                        }
                    case .Error(let error):
                        // #display error message here
                        target?.showAlert(with: error)
                        complition(nil)
                    }
                }
            }
        }
    
    // MARK: addAppointment
    func addAppointment(with patient_name:String, appoinment_date:String, reservation_type:String,add_note:String,patient_id:String,devicetype:String,devicetoken:String,user_id:String, target:BaseViewControler?, complition:@escaping (User?) -> Void){
        let param:[String : Any] = ["patient_name":patient_name, "appoinment_date":appoinment_date,"reservation_type":reservation_type, "add_note":add_note,"patient_id":patient_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.add_appointment, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(User(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //getDrCategoryList
    func getDrCategoryList(with target:UIViewController?, complition:@escaping ([CategoryList]?) -> Void){
        
        target?.showLoader(with: "Loading...")
        super.startService(with: .get, baseURL: Config.baseUrl, path: Config.get_categories, parameters: nil, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>,let user = dict["data"] as? Array<Dictionary<String,Any>> {
                                complition(CategoryList.modelsFromDictionaryArray(array: user))
                            } else {
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //Get Invoices
    func getInvoices(with param:[String : Any],target:UIViewController?, complition:@escaping (AnyObject?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.invoice_dr, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(dict as AnyObject)
                            }else if let dict = data as? Array<Dictionary<String,Any>>{
                                complition(dict as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else {
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //Get Sub Categories
    func getSubCategories(with param:[String : Any],target:UIViewController?, complition:@escaping (AnyObject?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.get_subcategories, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(dict as AnyObject)
                            }else if let dict = data as? Array<Dictionary<String,Any>>{
                                complition(dict as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else {
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //drAcceptedAppointments
    func drAcceptedAppointments(with doctor_id:String,current_page:String,date:String,type:String,target:BaseViewControler?,isAnimated: Bool, complition:@escaping (Appointment?) -> Void){
        let param:[String : Any] = ["doctor_id":doctor_id, "current_page":current_page, "date":date, "type":type]
        target?.showLoader(with: "Loading...")

        if isAnimated{
            
//            target?.showLoader(with: "Loading...")
        }
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.appointment_to_doctor, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                  target?.hideLoader()
                if isAnimated{
                    
//                    target?.hideLoader()
                }
                
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(Appointment(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else {
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    
    //drPendingAppointments
    func drPendingAppointments(with doctor_id:String, type: String,target:BaseViewControler?, complition:@escaping (RequestListDoctor?) -> Void){
        let param:[String : Any] = ["doctor_id": doctor_id, "type": type]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.assistant_list, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(RequestListDoctor(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else{
                
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //approve Assistant
    func approveAssistant(with assistant_id:String,type:String,doctor_id:String,target:BaseViewControler?, complition:@escaping (Bool?) -> Void){
        let param:[String : Any] = ["assistant_id":assistant_id,"type":type,"doctor_id":doctor_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.approve_assistant, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //DrPendingPatientAppointments list
    func DrPendingPatientAppointments(with doctor_id:String,current_page:String,clinic_id:String,target:BaseViewControler?, complition:@escaping (DataAppointment?) -> Void){
        let param:[String : Any] = ["doctor_id":doctor_id, "current_page":current_page,"token":clinic_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.pending_appointments_dr, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>,let user = dict["data"] as? NSDictionary {
                                complition(DataAppointment(dictionary: user))
                            } else {
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else{
                           
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    //drPendingPatientAcceptAppointment
    func drPendingPatientAcceptAppointment(with appointment_id:String,user_id:String,set_time:String,first_time:String,token_number:String,notes:String,doctor_id:String,target:BaseViewControler?, complition:@escaping (Bool?) -> Void){
        let param:[String : Any] = ["appointment_id":appointment_id ,"user_id":user_id,"set_time":set_time,"first_time":first_time, "token_number":token_number, "notes":notes,"doctor_id":doctor_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.accept_appointment_dr, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    //target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    //drPendingPatientRejectAppointment
    func drPendingPatientRejectAppointment(with appointment_id:String,user_id:String,first_time:String,notes:String,reject_date:String,target:BaseViewControler?, complition:@escaping (Bool?) -> Void){
        let param:[String : Any] = ["appointment_id":appointment_id ,"user_id":user_id,"first_time":first_time, "notes":notes,"reject_date":reject_date]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.reject_appointment_dr, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    //target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //getNotification
    func getNotification(with userId:String,target:BaseViewControler?, complition:@escaping (Notification?) -> Void){
        let param:[String : Any] = ["userId":userId]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.get_notification, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["result"] as? String
                        if status == "Success" {
                            complition(Notification(dictionary: dict as NSDictionary))
                        }else{
                          
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    // MARK: editProfileDoctor
    func editProfileDoctor(with param:[String : Any],image:Array<File>,
                  target:UIViewController?, complition:@escaping (User?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.edit_profile_doctor, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>,let userDict = dict["data"] as? Dictionary<String,Any>    {
                                AppInstance.shared.doctorSignUp = userDict
                                complition(User(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    // MARK: getCityList
    func getCityList( target:UIViewController?, complition:@escaping (City?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .get, baseURL: Config.baseUrl, path: Config.city_listing, parameters: nil, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(City(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    // MARK: getAreaList
    func getAreaList(with city_id:String, target:UIViewController?, complition:@escaping (City?) -> Void){
        target?.showLoader(with: "Loading...")
        let param:[String : Any] = ["city_id":city_id]
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.area_listing, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(City(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    // MARK: getClinicList
    func getClinicList(with user_id:String, target:BaseViewControler?, complition:@escaping (ClinicList?) -> Void){
        target?.showLoader(with: "Loading...")
        let param:[String : Any] = ["user_id":user_id]
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.clinic_listing, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(ClinicList(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    // Delete Clinic
    func delClinicList(with user_id:String,clinic_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        target?.showLoader(with: "Loading...")
        let param:[String : Any] = ["user_id":user_id,"clinic_id":clinic_id]
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.delete_clinic, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                target?.showAlert(with: dict["message"]! as? String)
                                complition(true)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(false)
                            }
                        }
                        else if status == 2{
                            target?.logOut()
                            complition(false)
                        } else {
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    
    // Update Clinic
    func updateClinicList(with parameter:[String:AnyObject], target:UIViewController?, complition:@escaping (AnyObject?) -> Void){
        target?.showLoader(with: "Loading...")
        
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.update_clinic, parameters: parameter, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(dict as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        } else if status == 2{
                            target?.logOut()
                            complition(nil)
                        } else {
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //addAssistant
    func addAssistant(with firstname:String,lastname:String,email:String,password:String,countrycode:String,mobile:String,image:Array<File>,devicetype:String,devicetoken:String,dr_id:String,clinic_id:String,user_id:String,target:BaseViewControler?, complition:@escaping (User?) -> Void){
        let param:[String : Any] = ["firstname":firstname, "lastname":lastname,"email":email,"password":password,"countrycode":countrycode, "mobile":mobile,"devicetype":devicetype,"devicetoken":devicetoken, "dr_id":dr_id,"clinic_id":clinic_id,"user_id":user_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.add_assistant, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(User(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //drPendingPatientAcceptAppointment
    func addManualAppointment(with doctor_id:String,countrycode: String,patient_mobile:String,availability_id: String,appointment_date:String,visit_type:String,appointment_type:String,notes:String,book_by:String,assistant_id:String,set_time:String,patient_email:String,lastname:String,firstname:String,clinic_id:String,gender:String,target:BaseViewControler?, complition:@escaping (Bool?) -> Void){
        let param:[String : Any] = ["doctor_id":doctor_id,"patient_mobile":patient_mobile,"appointment_date":appointment_date, "visit_type":visit_type, "appointment_type":appointment_type,"notes":notes,"book_by":book_by,"assistant_id":assistant_id,"set_time":set_time,"patient_email":patient_email,"lastname":lastname,"firstname":firstname,"clinic_id":clinic_id,"gender":gender,"countrycode":countrycode,"availability_id":availability_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.book_manual_appointment, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    func getPaymentDetails(target:BaseViewControler?, complition:@escaping (PaymentResponseModel?) -> Void){
        target?.showLoader(with: "Loading...")
        let param:[String : Any] = ["user_id": AppInstance.shared.user!.id!]
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.payment, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        var payments = PaymentResponseModel()
                        
                        
                        if let dict = data as? Dictionary<String,Any>,let userDict = dict["data"] as? Dictionary<String,Any>    {
                           
                            payments = PaymentResponseModel(dictionary: dict as NSDictionary)!
                        }
                        
                        
                        complition(payments)
                    }else {
                        target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                        complition(nil)
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    // MARK: getAreaList
    func appointmentTimeSlots(with date:String, clinic_id:String, day:String, target:BaseViewControler?, complition:@escaping (TimeSlot?) -> Void){
        target?.showLoader(with: "Loading...")
        let param:[String : Any] = ["date":date,"clinic_id":clinic_id,"day":day]
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.appointment_time_slots, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                         complition(TimeSlot(dictionary: dict as NSDictionary))
                    }else {
                        //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                        complition(nil)
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //patientListDr
    func patientListDr(with user_id:String,search_type:String,name:String,current_page:String,target:BaseViewControler?, complition:@escaping ([Patient_info]?) -> Void){
        let param:[String : Any] = ["user_id":user_id, "search_type":search_type,"name":name,"current_page":current_page]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.patient_listing_dr, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>,let user = dict["data"] as? Dictionary<String,Any> {
                                complition(Patient_info.modelsFromDictionaryArray(array: user["data"] as! NSArray))
                            } else {
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            target?.logOut()
                            complition(nil)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    // MARK: forgot Password mobileVarification
    func forgotPassword(with mobile:String, type:String, countryCode:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["mobile":mobile, "type":type,"countrycode":countryCode]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.forgotPassword, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        } else {
                            if dict["message"] as? String == "Mobile does not registered with account" {
                                target?.showAlert(with: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mobile does not registered with account", comment: ""))
                                complition(false)
                            }
                            
                        }
                    } else {
                        target?.showAlert(with: (((data as! Dictionary<String,Any>)["response"]!) as! NSDictionary)["message"]! as? String)
                        complition(false)
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    // MARK: forgotOtpVerification
    func forgotOtpVerification(with mobile:String, otp:String, type:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["mobile":mobile, "otp":otp, "type":type]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.forgotOtpVerification, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else{
                            target?.showAlert(with: LocalizationSystem.sharedInstance.localizedStringForKey(key: "OTP doesn't match", comment: ""))
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    // MARK: forgotOtpVerification
    func changePassword(with mobile:String, password:String, type:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["mobile":mobile, "password":password, "type":type]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.changePassword, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    // MARK: changePassword
    func resetPassword(with id:String, password:String, type:String, old_password:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["id":id, "password":password, "type":type,"old_password":old_password]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.resetPassword, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            if dict["message"] as? String == "Old Password does not matched" {
                                target?.showAlert(with: self.localizeValue(key: "Old Password does not matched"))
                            }
                           
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    // MARK: blockAssistant
    func blockAssistant(with user_id:String, assistant_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["user_id":user_id, "assistant_id":assistant_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.block_assistant, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    // MARK: deleteAssistant
    func deleteAssistant(with user_id:String, assistant_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["user_id":user_id, "assistant_id":assistant_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.delete_assistant, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //patientHistory
    func patientHistory(with user_id:String,patient_id:String,current_page:String,type:String,target:BaseViewControler?, complition:@escaping (PatientHistory?) -> Void){
       
        let currentDateTime = TimeUtils.sharedUtils.stringFromdate(Date(), with: TimeUtilsFormatter.fullDate)
       let currentDatum = TimeUtils.sharedUtils.convertDateFormater(currentDateTime, fromFormat: TimeUtilsFormatter.fullDate, toFormat: TimeUtilsFormatter.serverDate)
       let currentTime = TimeUtils.sharedUtils.convertDateFormater(currentDateTime, fromFormat: TimeUtilsFormatter.fullDate, toFormat: TimeUtilsFormatter.time)
        
      let param:[String : Any] = ["user_id":user_id, "patient_id":patient_id,"current_page":current_page,"current_date":currentDatum,"current_time":currentTime,"type":type]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.patient_history_dr, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(PatientHistory(dictionary: dict as NSDictionary))
                            } else {
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
//                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    // MARK: blockAssistant
    func patientCheckinDr(with user_id:String, appointment_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["user_id":user_id, "appointment_id":appointment_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.patient_checkin_dr, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    func updatePatientCase(param:[String : Any], complition:@escaping (Bool) -> Void){
        super.startService(with: .post, baseURL: Config.baseUrl, path: ((AppInstance.shared.userType == 0) ? Config.updatePatientCase : Config.cancelAppointmentAsst), parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["result"] as? String
                        if status == "Success" {
                            complition(true)
                        }else{
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    complition(false)
                }
            }
        }
    }
    
    func paymentAuthentication(with  param:[String : Any], target:BaseViewControler?, complition:@escaping (Any?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.paymentAuthentication, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        if let url = dict["iframe"] {
                            complition(url)
                        } else {
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //Cancel Appointments
    func cancelAppointments(with user_id:String, appointment_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["user_id":user_id, "appointment_id":appointment_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: ((AppInstance.shared.userType == 0) ? Config.cancelAppointmentDr : Config.cancelAppointmentAsst), parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //checkup Appointments
    func checkupAppointment(with user_id:String, appointment_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["user_id":user_id, "appointment_id":appointment_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: ((AppInstance.shared.userType == 0) ? Config.patientCheckupDR : Config.patientCheckupAssit), parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //noShow Appointments
    func noShowAppointments(with user_id:String, appointment_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["user_id":user_id, "appointment_id":appointment_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: ((AppInstance.shared.userType == 0) ? Config.drNoShow : Config.assitantNoShow), parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //getPrescriptionDetail
    func getPrescriptionDetail(with user_id:String,appointment_id:String,patient_id:String,target:BaseViewControler?, complition:@escaping (Prescription?) -> Void){
        let param:[String : Any] = ["user_id":user_id, "appointment_id":appointment_id,"patient_id":patient_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.get_prescription_detail, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(Prescription(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
//                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    // MARK: deletePrescription
    func deletePrescription(with user_id:String, pr_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["user_id":user_id, "pr_id":pr_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.delete_prescription, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    
    //getPrescriptionDetail
    func genericList(with user_id:String,trade_id: String, target:BaseViewControler?, complition:@escaping (Generic?) -> Void){
        let param:[String : Any] = ["user_id":user_id, "trade_id":trade_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.genric_list, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(Generic(dictionary: dict as NSDictionary))
                            } else {
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //getPrescriptionDetail
    func tradeList(with param:[String : Any], target:BaseViewControler?, complition:@escaping (Trade?) -> Void){
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.trade_list, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(Trade(dictionary: dict as NSDictionary))
                            } else {
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    
    
    // MARK: blockAssistant
    func addPrescription(with priscription:String,patient_id:String,doctor_id:String,appointment_id:String,patient_case:String,image:Array<File>, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["priscription":priscription, "patient_id":patient_id,"doctor_id":doctor_id,"appointment_id":appointment_id,"patient_case":patient_case]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.add_prescription, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }///
    
    // MARK: blockAssistant
    func editPrescription(with priscription:String,prescription_id:String, target:BaseViewControler?, complition:@escaping (Bool) -> Void){
        let param:[String : Any] = ["priscription":priscription, "prescription_id":prescription_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.edit_prescription, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
 //---------------------------
// --------- //Assistant---API
//---------------------------
    //assistantSignup
    func assistantSignup(with firstname:String,lastname:String,email:String,password:String,countrycode:String,mobile:String,image:Array<File>,devicetype:String,devicetoken:String,dr_id:String,clinic_id:String,target:BaseViewControler?, complition:@escaping (User?) -> Void){
        let param:[String : Any] = ["firstname":firstname, "lastname":lastname,"email":email,"password":password,"countrycode":countrycode, "mobile":mobile,"devicetype":devicetype,"devicetoken":devicetoken, "dr_id":dr_id,"clinic_id":clinic_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.asssistant_signup, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(User(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            if dict["message"] as? String == "The password must be at least 6 characters."{
                                target?.showAlert(with: self.localizeValue(key: "Password must contain 6 digits"))
                            }else if dict["message"] as? String == "Email already exist" {
                                target?.showAlert(with: self.localizeValue(key: "Email already registered"))
                            }
                           
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    //getDrList
    func getDrList(with target:BaseViewControler?, complition:@escaping ([UserData]?) -> Void){

        target?.showLoader(with: "Loading...")
        super.startService(with: .get, baseURL: Config.baseUrl, path: Config.drlist, parameters: nil, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>,let user = dict["data"] as? Array<Dictionary<String,Any>> {
                                complition(UserData.modelsFromDictionaryArray(array: user as NSArray))
                            } else {
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //getVacationList
    func getVacationList(with param:[String : Any],target:UIViewController?, complition:@escaping (AnyObject?) -> Void){

        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.getVacation, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(dict as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    func checkVacationList(with param:[String : Any],target:UIViewController?, complition:@escaping (AnyObject?) -> Void){
        
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.checkVacation, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(dict as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //addVacation
    func addVacation(with param:[String : Any],target:UIViewController?, complition:@escaping (AnyObject?) -> Void){
        
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.addVacation, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(dict as AnyObject)
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(dict as AnyObject)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    func deleteVacation(with param:[String : Any],target:UIViewController?, complition:@escaping (Bool) -> Void){
        
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.deleteVacation, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["result"] as? String
                        if status == "Success" {
                                complition(true)
                        }else {
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }

    //acceptedAppointments
    func acceptedAppointments(with assistant_id:String,current_page:String,date:String,type: String,doctor_id:String,clinic_id:String,target:BaseViewControler?, complition:@escaping (Appointment?) -> Void){
        let param:[String : Any] = ["assistant_id":assistant_id, "current_page":current_page,"date":date,"type":type,"doctor_id":doctor_id,"clinic_id":clinic_id]
      target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.accepted_appointments, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
            target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>{
                                complition(Appointment(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //assistantPendingAppointments
    func assistantPendingAppointments(with assistant_id:String,current_page:String,doctor_id:String,clinic_id:String,target:BaseViewControler?, complition:@escaping (DataAppointment?) -> Void){
        let param:[String : Any] = ["assistant_id":assistant_id, "current_page":current_page, "doctor_id":doctor_id,"clinic_id":clinic_id]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.pending_appointments, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                           if let dict = data as? Dictionary<String,Any>,let user = dict["data"] as? NSDictionary {
                               complition(DataAppointment(dictionary: user))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //acceptAppointment
    func acceptAppointment(with appointment_id:String,user_id:String,set_time:String,first_time:String,token_number:String,notes:String,target:BaseViewControler?, complition:@escaping (Bool?) -> Void){
        let param:[String : Any] = ["appointment_id":appointment_id ,"user_id":user_id,"set_time":set_time,"first_time":first_time, "token_number":token_number, "notes":notes]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.accept_appointment, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //rejectAppointment
    func rejectAppointment(with appointment_id:String,user_id:String,reject_type:String,notes:String,target:BaseViewControler?, complition:@escaping (Bool?) -> Void){
        let param:[String : Any] = ["appointment_id":appointment_id ,"user_id":user_id,"reject_type":reject_type, "notes":notes]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.reject_appointment, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            complition(true)
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(false)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(false)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(false)
                }
            }
        }
    }
    
    //assistantEditProfile
    func assistantEditProfile(with user_id:String,firstname:String,lastname:String,image:Array<File>,target:BaseViewControler?, complition:@escaping (User?) -> Void){
        let param:[String : Any] = ["user_id":user_id, "firstname":firstname, "lastname":lastname]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.edit_profile_assitant, parameters: param, files: image) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any> {
                                complition(User(dictionary: dict as NSDictionary))
                            } else {
                                //target?.showAlert(with: "Network Error! Please try after sometime")
                                target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    //patientList
    func patientList(with user_id:String,search_type:String,name:String,current_page:String,target:BaseViewControler?, complition:@escaping ([Patient_info]?) -> Void){
        let param:[String : Any] = ["user_id":user_id, "search_type":search_type,"name":name,"current_page":current_page]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.patient_listing, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if let dict = data as? Dictionary<String,Any> {
                        let status = dict["return"] as? Int
                        if status == 1 {
                            if let dict = data as? Dictionary<String,Any>,let user = dict["data"] as? Dictionary<String,Any> {
                                complition(Patient_info.modelsFromDictionaryArray(array: user["data"] as! NSArray))
                            } else {
                                //target?.showAlert(with: (data as! Dictionary<String,Any>)["message"]! as? String)
                                complition(nil)
                            }
                        }else if status == 2{
                            
                            target?.logOut()
                            complition(nil)
                        }else{
                            //target?.showAlert(with: dict["message"] as? String)
                            complition(nil)
                        }
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    
    //get_last_token
    func get_last_token(with doctor_id:String,clinic_id:String,date:String,target:BaseViewControler?, complition:@escaping (Int?) -> Void){
        
        let param:[String : Any] = ["doctor_id":doctor_id ,"clinic_id":clinic_id,"date":date]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.get_last_token, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                     if let dict = data as? Dictionary<String,Any>,let user = dict["data"]  {
                        complition((user as! Int))
                        }else{
                        complition(nil)
                        }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition(nil)
                }
            }
        }
    }
    func upload_feedback(with user_id:String,message:String,type:String,target:BaseViewControler?, complition:@escaping (String) -> Void){
        let param:[String : Any] = ["user_id":user_id,"message":message,"type":type]
        target?.showLoader(with: "Loading...")
        super.startService(with: .post, baseURL: Config.baseUrl, path: Config.feedback, parameters: param, files: []) { (result) in
            DispatchQueue.main.async {
                target?.hideLoader()
                switch result {
                case .Success(let data):
                    // #parse response here
                    if data is Dictionary<String,Any> {
                        complition("success")
                    }else{
                        complition("failure")
                    }
                case .Error(let error):
                    // #display error message here
                    target?.showAlert(with: error)
                    complition("failure")
                }
            }
        }
    }
}


