//
//  AppDelegate.swift
//  3yadty For Doctors
//
//  Created by apple on 20/09/18.
//  Copyright © 2018 Xperge. All rights reserved.

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import GooglePlaces
import GoogleMaps
import CoreLocation
import SlideMenuController
import UserNotifications
import Firebase
import FirebaseInstanceID
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?
    var mainViewController : UINavigationController?
    
    func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Splash delay
        
        Thread.sleep(forTimeInterval: 3)
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        
        //IQKeyboardManager Start
        IQKeyboardManager.shared.enable = true
        
        
        //        FABRIC
        Fabric.with([Crashlytics.self])
        
        //facebook login
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Initialize google sign-in
        GIDSignIn.sharedInstance().clientID = "1075335187706-2haha3vdjangsp2du52tohfrcpmeg7vl.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        //google places key
        GMSPlacesClient.provideAPIKey("AIzaSyArNYwJ5KwQX06IDJK8r_KRnr7kkURwvA0")
        GMSServices.provideAPIKey("AIzaSyArNYwJ5KwQX06IDJK8r_KRnr7kkURwvA0")
        //location
        LocationManager.sharedInstance.autoUpdate = true
        LocationManager.sharedInstance.startUpdatingLocation()
        
        
        
        //FireBase
        FirebaseApp.configure()
        
        //---Device token----
        // Register for Remote Notification
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                guard error == nil else {
                    //Display Error.. Handle Error.. etc..
                    return
                }
                if granted {
                    //Register for RemoteNotifications. Your Remote Notifications can display alerts now :slightly_smiling_face:
                    DispatchQueue.main.async { () -> Void in
                        application.registerForRemoteNotifications()
                    }
                }else {
                    //Handle user denying permissions..
                }
            }
        }else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("DEVICE TOKEN = \(deviceToken)")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let userInfo = notification.request.content.userInfo
        let targetValue = userInfo["aps"] as! Dictionary<String,Any>
        let alert = targetValue["alert"] as! Dictionary<String,Any>
        let orderStatus = alert["order_status"] as! String
//        UserDefaults.standard.set(orderStatus, forKey: "notificationType")
//
//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name("push"), object: nil)
        completionHandler([.alert, .badge, .sound])
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print("Handle push from background or closed")
//        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
//        print("\(response.notification.request.content.userInfo)")
//        
//        print("user clicked on the notification")
//        let userInfo = response.notification.request.content.userInfo
//        let targetValue = userInfo["aps"] as! Dictionary<String,Any>
//        let alert = targetValue["alert"] as! Dictionary<String,Any>
//        let orderStatus = alert["order_status"] as! String
//        print(orderStatus)
//        
//        UserDefaults.standard.set(orderStatus, forKey: "completed")
//        
        
        
        completionHandler()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Remote Notification Delegate
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.setValue(tokenString, forKey: "token")
        UserDefaults.standard.synchronize()
        print("tokenString: \(tokenString)")
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed:--- \(error)")
    }
    
    //--Facebook
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation])
        // Add any custom logic here.
        return handled
    }
    //--Google
    func Application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
//            let userId = user.userID!
//            let idToken = user.authentication.idToken!
//            let fullName = user.profile.name!
//            let givenName = user.profile.givenName!
//            let familyName = user.profile.familyName!
//            let email = user.profile.email!
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

}
