/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class UserData: NSObject, NSCoding {
    
    public var id : Int?
    public var countrycode : String?
    public var mobile : String?
    public var otp : String?
    public var mobilestatus : String?
    public var image : String?
    public var firstname : String?
    public var lastname : String?
    public var gendor : String?
    public var arabicfname : String?
    public var arabilname : String?
    public var email : String?
    public var age : String?
    public var address : String?
    public var visible_password : String?
    public var password : String?
    public var newvisitfee : String?
    public var consultantfee : String?
    public var no_of_allotment : String?
    public var accounttype : Int?
    public var clinic_id : Int?
    public var dr_id : Int?
    public var dr_position : String?
    public var arabic_dr_postion : String?
    public var dr_category : Int?
    public var dr_certificate : String?
    public var dr_certificate_number : String?
    public var about : String?
    public var aboutar : String?
    public var education : String?
    public var remembertoken : String?
    public var resetpasswordtoken : String?
    public var emailstatus : Int?
    public var blockstatus : String?
    public var signupstatus : Int?
    public var status : Int?
    public var delete_status : Int?
    public var devicetype : String?
    public var devicetoken : String?
    public var createdat : String?
    public var updatedat : String?
    public var title : String?
    public var titlear : String?
    public var dr_category_name : String?
    public var clinic : Array<Clinic>?
    public var clinic_name : String?
    public var doctor_name : String?
    public var doctor_info: Any?
    public var dr_subcategory_name: Any?
    public var dr_category_name_arabic : String?

    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.titlear, forKey: "titlear")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.countrycode, forKey: "countrycode")
        aCoder.encode(self.mobile, forKey: "mobile")
        aCoder.encode(self.otp, forKey: "otp")
        aCoder.encode(self.mobilestatus, forKey: "mobilestatus")
        aCoder.encode(self.image, forKey: "image")
        aCoder.encode(self.firstname, forKey: "firstname")
        aCoder.encode(self.lastname, forKey: "lastname")
        aCoder.encode(self.gendor, forKey: "gendor")
        aCoder.encode(self.arabicfname, forKey: "arabicfname")
        aCoder.encode(self.arabilname, forKey: "arabilname")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.age, forKey: "age")
        aCoder.encode(self.address, forKey: "address")
        aCoder.encode(self.visible_password, forKey: "visible_password")
        aCoder.encode(self.password, forKey: "password")
        aCoder.encode(self.newvisitfee, forKey: "newvisitfee")
        aCoder.encode(self.consultantfee, forKey: "consultantfee")
        aCoder.encode(self.no_of_allotment, forKey: "no_of_allotment")
        aCoder.encode(self.accounttype, forKey: "accounttype")
        aCoder.encode(self.clinic_id, forKey: "clinic_id")
        aCoder.encode(self.dr_id, forKey: "dr_id")
        aCoder.encode(self.dr_position, forKey: "dr_position")
        aCoder.encode(self.arabic_dr_postion, forKey: "arabic_dr_postion")
        aCoder.encode(self.dr_category, forKey: "dr_category")
        aCoder.encode(self.dr_certificate, forKey: "dr_certificate")
        aCoder.encode(self.dr_certificate_number, forKey: "dr_certificate_number")
        aCoder.encode(self.about, forKey: "about")
        aCoder.encode(self.aboutar, forKey: "aboutar")
        aCoder.encode(self.education, forKey: "education")
        aCoder.encode(self.remembertoken, forKey: "remembertoken")
        aCoder.encode(self.resetpasswordtoken, forKey: "resetpasswordtoken")
        aCoder.encode(self.emailstatus, forKey: "emailstatus")
        aCoder.encode(self.blockstatus, forKey: "blockstatus")
        aCoder.encode(self.signupstatus, forKey: "signupstatus")
        aCoder.encode(self.status, forKey: "status")
        aCoder.encode(self.delete_status, forKey: "delete_status")
        aCoder.encode(self.devicetype, forKey: "devicetype")
        aCoder.encode(self.devicetoken, forKey: "devicetoken")
        aCoder.encode(self.createdat, forKey: "createdat")
        aCoder.encode(self.updatedat, forKey: "updatedat")
        aCoder.encode(self.dr_category_name, forKey: "dr_category_name")
        aCoder.encode(self.clinic, forKey: "clinic")
        aCoder.encode(self.clinic_name, forKey: "clinic_name")
        aCoder.encode(self.doctor_info, forKey: "doctor_info")
        aCoder.encode(self.doctor_name, forKey: "doctor_name")
        aCoder.encode(self.dr_subcategory_name, forKey: "dr_subcategory_name")
        aCoder.encode(self.dr_category_name_arabic, forKey: "dr_category_name_arabic")
    }
    
    required convenience public init?(coder aDecoder: NSCoder) {
        self.init()
        
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.countrycode = aDecoder.decodeObject(forKey: "countrycode") as? String
        self.mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        self.otp = aDecoder.decodeObject(forKey: "otp") as? String
        self.title = aDecoder.decodeObject(forKey: "title") as? String
        self.titlear = aDecoder.decodeObject(forKey: "titlear") as? String
        self.mobilestatus = aDecoder.decodeObject(forKey: "mobilestatus") as? String
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.firstname = aDecoder.decodeObject(forKey: "firstname") as? String
        self.lastname = aDecoder.decodeObject(forKey: "lastname") as? String
        self.gendor = aDecoder.decodeObject(forKey: "gendor") as? String
        self.arabicfname = aDecoder.decodeObject(forKey: "arabicfname") as? String
        self.arabilname = aDecoder.decodeObject(forKey: "arabilname") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.age = aDecoder.decodeObject(forKey: "age") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.visible_password = aDecoder.decodeObject(forKey: "visible_password") as? String
        self.password = aDecoder.decodeObject(forKey: "password") as? String
        self.newvisitfee = aDecoder.decodeObject(forKey: "newvisitfee") as? String
        self.consultantfee = aDecoder.decodeObject(forKey: "consultantfee") as? String
        self.no_of_allotment = aDecoder.decodeObject(forKey: "no_of_allotment") as? String
        self.accounttype = aDecoder.decodeObject(forKey: "accounttype") as? Int
        self.clinic_id = aDecoder.decodeObject(forKey: "clinic_id") as? Int
        self.dr_id = aDecoder.decodeObject(forKey: "dr_id") as? Int
        self.dr_position = aDecoder.decodeObject(forKey: "dr_position") as? String
        self.arabic_dr_postion = aDecoder.decodeObject(forKey: "arabic_dr_postion") as? String
        self.dr_category = aDecoder.decodeObject(forKey: "dr_category") as? Int
        self.dr_certificate = aDecoder.decodeObject(forKey: "dr_category") as? String
        self.dr_certificate_number = aDecoder.decodeObject(forKey: "dr_certificate_number") as? String
        self.about = aDecoder.decodeObject(forKey: "about") as? String
        self.aboutar = aDecoder.decodeObject(forKey: "aboutar") as? String
        self.education = aDecoder.decodeObject(forKey: "education") as? String
        self.remembertoken = aDecoder.decodeObject(forKey: "remembertoken") as? String
        self.resetpasswordtoken = aDecoder.decodeObject(forKey: "resetpasswordtoken") as? String
        self.emailstatus = aDecoder.decodeObject(forKey: "emailstatus") as? Int
        self.blockstatus = aDecoder.decodeObject(forKey: "blockstatus") as? String
        self.signupstatus = aDecoder.decodeObject(forKey: "signupstatus") as? Int
        self.status = aDecoder.decodeObject(forKey: "status") as? Int
        self.delete_status = aDecoder.decodeObject(forKey: "signupstatus") as? Int
        self.devicetype = aDecoder.decodeObject(forKey: "devicetype") as? String
        self.devicetoken = aDecoder.decodeObject(forKey: "devicetoken") as? String
        self.doctor_name = aDecoder.decodeObject(forKey: "doctor_name") as? String
        self.clinic_name = aDecoder.decodeObject(forKey: "clinic_name") as? String
        self.createdat = aDecoder.decodeObject(forKey: "createdat") as? String
        self.updatedat = aDecoder.decodeObject(forKey: "updatedat") as? String
        self.dr_category_name = aDecoder.decodeObject(forKey: "dr_category_name") as? String
        self.doctor_info = aDecoder.decodeObject(forKey: "doctor_info")
        self.dr_subcategory_name = aDecoder.decodeObject(forKey: "dr_subcategory_name")
        self.dr_category_name_arabic = aDecoder.decodeObject(forKey: "dr_category_name_arabic") as? String
        self.clinic = aDecoder.decodeObject(forKey: "clinic") as? NSArray as? Array<Clinic>
    }
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Data Instances.
     */
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [UserData]
    {
        var models:[UserData] = []
        for item in array
        {
            models.append(UserData.parse(item as! NSDictionary))
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let data = Data(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Data Instance.
     */
    class func parse(_ dictionary: NSDictionary) -> UserData{
        
        let userObj = UserData.init()
        userObj.id = dictionary["id"] as? Int
        userObj.countrycode = dictionary["countrycode"] as? String
        userObj.mobile = dictionary["mobile"] as? String
        userObj.otp = dictionary["otp"] as? String
        userObj.title = dictionary["title"] as? String
        userObj.titlear = dictionary["titlear"] as? String
        userObj.mobilestatus = dictionary["mobilestatus"] as? String
        userObj.image = dictionary["image"] as? String
        userObj.firstname = dictionary["firstname"] as? String
        userObj.lastname = dictionary["lastname"] as? String
        userObj.gendor = dictionary["gendor"] as? String
        userObj.arabicfname = dictionary["arabicfname"] as? String
        userObj.arabilname = dictionary["arabilname"] as? String
        userObj.email = dictionary["email"] as? String
        userObj.age = dictionary["age"] as? String
        userObj.address = dictionary["address"] as? String
        userObj.visible_password = dictionary["visible_password"] as? String
        userObj.password = dictionary["password"] as? String
        userObj.newvisitfee = dictionary["newvisitfee"] as? String
        userObj.consultantfee = dictionary["consultantfee"] as? String
        userObj.no_of_allotment = dictionary["no_of_allotment"] as? String
        userObj.accounttype = dictionary["accounttype"] as? Int
        userObj.clinic_id = dictionary["clinic_id"] as? Int
        userObj.dr_id = dictionary["dr_id"] as? Int
        userObj.dr_position = dictionary["dr_position"] as? String
        userObj.arabic_dr_postion = dictionary["arabic_dr_postion"] as? String
        userObj.dr_category = dictionary["dr_category"] as? Int
        userObj.dr_certificate = dictionary["dr_certificate"] as? String
        userObj.dr_certificate_number = dictionary["dr_certificate_number"] as? String
        userObj.about = dictionary["about"] as? String
        userObj.aboutar = dictionary["aboutar"] as? String
        userObj.education = dictionary["education"] as? String
        userObj.remembertoken = dictionary["remembertoken"] as? String
        userObj.resetpasswordtoken = dictionary["resetpasswordtoken"] as? String
        userObj.emailstatus = dictionary["emailstatus"] as? Int
        userObj.blockstatus = dictionary["blockstatus"] as? String
        userObj.signupstatus = dictionary["signupstatus"] as? Int
        userObj.status = dictionary["status"] as? Int
        userObj.delete_status = dictionary["delete_status"] as? Int
        userObj.devicetype = dictionary["devicetype"] as? String
        userObj.devicetoken = dictionary["devicetoken"] as? String
        userObj.doctor_name = dictionary["doctor_name"] as? String
        userObj.clinic_name = dictionary["clinic_name"] as? String
        userObj.createdat = dictionary["createdat"] as? String
        userObj.updatedat = dictionary["updatedat"] as? String
        userObj.dr_category_name = dictionary["dr_category_name"] as? String
        userObj.dr_category_name_arabic = dictionary["dr_category_name_arabic"] as? String
        userObj.doctor_info = dictionary["doctor_info"]
        userObj.dr_subcategory_name = dictionary["dr_subcategory_name"]
        if let clinic = dictionary["clinic"] as? NSArray{
            
            userObj.clinic = Clinic.modelsFromDictionaryArray(array: clinic)
        }else if let clinic = dictionary["clinic"] as? [Clinic]{
            
            userObj.clinic = Clinic.modelsFromDictionaryArray(array: clinic as NSArray)
        }
        return userObj
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.titlear, forKey: "titlear")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.countrycode, forKey: "countrycode")
        dictionary.setValue(self.mobile, forKey: "mobile")
        dictionary.setValue(self.otp, forKey: "otp")
        dictionary.setValue(self.mobilestatus, forKey: "mobilestatus")
        dictionary.setValue(self.image, forKey: "image")
        dictionary.setValue(self.firstname, forKey: "firstname")
        dictionary.setValue(self.lastname, forKey: "lastname")
        dictionary.setValue(self.gendor, forKey: "gendor")
        dictionary.setValue(self.arabicfname, forKey: "arabicfname")
        dictionary.setValue(self.arabilname, forKey: "arabilname")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.age, forKey: "age")
        dictionary.setValue(self.address, forKey: "address")
        dictionary.setValue(self.visible_password, forKey: "visible_password")
        dictionary.setValue(self.password, forKey: "password")
        dictionary.setValue(self.newvisitfee, forKey: "newvisitfee")
        dictionary.setValue(self.consultantfee, forKey: "consultantfee")
        dictionary.setValue(self.no_of_allotment, forKey: "no_of_allotment")
        dictionary.setValue(self.accounttype, forKey: "accounttype")
        dictionary.setValue(self.clinic_id, forKey: "clinic_id")
        dictionary.setValue(self.dr_id, forKey: "dr_id")
        dictionary.setValue(self.dr_position, forKey: "dr_position")
        dictionary.setValue(self.arabic_dr_postion, forKey: "arabic_dr_postion")
        dictionary.setValue(self.dr_category, forKey: "dr_category")
        dictionary.setValue(self.dr_certificate, forKey: "dr_certificate")
        dictionary.setValue(self.dr_certificate_number, forKey: "dr_certificate_number")
        dictionary.setValue(self.about, forKey: "about")
        dictionary.setValue(self.aboutar, forKey: "aboutar")
        dictionary.setValue(self.education, forKey: "education")
        dictionary.setValue(self.remembertoken, forKey: "remembertoken")
        dictionary.setValue(self.resetpasswordtoken, forKey: "resetpasswordtoken")
        dictionary.setValue(self.emailstatus, forKey: "emailstatus")
        dictionary.setValue(self.blockstatus, forKey: "blockstatus")
        dictionary.setValue(self.signupstatus, forKey: "signupstatus")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.delete_status, forKey: "delete_status")
        dictionary.setValue(self.devicetype, forKey: "devicetype")
        dictionary.setValue(self.devicetoken, forKey: "devicetoken")
        dictionary.setValue(self.createdat, forKey: "createdat")
        dictionary.setValue(self.updatedat, forKey: "updatedat")
        dictionary.setValue(self.dr_category_name, forKey: "dr_category_name")
        dictionary.setValue(self.clinic, forKey: "clinic")
        dictionary.setValue(self.clinic_name, forKey: "clinic_name")
        dictionary.setValue(self.doctor_info, forKey: "doctor_info")
        dictionary.setValue(self.dr_subcategory_name, forKey: "dr_subcategory_name")
        dictionary.setValue(self.dr_category_name_arabic, forKey: "dr_category_name_arabic")
        dictionary.setValue(self.doctor_name, forKey: "doctor_name")
        return dictionary
    }
}

