/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Clinic: NSObject,NSCoding {
    
	public var id : Int?
	public var user_id : Int?
	public var clinic_city : Int?
	public var clinic_area : Int?
	public var clinic_name : String?
    public var clinic_name_ar : String?
	public var clinic_address : String?
	public var clinic_lat : String?
	public var clinic_long : String?
	public var clinic_city_name : String?
    public var clinic_city_name_ar : String?
    public var clinic_phone : String?
    public var clinic_code : String?
    public var clinic_schedule_count : String?
    public var clinic_schedule : String?
    public var clinic_fees : Int?
    public var clinic_image : Array<ClinicImages>?
	public var clinic_area_name : String?
    public var clinic_area_name_ar : String?
    public var google_address: String?
	public var availability : Array<Availability>?
    public var waiting_time: String?
    public var clinic_address_ar: String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let clinic_list = Clinic.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Clinic Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Clinic]
    {
        var models:[Clinic] = []
        for item in array{
            
            if item is Clinic{
                
                models.append(item as! Clinic)
            }else{
                
                models.append(Clinic.parse(dictionary: item as! NSDictionary))
            }
        }
        return models
    }
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.user_id, forKey: "user_id")
        aCoder.encode(self.clinic_city, forKey: "clinic_city")
        aCoder.encode(self.clinic_area, forKey: "clinic_area")
        aCoder.encode(self.clinic_name, forKey: "clinic_name")
        aCoder.encode(self.clinic_name_ar, forKey: "clinic_name_ar")
        aCoder.encode(self.clinic_address, forKey: "clinic_address")
        aCoder.encode(self.google_address, forKey: "google_address")
        aCoder.encode(self.clinic_lat, forKey: "clinic_lat")
        aCoder.encode(self.clinic_long, forKey: "clinic_long")
        aCoder.encode(self.clinic_schedule, forKey: "clinic_schedule")
        aCoder.encode(self.clinic_schedule_count, forKey: "clinic_schedule_count")
        aCoder.encode(self.clinic_city_name, forKey: "clinic_city_name")
        aCoder.encode(self.clinic_area_name, forKey: "clinic_area_name")
        aCoder.encode(self.clinic_city_name_ar, forKey: "clinic_city_name_ar")
        aCoder.encode(self.clinic_area_name_ar, forKey: "clinic_area_name_ar")
        aCoder.encode(self.clinic_phone, forKey: "clinic_mobile")
        aCoder.encode(self.clinic_code, forKey: "clinic_country_code")
        aCoder.encode(self.clinic_fees, forKey: "clinic_fees")
        aCoder.encode(self.clinic_image, forKey: "clinic_image")
        aCoder.encode(self.availability, forKey: "availability")
        aCoder.encode(self.waiting_time, forKey: "waiting_time")
        aCoder.encode(self.clinic_address_ar, forKey: "clinic_address_ar")
    }
    
    required convenience public init?(coder aDecoder: NSCoder) {
        self.init()
        
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.user_id = aDecoder.decodeObject(forKey: "user_id") as? Int
        self.clinic_city = aDecoder.decodeObject(forKey: "clinic_city") as? Int
        self.clinic_area = aDecoder.decodeObject(forKey: "clinic_area") as? Int
        self.clinic_name = aDecoder.decodeObject(forKey: "clinic_name") as? String
        self.clinic_name_ar = aDecoder.decodeObject(forKey: "clinic_name_ar") as? String
        self.clinic_address = aDecoder.decodeObject(forKey: "clinic_address") as? String
        self.google_address = aDecoder.decodeObject(forKey: "google_address") as? String
        self.clinic_lat = aDecoder.decodeObject(forKey: "clinic_lat") as? String
        self.clinic_long = aDecoder.decodeObject(forKey: "clinic_long") as? String
        self.clinic_schedule = aDecoder.decodeObject(forKey: "clinic_schedule") as? String
        self.clinic_schedule_count = aDecoder.decodeObject(forKey: "clinic_schedule_count") as? String
        self.clinic_area_name = aDecoder.decodeObject(forKey: "clinic_area_name") as? String
        self.clinic_city_name = aDecoder.decodeObject(forKey: "clinic_city_name") as? String
        self.clinic_area_name_ar = aDecoder.decodeObject(forKey: "clinic_area_name_ar") as? String
        self.clinic_city_name_ar = aDecoder.decodeObject(forKey: "clinic_city_name_ar") as? String
        self.clinic_phone = aDecoder.decodeObject(forKey: "clinic_mobile") as? String
        self.clinic_code = aDecoder.decodeObject(forKey: "clinic_country_code") as? String
        self.clinic_fees = aDecoder.decodeObject(forKey: "clinic_fees") as? Int
        self.clinic_image = aDecoder.decodeObject(forKey: "clinic_image") as? Array<ClinicImages>
        self.availability = aDecoder.decodeObject(forKey: "availability") as? Array<Availability>
        self.waiting_time = aDecoder.decodeObject(forKey: "availability") as? String
        self.clinic_address_ar = aDecoder.decodeObject(forKey: "clinic_address_ar") as? String
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let clinic = Clinic(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Clinic Instance.
*/
	public class func parse(dictionary: NSDictionary) -> Clinic{

        let clinicObj = Clinic()
		clinicObj.id = dictionary["id"] as? Int
		clinicObj.user_id = dictionary["user_id"] as? Int
		clinicObj.clinic_city = dictionary["clinic_city"] as? Int
		clinicObj.clinic_area = dictionary["clinic_area"] as? Int
		clinicObj.clinic_name = dictionary["clinic_name"] as? String
        clinicObj.clinic_name_ar = dictionary["clinic_name_ar"] as? String
		clinicObj.clinic_address = dictionary["clinic_address"] as? String
        clinicObj.clinic_fees = dictionary["clinic_fees"] as? Int
        clinicObj.google_address = dictionary["google_address"] as? String
		clinicObj.clinic_lat = dictionary["clinic_lat"] as? String
		clinicObj.clinic_long = dictionary["clinic_long"] as? String
		clinicObj.clinic_city_name = dictionary["clinic_city_name"] as? String
		clinicObj.clinic_area_name = dictionary["clinic_area_name"] as? String
        clinicObj.clinic_city_name_ar = dictionary["clinic_city_name_ar"] as? String
        clinicObj.clinic_area_name_ar = dictionary["clinic_area_name_ar"] as? String
        clinicObj.clinic_phone = dictionary["clinic_mobile"] as? String
        clinicObj.clinic_code  = dictionary["clinic_country_code"] as? String
        clinicObj.clinic_address_ar  = dictionary["clinic_address_ar"] as? String
        clinicObj.clinic_schedule  = dictionary["clinic_schedule"] as? String ?? "\(String(describing: (dictionary["clinic_schedule"] as? Int ?? 0)))"
        clinicObj.waiting_time  = dictionary["waiting_time"] as? String ?? "\(String(describing: (dictionary["waiting_time"] as? Int ?? 0)))"
        clinicObj.clinic_schedule_count  = dictionary["clinic_schedule_count"] as? String ?? "\(String(describing: (dictionary["clinic_schedule_count"] as? Int ?? 0)))"
        clinicObj.clinic_area_name = dictionary["clinic_area_name"] as? String
        if (dictionary["clinic_image"] != nil) { clinicObj.clinic_image = ClinicImages.shared.parse(dictArray: dictionary["clinic_image"] as! [[String:AnyObject]]) }
//        clinicObj.clinic_image = dictionary["clinic_image"] as? Array<ClinicImages>
        if (dictionary["availability"] != nil) { clinicObj.availability = Availability.modelsFromDictionaryArray(array: dictionary["availability"] as! NSArray) }
        return clinicObj
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.user_id, forKey: "user_id")
		dictionary.setValue(self.clinic_city, forKey: "clinic_city")
		dictionary.setValue(self.clinic_area, forKey: "clinic_area")
		dictionary.setValue(self.clinic_name, forKey: "clinic_name")
        dictionary.setValue(self.clinic_name_ar, forKey: "clinic_name_ar")
		dictionary.setValue(self.clinic_address, forKey: "clinic_address")
        dictionary.setValue(self.google_address, forKey: "google_address")
		dictionary.setValue(self.clinic_lat, forKey: "clinic_lat")
		dictionary.setValue(self.clinic_long, forKey: "clinic_long")
        dictionary.setValue(self.clinic_schedule, forKey: "clinic_schedule")
        dictionary.setValue(self.clinic_schedule_count, forKey: "clinic_schedule_count")
		dictionary.setValue(self.clinic_city_name, forKey: "clinic_city_name")
		dictionary.setValue(self.clinic_area_name, forKey: "clinic_area_name")
        dictionary.setValue(self.clinic_city_name_ar, forKey: "clinic_city_name_ar")
        dictionary.setValue(self.clinic_area_name_ar, forKey: "clinic_area_name_ar")
        dictionary.setValue(self.clinic_phone, forKey: "clinic_mobile")
        dictionary.setValue(self.clinic_code, forKey: "clinic_country_code")
        dictionary.setValue(self.clinic_fees, forKey: "clinic_fees")
        dictionary.setValue(self.clinic_image, forKey: "clinic_image")
        dictionary.setValue(self.waiting_time, forKey: "waiting_time")
        dictionary.setValue(self.clinic_address_ar, forKey: "clinic_address_ar")
		return dictionary
	}

}
