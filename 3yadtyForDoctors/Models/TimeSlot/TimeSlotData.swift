/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class TimeSlotData {
	public var id : Int?
	public var clinic_id : Int?
	public var availability_id : Int?
	public var time_slot : String?
	public var appointment_type : Int?
	public var patient_id : Int?
	public var patient_info : Patient_info?
    public var clinic : Clinic?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [TimeSlotData]
    {
        var models:[TimeSlotData] = []
        for item in array
        {
            models.append(TimeSlotData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		clinic_id = dictionary["clinic_id"] as? Int
		availability_id = dictionary["availability_id"] as? Int
		time_slot = dictionary["time_slot"] as? String
		appointment_type = dictionary["appointment_type"] as? Int
		patient_id = dictionary["patient_id"] as? Int
		if (dictionary["patient_info"] != nil) { patient_info = Patient_info(dictionary: dictionary["patient_info"] as! NSDictionary) }
        if let clinicObj = dictionary["clinic"] as? NSDictionary{
            
            clinic = Clinic.parse(dictionary:clinicObj)
        }else if let clinicObj = dictionary["clinic"] as? Clinic{
            
            clinic = clinicObj
        }

	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.clinic_id, forKey: "clinic_id")
		dictionary.setValue(self.availability_id, forKey: "availability_id")
		dictionary.setValue(self.time_slot, forKey: "time_slot")
		dictionary.setValue(self.appointment_type, forKey: "appointment_type")
		dictionary.setValue(self.patient_id, forKey: "patient_id")
		dictionary.setValue(self.patient_info?.dictionaryRepresentation(), forKey: "patient_info")
        dictionary.setValue(self.clinic?.dictionaryRepresentation(), forKey: "clinic")

		return dictionary
	}

}
