//
//  User.swift
//  ChatApp-Swift-And-Firebase
//
//  Created by Surya on 9/28/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit

class Person: NSObject {
    var email: String?
    var fcmUserId: String?
    var image: String?
    var lastMessage: String?
    var fcmToken: String?
    var userName: String?
    
    
        public class func modelsFromDictionaryArray(array:NSArray) -> [Person]
        {
            var models:[Person] = []
            for item in array
            {
                models.append(Person(dictionary: item as! NSDictionary)!)
            }
            return models
        }
        
        /**
         Constructs the object based on the given dictionary.
         
         Sample usage:
         let appointment_data = Appointment_data(someDictionaryFromJSON)
         
         - parameter dictionary:  NSDictionary from JSON.
         
         - returns: Appointment_data Instance.
         */
        required public init?(dictionary: NSDictionary) {
            
            email = dictionary["email"] as? String
            fcmUserId = dictionary["fcmUserId"] as? String
            image = dictionary["image"] as? String
            lastMessage = dictionary["lastMessage"] as? String
            fcmToken = dictionary["fcmToken"] as? String
            userName = dictionary["userName"] as? String
         
        }
        
        
        /**
         Returns the dictionary representation for the current instance.
         
         - returns: NSDictionary.
         */
        public func dictionaryRepresentation() -> NSDictionary {
            
            let dictionary = NSMutableDictionary()
            
            dictionary.setValue(self.email, forKey: "email")
            dictionary.setValue(self.fcmUserId, forKey: "fcmUserId")
            dictionary.setValue(self.image, forKey: "image")
            dictionary.setValue(self.lastMessage, forKey: "lastMessage")
            dictionary.setValue(self.fcmToken, forKey: "fcmToken")
            dictionary.setValue(self.userName, forKey: "userName")
            
            return dictionary
        }
        
}
