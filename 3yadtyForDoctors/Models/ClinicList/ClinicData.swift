/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ClinicData {
	public var id : Int?
	public var user_id : Int?
	public var clinic_name : String?
	public var clinic_city : Int?
	public var clinic_area : Int?
	public var clinic_address : String?
	public var clinic_lat : String?
	public var clinic_long : String?
    public var waiting_time : String?
    public var clinic_schedule : Int?
    public var clinic_name_ar : String?
    public var clinic_address_ar : String?
	public var availability : Array<Availability>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ClinicData]
    {
        var models:[ClinicData] = []
        for item in array
        {
            models.append(ClinicData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		user_id = dictionary["user_id"] as? Int
		clinic_name = dictionary["clinic_name"] as? String
		clinic_city = dictionary["clinic_city"] as? Int
		clinic_area = dictionary["clinic_area"] as? Int
		clinic_address = dictionary["clinic_address"] as? String
		clinic_lat = dictionary["clinic_lat"] as? String
        clinic_lat = dictionary["waiting_time"] as? String
		clinic_long = dictionary["clinic_long"] as? String
        clinic_schedule = dictionary["clinic_schedule"] as? Int
        clinic_name_ar = dictionary["clinic_name_ar"] as? String
        clinic_address_ar = dictionary["clinic_address_ar"] as? String
        if (dictionary["availability"] != nil) { availability = Availability.modelsFromDictionaryArray(array: dictionary["availability"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.user_id, forKey: "user_id")
		dictionary.setValue(self.clinic_name, forKey: "clinic_name")
		dictionary.setValue(self.clinic_city, forKey: "clinic_city")
		dictionary.setValue(self.clinic_area, forKey: "clinic_area")
		dictionary.setValue(self.clinic_address, forKey: "clinic_address")
		dictionary.setValue(self.clinic_lat, forKey: "clinic_lat")
		dictionary.setValue(self.clinic_long, forKey: "clinic_long")
        dictionary.setValue(self.waiting_time, forKey: "waiting_time")
        dictionary.setValue(self.clinic_schedule, forKey: "clinic_schedule")
        dictionary.setValue(self.clinic_name_ar, forKey: "clinic_name_ar")
        dictionary.setValue(self.clinic_address_ar, forKey: "clinic_address_ar")
		return dictionary
	}

}
