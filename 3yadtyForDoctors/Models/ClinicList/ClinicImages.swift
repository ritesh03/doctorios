//
//  ClinicImages.swift
//  3yadtyForDoctors
//
//  Created by apple on 26/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

public class ClinicImages: NSObject,NSCoding {

    static let shared: ClinicImages = ClinicImages()
    public var clinic_id: Int?
    public var id: Int?
    public var image: String?
    
    public func parse(dictArray: [[String:AnyObject]]) -> [ClinicImages]{
        
        var arrObj = [ClinicImages]()
        for dict in dictArray{
            
            arrObj.append(self.parse(dict: dict))
        }
        return arrObj
    }
    
    public func parse(dict: [String:AnyObject]) -> ClinicImages{
        
        let object = ClinicImages()
        
        object.id = dict["id"] as? Int
        object.clinic_id = dict["clinic_id"] as? Int
        object.image = dict["image"] as? String
        return object
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.clinic_id, forKey: "clinic_id")
        dictionary.setValue(self.image, forKey: "image")
        return dictionary
    }
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.clinic_id, forKey: "clinic_id")
        aCoder.encode(self.image, forKey: "image")
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.clinic_id = aDecoder.decodeObject(forKey: "clinic_id") as? Int
        self.image = aDecoder.decodeObject(forKey: "image") as? String
    }
}
