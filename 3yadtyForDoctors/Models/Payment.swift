//
//  Payment.swift
//  3yadtyForDoctors
//
//  Created by apple on 17/06/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import Foundation

public class Payment {
    public var id : Int?
    public var doctorID : Int?
    public var totalAmount : String?
    public var appointmentDate : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [Payment]
    {
        var models:[Payment] = []
        for item in array
        {
            models.append(Payment(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? Int
        doctorID = dictionary["doctor_id"] as? Int
        totalAmount = dictionary["total_payable_amount"] as? String
        appointmentDate = dictionary["appointment_date"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.doctorID, forKey: "doctor_id")
        dictionary.setValue(self.totalAmount, forKey: "total_payable_amount")
        dictionary.setValue(self.appointmentDate, forKey: "appointment_date")
        
        return dictionary
    }
}
