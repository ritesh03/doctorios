//
//  Message.swift
//  ChatApp-Swift-And-Firebase
//
//  Created by Surya on 9/30/17.
//  Copyright © 2017 Surya. All rights reserved.
//

import UIKit
import FirebaseAuth

class Message: NSObject {
    
//    var fromId: String?
//    var text: String?
//    var timeStamp: NSNumber?
//    var toId: String?
//    var imageUrl: String?
//    var imageWidth: NSNumber?
//    var imageHeight: NSNumber?
//    var videoUrl: String?
    
    var message: String?
    var receiver: String?
    var receiverUid: String?
    var sender: String?
    var senderUid: String?
    var timestamp: String?

    func chatPartnerId() -> String {
        return (senderUid == Auth.auth().currentUser?.uid ? receiverUid : senderUid)!
    }
 
    public class func modelsFromDictionaryArray(array:NSArray) -> [Message]
    {
        var models:[Message] = []
        for item in array
        {
            models.append(Message(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let appointment_data = Appointment_data(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Appointment_data Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        message = dictionary["message"] as? String
        receiver = dictionary["receiver"] as? String
        receiverUid = dictionary["receiverUid"] as? String
        sender = dictionary["sender"] as? String
        senderUid = dictionary["senderUid"] as? String
        timestamp = dictionary["timestamp"] as? String
        
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.receiver, forKey: "receiver")
        dictionary.setValue(self.receiverUid, forKey: "receiverUid")
        dictionary.setValue(self.sender, forKey: "sender")
        dictionary.setValue(self.senderUid, forKey: "senderUid")
        dictionary.setValue(self.timestamp, forKey: "timestamp")
        
        return dictionary
    }
    
    
}

