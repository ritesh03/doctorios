/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class PrescriptionData {
	public var id : Int?
	public var prescription_type : Int?
	public var appointment_id : Int?
	public var doctor_id : Int?
	public var patient_id : Int?
	public var patient_case : String?
	public var image : String?
	public var createdat : String?
	public var updatedat : String?
	public var medicine : Array<Medicine>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [PrescriptionData]
    {
        var models:[PrescriptionData] = []
        for item in array
        {
            models.append(PrescriptionData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		prescription_type = dictionary["prescription_type"] as? Int
		appointment_id = dictionary["appointment_id"] as? Int
		doctor_id = dictionary["doctor_id"] as? Int
		patient_id = dictionary["patient_id"] as? Int
		patient_case = dictionary["patient_case"] as? String
		image = dictionary["image"] as? String
		createdat = dictionary["createdat"] as? String
		updatedat = dictionary["updatedat"] as? String
        if (dictionary["medicine"] != nil) { medicine = Medicine.modelsFromDictionaryArray(array: dictionary["medicine"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.prescription_type, forKey: "prescription_type")
		dictionary.setValue(self.appointment_id, forKey: "appointment_id")
		dictionary.setValue(self.doctor_id, forKey: "doctor_id")
		dictionary.setValue(self.patient_id, forKey: "patient_id")
		dictionary.setValue(self.patient_case, forKey: "patient_case")
		dictionary.setValue(self.image, forKey: "image")
		dictionary.setValue(self.createdat, forKey: "createdat")
		dictionary.setValue(self.updatedat, forKey: "updatedat")//medicine
        dictionary.setValue(self.medicine, forKey: "medicine")
        
		return dictionary
	}

}
