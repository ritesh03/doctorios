/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Medicine:NSObject  {
	public var id : Int?
	public var prescription_id : Int?
	public var genric_name : String?
    public var genric_id : Int?
    public var trade_id : Int?
	public var trade_name : String?
	public var frequency : Int?
	public var frequency_day : String?
	public var duration : Int?
	public var duration_day : String?
	public var dose_time : String?
    public var dose_number : String?
	public var notes : String?
	public var image : String?
	public var createdat : String?
	public var updatedat : String?
	//public var precription_sence : Array<Precription_sence>?
    public var precription_sence : Array<String>?
    
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let medicine_list = Medicine.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Medicine Instances.
*/
    public override init() {
    }
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Medicine]
    {
        var models:[Medicine] = []
        for item in array
        {
            models.append(Medicine(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let medicine = Medicine(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Medicine Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		prescription_id = dictionary["prescription_id"] as? Int
		genric_name = dictionary["genric_name"] as? String
        genric_id = dictionary["genric_id"] as? Int
        trade_id = dictionary["trade_id"] as? Int
        trade_name = dictionary["trade_name"] as? String
		frequency = dictionary["frequency"] as? Int
		frequency_day = dictionary["frequency_day"] as? String
		duration = dictionary["duration"] as? Int
		duration_day = dictionary["duration_day"] as? String
		dose_time = dictionary["dose_time"] as? String
        dose_number = dictionary["dose_number"] as? String
		notes = dictionary["notes"] as? String
		image = dictionary["image"] as? String
		createdat = dictionary["createdat"] as? String
		updatedat = dictionary["updatedat"] as? String
        if (dictionary["precription_sence"] != nil){
            precription_sence = dictionary["precription_sence"] as? Array<String>
        }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> Dictionary<String,Any> {

//        let dictionary = NSMutableDictionary()
//
//        dictionary.setValue(self.id, forKey: "id")
//        dictionary.setValue(self.prescription_id, forKey: "prescription_id")
//        dictionary.setValue(self.genric_name, forKey: "genric_name")
//        dictionary.setValue(self.genric_id, forKey: "genric_id")
//        dictionary.setValue(self.trade_id, forKey: "trade_id")
//        dictionary.setValue(self.trade_name, forKey: "trade_name")
//        dictionary.setValue(self.frequency, forKey: "frequency")
//        dictionary.setValue(self.frequency_day, forKey: "frequency_day")
//        dictionary.setValue(self.duration, forKey: "duration")
//        dictionary.setValue(self.duration_day, forKey: "duration_day")
//        dictionary.setValue(self.dose_time, forKey: "dose_time")
//        dictionary.setValue(self.dose_time, forKey: "dose_number")
//        dictionary.setValue(self.notes, forKey: "notes")
//        dictionary.setValue(self.image, forKey: "image")
//        dictionary.setValue(self.createdat, forKey: "createdat")
//        dictionary.setValue(self.updatedat, forKey: "updatedat")
//        return dictionary
        
        var dictionary = Dictionary<String,Any>()
        dictionary["dose_number"] = dose_number
        dictionary["dose_time"] = dose_time
        dictionary["duration"] = duration
        dictionary["duration_day"] = duration_day
        dictionary["frequency"] = frequency
        dictionary["frequency_day"] = frequency_day
        dictionary["genric_id"] = genric_id
        dictionary["genric_name"] = genric_name
        dictionary["notes"] = notes
        dictionary["precription_sence"] = precription_sence
        dictionary["trade_id"] = trade_id
        dictionary["trade_name"] = trade_name
        return dictionary
	}

}
