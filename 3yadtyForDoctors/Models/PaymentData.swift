/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class PaymentData {
	public var id : Int?
	public var doctor_id : Int?
	public var amount : String?
	public var amount_with_vat : String?
	public var vat : String?
	public var payment_source : Int?
	public var payment_type : Int?
	public var commission : String?
	public var clinic_amount : Array<Clinic_amount>?
	public var invoice_date : String?
	public var status : Int?
	public var created_at : String?
	public var total_payable_amount : Int?
	public var due_date : String?
    public var kiosk_scan_code: String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [PaymentData]
    {
        var models:[PaymentData] = []
        for item in array
        {
            models.append(PaymentData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		doctor_id = dictionary["doctor_id"] as? Int
		amount = dictionary["amount"] as? String
		amount_with_vat = dictionary["amount_with_vat"] as? String
		vat = dictionary["vat"] as? String
		payment_source = dictionary["payment_source"] as? Int
		payment_type = dictionary["payment_type"] as? Int
		commission = dictionary["commission"] as? String
        if (dictionary["clinic_amount"] != nil) { clinic_amount = Clinic_amount.modelsFromDictionaryArray(array: dictionary["clinic_amount"] as! NSArray) }
		invoice_date = dictionary["invoice_date"] as? String
		status = dictionary["status"] as? Int
		created_at = dictionary["created_at"] as? String
		total_payable_amount = dictionary["total_payable_amount"] as? Int
		due_date = dictionary["due_date"] as? String
        kiosk_scan_code = dictionary["kiosk_scan_code"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.doctor_id, forKey: "doctor_id")
		dictionary.setValue(self.amount, forKey: "amount")
		dictionary.setValue(self.amount_with_vat, forKey: "amount_with_vat")
		dictionary.setValue(self.vat, forKey: "vat")
		dictionary.setValue(self.payment_source, forKey: "payment_source")
		dictionary.setValue(self.payment_type, forKey: "payment_type")
		dictionary.setValue(self.commission, forKey: "commission")
		dictionary.setValue(self.invoice_date, forKey: "invoice_date")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.created_at, forKey: "created_at")
		dictionary.setValue(self.total_payable_amount, forKey: "total_payable_amount")
		dictionary.setValue(self.due_date, forKey: "due_date")
        dictionary.setValue(self.kiosk_scan_code, forKey: "kiosk_scan_code")

		return dictionary
	}

}
