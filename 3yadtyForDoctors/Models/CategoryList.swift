//
//  CategoryList.swift
//  3yadtyForDoctors
//
//  Created by apple on 23/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class CategoryList: NSObject {
    
    public var id : Int?
    public var name : String?
    public var arabic_name: String?
    
    public class func modelsFromDictionaryArray(array:Array<Dictionary<String,Any>>) -> [CategoryList]
    {
        var models:[CategoryList] = []
        for item in array
        {
            if let model = CategoryList(dictionary: item) {
                models.append(model)
            }
        }
        return models
    }
    public override init() {
        
    }
    
    required public init?(dictionary: Dictionary<String,Any>) {
        print(dictionary)
        id = dictionary["id"] as? Int
        name = dictionary["name"] as? String ?? ""
        arabic_name = dictionary["arabic_name"] as? String ?? ""
    }
    
    public required init(coder decoder: NSCoder) {
        id = decoder.decodeObject(forKey: "id") as? Int
        name = decoder.decodeObject(forKey: "name") as? String ?? ""
        arabic_name = decoder.decodeObject(forKey: "arabic_name") as? String ?? ""
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.name, forKey: "name")
        coder.encode(self.arabic_name, forKey: "arabic_name")
    }
    
    public func dictionaryRepresentation() -> Dictionary<String,Any> {
        
        var dictionary = Dictionary<String,Any>()
        
        dictionary["id"] = self.id
        dictionary["name"] = self.name
        dictionary["arabic_name"] = self.arabic_name
        return dictionary
    }
}
