/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class DataDoctorRequest {
	public var id : Int?
	public var countrycode : String?
	public var mobile : String?
	public var otp : String?
	public var mobilestatus : String?
	public var image : String?
	public var firstname : String?
	public var lastname : String?
	public var gendor : String?
	public var arabicfname : String?
	public var arabilname : String?
	public var email : String?
	public var birthday : String?
	public var age : String?
	public var address : String?
	public var visible_password : String?
	public var password : String?
	public var newvisitfee : String?
	public var consultantfee : String?
	public var no_of_allotment : String?
	public var accounttype : Int?
	public var dr_category : Int?
	public var dr_position : String?
	public var arabic_dr_postion : String?
	public var dr_certificate : String?
	public var dr_certificate_number : String?
	public var blockstatus : String?
	public var remembertoken : String?
	public var dr_id : Int?
	public var clinic_id : Int?
	public var resetpasswordtoken : String?
	public var emailstatus : Int?
	public var signupstatus : String?
	public var status : Int?
	public var devicetype : String?
	public var devicetoken : String?
	public var createdat : String?
	public var updatedat : String?
    public var clinic : Clinic?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [DataDoctorRequest]
    {
        var models:[DataDoctorRequest] = []
        for item in array
        {
            models.append(DataDoctorRequest(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		countrycode = dictionary["countrycode"] as? String
		mobile = dictionary["mobile"] as? String
		otp = dictionary["otp"] as? String
		mobilestatus = dictionary["mobilestatus"] as? String
		image = dictionary["image"] as? String
		firstname = dictionary["firstname"] as? String
		lastname = dictionary["lastname"] as? String
		gendor = dictionary["gendor"] as? String
		arabicfname = dictionary["arabicfname"] as? String
		arabilname = dictionary["arabilname"] as? String
		email = dictionary["email"] as? String
		birthday = dictionary["birthday"] as? String
		age = dictionary["age"] as? String
		address = dictionary["address"] as? String
		visible_password = dictionary["visible_password"] as? String
		password = dictionary["password"] as? String
		newvisitfee = dictionary["newvisitfee"] as? String
		consultantfee = dictionary["consultantfee"] as? String
		no_of_allotment = dictionary["no_of_allotment"] as? String
		accounttype = dictionary["accounttype"] as? Int
		dr_category = dictionary["dr_category"] as? Int
		dr_position = dictionary["dr_position"] as? String
		arabic_dr_postion = dictionary["arabic_dr_postion"] as? String
		dr_certificate = dictionary["dr_certificate"] as? String
		dr_certificate_number = dictionary["dr_certificate_number"] as? String
		blockstatus = dictionary["blockstatus"] as? String
		remembertoken = dictionary["remembertoken"] as? String
		dr_id = dictionary["dr_id"] as? Int
		clinic_id = dictionary["clinic_id"] as? Int
		resetpasswordtoken = dictionary["resetpasswordtoken"] as? String
		emailstatus = dictionary["emailstatus"] as? Int
		signupstatus = dictionary["signupstatus"] as? String
		status = dictionary["status"] as? Int
		devicetype = dictionary["devicetype"] as? String
		devicetoken = dictionary["devicetoken"] as? String
		createdat = dictionary["createdat"] as? String
		updatedat = dictionary["updatedat"] as? String
        if let clinicObj = dictionary["clinic"] as? NSDictionary{
            
            clinic = Clinic.parse(dictionary:clinicObj)
        }else if let clinicObj = dictionary["clinic"] as? Clinic{
            
            clinic = clinicObj
        }
	}
/**
    Returns the dictionary representation for the current instance.
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.countrycode, forKey: "countrycode")
		dictionary.setValue(self.mobile, forKey: "mobile")
		dictionary.setValue(self.otp, forKey: "otp")
		dictionary.setValue(self.mobilestatus, forKey: "mobilestatus")
		dictionary.setValue(self.image, forKey: "image")
		dictionary.setValue(self.firstname, forKey: "firstname")
		dictionary.setValue(self.lastname, forKey: "lastname")
		dictionary.setValue(self.gendor, forKey: "gendor")
		dictionary.setValue(self.arabicfname, forKey: "arabicfname")
		dictionary.setValue(self.arabilname, forKey: "arabilname")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.birthday, forKey: "birthday")
		dictionary.setValue(self.age, forKey: "age")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.visible_password, forKey: "visible_password")
		dictionary.setValue(self.password, forKey: "password")
		dictionary.setValue(self.newvisitfee, forKey: "newvisitfee")
		dictionary.setValue(self.consultantfee, forKey: "consultantfee")
		dictionary.setValue(self.no_of_allotment, forKey: "no_of_allotment")
		dictionary.setValue(self.accounttype, forKey: "accounttype")
		dictionary.setValue(self.dr_category, forKey: "dr_category")
		dictionary.setValue(self.dr_position, forKey: "dr_position")
		dictionary.setValue(self.arabic_dr_postion, forKey: "arabic_dr_postion")
		dictionary.setValue(self.dr_certificate, forKey: "dr_certificate")
		dictionary.setValue(self.dr_certificate_number, forKey: "dr_certificate_number")
		dictionary.setValue(self.blockstatus, forKey: "blockstatus")
		dictionary.setValue(self.remembertoken, forKey: "remembertoken")
		dictionary.setValue(self.dr_id, forKey: "dr_id")
		dictionary.setValue(self.clinic_id, forKey: "clinic_id")
		dictionary.setValue(self.resetpasswordtoken, forKey: "resetpasswordtoken")
		dictionary.setValue(self.emailstatus, forKey: "emailstatus")
		dictionary.setValue(self.signupstatus, forKey: "signupstatus")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.devicetype, forKey: "devicetype")
		dictionary.setValue(self.devicetoken, forKey: "devicetoken")
		dictionary.setValue(self.createdat, forKey: "createdat")
		dictionary.setValue(self.updatedat, forKey: "updatedat")
        dictionary.setValue(self.clinic?.dictionaryRepresentation(), forKey: "clinic")

		return dictionary
	}
}
