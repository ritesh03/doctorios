//
//  EditProfile.swift
//  3yadtyForDoctors
//
//  Created by apple on 01/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class EditProfile: NSObject {
    
    public var accounttype : Int?
    public var address : String?
    public var age : String?
    public var arabic_dr_postion : String?
    public var arabicfname : String?
    public var arabilname : String?
    public var birthday : String?
    public var blockstatus: Int?
    public var consultantfee: String?
    public var no_of_allotment: String?
    public var countrycode: String?
    public var createdat : String?
    public var devicetoken : String?
    public var devicetype : String?
    public var dr_category : String?
    public var dr_category_name : String?
    public var dr_certificate : String?
    public var dr_certificate_number : String?
    public var dr_position : String?
    public var email : String?
    public var emailstatus: String?
    public var emailverifiedat: String?
    public var firstname: String?
    public var gendor: String?
    public var id : Int?
    public var lastname : String?
    public var mobile : String?
    public var mobilestatus : Int?
    public var newvisitfee : String?
    public var otp : String?
    public var password: String?
    public var remembertoken: String?
    public var resetpasswordtoken: String?
    public var signupstatus: Int?
    public var updatedat: String?
    
    var clinic : Array<Clinic>? = nil
    var imagesArray = [File]()
    
}
