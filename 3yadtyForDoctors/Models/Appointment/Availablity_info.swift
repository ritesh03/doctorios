/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Availablity_info {
	public var id : Int?
	public var clinic_id : Int?
	public var dr_id : Int?
	public var day : String?
	public var start_time : String?
	public var end_time : String?
	public var start_time_milis : String?
	public var end_time_milis : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let availablity_info_list = Availablity_info.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Availablity_info Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Availablity_info]
    {
        var models:[Availablity_info] = []
        for item in array
        {
            models.append(Availablity_info(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let availablity_info = Availablity_info(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Availablity_info Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		clinic_id = dictionary["clinic_id"] as? Int
		dr_id = dictionary["dr_id"] as? Int
		day = dictionary["day"] as? String
		start_time = dictionary["start_time"] as? String
		end_time = dictionary["end_time"] as? String
		start_time_milis = dictionary["start_time_milis"] as? String
		end_time_milis = dictionary["end_time_milis"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.clinic_id, forKey: "clinic_id")
		dictionary.setValue(self.dr_id, forKey: "dr_id")
		dictionary.setValue(self.day, forKey: "day")
		dictionary.setValue(self.start_time, forKey: "start_time")
		dictionary.setValue(self.end_time, forKey: "end_time")
		dictionary.setValue(self.start_time_milis, forKey: "start_time_milis")
		dictionary.setValue(self.end_time_milis, forKey: "end_time_milis")

		return dictionary
	}

}