/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class DataAppointmentList {
    public var id : Int?
    public var appointment_type : Int?
    public var appointment_date : String?
    public var notes : String?
    public var reject_date : String?
    public var token_number : Int?
    public var firstname : String?
    public var lastname : String?
    public var patient_mobile : String?
    public var availability_id : Int?
    public var patient_checkin : Int?
    public var patient_id : Int?
    public var clinic_id : Int?
    public var doctor_id : Int?
    public var assistant_id : Int?
    public var first_time : Int?
    public var set_time : String?
    public var status : Int?
    public var noshow : Int?
    public var type : Int?
    public var reject_type : Int?
    public var accept_by : Int?
    public var visit_type : Int?
    public var createdat : String?
    public var updatedat : String?
    public var doctor_info : Doctor_info?
    public var patient_info : Patient_info?
    public var availablity_info : Availablity_info?
    public var clinic : Clinic?
    
    init() {
        
    }
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Data Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [DataAppointmentList]
    {
        var models:[DataAppointmentList] = []
        for item in array
        {
            models.append(DataAppointmentList(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let data = Data(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Data Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? Int
        appointment_type = dictionary["appointment_type"] as? Int
        appointment_date = dictionary["appointment_date"] as? String
        notes = dictionary["notes"] as? String
        reject_date = dictionary["reject_date"] as? String
        token_number = dictionary["token_number"] as? Int
        firstname = dictionary["firstname"] as? String
        lastname = dictionary["lastname"] as? String
        patient_mobile = dictionary["patient_mobile"] as? String
        availability_id = dictionary["availability_id"] as? Int
        patient_checkin = dictionary["patient_checkin"] as? Int
        patient_id = dictionary["patient_id"] as? Int
        clinic_id = dictionary["clinic_id"] as? Int
        doctor_id = dictionary["doctor_id"] as? Int
        assistant_id = dictionary["assistant_id"] as? Int
        first_time = dictionary["first_time"] as? Int
        set_time = dictionary["set_time"] as? String
        status = dictionary["status"] as? Int
        noshow = dictionary["noShow"] as? Int
        type = dictionary["type"] as? Int
        reject_type = dictionary["reject_type"] as? Int
        accept_by = dictionary["accept_by"] as? Int
        visit_type = dictionary["visit_type"] as? Int
        createdat = dictionary["createdat"] as? String
        updatedat = dictionary["updatedat"] as? String
        if (dictionary["doctor_info"] != nil) { doctor_info = Doctor_info(dictionary: dictionary["doctor_info"] as! NSDictionary) }
        if (dictionary["patient_info"] != nil) { patient_info = Patient_info(dictionary: dictionary["patient_info"] as! NSDictionary) }
        
        if let avail = dictionary["availablity_info"] as? NSDictionary{
            
            availablity_info = Availablity_info(dictionary: avail)
        }else if let avail = dictionary["availablity_info"] as? Availablity_info{
            
            availablity_info = avail
        }
        
        if let clinics = dictionary["clinic"] as? NSDictionary{
            
            clinic = Clinic.parse(dictionary: clinics)
        }else if let clinics = dictionary["clinic"] as? Clinic{
            
            clinic = clinics
        }
    }
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.appointment_type, forKey: "appointment_type")
        dictionary.setValue(self.appointment_date, forKey: "appointment_date")
        dictionary.setValue(self.notes, forKey: "notes")
        dictionary.setValue(self.reject_date, forKey: "reject_date")
        dictionary.setValue(self.token_number, forKey: "token_number")
        dictionary.setValue(self.firstname, forKey: "firstname")
        dictionary.setValue(self.lastname, forKey: "lastname")
        dictionary.setValue(self.patient_mobile, forKey: "patient_mobile")
        dictionary.setValue(self.availability_id, forKey: "availability_id")
        dictionary.setValue(self.patient_checkin, forKey: "patient_checkin")
        dictionary.setValue(self.patient_id, forKey: "patient_id")
        dictionary.setValue(self.clinic_id, forKey: "clinic_id")
        dictionary.setValue(self.doctor_id, forKey: "doctor_id")
        dictionary.setValue(self.assistant_id, forKey: "assistant_id")
        dictionary.setValue(self.first_time, forKey: "first_time")
        dictionary.setValue(self.set_time, forKey: "set_time")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.noshow, forKey: "noshow")
        dictionary.setValue(self.type, forKey: "type")
        dictionary.setValue(self.reject_type, forKey: "reject_type")
        dictionary.setValue(self.accept_by, forKey: "accept_by")
        dictionary.setValue(self.visit_type, forKey: "visit_type")
        dictionary.setValue(self.createdat, forKey: "createdat")
        dictionary.setValue(self.updatedat, forKey: "updatedat")
        dictionary.setValue(self.doctor_info?.dictionaryRepresentation(), forKey: "doctor_info")
        dictionary.setValue(self.patient_info?.dictionaryRepresentation(), forKey: "patient_info")
        dictionary.setValue(self.availablity_info?.dictionaryRepresentation(), forKey: "availablity_info")
        dictionary.setValue(self.clinic?.dictionaryRepresentation(), forKey: "clinic")
        
        return dictionary
    }
    
}
