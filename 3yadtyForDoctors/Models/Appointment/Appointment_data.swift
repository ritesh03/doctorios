/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Appointment_data {
	public var new : Int?
	public var consult : Int?
	public var total_patient : Int?
	public var total_pending_reservation : Int?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let appointment_data_list = Appointment_data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Appointment_data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Appointment_data]
    {
        var models:[Appointment_data] = []
        for item in array
        {
            models.append(Appointment_data(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let appointment_data = Appointment_data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Appointment_data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		new = dictionary["new"] as? Int
		consult = dictionary["consult"] as? Int
		total_patient = dictionary["total_patient"] as? Int
		total_pending_reservation = dictionary["total_pending_reservation"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.new, forKey: "new")
		dictionary.setValue(self.consult, forKey: "consult")
		dictionary.setValue(self.total_patient, forKey: "total_patient")
		dictionary.setValue(self.total_pending_reservation, forKey: "total_pending_reservation")

		return dictionary
	}

}