//
//  String+Extension.swift
//  ThinkBeauty
//
//  Created by Amit on 05/05/18.
//  Copyright © 2018 Ritesh Chopra. All rights reserved.
//

import Foundation

extension String {
    
    func isValidEmail() -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{3})(\\d{8})", with: "$1 $2", options: .regularExpression, range: nil)
    }
    
    func capitalizeFirst() -> String {
        return String(self[self.startIndex]).capitalized + String(self.dropFirst())
    }
    
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }

    func localize() -> String{
        
        return NSLocalizedString(self, comment: "")
    }
}

