//
//  LayoutConstraint.swift
//  HealthKitSample
//
//  Created by Surjeet Singh on 9/22/17.
//  Copyright © 2017 Surjeet Singh. All rights reserved.
//

import UIKit

@IBDesignable
//extension NSLayoutConstraint {
class LayoutConstraint: NSLayoutConstraint {
    
    @IBInspectable
    var iPhone6: CGFloat = 0 { // iPhone 6, 6S, 7, 8
        didSet {
            if UIScreen.main.bounds.maxY == 667 {
                constant = iPhone6
            }
        }
    }
    
    @IBInspectable
    var iPhone6Plus: CGFloat = 0 { // iPhone 6Plus, 6SPlus, 7Plus, 8Plus
        didSet {
            if UIScreen.main.bounds.maxY == 736 {
                constant = iPhone6Plus
            }
        }
    }
    
    @IBInspectable
    var iPhoneX: CGFloat = 0 { // iPhone X
        didSet {
            if UIScreen.main.bounds.maxY == 812 {
                constant = iPhoneX //iPhone_5¨8
            }
        }
    }
}
