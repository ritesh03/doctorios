//
//  CustomObjects.swift
//  DairyApp
//
//  Created by Apple on 24/03/17.
//  Copyright © 2017 Amit. All rights reserved.
//

import UIKit

class CustomObjects: NSObject {
    
    class func getStoryBoard(name:String)->UIStoryboard
    {
        return UIStoryboard.init(name: name, bundle: nil)
    }
    
    class func adjustFont(control:AnyObject)->UIFont
    {
         let ration = (UIScreen.main.bounds).size.height / (UIScreen.main.bounds).size.width
        
        if (control).isKind(of: UILabel.self) {
            
            let fontName = (control as! UILabel).font.fontName
            let fontSize = (control as! UILabel).font.pointSize
            let font = UIFont(name: fontName, size: fontSize*ration)
            return font!
    
        }else if (control).isKind(of: UITextField.self) {
           
            let fontName = (control as! UITextField).font?.fontName
            let fontSize = (control as! UITextField).font?.pointSize
            let font = UIFont(name: fontName!, size: fontSize!*ration)
            return font!
            
        }else if (control).isKind(of: UIButton.self) {
            
            let fontName = (control as! UIButton).titleLabel?.font?.fontName
            let fontSize = (control as! UIButton).titleLabel?.font.pointSize
            let font = UIFont(name: fontName!, size: fontSize!*ration)
            return font!
            
        }else if (control).isKind(of: UITextView.self) {
            
            let fontName = (control as! UITextView).font?.fontName
            let fontSize = (control as! UITextField).font?.pointSize
            let font = UIFont(name: fontName!, size: fontSize!*ration)
            return font!
            
        } else {
            
            return UIFont.systemFont(ofSize: 15.0)
        }
    }
    

    class func showActionSheet(_ sender: UIViewController, title: String?, message: String?, cancelButtonTitle: String, destructiveButtonTitle: String, otherButtonTitles: [String], completion: ((String) -> Void)?) {
        
        
        let actionSheetController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        if ((cancelButtonTitle.isEmpty) == false) {
            actionSheetController.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.cancel, handler: {
                (action : UIAlertAction) -> Void in
                completion!(action.title!)
            }))
        }
        if ((destructiveButtonTitle.isEmpty) == false) {
            actionSheetController.addAction(UIAlertAction(title: destructiveButtonTitle, style: UIAlertActionStyle.destructive, handler: {
                (action : UIAlertAction) -> Void in
                completion!(action.title!)
            }))
        }
        for buttonTitle in otherButtonTitles {
            actionSheetController.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: {
                (action : UIAlertAction) -> Void in
                completion!(action.title!)
            }))
        }
        
        if UIDevice().userInterfaceIdiom == .pad {
            
            actionSheetController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            actionSheetController.popoverPresentationController?.sourceRect = CustomObjects.sourceRectForBottomAlertController(sender)
            actionSheetController.popoverPresentationController!.sourceView = sender.view;
            sender.present(actionSheetController, animated: true, completion: nil)
            
        }
        else {
            sender.present(actionSheetController, animated: true, completion: nil)
        }
        
    }
    
    class func sourceRectForBottomAlertController(_ sender:UIViewController) -> CGRect {
        var sourceRect :CGRect = CGRect.zero
        sourceRect.origin.x = sender.view.bounds.midX - sender.view.frame.origin.x/2.0
        sourceRect.origin.y = sender.view.bounds.midY
        return sourceRect
    }
 
    class func archive(Object object: AnyObject,WithKey key: String){
        
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: object)
        UserDefaults.standard.set(archivedData, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func unArchiveObject(WithKey key: String) -> AnyObject?{
        
        if UserDefaults.standard.object(forKey: key) == nil {
            return nil
        }
        if let data = UserDefaults.standard.object(forKey: key) as? Data{
            
            let unarchivedObject = NSKeyedUnarchiver.unarchiveObject(with: data)
            return unarchivedObject! as AnyObject?
        }else if let data = UserDefaults.standard.object(forKey: key) as? AnyObject{
            
            return data
        }
    }
}
