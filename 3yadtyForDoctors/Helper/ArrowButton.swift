//
//  ArrowButton.swift
//  DairyApp
//
//  Created by Amit on 01/04/17.
//  Copyright © 2017 Amit. All rights reserved.
//

import Foundation
import UIKit

class ArrowButton: UIButton {
    
    var touchUp: ((_ button: UIButton) -> ())?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addTarget()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //this is my most common setup, but you can customize to your liking
        
        self.addTarget()
    }
    
    func addTarget() {
        self.addTarget(self, action: #selector(touchUp(_:)), for:.touchUpInside)
    }
    
    //actions
@objc  func touchUp(_ sender: UIButton) {
        touchUp?(sender)
    }
}
