//
//  Alert.swift
//  ThinkBeauty
//
//  Created by Amit on 02/05/18.
//  Copyright © 2018 Ritesh Chopra. All rights reserved.
//

import UIKit

enum AlertAction :String{
    case Ok
    case Cancel
    case Yes
    case No
    case Login
    case SignUp
}

extension UIAlertController {
    
    class func show(title: String?, message: String?, target:UIViewController?,actions: [AlertAction]?,preferredStyle:UIAlertControllerStyle?,handler: ((AlertAction) -> ())?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle!)
        if let array = actions,array.count>0 {
            for ac in array {
                alertController.addAction(UIAlertAction(title: ac.rawValue, style: .default) { action -> Void in
                    handler?(ac)
                })
            }
            
        } else {
            alertController.addAction(UIAlertAction(title: AlertAction.Ok.rawValue, style: .cancel) { action -> Void in
                handler?(AlertAction.Ok)
            })
        }
        target?.present(alertController, animated: true, completion: nil)
        
    }
    
    class func show(title: String?, message: String?, target:UIViewController?,handler: ((AlertAction) -> ())?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Ok", comment: ""), style: .cancel) { action -> Void in
            handler?(AlertAction.Ok)
        })
        target?.present(alertController, animated: true, completion: nil)
    }
    
    
    class func showToast(title: String?, message: String?, target:UIViewController?,handler: ((AlertAction) -> ())?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        target?.present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
            
            alertController.dismiss(animated: true, completion: nil)
        })
    }
}

extension UIView {
    
    // MARK: Activity Indicator
    
    func activityIndicator(show: Bool) {
        activityIndicator(show: show, style: .gray)
    }
    
    func activityIndicator(show: Bool, style: UIActivityIndicatorViewStyle) {
        var spinner: UIActivityIndicatorView? = viewWithTag(NSIntegerMax - 1) as? UIActivityIndicatorView
        
        if spinner != nil {
            spinner?.removeFromSuperview()
            spinner = nil
        }
        
        if spinner == nil && show {
            spinner = UIActivityIndicatorView.init(activityIndicatorStyle: style)
            spinner?.translatesAutoresizingMaskIntoConstraints = false
            spinner?.hidesWhenStopped = true
            spinner?.tag = NSIntegerMax - 1
            
            if Thread.isMainThread {
                spinner?.startAnimating()
            } else {
                DispatchQueue.main.async {
                    spinner?.startAnimating()
                }
            }
            
            insertSubview((spinner)!, at: 0)
            
            NSLayoutConstraint.init(item: spinner!, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0).isActive = true
            NSLayoutConstraint.init(item: spinner!, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
            
            spinner?.isHidden = !show
        }
    }
    
}


