//
//  CustomTextField.swift
//  DairyApp
//
//  Created by Dinesh on 24/03/17.
//  Copyright © 2017 Amit. All rights reserved.
//

import UIKit

class CustomTextField: UITextField,UITextFieldDelegate {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clear
        //self.layer.borderColor = UIColor.lightGray.cgColor
        //self.layer.borderWidth = 1.0
        //self.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
    }
    
    // Add dropdown for textfield
    func addDropDown(ddImage:UIImage, action: @escaping (_ button: UIButton) -> Void) {
        
        self.delegate = self
        let image = ddImage
        var rightView = UIButton()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar"  {
             rightView = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        }else{
             rightView = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        }
       
        rightView.isUserInteractionEnabled = false
        rightView.isEnabled = false
        rightView.setImage(image, for: .normal)
        self.contentMode = UIViewContentMode.center
        
        self.rightView = rightView
        self.rightViewMode = UITextFieldViewMode.always
        self.leftView = UIView(frame: rightView.frame)
        self.leftViewMode = UITextFieldViewMode.always
        
        //remove btn before adding new one 
        self.viewWithTag(100)?.removeFromSuperview()
        
        let button = ArrowButton()
        button.tag = 100
        button.touchUp = { button in
            print("Touch Up")
            action(button)
        }
        
        self.addSubview(button)
        
        // Add constraints
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.topMargin,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.topMargin,
            multiplier: 1,
            constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.bottomMargin,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.bottomMargin,
            multiplier: 1,
            constant: 0)
        
        let leadingConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.leading,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.leading,
            multiplier: 1,
            constant: 0)
        
        let trailingConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.trailing,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.trailing,
            multiplier: 1,
            constant: 0)
        
        self.addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
    }
    
    func addCalendar(action: @escaping (_ button: UIButton) -> Void) {
    
        self.delegate = self
        
        let image = UIImage(named: "calnder")
        let rightView = UIButton(frame: CGRect(x: 0, y: 0, width: (image?.size.width)!+15, height: (image?.size.height)!))
        rightView.isUserInteractionEnabled = false
        rightView.isEnabled = false
        rightView.setImage(image, for: .normal)
        self.contentMode = UIViewContentMode.center
        
        self.rightView = rightView
        self.rightViewMode = UITextFieldViewMode.always
        self.leftView = UIView(frame: rightView.frame)
        self.leftViewMode = UITextFieldViewMode.always
        
        //remove btn before adding new one
        self.viewWithTag(100)?.removeFromSuperview()
        
        let button = ArrowButton()
        button.tag = 100
        button.touchUp = { button in
            print("Touch Up")
            action(button)
        }
        
        self.addSubview(button)
        
        // Add constraints
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.topMargin,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.topMargin,
            multiplier: 1,
            constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.bottomMargin,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.bottomMargin,
            multiplier: 1,
            constant: 0)
        
        let leadingConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.leading,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.leading,
            multiplier: 1,
            constant: 0)
        
        let trailingConstraint = NSLayoutConstraint(
            item: button,
            attribute: NSLayoutAttribute.trailing,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.trailing,
            multiplier: 1,
            constant: 0)
        
        self.addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1{
            
            return true
        }else{
            
            self.resignFirstResponder()
            return false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1{
            
            return true
        }else{
            
            self.resignFirstResponder()
            return false
        }
    }
}
