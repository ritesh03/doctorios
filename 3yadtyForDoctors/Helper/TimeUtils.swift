//
//  TimeUtils.swift
//  Docsink
//
//  Created by Amit Kumar Shukla on 12/11/17.
//  Copyright © 2017 Docsink. All rights reserved.
//

struct TimeUtilsFormatter {
    static let fullDate = "yyyy-MM-dd HH:mm:ss"
    static let serverDate = "yyyy-MM-dd"
    static let displayDobDate = "dd-MMM-yyyy"
    static let serverDobDate = "dd/MM/yyyy"
    static let strDate = "dd MMM, yyyy"
    static let withFullMonthDate = "MMMM, dd, yyyy"
    static let time = "hh:mm a"
    static let month = "MM"
    static let year = "yyyy"
     static let newfullMonth = "yyyy-MMM-dd"
}

import Foundation

extension TimeInterval {
    private var milliseconds: Int {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }
    
    private var seconds: Int {
        return Int(self) % 60
    }
    
    private var minutes: Int {
        return (Int(self) / 60 ) % 60
    }
    
    private var hours: Int {
        return Int(self) / 3600
    }
    
    var stringTime: String {
        return "\(hours):\(minutes):\(seconds)"
    }
}

class TimeUtils {
    
    class var sharedUtils : TimeUtils {
        struct Static {
            static let instance = TimeUtils()
        }
        return Static.instance
    }
    
    // Mark- Convert date from string
    func dateFromString(_ strDate:String?, with format:String) -> Date? {
        guard let date = strDate else {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let result = dateFormatter.date(from: date)
        return result
    }
    
    // Mark- Convert string form date
    func stringFromdate(_ date:Date?, with format:String) -> String {
        guard let newDate = date else {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            dateFormatter.locale =  Locale(identifier: "ar")
        }
        
        return dateFormatter.string(from: newDate)
    }
    
    func fromdate(_ date:Date?, with format:String) -> String {
        guard let newDate = date else {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
      
        
        return dateFormatter.string(from: newDate)
    }
    func compare(startTime:String?, with endTime:String?) -> Bool {
        
        guard let startTime = startTime else { return false }
        guard let endTime = endTime else { return false }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        guard let sDate = dateFormatter.date(from: startTime) else { return false }
        guard let eDate = dateFormatter.date(from: endTime) else { return false }
        return ((sDate.addingTimeInterval((30.0 * 60.0))) < eDate)
    }
    
    //Mark- Convert 24 hour time format to 12 hour format
    func changeTimeString(_ strDate:String?) -> String {
        guard let date = strDate else {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let newDate = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: newDate!)
    }
    
    func changeDateString(_ strDate:String?) -> String {
        guard let date = strDate else {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let newDate = dateFormatter.date(from: date)
        return dateFormatter.string(from: newDate!)
    }
    
    // Mark- Convert date to new formate
    func convertDateFormater(_ strDate:String?, fromFormat:String, toFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let newDate = dateFormatter.date(from: strDate!)
        return self.stringFromdate(newDate, with: toFormat)
    }
    
    // Mark- Get time diff between dates
    func timeIntervalBetweenDates(startDate strStartDate:String?, enddate strEndDate:String?, with format:String)->TimeInterval {
        let sDate = self.dateFromString(strStartDate, with: format)
        let edate = self.dateFromString(strEndDate, with: format)
        return edate!.timeIntervalSince(sDate!)
    }
    
    func daysBetweenDates(startDate: Date?, endDate: Date?) -> Int {
        if let startDate = startDate,let endDate = endDate {
            let calendar = Calendar.current
            let components = calendar.dateComponents([Calendar.Component.day], from: startDate, to: endDate)
            return components.day ?? 0
        }
        return 0
    }

    func chaneDateFormet(_ strDate:String?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strDate!)
        dateFormatter.dateFormat = "MMM d, yyyy"
         if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
        dateFormatter.locale = Locale(identifier: "ar")
        }
         else{
             dateFormatter.locale = Locale(identifier: "en")
        }

        return dateFormatter.string(from: date!)
    }
    func chaneTimeFormet(_ strDate:String?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: strDate!)
        dateFormatter.dateFormat = "h:mm a"
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            dateFormatter.locale = Locale(identifier: "ar")
        }
        else{
            dateFormatter.locale = Locale(identifier: "en")
        }
        
        return dateFormatter.string(from: date!)
    }
}
