import UIKit
import IQKeyboardManagerSwift

open class CodeInputView: UIView, UIKeyInput {
    open var delegate: CodeInputViewDelegate?
    private var nextTag:Int = 1

    // MARK: - UIResponder

    open override var canBecomeFirstResponder : Bool {
        return true
    }

    // MARK: - UIView

    public override init(frame: CGRect) {
        super.init(frame: frame)

        // Add six digitLabels
        var frame = CGRect(x: 0, y: 10, width: 44, height: 40)
        for index in 1...4 {
            let digitLabel = UILabel(frame: frame)
            digitLabel.font = UIFont.boldSystemFont(ofSize: 22.0)
            digitLabel.tag = index
            digitLabel.text = "-"
            digitLabel.textColor = UIColor(red: 0/255.0, green: 149/255.0, blue: 188/255.0, alpha: 1.0)
            digitLabel.textAlignment = .right
            addSubview(digitLabel)
            frame.origin.x += 44 + 40
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    // required public init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") } // NSCoding

    // MARK: - UIKeyInput

    public var hasText : Bool {
        return self.nextTag > 1
    }

    open func insertText(_ text: String) {
        
        if self.nextTag < 5 {
            (viewWithTag(self.nextTag)! as! UILabel).text = text
            self.nextTag += 1

            if self.nextTag == 5 {
                var code = ""
                for index in 1..<self.nextTag {
                    code += (viewWithTag(index)! as! UILabel).text!
                }
                self.delegate?.codeInputView(self, didFinishWithCode: code)
            }
        }
    }

    open func deleteBackward() {
        if self.nextTag > 1 {
            self.nextTag -= 1
            (viewWithTag(self.nextTag)! as! UILabel).text = "–"
        }
        self.delegate?.codeInputView(self, didDeleteCode: self.nextTag)
    }

    open func clear() {
        while self.nextTag > 1 {
            self.deleteBackward()
        }
    }

    // MARK: - UITextInputTraits

    open var keyboardType: UIKeyboardType { get { return .phonePad } set { } }
   
}

public protocol CodeInputViewDelegate {
    func codeInputView(_ codeInputView: CodeInputView, didFinishWithCode code: String)
    func codeInputView(_ codeInputView: CodeInputView, didDeleteCode code: Int)
}
