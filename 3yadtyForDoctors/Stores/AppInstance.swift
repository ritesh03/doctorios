//
//  AppInstance.swift
//  Docsink
//
//  Created by Amit Kumar Shukla on 9/6/16.
//  Copyright (c) 2016 Docsink. All rights reserved.
//
// Use this class for storing shared instances

import Foundation
import GoogleMaps


class AppInstance: NSObject {

    static let shared = AppInstance()
    var user: UserData?
    var isLoggedIn:Bool?
    var access_token:String?
    var userType: Int? = 0
    var isFromMenu: Bool?
    var isOtpVerified: Bool?
    var isFromForgot: Bool?
    var doctorSignUp: [String:Any]?
    var clinicTimming = [[String:Any]]()
    var selectCategoryID : Int?
    //pass value in controllers
    var mobile: String?
    var selectedFlag: UIImage?
    var countryCode: String?
    var phoneCode: String?
    var user_latitude: String?
    var user_longitude: String?
    var selectedPatient: DataAppointmentList?
    var isShowPlus: Bool?
    var selectedDr: DataAppointmentList?
    var isAssistantCheckIn: Bool?
    var isShowHistoryHeaderCell: Bool?
    var patientListSelectedPatient: Patient_info?
    var arrayAvailablity: [String] = []
    
    var patientName: String?
    override init() {
      super.init()
    }
}
