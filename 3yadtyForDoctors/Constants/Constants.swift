//
//  Constants.swift
//  iOSArchitecture
//
//  Created by Amit on 23/02/18.
//  Copyright © 2018 smartData. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

let kAppDelegate  = AppDelegate().sharedInstance()
let kAppInstance = AppInstance.shared

enum Config {
    static let baseIP = "https://clinido.com" //Live
    //static let baseIP = "http://18.188.101.203" //Staging
    static let baseUrl =  baseIP + "/clinido/public/api"
    static let imageBaseUrl = baseIP + "/clinido/storage/app/public/profile_images/"
    static let clinicImageBaseUrl = baseIP +  "/clinido/storage/app/public/clinic_images/"
    static let doctorCertificateBaseUrl = baseIP + "/clinido/storage/app/public/dr_licence/"
    static let mobileVarification = "/drmobileverification"
    static let otpVarify = "/drotpverify"
    static let drsignup = "/drsignupFirst"//"/drsignup"
    static let drsignupSecond = "/drsignupSecond"//"/drsignup"
    static let doctor_login = "/doctor_login"
    static let add_appointment = "/add_appointment"
    static let get_categories = "/get_categories"
    static let appointment_to_doctor = "/appointment_to_doctor"
    static let assistant_list = "/assistant_list"
    static let approve_assistant = "/approve_assistant"
    static let pending_appointments_dr = "/pending_appointments_dr"
    static let accept_appointment_dr = "/accept_appointment_dr"
    static let reject_appointment_dr = "/reject_appointment_dr"
    static let get_notification = "/get_notification"
    static let edit_profile_doctor = "/edit_profile_doctor"
    static let get_subcategories = "/get_subcategories"
    static let city_listing = "/city_listing"
    static let area_listing = "/area_listing"
    static let clinic_listing = "/clinic_listing"
    static let delete_clinic = "/delete_clinic_slot"
    static let update_clinic = "/update_clinic_slot"
    static let add_assistant = "/add_assistant"
    static let book_manual_appointment = "/book_manual_appointment"
    static let appointment_time_slots = "/appointment_time_slots"
    static let patient_listing_dr = "/patient_listing_dr"
    static let forgotPassword = "/forgotPassword"
    static let forgotOtpVerification = "/forgotOtpVerification"
    static let changePassword = "/changePassword"
    static let resetPassword = "/ResetPassword"
    static let block_assistant = "/block_assistant"
    static let delete_assistant = "/delete_assistant"
    static let patient_history_dr = "/patient_history_dr"
    static let patient_checkin_dr = "/patient_checkin_dr"
    static let get_prescription_detail = "/get_prescription_detail"
    static let genric_list = "/genric_list"//
    static let trade_list = "/trade_list"
    static let add_prescription = "/add_prescription"
    static let edit_prescription = "/edit_prescription"
    static let delete_prescription = "/delete_prescription"
    static let addClinic = "/addClinic"
    static let addClinicImages = "/addClinicImages"
    static let cancelAppointmentAsst = "/cancel_appointment_asst"
    static let cancelAppointmentDr = "/cancel_appointment_dr"
    static let drNoShow = "/drNoShow"
    static let assitantNoShow = "/AssitantNoShow"
    static let resendDrOtp = "/resendDrOtp"
    
    static let doctorResend = "/drmobileverification"
    static let invoice_dr = "/invoice_dr"
    static let getVacation = "/get_vacation"
    static let checkVacation = "/check_vacation"
    static let addVacation = "/add_vacation"
    static let deleteVacation = "/delete_vacation"
    static let patientCheckupDR = "/patient_checkup_dr"
    static let patientCheckupAssit = "/patient_checkup_assit"
    static let updatePatientCase = "/updatePatientCase"
    static let payment = "/payment_dr"
    static let paymentAuthentication = "/paymentAuthentication"

    //Assistant
    static let assistant_otpverify = "/assistant_otpverify"
    static let assistant_mobile_verification = "/assistant_mobile_verification"
    static let asssistant_signup = "/asssistant_signup"
    static let assistantResend = "/assistant_mobile_verification"
    static let drlist = "/drlist"
    static let assistant_login = "/assistant_login"
    static let accepted_appointments = "/accepted_appointments"
    static let pending_appointments = "/pending_appointments"
    static let accept_appointment = "/accept_appointment"
    static let reject_appointment = "/reject_appointment"
    static let edit_profile_assitant = "/edit_profile_assitant"
    static let patient_listing = "/patient_listing"
    static let feedback = "/feedback"
    static let get_last_token = "/get_last_token"
    static let termsOfServices = "https://clinido.com/clinido/public/term_and_condition"
    static let privacyPolicy = "https://clinido.com/clinido/public/privacy_doctor"
}

enum Color {
    //static let navColor = UIColor(red: 103.0, green: 167.0, blue: 232.00, alpha: 1.0)
//    static var theme_color = UIColor(red:172/255.0,green:136/255.0,blue:70/255.0,alpha:1.0)
//    static var theme_color_light = UIColor(red:200/255.0,green:178/255.0,blue:138/255.0,alpha:1.0)
//    static var selectionIndicatorColor = UIColor(red: 114.0/255.0, green: 89.0/255.0, blue: 45.0/255.0, alpha: 1.0)
    
    static var selected_color = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1.0)
    static var unSelected_color = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
}

enum Alert{
    
   static let PleaseUploadImage = "Please upload image"
   static let FirstName = "Please enter first name"
   static let LastName = "Please enter last name"
   static let email = "Please enter email"
   static let validEmail = "Invaild email"
   static let password =  "Please enter password"
   static let passwordNotMatch = "Password and confirm password did not match"
   static let selectDoctor = "Please select doctor"
   static let clenicName = "Enter clinic name"
}

enum StringLiteral {
    static let loading = "Loading..."
}
var currentTimeStamp:Int64 {
    return Int64(Date().timeIntervalSince1970*1000)
}
enum Identifiers {
    enum Storyboard {
        static let main = "Main"
    }
    
    enum Controler {
        static let login = "Login"
        static let home = "Home"
    }
}

struct Days{
    
    var name: String
    var isAvailable: Bool
    var startTime: String
    var endTime: String
}

struct Constants
{
    struct refs
    {
        static let databaseRoot = Database.database().reference()
        static let databaseChats = databaseRoot.child("UserDetails")
        static let databaseChatsRoom = databaseRoot.child("Chat_Room")
        static let testDatabaseChats = databaseRoot.child("Test_Server_UserDetails")
        static let testDatabaseChatsRoom = databaseRoot.child("Test_Server_Chat_Room")
        static let databaseChatUser = databaseRoot.child("chat_user")
    }
}

struct Vacations{
    
    var id: Int
    var user_id: Int
    var start_on: String
    var end_on: String
}

struct SubCategories {
    
    var category_id:Int
    var id: Int
    var name: String
}

enum MonthFormat{
    
    case jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec
    var inMM: String {
        
        switch self {
        case .jan:
            return "01"
        case .feb:
            return "02"
        case .mar:
            return "03"
        case .apr:
            return "04"
        case .may:
            return "05"
        case .jun:
            return "06"
        case .jul:
            return "07"
        case .aug:
            return "08"
        case .sep:
            return "09"
        case .oct:
            return "10"
        case .nov:
            return "11"
        case .dec:
            return "12"
        }
    }
    
    var inMMM: String {
        
        switch self {
        case .jan:
            return "Jan"
        case .feb:
            return "Feb"
        case .mar:
            return "Mar"
        case .apr:
            return "Apr"
        case .may:
            return "May"
        case .jun:
            return "Jun"
        case .jul:
            return "Jul"
        case .aug:
            return "Aug"
        case .sep:
            return "Sep"
        case .oct:
            return "Oct"
        case .nov:
            return "Nov"
        case .dec:
            return "Dec"
        }
    }
    
    var inMMMM: String {
        
        switch self {
        case .jan:
            return "January"
        case .feb:
            return "February"
        case .mar:
            return "March"
        case .apr:
            return "April"
        case .may:
            return "May"
        case .jun:
            return "June"
        case .jul:
            return "July"
        case .aug:
            return "August"
        case .sep:
            return "September"
        case .oct:
            return "October"
        case .nov:
            return "November"
        case .dec:
            return "December"
        }
    }
}

enum KeyboardType{
    
    static let english: String = "en"
    static let arabic: String = "ar"
}
