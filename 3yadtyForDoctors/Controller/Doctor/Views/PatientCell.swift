//
//  PatientCell.swift
//  3yadty For Doctors
//
//  Created by apple on 08/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class PatientCell: UITableViewCell {

    @IBOutlet weak var imageViewPatient: RoundImage!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelday: UILabel!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonMessage: UIButton!
    @IBOutlet weak var labelClinicName: UILabel!
    @IBOutlet weak var buttonSelect: UIButton!
    @IBOutlet weak var onsiteLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
