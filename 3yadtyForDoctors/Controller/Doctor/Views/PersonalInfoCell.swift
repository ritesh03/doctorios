//
//  PersonalInfoCell.swift
//  3yadty For Doctors
//
//  Created by apple on 25/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class PersonalInfoCell: UITableViewCell {

    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var textFielFirstName: UITextField!
    @IBOutlet weak var TextFieldLastName: UITextField!
    @IBOutlet weak var imageViewFlag: UIImageView!
    @IBOutlet weak var labelCountaryCode: UILabel!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    @IBOutlet weak var labelEmail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
