//
//  WorkInfoCell.swift
//  3yadty For Doctors
//
//  Created by apple on 25/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class WorkInfoCell: UITableViewCell {

    @IBOutlet weak var hospitalClinicNameLabel: UILabel!
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelHospitalName: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
