//
//  PatientHistoryCell.swift
//  3yadtyForDoctors
//
//  Created by apple on 29/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class PatientHistoryCell: UITableViewCell {

    @IBOutlet weak var labelAppointmentType: UILabel!
    @IBOutlet weak var labelAppointmrntDate: UILabel!
  
    @IBOutlet weak var labelDrName: UILabel!
    @IBOutlet weak var labelDr: UILabel!
    @IBOutlet weak var buttonType: RoundButton!
    @IBOutlet weak var imageViewProfile: UIImageView!
 
    @IBOutlet weak var labelAppointmentTypeHeader: UILabel!
    @IBOutlet weak var labelAppointmrntDateHeader: UILabel!
    
    @IBOutlet weak var labelDrNameHeader: UILabel!
    @IBOutlet weak var labelDrHeader: UILabel!
    @IBOutlet weak var buttonTypeHeader: RoundButton!
    @IBOutlet weak var imageViewProfileHeader: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
