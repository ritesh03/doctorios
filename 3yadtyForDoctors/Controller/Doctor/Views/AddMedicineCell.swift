//
//  AddMedicineCell.swift
//  3yadtyForDoctors
//
//  Created by apple on 03/12/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AddMedicineCell: UITableViewCell {

    @IBOutlet weak var textFieldName: CustomTextField!
    @IBOutlet weak var labelDose: UILabel!
    @IBOutlet weak var labelTitleDay: UILabel!
    @IBOutlet weak var buttonDay: RoundButton!
    @IBOutlet weak var buttonWeek: RoundButton!
    @IBOutlet weak var buttonMonth: RoundButton!
    @IBOutlet weak var buttonYear: RoundButton!
    
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var buttonCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
