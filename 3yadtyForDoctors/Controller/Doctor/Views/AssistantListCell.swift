//
//  AssistantListCell.swift
//  3yadtyForDoctors
//
//  Created by apple on 31/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantListCell: UITableViewCell {

    @IBOutlet weak var imageViewAssistant: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonMessage: UIButton!
    @IBOutlet weak var blockedLabel: UILabel!

    var call_block: (() -> Void)? = nil
    var message_block: (() -> Void)? = nil
    
    @IBAction func didTapCALLButton(sender: UIButton) {
        call_block?()
    }
    @IBAction func didTapmessageButton(sender: UIButton) {
        message_block?()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }

}
