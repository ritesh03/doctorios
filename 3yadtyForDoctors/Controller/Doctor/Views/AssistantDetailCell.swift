//
//  AssistantDetailCell.swift
//  3yadtyForDoctors
//
//  Created by apple on 20/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantDetailCell: UITableViewCell {

    @IBOutlet weak var labelClinicName: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var hospitalClinicLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
