//
//  AddNewPatientCell.swift
//  3yadty For Doctors
//
//  Created by apple on 08/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AddNewPatientCell: UITableViewCell {

    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var imageViewFlag: RoundImage!
    @IBOutlet weak var labelCountaryCode: UILabel!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonCountary: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
