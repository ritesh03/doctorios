//
//  ClinicDetailPhotosCVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 14/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

protocol ClinicImageDelegate {
    func performActionToRemoveImage(tag: Int)
}

class ClinicDetailPhotosCVC: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var crossImageView: RoundImage!
    
    var delegate: ClinicImageDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBAction func crossButtonTapped(_ sender: UIButton) {
        self.delegate?.performActionToRemoveImage(tag: sender.tag)
    }
}
