//
//  LogInCell.swift
//  3yadty For Doctors
//
//  Created by apple on 20/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class LogInCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var buttonShow: UIButton!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var imageViewFlag: UIImageView!
    @IBOutlet weak var labelCountaryCode: UILabel!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var iconsImageView: UIImageView!
    
    @IBOutlet weak var emailPhoneTFLeading: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
