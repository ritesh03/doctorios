//
//  WelcomeScreenCell.swift
//  3yadty For Doctors
//
//  Created by apple on 20/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class WelcomeScreenCell: UITableViewCell {

    @IBOutlet weak var lableTitle: UILabel!
    @IBOutlet weak var buttonRadio: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

