//
//  DoctorAvailabiltyTVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 14/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DoctorAvailabiltyTVC: UITableViewCell {
    
    @IBOutlet weak var view_time: UIView!
    @IBOutlet weak var switch_day: UISwitch!
    @IBOutlet weak var lbl_from: UILabel!
    @IBOutlet weak var tf_time_from: CustomTextField!
    @IBOutlet weak var lbl_to: UILabel!
    @IBOutlet weak var tf_time_to: CustomTextField!
    @IBOutlet weak var lbl_day: UILabel!
    
    var dayObj: Days!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.switch_day.transform = .init(scaleX: 0.7, y: 0.7)
        self.textFieldsSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    func day(tag: Int) -> String {
        switch tag {
        case 0:
            return "Sunday"
        case 1:
            return "Monday"
        case 2:
            return "Tuesday"
        case 3:
            return "Wednesday"
        case 4:
            return "Thursday"
        case 5:
            return "Friday"
        default:
            return "Saturday"
        }
    }
    
    private func textFieldsSetup(){
        
        let startTime = "12:00 AM"
        let endTime = "09:00 PM"
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let startDate = formatter.date(from: startTime)
        let endDate = formatter.date(from: endTime)
        self.tf_time_from.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            
            self.endEditing(true)
            ActionSheetDatePicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "From"), comment: "") , datePickerMode: UIDatePickerMode.time, selectedDate: startDate,doneBlock: {
                picker, value, index in
                
                

                guard let tempDate = formatter.date(from: self.tf_time_to.text!) else { return }
                let startTime = TimeUtils.sharedUtils.fromdate(value as? Date, with: TimeUtilsFormatter.time)
                let endTime = TimeUtils.sharedUtils.fromdate(tempDate, with: TimeUtilsFormatter.time)
                if TimeUtils.sharedUtils.compare(startTime: startTime, with: endTime){
                    
                    self.tf_time_from.text = startTime
                    self.dayObj.startTime = startTime
                    
                    var infoDict:[String:Any] = [:]
                    infoDict["index"] = self.switch_day.tag
                    infoDict["time"] = startTime
                    infoDict["isfromValue"] = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTime"), object: nil, userInfo: infoDict)
                }else{
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                        
                        UIAlertController.showToast(title: self.localizeValue(key: "Start date could not be greater than end date."), message: nil, target:  UIApplication.shared.keyWindow?.rootViewController!, handler: nil)
                    })
                }
                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
        
        self.tf_time_to.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            
            self.endEditing(true)
            ActionSheetDatePicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "To"), comment: "") , datePickerMode: UIDatePickerMode.time, selectedDate: endDate,doneBlock: {
                picker, value, index in
             
                guard let tempDate = formatter.date(from: self.tf_time_from.text!) else { return }
                
                let startTime = TimeUtils.sharedUtils.fromdate(tempDate, with: TimeUtilsFormatter.time)
                let endTime = TimeUtils.sharedUtils.fromdate(value as? Date, with: TimeUtilsFormatter.time)
                if TimeUtils.sharedUtils.compare(startTime: startTime, with: endTime){
                    
                    self.tf_time_to.text = endTime
                    self.dayObj.endTime = endTime
                    
                    var infoDict:[String:Any] = [:]
                    infoDict["index"] = self.switch_day.tag
                    infoDict["time"] = endTime
                    infoDict["isfromValue"] = false
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTime"), object: nil, userInfo: infoDict)
                }else{
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                        
                        UIAlertController.showToast(title: "\(self.localizeValue(key: "Invalid timing for")) \(self.localizeValue(key: self.day(tag: self.switch_day.tag)))", message: nil, target:  UIApplication.shared.keyWindow?.rootViewController!, handler: nil)
                    })
                }
                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
    }
}
