//
//  AddAddressCell.swift
//  3yadty For Doctors
//
//  Created by apple on 27/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol DayCollectionViewCellDelegate {
    func collectionDayView(didDeleteItemAt indexPath: IndexPath)
    func collectionDayView(didSelectItemAt indexPath: IndexPath)
}

class AddAddressCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tfHospitalName: CustomTextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var collectionCell: UICollectionView!
    var daysSelected = [[String:Any]]()
    var arrWeekDays = [String]()
    var collectionDelegate:DayCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        arrWeekDays = [localizeValue(key: "Sunday"), localizeValue(key: "Monday"), localizeValue(key: "Tuesday"), localizeValue(key: "Wednesday"), localizeValue(key: "Thursday"), localizeValue(key: "Friday"), localizeValue(key: "Saturday")]
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    


}
extension AddAddressCell: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddAddressCollectionCell", for: indexPath) as! AddAddressCollectionCell
        cell.labelDay.text = String(((arrWeekDays[indexPath.row]) ).prefix(1))
        if daysSelected.count > 0 {
        for index in 0..<daysSelected.count{
            let dict = daysSelected[index]
            let key = dict["day"] as? String
            if key == arrWeekDays[indexPath.row] {
                cell.buttonRadio.isSelected = true
                break
            } else {
                cell.buttonRadio.isSelected = false
            }
        }
        } else {
             cell.buttonRadio.isSelected =  false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for index in 0..<daysSelected.count{
            let dict = daysSelected[index]
            let key = dict["day"] as? String
            if key == arrWeekDays[indexPath.row] {
                let indexCell = IndexPath(row:index,section:0)
                self.collectionDelegate?.collectionDayView(didDeleteItemAt: indexCell)
                return
            }
        }
        self.collectionDelegate?.collectionDayView(didSelectItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  5
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/7, height: collectionViewSize/4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
