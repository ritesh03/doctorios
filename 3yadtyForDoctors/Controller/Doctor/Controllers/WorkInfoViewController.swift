//
//  WorkInfoViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 25/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class WorkInfoViewController: BaseViewControler, IndicatorInfoProvider, UITextFieldDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var setDoctorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableWorkInfo: UITableView!
    @IBOutlet weak var headerView: NSLayoutConstraint!
    @IBOutlet weak var buttonNext: RoundButton!
    @IBOutlet weak var btn_skip: UIButton!

    var availability = NSArray()
    var strVistFees: String?
    var strConsultantFees: String?
    var strAllowedConsultant: String?
    var clinicTiming = Array<Clinic>()
//
    var nextBlockWork : (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.setTitle(localizeValue(key: "Add +"), for: .normal)
        setDoctorLabel.text = localizeValue(key: "Set Doctor / Clinic address and Working hours")
        titleLabel.text = localizeValue(key: "Doctor Clinic")
        btn_skip.setTitle(localizeValue(key: "Skip"), for: .normal)
        buttonNext.setTitle(localizeValue(key: "Done"), for: .normal)
        
        headerView.constant += UIApplication.shared.statusBarFrame.size.height
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
//        self.btn_skip.setTitle((AppInstance.shared.isFromMenu ?? false) ? "Done".localize() : "Skip".localize(), for: .normal)
//        self.buttonNext.isHidden = true
        
        // Do any additional setup after loading the view.
        
         //get previous data
//        paramEdit = AppInstance.shared.doctorSignUp!
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if let clinics = AppInstance.shared.user?.clinic{
            
            self.clinicTiming = clinics
        }
//        if AppInstance.shared.isFromMenu == true {
//            headerView.constant = self.headerView.constant
//            buttonNext.isHidden = false
//        }else{
//
//            headerView.constant = self.headerView.constant
//            buttonNext.isHidden = false
//        }
        tableWorkInfo.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str: NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        return (((str as String).first == "0") ? str.length <= 12 : str.length <= 11)
    }

    @IBAction func buttonDotClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: localizeValue(key: "Edit"), style: .default , handler:{ (UIAlertAction)in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ClinicDetailVC") as! ClinicDetailVC
            vc.selectedClinic = sender.tag
            self.navigationController?.pushViewController(vc, animated: false)
        }))
        alert.addAction(UIAlertAction(title: localizeValue(key: "Delete"), style: .destructive , handler:{ (UIAlertAction)in
            
            
            self.deleteItem(at: sender.tag)
        }))
        alert.addAction(UIAlertAction(title: localizeValue(key: "Dismiss"), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    private func deleteItem(at index:Int){
        
        let alert = UIAlertController.init(title: localizeValue(key: "Do you want to Delete ?"), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: localizeValue(key: "No"), style: .destructive, handler: nil))
        alert.addAction(UIAlertAction.init(title: localizeValue(key: "Yes"), style: .default, handler: { (_) in
            
            if let avail = self.clinicTiming[index].availability {
                
                if avail.count > 0{
                    
                    if let clinic_id = avail[0].clinic_id {
                        let service = BusinessService()
                        service.delClinicList(with: String(describing: AppInstance.shared.user!.id!), clinic_id: String(describing: clinic_id), target: self, complition: { (response) in
                            
                            if response {
                                
                                self.clinicTiming.remove(at: index)
                                self.tableWorkInfo.reloadData()
                            }
                        })
                    }
                }
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func buttonAddClicked(_ sender: Any) {//AddAddressViewController
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ClinicDetailVC") as! ClinicDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func actionOnSkip(_ sender: UIButton){
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController!.pushViewController(VC, animated: true)
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
       
        var params = [String:Any]()
        
        if let paramObj = AppInstance.shared.doctorSignUp {
            params = paramObj
        } else{
            params = paramEdit
        }
        
        if self.clinicTiming.count <= 0{
            UIAlertController.show(title: nil, message: "Please add clinic".localize(), target: self, handler: nil)
            return
        }
        
        print(params)
        
        if nextBlockWork != nil {
            nextBlockWork!()
        } else{
            //Second controller
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController!.pushViewController(VC, animated: true)
        }

    }
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "Doctor Clinic"))
    }
}

extension WorkInfoViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return self.clinicTiming.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dict = self.clinicTiming[section]
        let availability = dict.availability
        return availability?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkInfoCell", for: indexPath) as! WorkInfoCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let availabilityObj = ((self.clinicTiming[indexPath.section].availability! as NSArray)[indexPath.row] as! Availability)
        cell.labelDay.text = localizeValue(key: availabilityObj.day ?? "")
//        if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
//            cell.labelTime.text = "From".localize() + ":\(String(describing: (availabilityObj.start_time ?? "")))" + "To".localize() + ":\(String(describing: (availabilityObj.end_time ?? "")))"
//        }else{
//            cell.labelTime.text = "\(String(describing: (availabilityObj.end_time ?? ""))):" + "To".localize() + "\(String(describing: (availabilityObj.start_time ?? ""))):" + "From".localize()
//        }
        var start_time = availabilityObj.start_time
        let last = String(start_time!.characters.suffix(2))
        var new_start_time = start_time!.dropLast(2)
    
        if last == "AM" || last == "am" {
            new_start_time = new_start_time + localizeValue(key: "AM")
        }else if last == "PM" || last == "pm" {
             new_start_time = new_start_time + localizeValue(key: "PM")
        }

        var end_time = availabilityObj.end_time
        let end_time_last = String(end_time!.characters.suffix(2))
        var new_end_time = end_time!.dropLast(2)
        if end_time_last == "AM" || end_time_last == "am" {
            new_end_time = new_end_time + localizeValue(key: "AM")
        }else if end_time_last == "PM" || end_time_last == "pm"{
            new_end_time = new_end_time + localizeValue(key: "PM")
        }
        
        cell.labelTime.text = "\(self.localizeValue(key: "From")) \(String(describing: (new_start_time ?? ""))) \(self.localizeValue(key: "To")) \(String(describing: (new_end_time ?? "")))"
//        cell.labelTime.text = "\(localizeValue(key: "From")): \(String(describing: (availabilityObj.start_time ?? ""))) \(localizeValue(key: "To")): \(String(describing: (availabilityObj.end_time ?? "")))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 70.0
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // Removes extra padding in Grouped style
        return 70 + CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // Removes extra padding in Grouped style
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! WorkInfoCell
        var title = String()
      
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
           title = (self.clinicTiming[section]).clinic_name_ar ?? ""
        }else{
             title = (self.clinicTiming[section]).clinic_name ?? ""
        }
            headerView.labelHospitalName.text = title
        headerView.hospitalClinicNameLabel.text = localizeValue(key: "Hospital / Clinic name")
            headerView.buttonEdit.tag = section
            return headerView
    }
}
