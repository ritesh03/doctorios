//
//  AssistantDetailViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 20/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantDetailViewController: BaseViewControler {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imageViewProfile: RoundImage!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var tableAssistantDetail: UITableView!
    
    var selectedAssistant: DataDoctorRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let data = selectedAssistant
        if data!.image?.isEmpty != true{
            imageViewProfile.sd_setShowActivityIndicatorView(true)
            imageViewProfile.sd_setIndicatorStyle(.gray)
            imageViewProfile.sd_setImage(with: URL(string: data!.image!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            imageViewProfile.sd_setShowActivityIndicatorView(false)
        }else{
            imageViewProfile.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            
            backButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    
        }
        labelName.text = "\(data!.firstname!) \(data!.lastname!)"
        if data?.countrycode?.first == "+" {
             labelPhone.text = "\(String(describing: data!.countrycode!)) \(String(describing: data!.mobile!))"
        }else{
             labelPhone.text = "+\(String(describing: data!.countrycode!)) \(String(describing: data!.mobile!))"
        }
       
        labelPhone.text = !(labelPhone.text!.contains("+")) ? "+\(labelPhone.text!)" : labelPhone.text!
    }
    
    @IBAction func buttonCallClicked(_ sender: UIButton) {
       let data = selectedAssistant
       let cCode = data!.countrycode!
       let cc = cCode
        let mobile = "\(String(describing: cc))\(String(describing: data!.mobile!))"
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @IBAction func buttonMessageClicked(_ sender: Any) {
        
        let data = selectedAssistant
        let user = ["email": data?.email ?? "","fcmUserId": String(describing: data!.id!) ,"image": data?.image ?? "","lastMessage":"","fcmToken":"","userName": "\(String(describing: data!.firstname!)) \(String(describing: data!.lastname!))" ] as [String : Any]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.selectedUser = Person.init(dictionary: user as NSDictionary)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonOptionClicked(_ sender: Any) {
        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "AssiatantDetailPopUpViewController") as! AssiatantDetailPopUpViewController
        vc.delegate = self
        vc.status = selectedAssistant?.blockstatus
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AssistantDetailViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssistantDetailCell", for: indexPath) as! AssistantDetailCell
        let data = selectedAssistant
         cell.labelTitle.text = localizeValue(key: "Blocked")
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             cell.labelClinicName.text = data?.clinic?.clinic_name_ar
            cell.labelClinicName.textAlignment = .right
        }else{
             cell.labelClinicName.text = data?.clinic?.clinic_name
        }
       
        if data?.blockstatus == "1"{
            //cell.BlockStatus.text = "Blocked"
            cell.labelTitle.isHidden = false
        }else{
            //cell.BlockStatus.text = "Unblocked"
            cell.labelTitle.isHidden = true
        }
        cell.hospitalClinicLabel.text = localizeValue(key: "Hospital / Clinic name")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension AssistantDetailViewController: AssistantDetailDelegate{
    
    func editProfileClicked() {
        
    }
    
    func blockAssistantClicked() {
        var str = ""
        var Btnstr = ""
        if self.selectedAssistant?.blockstatus == "1"{
            
            str = "\(localizeValue(key: "Do you want to")) \(localizeValue(key: "Unblock")) \(localizeValue(key: "assistant?"))"
                
            Btnstr = localizeValue(key: "Unblock")
        }else{
            
            str = "\(localizeValue(key: "Do you want to")) \(localizeValue(key: "Block")) \(localizeValue(key: "assistant?"))"
                
            
            Btnstr = localizeValue(key: "Block")
        }
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Btnstr, style: .default, handler: { action in
            switch action.style{
            case .default:
                let service = BusinessService()
                service.blockAssistant(with: String(describing: AppInstance.shared.user!.id!), assistant_id: String(describing: self.selectedAssistant!.id!), target: self) { (response) in
                    if response == true {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: localizeValue(key: "Cancel"), style: .cancel, handler: { action in
            switch action.style{
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            case .default:
                 print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func deleteAssistantClicked() {
        let alert = UIAlertController(title: "", message: localizeValue(key: "Do you want to remove assistant?"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: localizeValue(key: "Delete"), style: .default, handler: { action in
            switch action.style{
            case .default:
                let service = BusinessService()
                service.deleteAssistant(with: String(describing: AppInstance.shared.user!.id!), assistant_id: String(describing: self.selectedAssistant!.id!), target: self) { (response) in
                    if response == true {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: localizeValue(key: "Cancel"), style: .cancel, handler: { action in
            switch action.style{
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            case .default:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
        

    }
}
