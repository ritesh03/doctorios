//
//  AppointmentCalenderViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 05/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import FSCalendar

class AppointmentCalenderViewController: BaseViewControler, IndicatorInfoProvider, FSCalendarDataSource, FSCalendarDelegate {
    
    // MARK:- IBOutlets
    
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet private weak var calendar: FSCalendar!
    @IBOutlet private weak var tableCalanderList: UITableView!
    @IBOutlet private weak var errorView: UIView!
    
    // MARK:- Variables
    
    private let formatter = DateFormatter()
    
    private var arrAcceptedAppointment: Appointment?
    private var statisticsData: Appointment_data?
    private var appointmentListData: DataAppointment?
    private var appointments = [DataAppointmentList]()
    private var selectedDate = Date()
    var isDataLoading = false
    var pageNo = 1
    var stopReload = false


    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorView.isHidden = true
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
             calendar.calendarHeaderView.calendar.locale =  Locale(identifier: "ar")
        }else{
             calendar.calendarHeaderView.calendar.locale =  Locale(identifier: "en_EN")
        }
        errorTitleLabel.text = localizeValue(key: "Reservation list is empty")
        errorDescriptionLabel.text = localizeValue(key: "You don't have any reservations")
       


        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         stopReload = false
         self.appointments.removeAll()
          self.pageNo = 1
         calendar.calendarHeaderView.collectionViewLayout.collectionView?.semanticContentAttribute = .forceLeftToRight
        if AppInstance.shared.userType == 0{
            
            let date = Date()
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            self.getDrAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
            self.calendar.select(Date())
                

        }else{
            let date = Date()
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            self.getAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
            self.calendar.select(Date())
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "Calendar"))
    }
    
    private func checkIfMessageButtonIsEnabled(data: DataAppointmentList) -> Bool {
        
        var isEnabled = false
        if data.visit_type == 0 || data.visit_type == 3 {
            isEnabled = false
        } else {
            isEnabled = true
        }
        if data.patient_info?.accounttype == 1 {
            if data.patient_info?.fcmStatus != "Y" {
                isEnabled = false
            }
        } else if data.patient_info?.accounttype == 4 || data.patient_info?.accounttype == 5 {
            isEnabled = false
        } else {
            isEnabled = true
        }
        return isEnabled
    }
    //FSCalendar 
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //        self.selectedDate = date
        let result = formatter.string(from: date)
          self.pageNo = 1
        self.appointments.removeAll()
        if AppInstance.shared.userType == 0{
            self.getDrAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
        }else{
            self.getAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
        }
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        for a in AppInstance.shared.arrayAvailablity{
            if a == result{
                return 1
            }
        }
        return 0
    }
    
    func getDrAcceptedAppointmentList(date: String, pageNo: String) {
        let service = BusinessService()
        service.drAcceptedAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: pageNo, date: date, type: "1", target: self,isAnimated: true) { (response) in
            if response == nil{
                self.errorView.isHidden = false
                self.arrAcceptedAppointment = nil
                self.statisticsData = nil
                self.appointmentListData = nil
            //    self.appointments = nil
            }
            if let user = response {
                self.arrAcceptedAppointment = user
                self.statisticsData = user.appointment_data!
                self.appointmentListData = user.data!
                for appointments in user.data!.data!{
                    self.appointments.append(appointments)
                }
             //  self.appointments = user.data!.data!
               
                if (self.appointments.count) > 0{
                    self.errorView.isHidden = true
                }else{
                    self.errorView.isHidden = false
                }
            }
            self.tableCalanderList.reloadData()
            
            if response?.data?.data?.count == 0 {
                self.stopReload = true
            }
        }
    }
    
    func getAcceptedAppointmentList(date: String,pageNo: String) {
        
        //String(AppInstance.shared.user.id!)
        let clinicId = AppInstance.shared.user?.clinic_id
        let service = BusinessService()
        service.acceptedAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: pageNo, date: date, type: "1", doctor_id: String(describing: AppInstance.shared.user!.dr_id!), clinic_id: String(describing: clinicId!), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
                self.arrAcceptedAppointment = nil
                self.statisticsData = nil
                self.appointmentListData = nil
              //  self.appointments = nil
            }
            if let user = response {
                self.arrAcceptedAppointment = user
                self.statisticsData = user.appointment_data!
                self.appointmentListData = user.data!
               // self.appointments = user.data!.data!
                for appointments in user.data!.data!{
                    self.appointments.append(appointments)
                }
                if self.statisticsData?.new != 0 || self.statisticsData?.new != 0
                    || (self.appointments.count) > 0 ||
                    self.statisticsData?.total_pending_reservation != 0 {
                    self.errorView.isHidden = true
                }
                else{
                    self.errorView.isHidden = false
                }
                self.tableCalanderList.reloadData()
                
                if response?.data?.data?.count == 0 {
                    self.stopReload = true
                }
            }
        }
    }
    
    func weekday_to_Int(day :String) -> Int {
        switch day {
        case "Sunday":
            return 1
        case "Monday":
            return 2
        case "Tuesday":
            return 3
        case "Wednesday":
            return 4
        case "Thursday":
            return 5
        case "Friday":
            return 6
        default:
            return 7
        }
    }
    
    @objc func buttonCallClicked(_ sender: UIButton) {
        let mobile = "+\(String(describing: self.appointments[sender.tag].patient_info!.countrycode!))\(String(describing: self.appointments[sender.tag].patient_info!.mobile!))"
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @objc func buttonMessageClicked(_ sender: UIButton) {
        let data = self.appointments[sender.tag]
        let user = ["email": data.patient_info?.email ?? "","fcmUserId": String(describing: data.patient_info!.id!) ,"image": data.patient_info?.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data.patient_info?.firstname ?? "") \(data.patient_info?.lastname ?? "")"] as [String : Any]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.selectedUser = Person.init(dictionary: user as NSDictionary)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func actionOnNoShow(_ sender: UIButton){
        
    //    if let object = self.appointments[sender.tag]{
            
            if ((self.appointments[sender.tag].status == 4) || (self.appointments[sender.tag].status == 3)){
                
                return
            }
            let alert = UIAlertController.init(title: localizeValue(key: "Do you want to no show appointment of this patient?"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: localizeValue(key: "No"), style: .destructive, handler: nil))
            alert.addAction(UIAlertAction.init(title: localizeValue(key: "Yes"), style: .default, handler: { (_) in
                
                let service = BusinessService()
                service.noShowAppointments(with: "\(AppInstance.shared.user?.id ?? 0)", appointment_id: "\(self.appointments[sender.tag].id ?? 0)", target: self) { (response) in
                    if response == true {
                        
                        
                        self.formatter.dateFormat = "yyyy-MM-dd"
                        let result = self.formatter.string(from: self.calendar.selectedDate ?? self.calendar.currentPage)
                         self.appointments.removeAll()
                          self.pageNo = 1
                        if AppInstance.shared.userType == 0{
                          
                            self.getDrAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
                        }else{
                            
                            self.getAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
                        }
                    }
                }
            }))
            self.present(alert, animated: true, completion: nil)
       // }
    }
    
    @objc func actionOnCancel(_ sender: UIButton){
        
      //  if let object = self.appointments[sender.tag]{
            
            if (self.appointments[sender.tag].status == 4 || (self.appointments[sender.tag].noshow == 1)){
                
                return
            }
            let alert = UIAlertController.init(title: localizeValue(key: "Do you want to cancel appointment of this patient?"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "No".localize(), style: .destructive, handler: nil))
            alert.addAction(UIAlertAction.init(title: "Yes".localize(), style: .default, handler: { (_) in
                
                let service = BusinessService()
                service.cancelAppointments(with: "\(AppInstance.shared.user?.id ?? 0)", appointment_id: "\(self.appointments[sender.tag].id ?? 0)", target: self) { (response) in
                    if response == true {
                        
                        self.formatter.dateFormat = "yyyy-MM-dd"
                        let result = self.formatter.string(from: self.calendar.selectedDate ?? self.calendar.currentPage)
                         self.appointments.removeAll()
                          self.pageNo = 1
                        if AppInstance.shared.userType == 0{
                            
                            self.getDrAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
                            NotificationCenter.default.post(name: NSNotification.Name("AppointmentSuccess"), object: nil)
                        }else{
                            
                            self.getAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
                        }
                    }
                }
            }))
            self.present(alert, animated: true, completion: nil)
       // }
    }
    
    @IBAction func buttonSelectedCell(_ sender: UIButton) {
        let data = self.appointments[sender.tag]
        let vc = storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
        AppInstance.shared.selectedPatient = data
        AppInstance.shared.isShowHistoryHeaderCell = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        print("scrollViewDidEndDragging")
        if ((tableCalanderList.contentOffset.y + tableCalanderList.frame.size.height) >= tableCalanderList.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                self.pageNo = self.pageNo + 1
                if !stopReload{
                    if AppInstance.shared.userType == 0{
                        
                        let date = Date()
                        formatter.dateFormat = "yyyy-MM-dd"
                        let result = formatter.string(from: date)
                        self.getDrAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
                        self.calendar.select(Date())
                        
                        
                    }else{
                        let date = Date()
                        formatter.dateFormat = "yyyy-MM-dd"
                        let result = formatter.string(from: date)
                        self.getAcceptedAppointmentList(date: result, pageNo: "\(self.pageNo)")
                        self.calendar.select(Date())
                    }
                }
                
                
                
            }
        }
        
        
    }
    
    @objc func buttonCheckInClicked(_ sender: UIButton) {
      //  if let object = self.appointments[sender.tag]{
            
            if ((self.appointments[sender.tag].status == 3) || (self.appointments[sender.tag].noshow == 1)){
                
                return
            }
            
            if self.appointments[sender.tag].status == 4{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
                vc.isMessageButtonEnabled = self.checkIfMessageButtonIsEnabled(data: self.appointments[sender.tag])
                AppInstance.shared.selectedPatient = self.appointments[sender.tag]
                AppInstance.shared.isShowHistoryHeaderCell = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let service = BusinessService()
                service.checkupAppointment(with: "\(AppInstance.shared.user?.id ?? 0)", appointment_id: "\(self.appointments[sender.tag].id ?? 0)", target: self) { (response) in
                    if response == true {
                        
                        self.appointments[sender.tag].status = 4
                        self.tableCalanderList.reloadData()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
                        vc.isMessageButtonEnabled = self.checkIfMessageButtonIsEnabled(data: self.appointments[sender.tag])
                        AppInstance.shared.selectedPatient = self.appointments[sender.tag]
                        AppInstance.shared.isShowHistoryHeaderCell = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
    //    }
    }
}

extension AppointmentCalenderViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appointments.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! AppointmentListCell
      
        let data = self.appointments[indexPath.row]
        if self.selectedDate > CommonClass.convertStringToDate(date: data.appointment_date!) {
            cell.btn_no_show.isHidden = true
            cell.btn_cancel.isHidden = true
            cell.buttonCheckedIn.isHidden = true
        }
        cell.labelAsName.text = "\(data.patient_info?.firstname ?? "") \(data.patient_info?.lastname ?? "")"
        if data.patient_info?.image?.isEmpty != true{
            cell.imageViewList.sd_setShowActivityIndicatorView(true)
            cell.imageViewList.sd_setIndicatorStyle(.gray)
            cell.imageViewList.sd_setImage(with: URL(string: Config.imageBaseUrl + (data.patient_info?.image)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewList.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewList.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.labelClinicName.textAlignment = .right
            cell.labelAsName.textAlignment = .right
              cell.labelClinicName.text = data.clinic?.clinic_name_ar ?? ""
        }else{
              cell.labelClinicName.text = data.clinic?.clinic_name ?? ""
        }
      
        cell.buttonCall.tag = indexPath.row
        cell.buttonMessage.tag = indexPath.row
        cell.buttonSelectedCell.tag = indexPath.row
        cell.buttonCheckedIn.tag = indexPath.row
        cell.btn_cancel.tag = indexPath.row
        cell.btn_no_show.tag = indexPath.row
        let isOnSite = (data.visit_type == 0)
        let dateStr = TimeUtils.sharedUtils.chaneDateFormet((data.appointment_date)!)
        
        if data.set_time == "" {
            cell.labelAsDate.text = "\(dateStr)"
        } else {
            
            
            var start_time = data.set_time
            let last = String(start_time!.characters.suffix(2))
            var new_start_time = start_time!.dropLast(2)
            if last != "" {
                if last == "AM" || last == "am"{
                    new_start_time = new_start_time + "am"
                }else{
                    new_start_time = new_start_time + "pm"
                }
            }
            
            
            cell.labelAsDate.text = "\(dateStr) | \(TimeUtils.sharedUtils.chaneTimeFormet("\(new_start_time)") )"
        }
        cell.labelOnSight.isHidden = !isOnSite
        cell.buttonLogo.imageView?.contentMode = .scaleAspectFit
        cell.buttonLogo.setImage(isOnSite ? nil : UIImage.init(named: "clinido_icon"), for: .normal)
        cell.buttonLogo.setTitle(isOnSite ? "Onsite".localize() : nil, for: .normal)
        cell.buttonCheckedIn.isHidden = isOnSite
        cell.constraint_bottom_checkup.constant = (cell.buttonCheckedIn.isHidden) ? 0.0 : 4.0
        cell.buttonCall.isHidden = false
        cell.buttonMessage.isEnabled = !isOnSite
        cell.buttonMessage.isHidden = false
        cell.btn_no_show.isHidden = isOnSite
        cell.cancelTopConstraint.constant = isOnSite ? -60 : 4
        cell.btn_cancel.isHidden = false
        cell.btn_cancel.backgroundColor = (data.status == 3) ? #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.btn_cancel.layer.borderColor = (data.status == 3) ? #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1).cgColor : #colorLiteral(red: 0, green: 0.6509803922, blue: 0.7843137255, alpha: 1).cgColor
        cell.btn_cancel.setTitleColor((data.status == 3) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        cell.btn_cancel.setTitle((data.status == 3) ? localizeValue(key: "Cancelled") : localizeValue(key: "Cancel"), for: .normal)
        cell.btn_no_show.setTitle(localizeValue(key: "No Show"), for: .normal)
        cell.buttonCheckedIn.backgroundColor = (data.status == 4) ? #colorLiteral(red: 0.4941176471, green: 0.8274509804, blue: 0.1294117647, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.buttonCheckedIn.layer.borderColor = (data.status == 4) ? #colorLiteral(red: 0.4941176471, green: 0.8274509804, blue: 0.1294117647, alpha: 1).cgColor : #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1).cgColor
        cell.buttonCheckedIn.setTitleColor((data.status == 4) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        cell.btn_no_show.backgroundColor = (data.noshow == 1) ? #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.btn_no_show.setTitleColor((data.noshow == 1) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        cell.btn_cancel.isUserInteractionEnabled = (data.status != 3)
     //   cell.btn_no_show.isUserInteractionEnabled = (data.noshow != 1)
        cell.btn_cancel.addTarget(self, action: #selector(self.actionOnCancel(_:)), for: .touchUpInside)
        cell.btn_no_show.addTarget(self, action: #selector(self.actionOnNoShow(_:)), for: .touchUpInside)
        cell.buttonCheckedIn.addTarget(self, action: #selector(self.buttonCheckInClicked(_:)), for: .touchUpInside)
        cell.buttonCall.addTarget(self, action: #selector(self.buttonCallClicked(_:)), for: .touchUpInside)
        cell.buttonMessage.addTarget(self, action: #selector(self.buttonMessageClicked(_:)), for: .touchUpInside)
        cell.buttonCheckedIn.setTitle(localizeValue(key: "Check Up"), for: .normal)
        
        //let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let currentStr = formatter.string(from: Date())
        // let currentDate = formatter.date(from: currentStr)
        if (CommonClass.convertStringToDate(date: currentStr) < CommonClass.convertStringToDate(date: (data.appointment_date!))) {
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
        }else if (CommonClass.convertStringToDate(date: currentStr) > CommonClass.convertStringToDate(date: (data.appointment_date!))){
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
            cell.btn_cancel.isEnabled = false
            cell.btn_cancel.setTitleColor(.lightGray, for: .normal)
            if data.status == 1 {
                
                
                let currentDate = currentStr.components(separatedBy: "-")
                let appointmentDate =  data.appointment_date?.components(separatedBy: "-")
                if currentDate[0] == appointmentDate![0] && currentDate[1] == appointmentDate![1]{
                    cell.btn_no_show.isEnabled = true
                }else{
                    cell.btn_no_show.isEnabled = false
                }
                
            }
        }
        
        if data.status == 4 {
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.btn_cancel.isEnabled = false
            cell.btn_cancel.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = true
            cell.buttonCheckedIn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) , for: .normal)
        } else if data.status == 3 {
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
        }
        else if data.status == 1 {
               cell.btn_no_show.isEnabled = true
        }
        if data.noshow == 1 {
            cell.btn_cancel.isEnabled = false
            cell.btn_cancel.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
            cell.btn_no_show.isEnabled = true
        }else if data.noshow == 2{
             cell.btn_cancel.isEnabled = true
            cell.buttonCheckedIn.isEnabled = true
        }
        cell.buttonMessage.isEnabled = !isOnSite || (data.type == 1)
        if isOnSite {
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
        } else {
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
        }
        
        if data.visit_type == 0 || data.visit_type == 3 {
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
            cell.buttonMessage.isEnabled = false
        } else {
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
            cell.buttonMessage.isEnabled = true
        }
        if data.patient_info?.accounttype == 1 {
            if data.patient_info?.fcmStatus != "Y" {
                cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
                cell.buttonMessage.isEnabled = false
            }
        } else if data.patient_info?.accounttype == 4 || data.patient_info?.accounttype == 5 {
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
            cell.buttonMessage.isEnabled = false
        } else {
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
            cell.buttonMessage.isEnabled = true
        }
        return cell
    }
}
extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
