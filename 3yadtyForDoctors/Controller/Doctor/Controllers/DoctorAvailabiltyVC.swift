//
//  DoctorAvailabiltyVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 14/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DoctorAvailabiltyVC: BaseViewControler {
    
    //MARK:- Outlets
    
    @IBOutlet weak var btnSave: RoundButton!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var workingHourLabel: UILabel!
    @IBOutlet weak var waitingTimeLabel: UILabel!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var examinationTypeLabel: UILabel!
    @IBOutlet weak var tf_waiting_time: CustomTextField!
    @IBOutlet weak var lbl_patient: UILabel!
    @IBOutlet weak var tf_patient: CustomTextField!
    @IBOutlet weak var tbl_days: UITableView!
    @IBOutlet weak var btn_fifo: UIButton!
    @IBOutlet weak var btn_appointment: UIButton!
    @IBOutlet weak var constraint_header_height: NSLayoutConstraint!
    
    //MARK:- Variables
    
    var dayArray = [String]()
    var daysArr: [Days] = [Days.init(name: "Sunday", isAvailable: true,startTime: "12:00 AM",endTime: "09:00 PM"),Days.init(name: "Monday", isAvailable: true,startTime: "12:00 AM",endTime: "09:00 PM"),Days.init(name: "Tuesday", isAvailable: true,startTime: "12:00 AM",endTime: "09:00 PM"),Days.init(name: "Wednesday", isAvailable: true,startTime: "12:00 AM",endTime: "09:00 PM"),Days.init(name: "Thursday", isAvailable: true,startTime: "12:00 AM",endTime: "09:00 PM"),Days.init(name: "Friday", isAvailable: true,startTime: "12:00 AM",endTime: "09:00 PM"),Days.init(name: "Saturday", isAvailable: true,startTime: "12:00 AM",endTime: "09:00 PM")]

    var tempPatient:CustomTextField!
    var parameters:[String:Any] = [:]
    var clinic: Clinic?
    var selectedClinic:Int?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dayArray = [localizeValue(key: "Sunday"),localizeValue(key: "Monday"),localizeValue(key: "Tuesday"),localizeValue(key: "Wednesday"),localizeValue(key: "Thursday"),localizeValue(key: "Friday"),localizeValue(key: "Saturday")]
        headerTitleLabel.text = localizeValue(key: "Working hours")
        btnSave.setTitle(localizeValue(key: "Save"), for: .normal)
        btn_fifo.setTitle(localizeValue(key: "First In First Out"), for: .normal)
        waitingTimeLabel.text = localizeValue(key: "Clinic waiting time")
         workingHourLabel.text = localizeValue(key: "Clinic working hours")
        btn_appointment.setTitle(localizeValue(key: "On Appointments"), for: .normal)
        examinationTypeLabel.text = localizeValue(key: "Examination type")
        tf_patient.placeholder = localizeValue(key: "Enter no")
        if #available(iOS 10.0, *) {
            tf_patient.keyboardType = .asciiCapableNumberPad
        } else {
            // Fallback on earlier versions
        }
        tf_waiting_time.placeholder = localizeValue(key: "Enter waiting time")
        NotificationCenter.default.addObserver(self, selector: #selector(changeTime(notification:)), name: NSNotification.Name(rawValue: "changeTime"), object: nil)
        self.viewDidLoadSetupUI()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            tf_patient.textAlignment = .right
            tf_waiting_time.textAlignment = .right
            lbl_patient.textAlignment = .right
            waitingTimeLabel.textAlignment = .right
        }
    }
    
    //MARK:- Functions
    private func viewDidLoadSetupUI(){
        
        self.btn_back.automaticallyRotateOnLanguageConversion()
        self.constraint_header_height.constant += (UIApplication.shared.statusBarFrame.size.height)
        self.tbl_days.tableFooterView = UIView.init(frame: .zero)
        self.updateCilnicDetail()
        self.textFieldsSetupUI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.buttonsSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    @objc func changeTime(notification: NSNotification){
        let dict = notification.userInfo as! Dictionary<String, Any>
        print(dict["index"]!)
        print(dict["time"]!)
        let index = dict["index"] as! Int
        let checkBool = dict["isfromValue"] as! Bool
        var fetchDay = self.daysArr[index]
        if checkBool {
            fetchDay.startTime = dict["time"] as! String
            print(fetchDay)
        }else{
            fetchDay.endTime = dict["time"] as! String
            print(fetchDay)
        }
        self.daysArr.remove(at: index)
        self.daysArr.insert(fetchDay, at: index)
    }
    private func buttonsSetup(){
        
        DispatchQueue.main.async {
            
            self.btn_fifo.imageView?.frame.origin.x = self.btn_fifo.frame.size.width - 35.0
            self.btn_appointment.imageView?.frame.origin.x = self.btn_fifo.frame.size.width - 35.0
        }
    }
    
    private func textFieldsSetupUI(){
        
        var data: [String] = ["10 Mins", "15 Mins", "20 Mins", "30 Mins", "45 Mins", "60 Mins"]
        self.tf_waiting_time.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            //Write code for picker view here
            self.view.endEditing(true)
            ActionSheetStringPicker.show(withTitle: self.localizeValue(key: "Clinic waiting time"), rows: data, initialSelection: 0, doneBlock: {
                picker, value, index in
                if data.count > 0 {
                    self.tf_waiting_time.text = data[value]
                }
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
    }
    
    private func check(SelectedButton button: UIButton){
        
        self.btn_fifo.setImage(UIImage.init(named: (button.tag != 0) ? "checkboxDisactive" : "checkboxActive"), for: .normal)
        self.btn_appointment.setImage(UIImage.init(named: (button.tag != 1) ? "checkboxDisactive" : "checkboxActive"), for: .normal)
    }
    
    private func updateCilnicDetail(){
        
        if let selectedClinic = self.selectedClinic{
            
            guard let userObj = AppInstance.shared.user else {return}
            guard let clinics = userObj.clinic else {return}
            let clinic = clinics[selectedClinic]
            let isSelectedTypeOnAppointment = ((clinic.clinic_schedule ?? "") == "2")
            self.tempPatient = self.tf_patient
            self.updatePatient(Using: (isSelectedTypeOnAppointment ? self.btn_appointment : self.btn_fifo))
            self.check(SelectedButton: (isSelectedTypeOnAppointment ? self.btn_appointment : self.btn_fifo))
            self.tf_patient.text = (isSelectedTypeOnAppointment ? "\((clinic.clinic_schedule_count ?? "")) Mins" : (clinic.clinic_schedule_count ?? ""))
            self.tf_waiting_time.text = (clinic.waiting_time ?? "").contains(" Mins") ? (clinic.waiting_time ?? "") : "\((clinic.waiting_time ?? "")) Mins"
            if let availability = clinic.availability{
                
                for avail in availability{
                    
                    for (index,day) in self.daysArr.enumerated() where (day.name == avail.day){
                        
                        self.daysArr[index] = Days.init(name: avail.day ?? "", isAvailable: false, startTime: avail.start_time ?? "", endTime: avail.end_time ?? "")
                        let indexPath = IndexPath.init(row: index, section: 0)
                        self.tbl_days.reloadRows(at: [indexPath], with: .fade)
                    }
                }
            }
        }else{
            
            self.tempPatient = self.tf_patient
            self.updatePatient(Using: self.btn_fifo)
            self.check(SelectedButton: self.btn_fifo)
        }
    }
    
    private func getParameters() -> Any{
        
        var paramsArr:[[String:Any]] = []
        for (index,day) in self.daysArr.enumerated() where !day.isAvailable{
            
//            let indexPath = IndexPath.init(row: index, section: 0)
//            guard let cell = self.tbl_days.cellForRow(at: indexPath) as? DoctorAvailabiltyTVC else { return ""}
            var params:[String:Any] = [:]
            params["day"] = self.daysArr[index].name
            params["dr_id"] = AppInstance.shared.user!.id!
            params["end_time"] = self.daysArr[index].endTime
            params["id"] = ""
            params["start_time"] = self.daysArr[index].startTime
            paramsArr.append(params)
        }
        
        return paramsArr
    }
    
    //Add availability parameters
    private func getEditparameters() -> Any{
        
        guard let userObj = AppInstance.shared.user else {return []}
        guard let clinics = userObj.clinic else {return [] }
        let clinic = clinics[self.selectedClinic!]
        var paramsArr:[[String:Any]] = []
        for (index,day) in self.daysArr.enumerated() where !day.isAvailable{
            
//            let indexPath = IndexPath.init(row: index, section: 0)
//            guard let cell = self.tbl_days.cellForRow(at: indexPath) as? DoctorAvailabiltyTVC else { return ""}
            let dr_id = (clinic.availability?.first?.dr_id ?? 0)
            let clinic_id = (clinic.availability?.first?.clinic_id ?? 0)
            let dayObj = clinic.availability?.filter({$0.day == day.name})
            var params:[String:Any] = [:]
            params["day"] = self.daysArr[index].name//cell.dayObj.name
            params["dr_id"] = "\(dr_id)"
            params["clinic_id"] = "\(clinic_id)"
            params["id"] = dayObj?.first?.id ?? ""
            params["end_time"] = self.daysArr[index].endTime
            params["start_time"] = self.daysArr[index].startTime
            if let start = dayObj?.first?.start_time_milis {
                params["start_time_milis"] = start
                params["end_time_milis"] = dayObj?.first!.end_time_milis
            } else {
                params["start_time_milis"] = ""
                params["end_time_milis"] = ""
            }
            
            paramsArr.append(params)
        }
        
        return paramsArr
    }
    
    private func convertJSON(parameters:[String:Any]) -> String{
        
        do {
            
            let data = try JSONSerialization.data(withJSONObject: [parameters], options: .prettyPrinted)
            guard let jsonString = String.init(data: data, encoding: String.Encoding.utf8) else { return "" }
            return jsonString
        }catch {
            
            print(error.localizedDescription)
            return ""
        }
    }
    
    private func clinicParam() -> [Any]{
        
        if let clinicImagesArr = self.parameters["clinic_image"] as? [Any]{
            
            var tempImgArr: [NSDictionary] = []
            for clinicImage in clinicImagesArr{
                
                if let tempClinic = clinicImage as? ClinicImages{
                    
                    tempImgArr.append(tempClinic.dictionaryRepresentation())
                }else{
                    
                    tempImgArr.append(clinicImage as! NSDictionary)
                }
            }
            self.parameters["clinic_image"] = tempImgArr
        }
        return self.parameters["clinic_image"] as! [Any]
    }
    
    
    private func updatePatient(Using selectedBtn: UIButton){
        
        self.tf_patient.text = ""
        self.tf_patient.tag = (selectedBtn.tag == 0) ? 1 : 0
        if (selectedBtn.tag == 0){
            
            self.lbl_patient.text = localizeValue(key: "Number of patients allowed per day")
            for patientView in self.tf_patient.subviews where (patientView.classForCoder == UIButton.classForCoder() || patientView.classForCoder == UIView.classForCoder() || patientView.classForCoder == ArrowButton.classForCoder()){
                
                if patientView is UIButton{
                    
                    patientView.subviews.last?.isHidden = true
                }
                patientView.removeFromSuperview()
            }
            self.tf_patient = self.tempPatient
            self.tf_patient.setNeedsLayout()
        }else{
            
            self.lbl_patient.text = localizeValue(key: "Interval between appointments")
            var data: [String] = ["10 Mins", "15 Mins", "20 Mins", "30 Mins", "45 Mins", "60 Mins", "75 Mins", "100 Mins"]
            self.tf_patient.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
                //Write code for picker view here
                
                self.view.endEditing(true)
                ActionSheetStringPicker.show(withTitle: self.localizeValue(key: "Interval between appointments"), rows: data, initialSelection: 0, doneBlock: {
                    
                    picker, value, index in
                    if data.count > 0 {
                        self.tf_patient.text = data[value]
                    }
                    return
                }, cancel: { ActionStringCancelBlock in return }, origin: button)
            }
        }
    }
    
    private func addClinicInformation(){
        
        let validation = self.isFieldsValid()
        if !validation.isEmpty{
            
            UIAlertController.showToast(title: validation, message: nil, target: self, handler: nil)
            return
        }
        
        guard let userObj = AppInstance.shared.user else {return}
        
        //Existing Clinic
        if let selectedClinic = self.selectedClinic {
            
            guard let clinics = userObj.clinic else {return}
            
            let clinic = clinics[selectedClinic]
            self.parameters["clinic_schedule"] = (self.tf_patient.tag == 1) ? 1 : 2
            self.parameters["id"] = clinic.id ?? 0
            self.parameters["clinic_schedule_count"] = (self.tf_patient.text! as NSString).replacingOccurrences(of: " Mins", with: "")
            self.parameters["user_id"] = userObj.id ?? 0
            self.parameters["waiting_time"] = (self.tf_waiting_time.text! as NSString).replacingOccurrences(of: " Mins", with: "")
            //Add availability parameters
            self.parameters["availability"] = self.getEditparameters()
            self.parameters["clinic_image"] = self.clinicParam()

            //Convert Parameters to JSON
            let params: [String:Any] = ["user_id":userObj.id ?? "","clinic":self.convertJSON(parameters: self.parameters)]
            
            let service = BusinessService()
            
            service.updateClinicList(with: params as [String:AnyObject], target: self) { (response) in
                
                if let response = response as? [String:AnyObject]{
                    
                    if let data = response["data"] as? [String:AnyObject]{
                        
                        AppInstance.shared.doctorSignUp = data
                        let userData = UserData.parse(data as NSDictionary)
                        let userDefaultObj = UserDefaults.standard
                        self.saveDataToFirebaseRefrence(userData: userData)
                        AppInstance.shared.user = userData
                        userDefaultObj.removeObject(forKey: "isLogIn")
                        userDefaultObj.removeObject(forKey: "userdata")
                        CustomObjects.archive(Object: data as AnyObject, WithKey: "isLogIn")
                        if let signUpDict = AppInstance.shared.doctorSignUp{
                            
                            CustomObjects.archive(Object: signUpDict as AnyObject, WithKey: "userdata")
                        }
                        if let clinics = data["clinic"] as? [[String:AnyObject]]{
                            
                            AppInstance.shared.clinicTimming = clinics
                        }
                        
                        for vc in self.navigationController!.viewControllers  where vc.classForCoder == WorkInfoViewController.classForCoder(){
                            
                            self.navigationController?.popToViewController(vc, animated: true)
                        }
                    }
                }
            }
        }else{
            
            self.parameters["user_id"] = userObj.id ?? 0
            self.parameters["clinic_schedule"] = (self.tf_patient.tag == 1) ? 1 : 2
            self.parameters["clinic_schedule_count"] = (self.tf_patient.text! as NSString).replacingOccurrences(of: " Mins", with: "")
            self.parameters["waiting_time"] = (self.tf_waiting_time.text! as NSString).replacingOccurrences(of: " Mins", with: "")
            //Add availability parameters
            self.parameters["availability"] = self.getParameters()
            self.parameters["clinic_image"] = self.clinicParam()
            
            //Convert Parameters to JSON
            let params: [String:Any] = ["user_id":userObj.id ?? "","clinic":self.convertJSON(parameters: self.parameters)]
            
            let service = BusinessService()
            
            service.addClinic(with: Config.addClinic,And: params, image: [], target: self) { (response) in
                
                if let response = response as? [String:AnyObject]{
                    
                    if let data = response["data"] as? [String:AnyObject]{
                        
                        AppInstance.shared.doctorSignUp = data
                        let userData = UserData.parse(data as NSDictionary)
                        let userDefaultObj = UserDefaults.standard
                        self.saveDataToFirebaseRefrence(userData: userData)
                        AppInstance.shared.user = userData
                        userDefaultObj.removeObject(forKey: "isLogIn")
                        userDefaultObj.removeObject(forKey: "userdata")
                        CustomObjects.archive(Object: data as AnyObject, WithKey: "isLogIn")
                        if let signUpDict = AppInstance.shared.doctorSignUp{
                            
                            CustomObjects.archive(Object: signUpDict as AnyObject, WithKey: "userdata")
                        }
                        if let clinics = data["clinic"] as? [[String:AnyObject]]{
                            
                            AppInstance.shared.clinicTimming = clinics
                        }
                        for vc in self.navigationController!.viewControllers  where vc.classForCoder == WorkInfoViewController.classForCoder(){
                            
                            self.navigationController?.popToViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    private func isFieldsValid() -> String{
        
        if self.tf_patient.text!.isEmpty{
            
            return NSLocalizedString(("Number of patients allowed per day" == self.lbl_patient.text!) ? localizeValue(key: "Number of patients allowed per day") : localizeValue(key:
                "Interval between appointments"), comment: "")
        }else if self.tf_patient.text == "0"{
             return localizeValue(key: "Number of patients allowed per day")
        }
        else if (self.daysArr.filter({!$0.isAvailable})).count == 0{
            
            return localizeValue(key: "Set Doctor / Clinic address and Working hours")
        }
        else if self.tf_waiting_time.text == "" {
            return localizeValue(key: "Clinic waiting time")
        }
        return ""
    }
    
    private func saveDataToFirebaseRefrence(userData: UserData?) {
        
        let token = UserDefaults.standard.value(forKey: "token")
        if CommonClass.isLiveServer() {
            Constants.refs.databaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
        } else {
            Constants.refs.testDatabaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
        }
    }
    
    @objc private func actionOnSwitch(_ sender: UISwitch){
        
        let indexPath = IndexPath.init(row: sender.tag, section: 0)
        self.daysArr[indexPath.row].isAvailable = !self.daysArr[indexPath.row].isAvailable
        self.tbl_days.reloadRows(at: [indexPath], with: .fade)
        self.tbl_days.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
    }
    
    //MARK:- Action Outlets
    @IBAction private func actionOnBack(_ sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func actionOnFIFO(_ sender: UIButton) {
        
        self.buttonsSetup()
        if sender.tag != self.tf_patient.tag{
            
            return
        }
        
        self.updatePatient(Using: sender)
        self.check(SelectedButton: sender)
    }
    
    @IBAction private func actionOnAppointment(_ sender: UIButton) {
        
        self.buttonsSetup()
        if sender.tag != self.tf_patient.tag{
            
            return
        }
        self.updatePatient(Using: sender)
        self.check(SelectedButton: sender)
    }
    
    @IBAction func actionOnNext(_ sender: Any) {
        
        self.addClinicInformation()
    }
}

extension DoctorAvailabiltyVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.daysArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorAvailabiltyTVC", for: indexPath) as! DoctorAvailabiltyTVC
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.tf_time_from.textAlignment = .right
            cell.tf_time_to.textAlignment = .right
        }
        cell.lbl_from.text = localizeValue(key: "From")
        cell.lbl_to.text = localizeValue(key: "To")
        
        let dayObj = self.daysArr[indexPath.row]
        cell.dayObj = dayObj
        cell.lbl_day.text = dayArray[indexPath.row]
        cell.tf_time_from.text = dayObj.startTime
        cell.tf_time_to.text = dayObj.endTime
        cell.view_time.isHidden = dayObj.isAvailable
        cell.switch_day.isOn = !dayObj.isAvailable
        cell.switch_day.tag = indexPath.row
        cell.switch_day.addTarget(self, action: #selector(self.actionOnSwitch(_:)), for: .touchUpInside)
        return cell
    }
}

extension DoctorAvailabiltyVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 67
    }
}
