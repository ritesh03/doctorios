//
//  ForgotPasswordViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 13/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class ForgotPasswordCell: UITableViewCell {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var buttonShow: UIButton!
}

class ForgotPasswordViewController: BaseViewControler {
    
    var showNewPwd = false
    var showConfirmPwd = false
    var oldPassword = false
    var cells = 2
    var newPassword: String?
    var confirmPassword: String?
    var oldPasswordText: String?
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonSubmit: RoundButton!
    @IBOutlet weak var tableChangePassword: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform =  CGAffineTransform(scaleX: -1.0, y: 1.0)
           titleLabel.textAlignment = .right
            subTitleLabel.textAlignment = .right
        }
        
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Change Password", comment: "")
        subTitleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter new password", comment: "")
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        
        if cells == 2 {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
    
        }else{
            print("buttonBackClicked")
            self.navigationController?.popViewController(animated: true)
        }
        
      
    }
    
    @IBAction func buttonSubmit(_ sender: Any) {
    
        guard let newPassword = newPassword,!newPassword.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter password"), target: self, handler: nil)
            return
        }
        if newPassword.count < 6{
            UIAlertController.show(title: nil, message: localizeValue(key: "Password must contain 6 digits"), target: self, handler: nil)
            return
        }
        
        if newPassword != confirmPassword{
            UIAlertController.show(title: nil, message: localizeValue(key: "Password doesn't match"), target: self, handler: nil)
            return
        }
        var type: String?
        if AppInstance.shared.userType == 0{
            type = "2"
        }else{
            type = "3"
        }
        if cells == 3 {
            let service = BusinessService()
            
            service.resetPassword(with: String(describing: AppInstance.shared.user!.id!), password: self.newPassword!, type: type!, old_password: self.oldPasswordText!, target: self) { (response) in
                if response == true {
                    let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ForgotSucessPopUpViewController") as! ForgotSucessPopUpViewController
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.cells = 3
                    vc.callback = {
                        _ = self.navigationController?.popViewController(animated: true)

                    }
                    vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                    self.present(vc, animated: false, completion: nil)
                }
            }
        }else{
            let service = BusinessService()
            service.changePassword(with: AppInstance.shared.mobile!, password: newPassword, type: type!, target: self) { (response) in
                if response == true {
                    let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ForgotSucessPopUpViewController") as! ForgotSucessPopUpViewController
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.delegate = self
                    vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                    self.present(vc, animated: false, completion: nil)
                }
            }
        }
        
      
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if cells == 3 {
            
            if textField.tag == 1 {
                oldPasswordText = textField.text
            }
            else if textField.tag == 2{
                newPassword = textField.text
            }else if textField.tag == 3{
                confirmPassword = textField.text
            }
            
            if newPassword?.isEmpty != true && confirmPassword?.isEmpty != true {
                buttonSubmit.isEnabled = true
                buttonSubmit.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
            }else{
                buttonSubmit.isEnabled = false
                buttonSubmit.backgroundColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
            }
        }
        else{
            if textField.tag == 1{
                newPassword = textField.text
            }else if textField.tag == 2{
                confirmPassword = textField.text
            }
            
            if newPassword?.isEmpty != true && confirmPassword?.isEmpty != true && newPassword == confirmPassword{
                buttonSubmit.isEnabled = true
                buttonSubmit.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
            }else{
                buttonSubmit.isEnabled = false
                buttonSubmit.backgroundColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
            }
        }
        
      
    }
    
    @objc private func buttonNewPasswordClicked() {
        print("NewPassword")
        if showNewPwd == false {
            showNewPwd = true
        }else{
            showNewPwd = false
        }
       tableChangePassword.reloadData()
    }
    @objc private func buttonConfirmPasswordClicked() {
        print("ConfirmPassword")
        if showConfirmPwd == false {
            showConfirmPwd = true
        }else{
            showConfirmPwd = false
        }
        tableChangePassword.reloadData()
    }
    @objc private func buttonOldPasswordClicked() {
        print("ConfirmPassword")
        if oldPassword == false {
            oldPassword = true
        }else{
            oldPassword = false
        }
        tableChangePassword.reloadData()
    }
}

extension ForgotPasswordViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        
        if cells == 3 {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordCell", for: indexPath) as! ForgotPasswordCell
                cell.textField.placeholder = localizeValue(key: "Old Password")
                cell.textField.keyboardType = .default

                 cell.textField.isSecureTextEntry = false
                cell.textField.tag = 1
                cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                cell.buttonShow.isHidden = true
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.textField.textAlignment = .right
                
                    
                }
                else{
                       cell.textField.textAlignment = .left
                }
                cell.buttonShow.addTarget(self, action: #selector(buttonOldPasswordClicked), for: .primaryActionTriggered)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordCell", for: indexPath) as! ForgotPasswordCell
                cell.textField.placeholder = localizeValue(key: "New Password")
                cell.textField.keyboardType = .default
                if showNewPwd == true{
                    cell.buttonShow.setTitle(localizeValue(key: "HIDE"), for: .normal)
                    cell.textField.isSecureTextEntry = false
                }else{
                    cell.buttonShow.setTitle(localizeValue(key: "SHOW"), for: .normal)
                    cell.textField.isSecureTextEntry = true
                }
                cell.textField.tag = 2
                cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                cell.buttonShow.addTarget(self, action: #selector(buttonNewPasswordClicked), for: .primaryActionTriggered)
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordCell", for: indexPath) as! ForgotPasswordCell
                cell.textField.placeholder = localizeValue(key: "Confirm password")
                cell.textField.keyboardType = .default
                if showConfirmPwd == true{
                    cell.buttonShow.setTitle(localizeValue(key: "HIDE"), for: .normal)
                    cell.textField.isSecureTextEntry = false
                }else{
                    cell.buttonShow.setTitle(localizeValue(key: "SHOW"), for: .normal)
                    cell.textField.isSecureTextEntry = true
                }
                cell.textField.tag = 3
                cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                cell.buttonShow.addTarget(self, action: #selector(buttonConfirmPasswordClicked), for: .primaryActionTriggered)
                return cell
            }
        }else{
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordCell", for: indexPath) as! ForgotPasswordCell
                cell.textField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password", comment: "")
                cell.textField.keyboardType = .default
                if showNewPwd == true{
                    cell.buttonShow.setTitle(localizeValue(key: "HIDE"), for: .normal)
                    cell.textField.isSecureTextEntry = false
                }else{
                    cell.buttonShow.setTitle(localizeValue(key: "SHOW"), for: .normal)
                    cell.textField.isSecureTextEntry = true
                }
                cell.textField.tag = 1
                cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                cell.buttonShow.addTarget(self, action: #selector(buttonNewPasswordClicked), for: .primaryActionTriggered)
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordCell", for: indexPath) as! ForgotPasswordCell
                cell.textField.placeholder = localizeValue(key: "Confirm password")
                cell.textField.keyboardType = .default
                if showConfirmPwd == true{
                    cell.buttonShow.setTitle(localizeValue(key: "HIDE"), for: .normal)
                    cell.textField.isSecureTextEntry = false
                }else{
                    cell.buttonShow.setTitle(localizeValue(key: "SHOW"), for: .normal)
                    cell.textField.isSecureTextEntry = true
                }
                cell.textField.tag = 2
                cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                cell.buttonShow.addTarget(self, action: #selector(buttonConfirmPasswordClicked), for: .primaryActionTriggered)
                return cell
            }
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
}

extension ForgotPasswordViewController: forgotSuccessDelegate{
    func forgotSuccess() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


