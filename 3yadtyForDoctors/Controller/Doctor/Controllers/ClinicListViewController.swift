//
//  ClinicListViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 02/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol selectedClinicDelegate{
    func selectedClinic(name: String, id: Int, ClinicData: ClinicData, clinicType: Int)
}

class clinicCell: UITableViewCell {
    @IBOutlet weak var labelClinicName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
}

class ClinicListViewController: BaseViewControler, UISearchBarDelegate {
    
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var searchBarDr: UISearchBar!
    @IBOutlet weak var tableClinicList: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headerTitleLabel: UILabel!
    var delegate: selectedClinicDelegate?
    var listType: String?
    var arrClinicList = [ClinicData]()
    var arrFilterList = [ClinicData]()
    var isSearched = Bool()
    var drID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        headerTitleLabel.text = localizeValue(key: "Select clinic")
        searchBarDr.placeholder = localizeValue(key: "Search name..")
        errorTitleLabel.text = localizeValue(key: "Empty List")
        errorDescriptionLabel.text = localizeValue(key: "List is empty")
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
             backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        searchBarDr.delegate = self
        searchBarDr.barTintColor = .clear
        let image = UIImage()
        searchBarDr.setBackgroundImage(image, for: .any, barMetrics: .default)
        searchBarDr.scopeBarBackgroundImage = image
        //searchBarPatient.showsCancelButton = true
        self.errorView.isHidden = true
        getClinicList()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    //get drList
    func getClinicList() {
        var id = ""
        if AppInstance.shared.userType == 0{
            id = String(describing: AppInstance.shared.user!.id!)
        } else {
            id = String(describing: AppInstance.shared.user!.dr_id!)
        }
            let service = BusinessService()
            service.getClinicList(with: id, target: self) { (response) in
                if response == nil{
                    self.errorView.isHidden = false
                }
                if let clinic = response {
                    print(clinic)
                    self.arrClinicList = clinic.data!
                    self.arrFilterList = clinic.data!
                    self.errorView.isHidden = true
                }
                self.tableClinicList.reloadData()
            }
    }
    
    // searchbar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.isSearched = true
        arrFilterList = arrClinicList.filter() { ($0.clinic_name?.contains(searchText))! || ($0.clinic_address?.contains(searchText))!}
        if searchText == ""{
            self.isSearched = false
            arrFilterList = arrClinicList
        }
        if arrFilterList.count == 0 {
            self.errorView.isHidden = false
        }else{
            self.errorView.isHidden = true
        }
        self.tableClinicList.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if searchBar.text == nil{
            arrFilterList = arrClinicList
        }

        self.errorView.isHidden = (arrFilterList.count < 1)
        self.tableClinicList.reloadData()
    }
    
}

extension ClinicListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrFilterList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "clinicCell", for: indexPath) as! clinicCell
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.labelClinicName.text = self.arrFilterList[indexPath.row].clinic_name_ar
            cell.labelAddress.text = self.arrFilterList[indexPath.row].clinic_address_ar
            cell.labelClinicName.textAlignment = .right
            cell.labelAddress.textAlignment = .right
        }else{
            cell.labelClinicName.text = self.arrFilterList[indexPath.row].clinic_name
            cell.labelAddress.text = self.arrFilterList[indexPath.row].clinic_address
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedClinic(name: self.arrFilterList[indexPath.row].clinic_name!, id: self.arrFilterList[indexPath.row].id!, ClinicData: self.arrFilterList[indexPath.row], clinicType: self.arrFilterList[indexPath.row].clinic_schedule!)
          self.navigationController?.popViewController(animated: true)
        }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 79
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
}
