//
//  PrescriptionViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 31/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class PrescriptionViewController: BaseViewControler,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label2: UILabel!
    let myPickerController = UIImagePickerController()
    
    var selectedPrescription: PrescriptionData?
    private var medicineArr = [[String: Any]]()
    private var prescriptionObj = [String: Any]()
    var imagesArray = [File]()

    var patient_case:String?
//    var patient_data : DataAppointmentList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitleLabel.text = localizeValue(key: "Prescription")
        label1.text = localizeValue(key: "Create prescription with header and footer")
        label2.text = localizeValue(key: "Create prescription without header and footer")
        label3.text = localizeValue(key: "Scan prescription")
        myPickerController.delegate = self
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            headerLabel.text = "اختار شكل الروشته"
           backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
     
        
    }
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonWithHeaderClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientPrescriptionViewController") as! PatientPrescriptionViewController
//        vc.patient_data = patient_data
        vc.prescriptionType = 1
        vc.patient_case = patient_case
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func buttonWithoutHeaderClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientPrescriptionViewController") as! PatientPrescriptionViewController
//        vc.patient_data = patient_data
        vc.prescriptionType = 2
        vc.patient_case = patient_case
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonScanClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "Perform Action", message: nil, preferredStyle: .actionSheet)
        
        let library = UIAlertAction(title: "Choose From Library", style: .default, handler: { (action) -> Void in
            self.myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.myPickerController.allowsEditing = false
            self.present(self.myPickerController, animated: true, completion: nil)
        })
        
        let camera = UIAlertAction(title: "Take From Camera", style: .default, handler: { (action) -> Void in
            
            if !UIImagePickerController.isSourceTypeAvailable(.camera){
                
                let alertController = UIAlertController.init(title: "Alert", message: "Device has no camera!", preferredStyle: .alert)
                
                let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in
                })
                
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            else {
                self.self.myPickerController.sourceType = .camera
                self.myPickerController.allowsEditing = false
                self.present(self.myPickerController, animated: true, completion: nil)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(library)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        
        self.navigationController?.present(alertController, animated: true){}
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
 {

        let imageName: String?
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {

            imageName = "image.jpg"
        let file = File(name: imageName!, image: chosenImage, type: 1)
        if (imagesArray.count) > 0 {
            imagesArray.removeLast()
        }
        imagesArray.append(file)
        }
    addPrescription()
        // do something interesting here!
        dismiss(animated: true)
    }
    
    
    func addPrescription() {
        
        
        if selectedPrescription?.medicine != nil{
            for arr in selectedPrescription!.medicine!{
                self.medicineArr.append(arr.dictionaryRepresentation())
            }
            self.prescriptionObj["medicine"] = self.medicineArr
            self.prescriptionObj["prescription_type"] = selectedPrescription!.prescription_type!
        }
        
        let finalJson = self.json(from:[self.prescriptionObj])
        print(finalJson!)
        let info = AppInstance.shared.selectedDr
        let service = BusinessService()
        service.addPrescription(with: "", patient_id: String(info!.patient_id!), doctor_id: String(info!.doctor_info!.id!), appointment_id: String(info!.id!), patient_case: "", image: imagesArray, target: self) { (response) in
            if response == true {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: PatientHistoryDetailViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }
    }

}
