//
//  InformationThreeViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 12/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class InformationThreeViewController: UIViewController {
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "welcome to Doctor account", comment: "")
        
        
        
        pointsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bulletsText3", comment: "")
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func buttonNextClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonSkipClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
