//
//  AppointmentListViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 05/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import UIScrollView_InfiniteScroll

class AppointmentListViewController: BaseViewControler, IndicatorInfoProvider {
    
    //    MARK:- IBOutlets
    
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet private weak var tableAppointmentList: UITableView!
    @IBOutlet private weak var errorView: UIView!
    
    //    MARK:- Variables
    private var refreshControl = UIRefreshControl()
    private var contentOffSet = CGFloat()
    private var arrAcceptedAppointment: Appointment?
    private var statisticsData: Appointment_data?
    private var appointmentListData: DataAppointment?
    private var appointments = [DataAppointmentList]()
    private var isRefreshed = false
    private var isDataLoading = Bool()
    private var pageNum = 1
    private var totalPages = 0
    private var isCheckInTapped = false
    
    //    MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorView.isHidden = true
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        errorTitleLabel.text = localizeValue(key: "Reservation list is empty")
        errorDescriptionLabel.text = localizeValue(key: "You don't have any reservations")
        
        // Configure Refresh Control
        if #available(iOS 10.0, *) {
            self.tableAppointmentList.refreshControl = refreshControl
        } else {
            self.tableAppointmentList.addSubview(self.refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        NotificationCenter.default.removeObserver(self)
        if AppInstance.shared.userType == 0 {
           // self.getDoctorAppointmentList(isAnimated: true)
          //  NotificationCenter.default.addObserver(self,selector:#selector(self.AppointmentSuccess),
                                                  // name:NSNotification.Name(rawValue: "AppointmentSuccess"),object: nil)
        } else {
         //   self.getAssistantAcceptedAppointmentList()
           // NotificationCenter.default.addObserver(self,selector:#selector(self.AssistantAppointmentSuccess),
                                              //     name:NSNotification.Name(rawValue: "AssistantAppointmentSuccess"),object: nil)
        }
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(refreshList), name: NSNotification.Name("loadAppointmentList"), object: nil)

    }
    @objc func refreshList()  {
        isDataLoading = true
        isRefreshed = true
        
        
        self.pageNum = 1
        self.checkUserType()
    }
    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        isDataLoading = true
        isRefreshed = true
        self.refreshControl.endRefreshing()
      
        self.pageNum = 1
        self.checkUserType()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isCheckInTapped {
                self.appointments.removeAll()
            self.pageNum = 1
            self.checkUserType()
        }else{
            self.refreshData(true)
        }
        
       
    }
    
    //    MARK:- ObjC Methods
    
    @objc private func buttonCallClicked(_ sender: UIButton) {
       
        let mobile = "\(String(describing: CommonClass.checkAndEditCountryCodeFor(countryCode: self.appointments[sender.tag].patient_info!.countrycode!)))\(String(describing: self.appointments[sender.tag].patient_info!.mobile!))"
         print(mobile)
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @objc private func buttonMessageClicked(_ sender: UIButton) {
        let data = self.appointments[sender.tag]
        let user = ["email": data.patient_info?.email ?? "","fcmUserId": String(describing: data.patient_info!.id!) ,"image": data.patient_info?.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data.patient_info?.firstname ?? "") \(data.patient_info?.lastname ?? "")"] as [String : Any]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.selectedUser = Person.init(dictionary: user as NSDictionary)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func actionOnNoShow(_ sender: UIButton){
        
        let object = self.appointments[sender.tag]
        let service = BusinessService()
        
        if ((object.status == 4) || (object.status == 3)) { return }
        if object.noshow == 1 {
            service.noShowAppointments(with: "\(AppInstance.shared.user?.id ?? 0)", appointment_id: "\(object.id ?? 0)", target: self) { (response) in
                self.appointments.removeAll()
                self.pageNum = 1
                self.checkUserType()
            }
            return
        }
        let alert = UIAlertController.init(title: localizeValue(key: "Do you want to no show appointment of this patient?"), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: localizeValue(key: "No"), style: .destructive, handler: nil))
        alert.addAction(UIAlertAction.init(title: localizeValue(key: "Yes"), style: .default, handler: { (_) in
            service.noShowAppointments(with: "\(AppInstance.shared.user?.id ?? 0)", appointment_id: "\(object.id ?? 0)", target: self) { (response) in
                if response == true {
                    self.appointments.removeAll()
                    self.pageNum = 1
                    self.checkUserType()
                }
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func actionOnCancel(_ sender: UIButton){
        
        let object = self.appointments[sender.tag]
        
        if (object.status == 4 || (object.noshow == 1)){ return }
        let alert = UIAlertController.init(title: localizeValue(key: "Do you want to cancel appointment of this patient?"), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: localizeValue(key: "No"), style: .destructive, handler: nil))
        alert.addAction(UIAlertAction.init(title: localizeValue(key: "Yes"), style: .default, handler: { (_) in
            
            let service = BusinessService()
            service.cancelAppointments(with: "\(AppInstance.shared.user?.id ?? 0)", appointment_id: "\(object.id ?? 0)", target: self) { (response) in
                if response == true {
                    self.appointments.removeAll()
                    self.pageNum = 1
                    self.checkUserType()
                }
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func buttonCheckInClicked(_ sender: UIButton) {
        let object = self.appointments[sender.tag]
        
        if ((object.status == 3) || (object.noshow == 1)){ return }
        if object.status == 4 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
            vc.isMessageButtonEnabled = self.checkIfMessageButtonIsEnabled(data: object)
            AppInstance.shared.selectedPatient = object
            AppInstance.shared.isShowHistoryHeaderCell = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            
            let service = BusinessService()
            service.checkupAppointment(with: "\(AppInstance.shared.user?.id ?? 0)", appointment_id: "\(object.id ?? 0)", target: self) { (response) in
                if response == true {
                    
                    self.appointments[sender.tag].status = 4
                    self.tableAppointmentList.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
                    vc.isMessageButtonEnabled = self.checkIfMessageButtonIsEnabled(data: object)
                    AppInstance.shared.selectedPatient = object
                    AppInstance.shared.isShowHistoryHeaderCell = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        self.isCheckInTapped = true
    }
    
    @objc private func AppointmentSuccess(notification: NSNotification){
        self.pageNum = 1
        self.appointments.removeAll()
        self.getDoctorAppointmentList(isAnimated: true)
    }
    
    @objc private func AssistantAppointmentSuccess(notification: NSNotification){
        self.pageNum = 1
        self.appointments.removeAll()
        self.getAssistantAcceptedAppointmentList()
    }

    // MARK:- IBActions
    
    @IBAction private func buttonSelectedCell(_ sender: UIButton) {
        let data = self.appointments[sender.tag]
        let vc = storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
        AppInstance.shared.selectedPatient = data
        AppInstance.shared.isShowHistoryHeaderCell = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK:- IndicatorInfoProvider
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "List"))
    }
    
    // MARK:- Methods
    
    private func checkIfMessageButtonIsEnabled(data: DataAppointmentList) -> Bool {
        
        var isEnabled = false
        if data.visit_type == 0 || data.visit_type == 3 {
            isEnabled = false
        } else {
            isEnabled = true
        }
        if data.patient_info?.accounttype == 1 {
            if data.patient_info?.fcmStatus != "Y" {
                isEnabled = false
            }
        } else if data.patient_info?.accounttype == 4 || data.patient_info?.accounttype == 5 {
            isEnabled = false
        } else {
            isEnabled = true
        }
        return isEnabled
    }
    
    private func getDoctorAppointmentList(isAnimated: Bool) {
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        
        let service = BusinessService()
        service.drAcceptedAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: String(describing: pageNum), date: result, type: "2", target: self,isAnimated: isAnimated) { (response) in
             //  self.appointments.removeAll()
            if response == nil{
                self.errorView.isHidden = false
                self.arrAcceptedAppointment = nil
                self.statisticsData = nil
                self.appointmentListData = nil
                self.appointments.removeAll()
            }
            if let user = response {
                if self.isRefreshed {
                    self.appointments.removeAll()
                    self.isRefreshed = false
                }
                if self.isCheckInTapped {
                    self.appointments.removeAll()
                    self.isCheckInTapped = false
                }
                self.arrAcceptedAppointment = user
                self.statisticsData = user.appointment_data!
                self.appointmentListData = user.data!
                self.appointments.append(contentsOf: user.data!.data!)
                self.totalPages = user.data!.last_page ?? 1
                if self.statisticsData?.new != 0 || self.statisticsData?.new != 0
                    || (self.appointments.count) > 0 ||
                    self.statisticsData?.total_pending_reservation != 0 {
                    self.errorView.isHidden = true
                } else {
                    self.errorView.isHidden = false
                }
                AppInstance.shared.arrayAvailablity.removeAll()
                if (self.appointments.count) > 0 {
                    for obj in self.appointments{
                        let day = obj.appointment_date
                        AppInstance.shared.arrayAvailablity.append(day!)
                    }
                }
            }
            self.tableAppointmentList.reloadData()
        }
    }
    
    private func getAssistantAcceptedAppointmentList() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        let clinicId = AppInstance.shared.user?.clinic_id
        let service = BusinessService()
        service.acceptedAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: "\(pageNum)", date: result, type: "2", doctor_id: String(describing: AppInstance.shared.user!.dr_id!), clinic_id: String(describing: clinicId!), target: self) { (response) in
         //   self.appointments.removeAll()
            if response == nil{
                self.errorView.isHidden = false
                self.arrAcceptedAppointment = nil
                self.statisticsData = nil
                self.appointmentListData = nil
                self.appointments.removeAll()
            }
            if let user = response {
                if self.isRefreshed {
                    self.appointments.removeAll()
                    self.isRefreshed = false
                }
                if self.isCheckInTapped {
                    self.appointments.removeAll()
                    self.isCheckInTapped = false
                }
                self.arrAcceptedAppointment = user
                self.statisticsData = user.appointment_data!
                self.appointmentListData = user.data!
                self.appointments.append(contentsOf: user.data!.data!)
                self.totalPages = user.data!.last_page ?? 1
                if self.statisticsData?.new != 0 || self.statisticsData?.new != 0 || (self.appointments.count) > 0 || self.statisticsData?.total_pending_reservation != 0 {
                    self.errorView.isHidden = true
                }
                else{
                    self.errorView.isHidden = false
                }
                AppInstance.shared.arrayAvailablity.removeAll()
                if (self.appointments.count) > 0 {
                    for obj in self.appointments{
                        let day = obj.appointment_date
                        AppInstance.shared.arrayAvailablity.append(day!)
                    }
                }
                self.tableAppointmentList.reloadData()
            }
        }
    }
    
    private func checkUserType() {
        if AppInstance.shared.userType == 0{
            self.getDoctorAppointmentList(isAnimated: true)
        } else {
            self.getAssistantAcceptedAppointmentList()
        }
    }
    
    // MARK:- Scroll View Methods
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        contentOffSet = self.tableAppointmentList.contentOffset.y;
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.pageNum < self.totalPages {
            if scrollView == self.tableAppointmentList && self.appointments.count>0 {
                if isDataLoading == false{
                    isDataLoading = true
                    pageNum = pageNum + 1
                    self.checkUserType()
                }
            }
        }
    }
}

// MARK:- UITableView DataSources & Delegates

extension AppointmentListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appointments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! AppointmentListCell
        cell.btn_no_show.setTitle(localizeValue(key: "No Show"), for: .normal)
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.labelClinicName.textAlignment = .right
            cell.labelAsName.textAlignment = .right
        }
        let data: DataAppointmentList!
        if self.appointments.count == 0 {
            return cell
        }else{
            data = self.appointments[indexPath.row]
        }

        cell.labelAsName.text = "\(data.patient_info?.firstname ?? "") \(data.patient_info?.lastname ?? "")"
        cell.labelAsName.sizeToFit()
        if data.patient_info?.image?.isEmpty != true{
            cell.imageViewList.sd_setShowActivityIndicatorView(true)
            cell.imageViewList.sd_setIndicatorStyle(.gray)
            cell.imageViewList.sd_setImage(with: URL(string: Config.imageBaseUrl + (data.patient_info!.image!) ?? ""), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewList.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewList.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.labelClinicName.text = data.clinic?.clinic_name_ar ?? ""
        }else{ cell.labelClinicName.text = data.clinic?.clinic_name ?? ""}
        
        cell.buttonCall.tag = indexPath.row
        cell.buttonMessage.tag = indexPath.row
        cell.buttonSelectedCell.tag = indexPath.row
        cell.buttonCheckedIn.tag = indexPath.row
        cell.btn_cancel.tag = indexPath.row
        cell.btn_no_show.tag = indexPath.row
        let isOnSite = (data.visit_type == 0)
        let dateStr = TimeUtils.sharedUtils.chaneDateFormet((data.appointment_date)!)
        
        if data.set_time == "" {
            cell.labelAsDate.text = "\(dateStr)"
        } else {
            
            
            var start_time = data.set_time
            let last = String(start_time!.characters.suffix(2))
            var new_start_time = start_time!.dropLast(2)
            if last != "" {
                if last == "AM" || last == "am"{
                    new_start_time = new_start_time + "am"
                }else{
                    new_start_time = new_start_time + "pm"
                }
            }
            
            cell.labelAsDate.text = "\(dateStr) | \(TimeUtils.sharedUtils.chaneTimeFormet("\(new_start_time)") )"
            print("\(dateStr) | \(data.set_time ?? "")")
        }
        cell.labelOnSight.isHidden = !isOnSite
        cell.buttonLogo.setImage(isOnSite ? nil : UIImage.init(named: "clinido_icon"), for: .normal)
        cell.buttonLogo.setTitle(isOnSite ? localizeValue(key: "Onsite") : nil, for: .normal)
        cell.buttonLogo.imageView?.contentMode = .scaleAspectFit
        cell.buttonCheckedIn.isHidden = isOnSite
        cell.constraint_bottom_checkup.constant = (cell.buttonCheckedIn.isHidden) ? 0.0 : 4.0
        cell.buttonCall.isHidden = false
        cell.buttonMessage.isEnabled = !isOnSite || (data.type == 1)
        if isOnSite {
            cell.cancelTopConstraint.constant = -60
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
        } else {
            cell.cancelTopConstraint.constant = 4
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
        }
        cell.buttonMessage.isHidden = false
        cell.btn_no_show.isHidden = (data.type == 1) ? false : true
        cell.btn_cancel.isHidden = false
        cell.btn_cancel.backgroundColor = (data.status == 3) ? #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.btn_cancel.layer.borderColor = (data.status == 3) ? #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1).cgColor : #colorLiteral(red: 0, green: 0.6509803922, blue: 0.7843137255, alpha: 1).cgColor
        cell.btn_cancel.setTitleColor((data.status == 3) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        cell.btn_cancel.setTitle((data.status == 3) ? localizeValue(key: "Cancelled") : localizeValue(key: "Cancel"), for: .normal)

        cell.buttonCheckedIn.backgroundColor = (data.status == 4) ? #colorLiteral(red: 0.4941176471, green: 0.8274509804, blue: 0.1294117647, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.buttonCheckedIn.layer.borderColor = (data.status == 4) ? #colorLiteral(red: 0.4941176471, green: 0.8274509804, blue: 0.1294117647, alpha: 1).cgColor : #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1).cgColor
        cell.buttonCheckedIn.setTitleColor((data.status == 4) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        cell.btn_no_show.backgroundColor = (data.noshow == 1) ? #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.btn_no_show.setTitleColor((data.noshow == 1) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        cell.btn_cancel.isUserInteractionEnabled = (data.status != 3)
        cell.btn_cancel.addTarget(self, action: #selector(self.actionOnCancel(_:)), for: .touchUpInside)
        cell.btn_no_show.addTarget(self, action: #selector(self.actionOnNoShow(_:)), for: .touchUpInside)
        cell.buttonCheckedIn.addTarget(self, action: #selector(self.buttonCheckInClicked(_:)), for: .touchUpInside)
        cell.buttonCall.addTarget(self, action: #selector(self.buttonCallClicked(_:)), for: .touchUpInside)
        cell.buttonMessage.addTarget(self, action: #selector(self.buttonMessageClicked(_:)), for: .touchUpInside)
        cell.buttonCheckedIn.setTitle(localizeValue(key: "Check Up"), for: .normal)
//        let currentDate = Date()
//        if (currentDate < CommonClass.convertStringToDate(date: data.appointment_date!)) {
//            cell.btn_no_show.isEnabled = false
//            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
//            cell.buttonCheckedIn.isEnabled = false
//            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
//        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let currentStr = formatter.string(from: Date())
        // let currentDate = formatter.date(from: currentStr)
        if (CommonClass.convertStringToDate(date: currentStr) < CommonClass.convertStringToDate(date: data.appointment_date!)) {
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
        }else if (CommonClass.convertStringToDate(date: currentStr) > CommonClass.convertStringToDate(date: data.appointment_date!)){
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
            cell.btn_cancel.isEnabled = false
            cell.btn_cancel.setTitleColor(.lightGray, for: .normal)
        }
        else{
            cell.btn_no_show.isEnabled = true
            cell.buttonCheckedIn.isEnabled = true
            cell.btn_cancel.isEnabled = true
        }
        
        if data.status == 4 {
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.btn_cancel.isEnabled = false
            cell.btn_cancel.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = true
            cell.buttonCheckedIn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) , for: .normal)
        } else if data.status == 3 {
            cell.btn_no_show.isEnabled = false
            cell.btn_no_show.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
        }
        if data.noshow == 1 {
            cell.btn_cancel.isEnabled = false
            cell.btn_cancel.setTitleColor(.lightGray, for: .normal)
            cell.buttonCheckedIn.isEnabled = false
            cell.buttonCheckedIn.setTitleColor(.lightGray, for: .normal)
            cell.btn_no_show.isEnabled = true
        }
        
        if data.visit_type == 0 || data.visit_type == 3 {
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
            cell.buttonMessage.isEnabled = false
        } else {
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
            cell.buttonMessage.isEnabled = true
        }
        if data.patient_info?.accounttype == 1 {
            if data.patient_info?.fcmStatus != "Y" {
                cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
                cell.buttonMessage.isEnabled = false
            }
        } else if data.patient_info?.accounttype == 4 || data.patient_info?.accounttype == 5 {
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
            cell.buttonMessage.isEnabled = false
        } else {
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
            cell.buttonMessage.isEnabled = true
        }
        if cell.buttonCheckedIn.backgroundColor == #colorLiteral(red: 0.4941176471, green: 0.8274509804, blue: 0.1294117647, alpha: 1) {
            cell.buttonCheckedIn.isUserInteractionEnabled = true
            cell.buttonCheckedIn.isEnabled = true
        }
        
        if cell.btn_cancel.backgroundColor ==  #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1) || cell.btn_cancel.titleColor == .lightGray {
            cell.btn_cancel.isUserInteractionEnabled = false
            cell.btn_cancel.isEnabled = false
        } else {
            cell.btn_cancel.isUserInteractionEnabled = true
            cell.btn_cancel.isEnabled = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0{
            if statisticsData?.total_pending_reservation != 0{
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantNewReservationsViewController") as! AssistantNewReservationsViewController
                vc.isPatientRequest = true
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.pageNum < self.totalPages {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableAppointmentList.tableFooterView = spinner
                self.tableAppointmentList.tableFooterView?.isHidden = false
            }
        } else {
            self.tableAppointmentList.tableFooterView?.isHidden = true
        }
    }
}
