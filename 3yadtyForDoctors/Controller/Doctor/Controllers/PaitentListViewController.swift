//
//  PaitentListViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 08/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class PaitentListViewController: BaseViewControler, UISearchBarDelegate {
    
    
    
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyDescriptionLabel: UILabel!
    
    
    @IBOutlet weak var errorImageView: UIImageView!
    

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBarPatient: UISearchBar!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tablePatientList: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var labelErrorMessage: UILabel!
    var pageNo = 1
    var isDataLoading = false
    var stopReload = false


    var arrPatientList = [Patient_info]()
    
    private var isMessageButtonEnabled: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tablePatientList.delegate = self
        tablePatientList.dataSource = self
        searchBarPatient.placeholder = localizeValue(key: "Find Patient")
        // Do any additional setup after loading the view.
        titleLabel.text = localizeValue(key: "Patients")
        searchBarPatient.delegate = self
        searchBarPatient.barTintColor = .clear
        let image = UIImage()
        searchBarPatient.setBackgroundImage(image, for: .any, barMetrics: .default)
        searchBarPatient.scopeBarBackgroundImage = image
        //searchBarPatient.showsCancelButton = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //menu Button
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.image = UIImage(named: "menu_right")!.withRenderingMode(.alwaysOriginal)
            emptyTitle.text = localizeValue(key: "Patient list is empty")
            emptyDescriptionLabel.text = localizeValue(key: "You don't  have any patients")
            
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
             
                
            }
            else{
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }
            
        //    self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        //get patient List search type 1 if
    stopReload = false
        searchBarPatient.text = ""
       // labelErrorMessage.text = "You don't have any patients".localize()
          self.arrPatientList.removeAll()
        pageNo = 1
        self.checkUserTypeAndFetchPatientsList(searchType: "2", searchText: "", pageNumber: self.pageNo)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func buttonAddPatientClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewPatientViewController") as! AddNewPatientViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        let searchStr = searchBar.text
        self.arrPatientList.removeAll()
       emptyDescriptionLabel.text = localizeValue(key: "You don't  have any patients")
        pageNo = 1
        self.checkUserTypeAndFetchPatientsList(searchType: "1", searchText: searchStr ?? "", pageNumber: self.pageNo)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            searchBar.text = ""
            searchBar.showsCancelButton = false
            self.view.endEditing(true)
            self.arrPatientList.removeAll()
             emptyDescriptionLabel.text = localizeValue(key: "You don't  have any patients")
            self.checkUserTypeAndFetchPatientsList(searchType: "2", searchText: "", pageNumber: 1)
        }
    }
    
    func getPatientList(searchType: String, searchText: String, pageNo: Int) {
        
        let service = BusinessService()
        service.patientListDr(with: String(describing: AppInstance.shared.user!.id!), search_type: searchType, name: searchText, current_page: String(describing: pageNo), target: self) { (response) in
            if let data  = response {
                for i in 0..<data.count {
                    self.arrPatientList.append(data[i])
                }
            }
            
            if self.arrPatientList.count == 0 {
                 self.errorView.isHidden = false
            }
            else{
                  self.tablePatientList.reloadData()
                self.errorView.isHidden = true

            }
            if response?.count == 0 {
                self.stopReload = true
            }
            
            
        }
    }
    
    func getPatientListForAssistant(searchType: String, searchText: String, pageNo: Int) {
        
        let service = BusinessService()
        service.patientList(with: String(describing: AppInstance.shared.user!.id!), search_type: searchType, name: searchText, current_page: String(describing: pageNo), target: self) { (response) in
            
            if let data = response{
                for i in 0..<response!.count {
                    self.arrPatientList.append(response![i])
                }
                if self.arrPatientList.count == 0 {
                    self.errorView.isHidden = false
                }
                else{
                    self.tablePatientList.reloadData()
                    self.errorView.isHidden = true
                    
                }
                if response?.count == 0 {
                    self.stopReload = true
                }
            }else{
                self.arrPatientList.removeAll()
                self.tablePatientList.reloadData()
                 self.errorView.isHidden = true
            }
          
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        print("scrollViewDidEndDragging")
        if ((tablePatientList.contentOffset.y + tablePatientList.frame.size.height) >= tablePatientList.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                self.pageNo = self.pageNo + 1
                if !stopReload{
                     self.checkUserTypeAndFetchPatientsList(searchType: "2", searchText: "", pageNumber: pageNo)
                }
                
               
                
            }
        }
        
        
    }
    
  
    
    func checkUserTypeAndFetchPatientsList(searchType: String, searchText: String, pageNumber: Int) {
        if AppInstance.shared.userType == 0 {
            self.getPatientList(searchType: searchType, searchText: searchText, pageNo: pageNumber)
        } else {
            self.getPatientListForAssistant(searchType: searchType, searchText: searchText, pageNo: pageNumber)
        }
    }
    
    private func checkIfMessageButtonIsEnabled(cell: PatientCell) -> Bool {
        return cell.buttonMessage.isEnabled
    }
    
    @IBAction func buttonCallClicked(_ sender: UIButton) {
     
        let mobile = CommonClass.checkAndEditCountryCodeFor(countryCode: "\(String(describing: self.arrPatientList[sender.tag].countrycode!))") + "\(String(describing: self.arrPatientList[sender.tag].mobile!))"
        print(mobile)
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @IBAction func buttonMessageClicked(_ sender: UIButton) {
        let data = self.arrPatientList[sender.tag]
        if data.devicetoken != nil {
            let user = ["email": data.email ?? "","fcmUserId": String(describing: data.id!) ,"image": data.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data.firstname ?? "") \(data.lastname ?? "")"] as [String : Any]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.selectedUser = Person.init(dictionary: user as NSDictionary)
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
//    This function is used to move the patient to patient detail screen on the patient view.
    
    @IBAction func buttonSelectClicked(_ sender: UIButton) {
        let data = self.arrPatientList[sender.tag]
        if data.devicetoken != nil {
            let vc = storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
            let indexPath = IndexPath.init(row: sender.tag, section: 0)
            vc.isMessageButtonEnabled = self.checkIfMessageButtonIsEnabled(cell: self.tablePatientList.cellForRow(at: indexPath) as! PatientCell)
            AppInstance.shared.patientListSelectedPatient = data
            AppInstance.shared.isShowHistoryHeaderCell = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension PaitentListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPatientList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) as! PatientCell
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.labelName.textAlignment = .right
            cell.labelPhone.textAlignment = .right
        }
        cell.onsiteLabel.text = localizeValue(key: "Onsite")
        let data = self.arrPatientList[indexPath.row]
        if data.image!.isEmpty != true{
            cell.imageViewPatient.sd_setShowActivityIndicatorView(true)
            cell.imageViewPatient.sd_setIndicatorStyle(.gray)
            cell.imageViewPatient.sd_setImage(with: URL(string: Config.imageBaseUrl + (data.image)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewPatient.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewPatient.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        cell.buttonCall.tag = indexPath.row
        cell.buttonMessage.tag = indexPath.row
        cell.buttonSelect.tag = indexPath.row
        cell.labelName.text = "\(String(describing: data.firstname!)) \(String(describing: data.lastname!))"
        cell.labelPhone.text = CommonClass.checkAndEditCountryCodeFor(countryCode: "\(String(describing: data.countrycode!))") + " \(String(describing: data.mobile!))"
        if data.devicetoken == nil {
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
            cell.buttonSelect.isEnabled = false
            cell.onsiteLabel.isHidden = false
        } else {
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
            cell.buttonMessage.isEnabled = true
            cell.buttonSelect.isEnabled = true
            cell.onsiteLabel.isHidden = true
        }
        if data.devicetype == nil {
            cell.onsiteLabel.isHidden = false
            cell.buttonSelect.isEnabled = false
        } else {
            cell.onsiteLabel.isHidden = true
            cell.buttonSelect.isEnabled = true
        }
        if data.accounttype == 1 {
            if data.fcmStatus != "Y" {
                cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
                cell.buttonMessage.isEnabled = false
                cell.buttonSelect.isEnabled = true
            }
        } else if data.accounttype == 4 || data.accounttype == 5 {
            cell.buttonMessage.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
            cell.buttonMessage.isEnabled = false
            cell.buttonSelect.isEnabled = true
        } else {
            cell.buttonMessage.setImage(UIImage.init(named: "btnMessage"), for: .normal)
            cell.buttonMessage.isEnabled = true
            cell.buttonSelect.isEnabled = true
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//       // let vc = storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
//       // self.navigationController?.pushViewController(vc, animated: true)
//    }
}

