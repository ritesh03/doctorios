//
//  SelectPaitentViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class selectPaitentCell: UITableViewCell {
    
}

class SelectPaitentViewController: UIViewController,UISearchBarDelegate {

    @IBOutlet weak var searchBarPatient: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBarPatient.delegate = self
        searchBarPatient.barTintColor = .clear
        let image = UIImage()
        searchBarPatient.setBackgroundImage(image, for: .any, barMetrics: .default)
        searchBarPatient.scopeBarBackgroundImage = image
        //searchBarPatient.showsCancelButton = true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        searchBar.text = ""
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
    @IBAction func buttonAddNewPaitentClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewPatientViewController") as! AddNewPatientViewController
        self.present(vc, animated: false, completion: nil)
        
    }
    @IBAction func buttonBackClicked(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
    }
}

extension SelectPaitentViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectPaitentCell", for: indexPath) as! selectPaitentCell
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
