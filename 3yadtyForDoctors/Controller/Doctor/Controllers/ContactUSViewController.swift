//
//  ContactUSViewController.swift
//  3yadtyForDoctors
//
//  Created by Sahil garg on 20/01/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

class ContactUSViewController: BaseViewControler {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet private var viewlogo: UIView!
    @IBOutlet private var aboutus: UIView!
    @IBOutlet private weak var headerLabel: UILabel!
    
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UIButton!
    @IBOutlet weak var emailLabel: UIButton!
    @IBOutlet weak var comanyNameLabel: UILabel!
    var viewtype = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
            
            emailLabel.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            phoneLabel.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            
            addressLabel.textAlignment = .right
        }
        
        label1.text = localizeValue(key: "Emails :")
        
        label2.text = localizeValue(key: "Phone no :")
        
        label3.text = localizeValue(key: "Address :")
        comanyNameLabel.text = localizeValue(key: "Egyapp for Information Technology")
        
        aboutus.isHidden = (viewtype == localizeValue(key: "About Us"))
        viewlogo.layer.cornerRadius = 10.0
        viewlogo.clipsToBounds = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.headerLabel.text = self.viewtype
    }
    
    @IBAction private func buttonBackClicked(_ sender: Any) {
        if viewtype == "About Us".localize(){
            self.navigationController?.popViewController(animated: true)
            return
        }
        if AppInstance.shared.userType == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = storyboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController?.pushViewController(VC, animated: true)
        }else {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController!.pushViewController(VC, animated: false)
        }
    }
    
    @IBAction private func actionOnclinicdo(_ sender: Any) {
        
        if let url = URL(string: "mailto:info@clinicDo.com") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: ["":""], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    @IBAction private func actionOnEgy(_ sender: Any) {
        
        if let url = URL(string: "mailto:info@Egyapp.com") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: ["":""], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    @IBAction private func actionOnContactThree(_ sender: Any) {
        if let url = URL(string: "tel://+201201111344") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: ["":""], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}
