//
//  DoctorProfileVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 14/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol SubCategoryDelegate {
    
    func didSelect(items: [SubCategories])
}

class DoctorProfileVC: BaseViewControler {
    
    //MARK:- Outlets
    
    @IBOutlet weak var doctorServiceLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var aboutDocEngLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var professionalTitleEngLabel: UILabel!
    @IBOutlet weak var lastNameEngLabel: UILabel!
    @IBOutlet weak var firstNameEngLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet private weak var btn_back: UIButton!
    @IBOutlet private weak var btn_next: RoundButton!
    @IBOutlet private weak var tbl_doctor_details: UITableView!
    @IBOutlet private weak var constraint_titlear_height: NSLayoutConstraint!
    @IBOutlet private weak var view_properties: UIView!
    @IBOutlet private weak var btn_skip: UIButton!
    @IBOutlet private weak var img_profile: UIImageView!
    @IBOutlet private weak var tf_first_name_arabic: UITextField!
    @IBOutlet private weak var constraint_header_height: NSLayoutConstraint!
    @IBOutlet private weak var tf_first_name_eng: UITextField!
    @IBOutlet private weak var tf_last_name_eng: UITextField!
    @IBOutlet private weak var tf_last_name_arabic: UITextField!
    @IBOutlet private weak var tf_professional_title_eng: CustomTextField!
    @IBOutlet private weak var tf_professional_title_arabic: CustomTextField!
    @IBOutlet private weak var tf_specialty: CustomTextField!
    @IBOutlet private weak var tf_sub_specialty: CustomTextField!
    @IBOutlet private weak var tv_about_eng: UITextView!
    @IBOutlet private weak var tv_about_arabic: UITextView!
    @IBOutlet private weak var tf_education: UITextField!
    @IBOutlet private weak var constraint_picker_height: NSLayoutConstraint!
    @IBOutlet weak var aboutDoctorTopConstraint: NSLayoutConstraint!
//    @IBOutlet weak var doctorServicesLabelHeight: NSLayoutConstraint!
//    @IBOutlet weak var doctorServicesLabel: UILabel!
    @IBOutlet weak var doctorServicesView: UIView!
    
    //MARK:-Variables
    var fromOtp = false
    var imagesArray:[File] = []
    var specialtyArr: [CategoryList] = []
    var subCategoriesArr: [SubCategories] = []
    
    let data = ["Professor","Lecturer","Consultant","Specialist","Fellowship","Assistant Lecturer","Assistant Professor"]
//    let dataAr = ["دكتور جامعى","محاضر","مستشار","متخصص","زمالة","مدرس مساعد","استاذ مساعد"]
    let dataAr = ["أستاذ جامعي", "مدرس جامعي", "إستشارى", "أخصائي", "زماله", "مدرس مساعد", "أستاذ مساعد"]
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = localizeValue(key: "Doctor Profile")
       firstNameEngLabel.text = localizeValue(key: "First Name(Eng)")
       // firstNameEngLabel.text = "First Name(Eng)"
        lastNameEngLabel.text = localizeValue(key: "Last Name(Eng)")
      //  lastNameEngLabel.text = "Last Name(Eng)"
       professionalTitleEngLabel.text = localizeValue(key: "Professional Title(Eng)")
      //  professionalTitleEngLabel.text = "Professional Title(Eng)"
        tf_professional_title_eng.placeholder = localizeValue(key: "Professional Title(Eng)")
       // tf_professional_title_eng.placeholder = "Professional Title(Eng)"
        specialityLabel.text = localizeValue(key: "Speciality")
     aboutDocEngLabel.text = localizeValue(key: "About Dr.(Eng)")
      //  aboutDocEngLabel.text = "About Dr.(Eng)"
        educationLabel.text = localizeValue(key: "Education")
        tf_education.placeholder = localizeValue(key: "Education")
        self.btn_next.setTitle(localizeValue(key: "NEXT"), for: .normal)
        doctorServiceLabel.text = localizeValue(key: "Doctor's Services")
        tf_sub_specialty.placeholder = localizeValue(key: "Doctor's Services")
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            firstNameEngLabel.textAlignment = .right
            lastNameEngLabel.textAlignment = .right
            professionalTitleEngLabel.textAlignment = .right
            tf_first_name_eng.textAlignment = .right
            tf_last_name_eng.textAlignment = .right
            tf_professional_title_eng.textAlignment = .right
            tv_about_eng.textAlignment = .right
            tf_education.textAlignment = .right
            specialityLabel.textAlignment = .right
            tf_specialty.textAlignment = .right
            aboutDocEngLabel.textAlignment = .right
            doctorServiceLabel.textAlignment = .right
            tf_sub_specialty.textAlignment = .right
            educationLabel.textAlignment = .right
        }
        self.viewDidLoadSetupUI()
        self.getCategoryList()
        if fromOtp{
            btn_back.isHidden = false
        }
    }
    
    //MARK:- Functions
    
    private func viewDidLoadSetupUI(){
        
        self.btn_back.automaticallyRotateOnLanguageConversion()
        self.constraint_header_height.constant += (UIApplication.shared.statusBarFrame.size.height)
        self.textFieldsSetup()
        self.updateUserInformation()
        self.btn_skip.setTitle(/*(AppInstance.shared.isFromMenu ?? false) ? "Save".localize() :*/ localizeValue(key: "Skip"), for: .normal)
//        self.btn_next.setTitle((AppInstance.shared.isFromMenu ?? false) ? "NEXT".localize() : "Save".localize(), for: .normal)
        self.img_profile.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.openImageSource)))
        self.img_profile.isUserInteractionEnabled = true
    }
    
    private func textFieldsSetup(){
        
        self.tf_professional_title_eng.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            
            self.view.endEditing(true)
            ActionSheetStringPicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "Please select title in english"), comment: ""), rows: self.data, initialSelection: 0, doneBlock: {
                picker, value, index in
                self.tf_professional_title_eng.text = self.data[value]
                self.updateDoctorProfessionalTitle(With: value)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
        
//        self.tf_sub_specialty.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
//            /*in storyboard the trailing of this text field was in equal relation with leading of image icDownArrow by constant 8 */
//        }
    }
    
    private func updateUserInformation(){
        
        DispatchQueue.main.async {
            
            guard let userObj = AppInstance.shared.user else {
                return
            }
            
            
            self.tf_first_name_eng.text = userObj.firstname ?? ""
            self.tf_last_name_eng.text = userObj.lastname ?? ""
            self.tf_first_name_arabic.text = userObj.arabicfname ?? ""
            self.tf_last_name_arabic.text = userObj.arabilname ?? ""
            self.tv_about_eng.text = ((userObj.about ?? "").isEmpty ? self.tv_about_eng.text! : (userObj.about ?? ""))
            self.tv_about_arabic.text = ((userObj.aboutar ?? "").isEmpty ? self.tv_about_arabic.text! : (userObj.aboutar ?? ""))
            self.tf_education.text = userObj.education ?? ""
            self.tf_professional_title_eng.text = userObj.title ?? ""
            self.tf_professional_title_arabic.text = userObj.titlear ?? ""
            self.tf_specialty.text = userObj.dr_category_name ?? ""
            var sub_cat: String = ""
            if let drSubcategoryDictArr = userObj.dr_subcategory_name as? [Dictionary<String, Any>]{
                for subCatDict in drSubcategoryDictArr{
                    let category_id = subCatDict["category_id"] as? Int ?? 0
                    let id = subCatDict["id"] as? Int ?? 0
                    var name = String()

                    if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                        name = subCatDict["arabic_name"] as? String ?? ""
                    }else{
                        name = subCatDict["name"] as? String ?? ""
                    }
                    
                    let subCategoriesObj = SubCategories(category_id: category_id, id: id, name: name)
                    self.subCategoriesArr.append(subCategoriesObj)
                    sub_cat = (sub_cat == "") ? name : "\(sub_cat), \(name)"
                }
                self.tf_sub_specialty.text = ""
                for item in self.subCategoriesArr.map({$0.name}){
                    self.tf_sub_specialty.text = (self.tf_sub_specialty.text == "") ? item : "\(self.tf_sub_specialty.text!), \(item)"
                }
            }
            
            print()
            
//            self.tf_sub_specialty.text = sub_cat
//            self.doctorServicesLabel.text = sub_cat
//            self.doctorServicesLabelHeight.constant = CommonClass.getTextFieldHeight(textField:self.tf_sub_specialty , text: sub_cat, width: self.tf_sub_specialty.bounds.width, font: self.tf_sub_specialty.font!)
            if let index = self.data.index(of: (userObj.title ?? "")){
                self.updateDoctorProfessionalTitle(With: index)
            }
            
            if AppInstance.shared.user?.image?.isEmpty != true{
                self.img_profile.sd_setShowActivityIndicatorView(true)
                self.img_profile.sd_setIndicatorStyle(.gray)
                guard let url = URL.init(string: Config.imageBaseUrl + AppInstance.shared.user!.image! ) else { return }
                self.img_profile.sd_setImage(with: url, placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                self.img_profile.sd_setShowActivityIndicatorView(false)
            }else{
                self.img_profile.image = UIImage(named: "imgUserPlaceholderSideMenu")
            }
            
//            self.img_profile.sd_setImage(with: URL.init(string: userObj.image ?? ""), placeholderImage: UIImage.init(named: "imgUserPlaceholderComment"), options: [], completed: nil)
        }
    }
    
    private func updateDoctorProfessionalTitle(With selectedItem:Int){
        
        if selectedItem == -1 {
            
            DispatchQueue.main.async {
                
                self.constraint_titlear_height.constant = 0.0
                self.view_properties.frame = CGRect.init(x: self.view_properties.frame.origin.x, y: self.view_properties.frame.origin.y, width: self.view_properties.frame.width, height: 724.0)
            }
        }else{
            
            DispatchQueue.main.async {
                
                self.constraint_titlear_height.constant = 67.0
                self.view_properties.frame = CGRect.init(x: self.view_properties.frame.origin.x, y: self.view_properties.frame.origin.y, width: self.view_properties.frame.width, height: 867.0)
                self.tbl_doctor_details.reloadData()
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            self.tf_professional_title_arabic.text = self.dataAr[selectedItem]
        }
    }
    
    private func saveDataToFirebaseRefrence(userData: UserData?) {
        
        let token = UserDefaults.standard.value(forKey: "token")
        if CommonClass.isLiveServer() {
            Constants.refs.databaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
        } else {
            Constants.refs.testDatabaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
        }
    }
    
    private func getCategoryList() {
        guard let userObj = AppInstance.shared.user else { return }
        let service = BusinessService()
        service.getSubCategories(with: ["category_id":userObj.dr_category ?? 1], target: self) { (response) in
            
            if response == nil {
                self.doctorServicesView.isHidden = true
                self.aboutDoctorTopConstraint.constant = 8
            }
        }
    }
    
    private func validate() -> String{
        
        if self.tf_first_name_eng.text!.isEmpty{
            
            return "Please enter first name in english"
        }else if self.tf_first_name_arabic.text!.isEmpty{
            
            return "Please enter first name in arabic" //"الرجاء إدخال الاسم الأول باللغة العربية"
        }else if self.tf_last_name_eng.text!.isEmpty{
            
            return "Please enter last name in english"
        }else if self.tf_last_name_arabic.text!.isEmpty{
            
            return "Please enter last name in arabic"//"الرجاء إدخال الاسم الأخير باللغة العربية"
        }else if self.tf_professional_title_eng.text!.isEmpty{
            
            return "Please select title in english"
        }else if self.tf_professional_title_arabic.text!.isEmpty{
            
            return "Please select title in arabic"//"الرجاء اختيار العنوان باللغة العربية"
        }else if self.tf_specialty.text!.isEmpty{
            
            return "Please select Specialty"
        }else if self.tf_specialty.text!.isEmpty{
            
            return "Please select Doctor's Services"
        }else if self.tv_about_eng.text!.isEmpty{
            
            return "Please enter about dr. in english"
        }else if self.tv_about_arabic.text!.isEmpty{
            
            return "Please enter about dr. in arabic"//"الرجاء إدخال عنا باللغة العربية"
        }else if self.tf_education.text!.isEmpty{
            
            return " Please enter education"
        }
        return ""
    }
    
    private func getSubCategoriesDict() -> [String:Any]{
        
        var tempStr: String = ""
        for subCategory in self.subCategoriesArr{
            
            tempStr = (tempStr == "") ? "\(subCategory.id)" : "\(tempStr),\(subCategory.id)"
        }
        return ["dr_subcategory":tempStr]
    }
    
    private func EditProfileDoctor() {
        
        let validationStr = self.validate()
        if !validationStr.isEmpty{
            
            UIAlertController.showToast(title: validationStr, message: nil, target: self, handler: nil)
            return
        }
        guard let userObj = AppInstance.shared.user else {return}
        var params:[String:Any] = ["user_id":userObj.id ?? "","firstname":self.tf_first_name_eng.text!,"lastname":self.tf_last_name_eng.text!,"arabicfname":self.tf_first_name_arabic.text!,"arabilname":self.tf_last_name_arabic.text!,"about":self.tv_about_eng.text!,"aboutar":self.tv_about_arabic.text!,"titlear":self.tf_professional_title_arabic.text!,"title":self.tf_professional_title_eng.text!,"education":self.tf_education.text!]
        
        let tempDict = params.merge(WithSecond: self.getSubCategoriesDict())
        
        let service = BusinessService()
        service.editProfileDoctor(with: tempDict, image: imagesArray, target: self) { (response) in
            
            if let user = response {
                
                let userDefaultObj = UserDefaults.standard
                self.saveDataToFirebaseRefrence(userData: user.data!)
                AppInstance.shared.user = user.data!
                
                print("\(String(describing: AppInstance.shared.user!.dr_subcategory_name))")
                userDefaultObj.removeObject(forKey: "isLogIn")
                userDefaultObj.removeObject(forKey: "userdata")
                CustomObjects.archive(Object: user.data!.dictionaryRepresentation(), WithKey: "isLogIn")
                if let signUpDict = AppInstance.shared.doctorSignUp{
                    
                    CustomObjects.archive(Object: signUpDict as AnyObject, WithKey: "userdata")
                }
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WorkInfoViewController") as! WorkInfoViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }
    
    @objc private func openImageSource(){
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Camera"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                let myPickerController = UIImagePickerController()
                myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                myPickerController.allowsEditing = true
                myPickerController.delegate = self
                self.present(myPickerController, animated: true, completion: nil)
            }else{
                UIAlertController.show(title: nil, message: "No camera available.".localize(), target: self, handler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Gallery"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            let myPickerController = UIImagePickerController()
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            myPickerController.delegate = self
            self.present(myPickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK:- Action Outlets
    
    @IBAction private func actionOnBack(_ sender: UIButton){
        
        for vc in self.navigationController!.viewControllers  where vc.classForCoder == PersonalInfoViewController.classForCoder(){
            
            self.navigationController?.popToViewController(vc, animated: true)
        }
    }
    
    @IBAction private func actionOnNext(_ sender: Any) {
        self.EditProfileDoctor()
    }
    
    @IBAction private func actionOnSkip(_ sender: UIButton){
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController!.pushViewController(VC, animated: true)
    }
}

extension DoctorProfileVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            self.img_profile.image = image
            self.imagesArray = [File.init(name: "\(arc4random()).jpg", image: image, type: 1 )]
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension DoctorProfileVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // To open Doctor's Service controller
        if self.tf_sub_specialty == textField{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoriesVC") as! SubCategoriesVC
            vc.delegate = self
            vc.selectedCategoriesArr = self.subCategoriesArr
            self.present(vc, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField == self.tf_last_name_arabic) || (textField == tf_first_name_arabic)){
            
            return textField.isSelectedLanguageArabic()
//        }else if ((textField == self.tf_last_name_eng) || (textField == tf_first_name_eng) || (textField == self.tf_education)){
             }else if ((textField == self.tf_last_name_eng) || (textField == tf_first_name_eng)){
            
            return textField.isSelectedLanguageEnglish()
        }
        return true
    }
}

extension DoctorProfileVC: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView == self.tv_about_arabic{
            
            return textView.isSelectedLanguageArabic()
        }else if textView == self.tv_about_eng{
            
            return textView.isSelectedLanguageEnglish()
        }
        return true
    }
}

// Show selected doctor's services
extension DoctorProfileVC: SubCategoryDelegate{
    
    func didSelect(items: [SubCategories]) {
        self.subCategoriesArr = items
        self.tf_sub_specialty.text = ""
        for item in items.map({$0.name}){
            self.tf_sub_specialty.text = (self.tf_sub_specialty.text == "") ? item : "\(self.tf_sub_specialty.text!), \(item)"
        }
    }
}
