//
//  WebViewController.swift
//  3yadtyForDoctors
//
//  Created by Sahil garg on 15/01/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

class WebViewController: BaseViewControler,UIWebViewDelegate {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var webView: UIWebView!
    @IBOutlet weak var Activity: UIActivityIndicatorView!
    
    var strUrl = ""
    var strHeader = ""
    var fromPayement = true
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.delegate = self
        lblHeader.text = strHeader
        webView.loadRequest(URLRequest.init(url: URL.init(string: strUrl)!))
        self.webView.addSubview(self.Activity)
        self.Activity.startAnimating()
        self.Activity.hidesWhenStopped = true
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        if fromPayement
        {
            let alertController = UIAlertController(title: "Do you want to cancel this payment ?", message: "" , preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                self.dismiss(animated: true, completion: nil)
            }
            
            // Add the actions
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
       
        
     
        
      
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.Activity.startAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.Activity.stopAnimating()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
