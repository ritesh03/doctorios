//
//  AddAddressViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 27/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import ActionSheetPicker_3_0
import MRCountryPicker

class AddAddressViewController: BaseViewControler, CLLocationManagerDelegate,MRCountryPickerDelegate {

    let arrWeekDays = ["Sunday".localize(), "Monday".localize(), "Tuesday".localize(), "Wednesday".localize(), "Thursday".localize(), "Friday".localize(), "Saturday".localize()]
    var arrSelectesDate = [String]()
    var address: String?
    var clenicName: String?
    var clinicID: Int?
    var clinic =  [[String:Any]]()
    var phone : String?
    var cityID: String?
    var areaID: String?
    var cityName: String?
    var areaName: String?
    var completeAddress: String?
    var selectedIndex: Int?
    var arrTimming = [[String:Any]]()
    var arrEdit = [String:Any]()
    var arrEditIndex = Int()
    var lat: String?
    var long: String?
    var arrCityList = [CityList]()
    var arrAreaList = [CityList]()
    var isCitySelected = false
    
    var flagImage: UIImage?
    var code: String?
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    
    @IBOutlet weak var tableAddAddress: UITableView!
    var updateClinic : (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        if !arrEdit.isEmpty {
            arrTimming = arrEdit["availability"]! as! [[String : Any]]
            lat = arrEdit["clinic_lat"]! as? String
            long = arrEdit["clinic_long"]! as? String
            clenicName = arrEdit["clinic_name"]! as? String
            completeAddress = arrEdit["clinic_address"]! as? String
            if let area = arrEdit["clinic_area"]! as? Int{
            areaID = "\(area)"
            } else {
               areaID = arrEdit["clinic_area"]! as? String
            }
            if let city = arrEdit["clinic_city"]! as? Int{
                cityID = "\(city)"
            } else {
                cityID = arrEdit["clinic_city"]! as? String
            }
            areaName = arrEdit["clinic_area_name"]! as? String
            cityName = arrEdit["clinic_city_name"]! as? String
            address = arrEdit["google_address"]! as? String
            phone = arrEdit["clinic_mobile"]! as? String
            code = arrEdit["clinic_country_code"]! as? String
            countryPicker.setCountryByPhoneCode(code!)
            tableAddAddress.reloadData()
        }
        
        else {
            let countryCode = Locale.current.regionCode
            countryPicker.setCountry(countryCode!)
            let countryName = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode!)
            countryPicker.setCountryByName(countryName!)
            countryPicker.setLocale("EN")
        }
        
        getCityList()
    }
   
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        flagImage = flag
        code = phoneCode
        tableAddAddress.reloadData()
    }
    
    @IBAction func buttonCountryClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    @IBAction func buttonCancelClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    @IBAction func buttonDoneClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @IBAction func backbtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttomSubmitClicked(_ sender: Any) {
       
        guard let clinicName = clenicName,!clinicName.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter clinic name".localize(), target: self, handler: nil)
            return
        }
        guard let address = address,!address.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter address".localize(), target: self, handler: nil)
            return
        }
        guard let city = cityID,!city.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter city".localize(), target: self, handler: nil)
            return
        }
        guard let area = areaID,!area.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter area".localize(), target: self, handler: nil)
            return
        }
        guard let completeAddress = completeAddress,!completeAddress.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter complete address".localize(), target: self, handler: nil)
            return
        }
        
        if phone?.isEmpty == true{
            UIAlertController.show(title: nil, message: "Please enter clinic phone".localize(), target: self, handler: nil)
            return
        }
        
        if arrTimming.count <= 0 {
            UIAlertController.show(title: nil, message: "Please select clinic timing".localize(), target: self, handler: nil)
            return
        }

        //MARK: add clinic data to share instance
        let clinicParams = ["clinic_name": clinicName, "clinic_address": completeAddress, "clinic_lat": "\(lat!)", "clinic_long": "\(long!)","clinic_city": cityID!,"clinic_area": areaID!, "availability": arrTimming] as [String : AnyObject]

        if !arrEdit.isEmpty {
            
            var localArray = [[String:AnyObject]]()
            for element in arrTimming {
                var localDict = [String:AnyObject]()
                localDict["clinic_id"] = String(describing: clinicID!) as AnyObject
                localDict["day"] = element["day"] as AnyObject
                localDict["end_time"] = element["end_time"] as AnyObject
                localDict["start_time"] = element["start_time"] as AnyObject
                localDict["dr_id"] = String(describing: AppInstance.shared.user!.id!) as AnyObject
                if element["end_time_milis"] == nil {
                    localDict["end_time_milis"] = "" as AnyObject
                } else {
                    localDict["end_time_milis"] = element["end_time_milis"] as AnyObject
                }
                
                if element["start_time_milis"] == nil {
                    localDict["start_time_milis"] = "" as AnyObject
                } else {
                    localDict["start_time_milis"] = element["start_time_milis"] as AnyObject
                }
                
                localArray.append(localDict)
            }
            
            
            var paramToHit = clinicParams
            paramToHit["clinic_id"] = String(describing: clinicID!) as AnyObject
            paramToHit["availability"] = self.json(from: localArray as AnyObject) as AnyObject
            paramToHit["clinic"] = self.json(from: localArray as AnyObject) as AnyObject
            paramToHit["user_id"] = String(describing: AppInstance.shared.user!.id!) as AnyObject
            
            let service = BusinessService()
            service.updateClinicList(with: paramToHit, target: self) { (response) in
                
                if (response != nil) {
                    AppInstance.shared.clinicTimming[self.arrEditIndex] = clinicParams
                    let clinicJson = self.json(from:AppInstance.shared.clinicTimming as Any)
                    paramEdit["clinic"] = clinicJson
                    AppInstance.shared.doctorSignUp = paramEdit
                    
                    if self.updateClinic != nil {
                        self.updateClinic!()
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
            
        } else {
            
            let clinicNew = ["clinic_name": clinicName as AnyObject, "clinic_address": completeAddress as AnyObject, "clinic_lat": "\(lat!)" as AnyObject, "clinic_long": "\(long!)" as AnyObject,"clinic_city": cityID! as AnyObject,"clinic_area": areaID! as AnyObject, "clinic_area_name": areaName! as AnyObject,"clinic_city_name": cityName! as AnyObject,"clinic_mobile":CommonClass.checkAndEditPhoneNumberFor(phoneNumber: phone!) as AnyObject,"clinic_country_code":code! as AnyObject,"google_address":address as AnyObject,"availability": arrTimming as AnyObject] as [String : AnyObject]
            
            var paramToHit = clinicParams
            
            paramToHit["clinic_id"] = "" as AnyObject
            paramToHit["availability"] = self.json(from: [clinicNew] as AnyObject) as AnyObject
            paramToHit["clinic"] = self.json(from: [clinicNew] as AnyObject) as AnyObject
            paramToHit["user_id"] = String(describing: AppInstance.shared.user!.id!) as AnyObject
            
            let service = BusinessService()
            service.updateClinicList(with: paramToHit, target: self) { (response) in
                
                if (response != nil) {
                    AppInstance.shared.clinicTimming.append(clinicParams)
                    let clinicJson = self.json(from:AppInstance.shared.clinicTimming as Any)
                    paramEdit["clinic"] = clinicJson
                    AppInstance.shared.doctorSignUp = paramEdit
                    
                    if self.updateClinic != nil {
                        self.updateClinic!()
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
            
            
        }
       
        
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 0 {
            phone = textField.text
        }else if textField.tag == 1{
            clenicName = textField.text
        }else if textField.tag == 2{
            address = textField.text
        }else if textField.tag == 3{
            cityID = textField.text
        }else if textField.tag == 4{
            areaID = textField.text
        }else if textField.tag == 5{
            completeAddress = textField.text
        }
    }
    
    func getCityList() {
        let service = BusinessService()
        service.getCityList(target: self) { (response) in
            if let city = response {
                self.isCitySelected = false
                self.arrCityList = city.data!
            }
            self.tableAddAddress.reloadData()
        }
    }
    
    func getAreaList(city_id: String) {
        let service = BusinessService()
        service.getAreaList(with: city_id, target: self) { (response) in
            if let area = response {
                self.isCitySelected = true
                self.arrAreaList = area.data!
            }
            self.tableAddAddress.reloadData()
        }
    }
}

extension AddAddressViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 6
        }
        return arrTimming.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddAddressCell
                  cell.selectionStyle = UITableViewCellSelectionStyle.none
                  cell.tfHospitalName.tag = 1
                  cell.labelTitle.text = "Hospital/ Clinic name".localize()
                  cell.tfHospitalName.placeholder = "Hospital/ Clinic name".localize()
                  cell.tfHospitalName.text = clenicName
                  if arrEdit.isEmpty != true{
                    
                  }
                  cell.tfHospitalName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! AddAddressCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.tfAddress.tag = 2
                cell.tfAddress.text = address
                cell.tfAddress.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! AddAddressCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.tfHospitalName.tag = 3
                cell.labelTitle.text = "City".localize()
                cell.tfHospitalName.placeholder = "City".localize()
                if arrEdit.isEmpty != true{
                    cell.tfHospitalName.text = cityName
                }
                var data = [String]()
                for city in arrCityList {
                    data.append(city.name!)
                }
                cell.tfHospitalName.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
                    //Write code for picker view here
                    self.view.endEditing(true)
                    ActionSheetStringPicker.show(withTitle: "Select City".localize(), rows: data, initialSelection: 0, doneBlock: {
                        picker, value, index in
                        if data.count > 0 {
                            cell.tfHospitalName.text = data[value]
                            self.cityName = data[value]
                            self.cityID = String(describing: self.arrCityList[value].id!)
                            //API get arealist
                            self.getAreaList(city_id: self.cityID!)
                        }
                        return
                    }, cancel: { ActionStringCancelBlock in return }, origin: button)
                }

                cell.tfHospitalName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "completeAddressCell", for: indexPath) as! AddAddressCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.tfHospitalName.tag = 5
                cell.labelTitle.text = "Complete Address".localize()
                cell.tfHospitalName.placeholder = "Enter your addresss".localize()
                if arrEdit.isEmpty != true{
                    cell.tfHospitalName.text = completeAddress
                }
                cell.tfHospitalName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feesCell", for: indexPath) as! AddAddressCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.tfHospitalName.tag = 1
                cell.labelTitle.text = "Fees".localize()
                cell.tfHospitalName.placeholder = "Enter fee".localize()
                cell.tfHospitalName.text = clenicName
                if arrEdit.isEmpty != true{
                    
                }
                cell.tfHospitalName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 5:
                let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as! LogInCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.imageViewFlag.image = flagImage
                cell.labelCountaryCode.text = code
                cell.textFieldPhone.keyboardType = .phonePad
                cell.textFieldPhone.tag = 0
                cell.textFieldPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
             default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "completeAddressCell", for: indexPath) as! AddAddressCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.tfHospitalName.tag = 5
                cell.labelTitle.text = "Complete Address".localize()
                cell.tfHospitalName.placeholder = "Enter your addresss".localize()
                if arrEdit.isEmpty != true{
                    cell.tfHospitalName.text = completeAddress
                }
                cell.tfHospitalName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            }
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "completeAddressCell", for: indexPath) as! AddAddressCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.tfHospitalName.tag = 5
            cell.labelTitle.text = "Complete Address".localize()
            cell.tfHospitalName.placeholder = "Enter your addresss".localize()
            if arrEdit.isEmpty != true{
                cell.tfHospitalName.text = completeAddress
            }
            cell.tfHospitalName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            switch indexPath.row {
            case 1:
//                let viewport = GMSCoordinateBounds()
//                let config =  GMSPlacePickerConfig(viewport: viewport)
//                let placePicker = GMSPlacePickerViewController(config: config)
//                placePicker.delegate = self
//                placePicker.modalPresentationStyle = .overFullScreen
//                present(placePicker, animated: true, completion: nil)
//                
//                
                        let autocompleteController = GMSAutocompleteViewController()
                        autocompleteController.delegate = self
                    self.present(autocompleteController, animated: true, completion: nil)
            default: break
            }
        }
        /*
        if indexPath.section == 1{
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "DatePickerPopUpViewController") as! DatePickerPopUpViewController
            vc.day = arrTimming[indexPath.row-1]["day"] as? String
            vc.delegate = self
            vc.salectedIndex = indexPath.row-1
            vc.fromTime = arrTimming[indexPath.row-1]["start_time"] as? String
            vc.toTime   = arrTimming[indexPath.row-1]["end_time"] as? String
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = UIColor.clear
            vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
            self.present(vc, animated: false, completion: nil)
        }
        */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! AddAddressCell
            return headerView
        }else{
            return nil
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}

extension AddAddressViewController : GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        address = place.formattedAddress
        lat = "\(place.coordinate.latitude)"
        long = "\(place.coordinate.longitude)"
        self.tableAddAddress.reloadData()
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)

    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
           dismiss(animated: true, completion: nil)
    }
    
    
   
}

extension AddAddressViewController:AvailabilityDelegate {
    
    func addAvailability(object: [String : Any], index: Int?) {
        if index != nil{
            arrTimming.remove(at: index!)
            arrTimming.insert(object, at: index!)
        }else{
            arrTimming.append(object)
        }
        tableAddAddress.reloadData()
    }
    
    func deleteAvailability(){
        let indexpath = IndexPath(row: 0, section: 1)
        let tableCell = self.tableAddAddress!.cellForRow(at: indexpath) as? AddAddressCell
        let indexPath = IndexPath(row: selectedIndex!, section: 0)
        let cell = tableCell!.collectionCell.dequeueReusableCell(withReuseIdentifier: "AddAddressCollectionCell", for: indexPath ) as! AddAddressCollectionCell
        cell.buttonRadio.isSelected = !cell.buttonRadio.isSelected
        self.tableAddAddress.reloadData()
     }
}

extension AddAddressViewController: DayCollectionViewCellDelegate {
    
    func collectionDayView(didSelectItemAt indexPath: IndexPath) {
            //select Timming
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "DatePickerPopUpViewController") as! DatePickerPopUpViewController
            vc.day = arrWeekDays[indexPath.row]
            vc.delegate = self
            vc.salectedIndex = nil
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = UIColor.clear
            vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
            self.present(vc, animated: false, completion: nil)
    }
    
    func collectionDayView(didDeleteItemAt indexPath: IndexPath) {
        self.arrTimming.remove(at: indexPath.row)
        self.tableAddAddress.reloadData()
    }
}
