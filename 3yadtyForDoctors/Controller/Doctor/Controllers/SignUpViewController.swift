//
//  SignUpViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 20/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn

class SignUpViewController: UIViewController, GIDSignInUIDelegate {
    
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var createAccountBtn: RoundButton!
    @IBOutlet weak var changeTypeBtn: UIButton!
    @IBOutlet weak var logoBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btn_back: UIButton!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        changeTypeBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Change account type", comment: ""), for: .normal)
        createAccountBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Create new account", comment: ""), for: .normal)
        let normalText = LocalizationSystem.sharedInstance.localizedStringForKey(key: "I already have an account.", comment: "")
        let boldText  = LocalizationSystem.sharedInstance.localizedStringForKey(key: " Login", comment: "")
        let attrs1 = [NSAttributedStringKey.foregroundColor : UIColor.black]
        let attributedString = NSMutableAttributedString(string:normalText, attributes:attrs1)
        let attrs = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 15),NSAttributedStringKey.foregroundColor : UIColor.black]
        let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
        attributedString.append(boldString)
        
        LoginButton.setAttributedTitle(attributedString, for: .normal)
        
        if  UIScreen.main.bounds.maxY > 800 {
            logoBottomConstraint.constant = 140
        } else if  UIScreen.main.bounds.maxY > 568 {
            logoBottomConstraint.constant = 70
        } else {
            logoBottomConstraint.constant = 20
        }
        
        
        self.btn_back.automaticallyRotateOnLanguageConversion()
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func openLoginController(){
        
        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        vc.loginDelegate = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func ButtonBackClicked(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: WelcomeScreenViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                self.navigationController?.pushViewController(vc, animated: true)
                break
            }
        }
    }
    
    @IBAction func buttonCreateAccountClicked(_ sender: Any) {
        
        AppInstance.shared.isFromForgot = false
        
        //Doctor SignUp
        if AppInstance.shared.userType == 0 {
            AppInstance.shared.isFromMenu = false
            AppInstance.shared.isOtpVerified = false
            paramEdit = [String:Any]()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoViewController") as! PersonalInfoViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneViewController") as! EnterPhoneViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func buttonFacebookClicked(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    @IBAction func buttonGoogleClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
        //        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantTabbarController")
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func ButtonLogInClicked(_ sender: Any) {
        
        self.openLoginController()
    }
    
    //Facebook
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result!)
                    _ = (result as! NSDictionary)["id"]!
                    _ = (result as! NSDictionary)["email"]!
                }
            })
        }
    }
}

extension SignUpViewController:logInSucessDelegate{
    func isForgotClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneViewController") as! EnterPhoneViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func userLogInType(object: Int) {
        if object == 1{
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if object == 2{
            
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController?.pushViewController(vc, animated: true)
            /*
             let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantTabbarController")
             self.navigationController?.pushViewController(vc, animated: true)
             */
        }
    }
}
