//
//  AddNewAppointmientsViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker
import ActionSheetPicker_3_0


class AddNewAppointmientCell: UITableViewCell {
    
   
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var btn_female: RoundButton!
    @IBOutlet weak var btn_male: RoundButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textFieldName: CustomTextField!
    @IBOutlet weak var tfFirstname: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var imageViewFlag: RoundImage!
    @IBOutlet weak var labelCode: UILabel!
    
    var gender: String = ""
   
    func updateGender(_ button: UIButton?){
        
        if let sender = button{
            
            self.btn_male.backgroundColor = (sender.tag != self.btn_male.tag) ? #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)
            self.btn_female.backgroundColor = (sender.tag != self.btn_female.tag) ? #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)
            self.btn_male.setTitleColor((sender.tag != self.btn_male.tag) ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            self.btn_female.setTitleColor((sender.tag != self.btn_female.tag) ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            self.gender = (sender.tag == self.btn_male.tag) ? "Male".localize() : "Female".localize()
            self.update(gender: self.gender)
            
        }
    }
    
    
    private func update(gender: String){
        
        if let activeController = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers.first?.childViewControllers.last?.navigationController?.childViewControllers.last?.childViewControllers.first?.childViewControllers.last as? AddNewAppointmientsViewController{
            
            
            activeController.gender = gender
        }
    }
    
    @IBAction func actionOnMale(_ sender: Any) {
        
        self.updateGender(sender as? UIButton)
    }
    
    @IBAction func actionOnFemale(_ sender: Any) {
        
        self.updateGender(sender as? UIButton)
    }
}

class AddNewAppointmientsViewController: BaseViewControler, UITextViewDelegate, UIImagePickerControllerDelegate,
UINavigationControllerDelegate, MRCountryPickerDelegate {
    
    @IBOutlet weak var noteLabel: UILabel!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    @IBOutlet weak var buttonNew: RoundButton!
    @IBOutlet weak var buttonConsult: RoundButton!
    @IBOutlet weak var textViewAppointment: UITextView!
    @IBOutlet weak var tableViewNewAppointment: UITableView!
    
    var selectedFlag: UIImage?
    var code: String?
    var gender: String = ""
    var fName: String?
    var lname: String?
    var phone: String?
    var email: String? = ""
    var selectedDate: String?
    var dateTime: String?
    var type: String?
    var note: String?
    
    var clinicName: String?
    var clinicID: String?
    var clinicType: Int?
    var clinicData: ClinicData?
    var reservationType: Int = 1
    var appointmentId = ""
 var selectedDateTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewAppointment.text = localizeValue(key: "Write your note here….")
        noteLabel.text = localizeValue(key: "Add note")
        headerTitleLabel.text = localizeValue(key: "New Appointments")
        // Do any additional setup after loading the view.
        textViewAppointment.delegate = self
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        let lat = LocationManager.sharedInstance.latitude
        let lon = LocationManager.sharedInstance.longitude
        let countryCode = Locale.current.regionCode
        countryPicker.setCountry(countryCode!)
        countryPicker.setLocale("EN")
        reservationType = 1
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            textViewAppointment.textAlignment = .right
              backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
         noteLabel.textAlignment = .right
        }
    }
    
    @IBAction func buttonReservationTypeClicked(_ sender: UIButton) {
        if sender.titleLabel?.text == "New Visit"{
            self.buttonNew.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
            self.buttonConsult.backgroundColor = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1.0)
            self.buttonConsult.setTitleColor(.black, for: .normal)
            self.buttonNew.setTitleColor(.white, for: .normal)
            reservationType = 1
        }else{
            self.buttonConsult.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1.0)
            self.buttonNew.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
            self.buttonNew.setTitleColor(.black, for: .normal)
            self.buttonConsult.setTitleColor(.white, for: .normal)
            reservationType = 2
        }
    }
    
    //countaryPicker Delegate
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.selectedFlag = flag
        self.code = phoneCode
        tableViewNewAppointment.reloadData()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == localizeValue(key: "Write your note here…."){
            textViewAppointment.text = ""
        }
        textViewAppointment.textColor = UIColor.black
    }
    
    func textViewDidChange(_ textView: UITextView) {
        note = textView.text
    }

    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSelectClicked(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        guard let fName = fName,!fName.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter first name"), target: self, handler: nil)
            return
        }
        
        guard let lname = lname,!lname.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter last name"), target: self, handler: nil)
            return
        }
        
        guard let phone = phone,!phone.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter phone number"), target: self, handler: nil)
            return
        }
        
        guard let selectedDate = selectedDate,!selectedDate.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Select date and time"), target: self, handler: nil)
            return
        }
        

        guard let clinicID = clinicID,!clinicID.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please select Clinic"), target: self, handler: nil)
            return
        }

        if textViewAppointment.text == "" || textViewAppointment.text == localizeValue(key: "Write your note here….") {
            note = ""
        }
        if self.code != nil {
             self.addManualAppointment()
        }
       
    }
    @IBAction func buttonSelectCountaryClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    @IBAction func buttonCancelClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    @IBAction func buttonDoneClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if textField.tag == 1{
            fName = textField.text
        }else if textField.tag == 2{
            lname = textField.text
        }else if textField.tag == 3{
            phone = textField.text
        }else if textField.tag == 4{
            email = textField.text
        }
    }
    
    func addManualAppointment() {
        
      //   let countryCode = (self.code ?? "").replacingOccurrences(of: "+", with: "")
        if AppInstance.shared.userType == 0{
            let service = BusinessService()
            service.addManualAppointment(with: String(describing: AppInstance.shared.user!.id!),countrycode:String(self.code!.dropFirst()), patient_mobile: CommonClass.checkAndEditPhoneNumberFor(phoneNumber: phone!), availability_id: self.appointmentId, appointment_date: selectedDate!, visit_type: "1", appointment_type: String(describing: reservationType), notes: note!, book_by: "2", assistant_id: "", set_time: dateTime!, patient_email: email!, lastname: lname!, firstname: fName!, clinic_id: clinicID!,gender: self.gender, target: self) { (response) in
                if response == true{
                    NotificationCenter.default.post(name: NSNotification.Name("AppointmentSuccess"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }else if AppInstance.shared.userType == 1{
            let service = BusinessService()
            service.addManualAppointment(with: String(describing: AppInstance.shared.user!.dr_id!),countrycode: String(self.code!.dropFirst()), patient_mobile: CommonClass.checkAndEditPhoneNumberFor(phoneNumber: phone!), availability_id: self.appointmentId, appointment_date: selectedDate!, visit_type: "1", appointment_type: String(describing: reservationType), notes: note!, book_by: "1", assistant_id: String(describing: AppInstance.shared.user!.id!), set_time: dateTime!, patient_email: email!, lastname: lname!, firstname: fName!, clinic_id: clinicID!, gender: self.gender, target: self) { (response) in
                if response == true{
                    NotificationCenter.default.post(name: NSNotification.Name("AssistantAppointmentSuccess"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension AddNewAppointmientsViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewAppointmientCell
            cell.tfFirstname.tag = 1
            cell.tfLastName.tag = 2
            cell.firstNameLabel.text = localizeValue(key: "First name")
            cell.tfFirstname.placeholder = localizeValue(key: "First name")
            cell.lastNameLabel.text = localizeValue(key: "Last name")
            cell.tfLastName.placeholder = localizeValue(key: "Last name")
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.tfFirstname.textAlignment = .right
                cell.tfLastName.textAlignment = .right
                cell.firstNameLabel.textAlignment = .right
                 cell.lastNameLabel.textAlignment = .right
            }
            
            cell.tfFirstname.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            cell.tfLastName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as! AddNewAppointmientCell
            
         
            cell.tfPhone.tag = 3
            if #available(iOS 10.0, *) {
                cell.tfPhone.keyboardType = .asciiCapableNumberPad
            } else {
                // Fallback on earlier versions
            }
            cell.imageViewFlag.image = self.selectedFlag
            cell.labelCode.text = self.code
            cell.countryCodeLbl.text = localizeValue(key: "Country Code")
            cell.phoneNumberLbl.text = localizeValue(key: "Phone Number")
            cell.tfPhone.placeholder = localizeValue(key: "Phone Number")
            if #available(iOS 10.0, *) {
                cell.tfPhone.keyboardType = .asciiCapableNumberPad
            } else {
                // Fallback on earlier versions
            }
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
              
                cell.phoneNumberLbl.textAlignment = .right
                cell.countryCodeLbl.textAlignment = .right
                
                
                cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                cell.tfPhone.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                cell.labelCode.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                cell.imageViewFlag.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
             
                
            }
            cell.tfPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAppointmientCell", for: indexPath) as! AddNewAppointmientCell
            cell.titleLabel.text = localizeValue(key: "Email address")
            cell.textFieldName.placeholder = localizeValue(key: "Email address")
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.textFieldName.textAlignment = .right
                cell.titleLabel.textAlignment = .right
            }
            cell.textFieldName.keyboardType = .emailAddress
            cell.textFieldName.tag = 4
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAppointmientCell", for: indexPath) as! AddNewAppointmientCell
            cell.titleLabel.text = localizeValue(key: "Hospital / Clinic name")
            cell.textFieldName.placeholder =  localizeValue(key: "Hospital / Clinic name")
            cell.textFieldName.tag = 5
            
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.textFieldName.textAlignment = .right
                cell.titleLabel.textAlignment = .right
            }
            if clinicName != nil{
                cell.textFieldName.text = clinicName
            }
            cell.textFieldName.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
                //Write code for picker view here
                self.dateTime = nil
                self.view.endEditing(true)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ClinicListViewController") as! ClinicListViewController
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAppointmientCell", for: indexPath) as! AddNewAppointmientCell
            cell.titleLabel.text = localizeValue(key: "Date & Time")
            cell.textFieldName.placeholder = localizeValue(key: "Date & Time")
            if self.dateTime != nil{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date = dateFormatter.date(from: self.selectedDate!)
                dateFormatter.dateFormat = "MMM d, yyyy"
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    dateFormatter.locale =  Locale(identifier: "ar")
                }
               
                let result = dateFormatter.string(from: date!)
                if self.dateTime == "" {
                    cell.textFieldName.text = "\(String(describing: result))"
                } else {
                    cell.textFieldName.text = "\(String(describing: result)): \(String(describing: self.selectedDateTime))"
                }
            } else {
                cell.textFieldName.text = ""
            }
            
            cell.textFieldName.tag = 6
            cell.textFieldName.isUserInteractionEnabled = false
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.textFieldName.textAlignment = .right
                cell.titleLabel.textAlignment = .right
            }
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAppointmientGenderCell", for: indexPath) as! AddNewAppointmientCell
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.genderLabel.textAlignment = .right
               
            }
            cell.btn_male.setTitle(localizeValue(key: "Male"), for: .normal)
            cell.btn_female.setTitle(localizeValue(key: "Female"), for: .normal)
            
            cell.genderLabel.text = localizeValue(key: "Gender")
             cell.updateGender((cell.gender == localizeValue(key: "Male")) ? cell.btn_male : (cell.gender == "Male".localize()) ? cell.btn_female : nil)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 75.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4{
            if clinicID != nil{
                let vc = storyboard?.instantiateViewController(withIdentifier: "SetAppointmentDate_TimeViewController") as! SetAppointmentDate_TimeViewController
                vc.clinicId = clinicID
                vc.clin_data = clinicData
                vc.delegate = self
                vc.isfromReject = false
                vc.clinicType = self.clinicType
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                self.showAlert(with: localizeValue(key: "Please select Clinic"))
            }
        }
    }
}
extension AddNewAppointmientsViewController: selectedClinicDelegate, TimeSelectedDelegate {

    
    func selectedTime(time: String, date: String, appointmentId: String) {
        selectedDate = date
        
        
        var start_time = time
        let last = String(start_time.characters.suffix(2))
        var new_start_time = start_time.dropLast(2)
        if last != "" {
            if last == "AM" || last == "am"{
                new_start_time = new_start_time + localizeValue(key: "AM")
            }else{
                new_start_time = new_start_time + localizeValue(key: "PM")
            }
        }
       
        
        dateTime = time
        selectedDateTime = "\(String(describing: (new_start_time ?? "")))"
        self.appointmentId = appointmentId
        tableViewNewAppointment.reloadData()
    }
    
    func selectedClinic(name: String, id: Int, ClinicData: ClinicData, clinicType: Int)  {
        if   LocalizationSystem.sharedInstance.getLanguage() == "ar" { clinicName = ClinicData.clinic_name_ar }
        else { clinicName = name}
       
        clinicID = String(describing: id)
        clinicData = ClinicData
        self.clinicType = clinicType
        tableViewNewAppointment.reloadData()
    }
}
