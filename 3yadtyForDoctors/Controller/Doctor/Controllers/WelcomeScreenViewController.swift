//
//  WelcomeScreenViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 20/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import SlideMenuController

class WelcomeScreenViewController: UIViewController {

  //  let arrTitle = ["Doctor".localize(), "Doctor Assistant".localize()]
    
    let arrTitle = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "Doctor", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Doctor Assistant", comment: "")]
    var selectIndex: Int? = (AppInstance.shared.userType != nil) ? AppInstance.shared.userType! : 0
    @IBOutlet weak var buttonSelect: RoundButton!
    @IBOutlet weak var tableWelcome: UITableView!
    
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
     //MARK:- LIFE CYCLE
    override func viewDidLoad() {
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Welcome to CliniDo", comment: "")
         subTitleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Please choose account type", comment: "")
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- ACTIONS
    
    
    @IBAction func ButtonSelectClicked(_ sender: Any) {
        
        if selectIndex == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InformationOneViewController") as! InformationOneViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{

            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssistantInfoOneViewController") as! AssistantInfoOneViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func buttonMenuClicked(_ sender: Any) {
        
    }
}

extension WelcomeScreenViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WelcomeScreenCell", for: indexPath) as! WelcomeScreenCell
        cell.selectionStyle = .none
        cell.lableTitle.text = arrTitle[indexPath.row]
        if selectIndex == indexPath.row {
            cell.buttonRadio.isSelected = true
        }else{
            cell.buttonRadio.isSelected = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if indexPath.row == 0 {
            selectIndex = 0
            AppInstance.shared.userType = 0
        }else{
            selectIndex = 1
            AppInstance.shared.userType = 1
        }
        
        if selectIndex != nil{
            buttonSelect.isEnabled = true
            buttonSelect.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
        }else{
            buttonSelect.isEnabled = false
            buttonSelect.backgroundColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
        }
         tableWelcome.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
    
}
