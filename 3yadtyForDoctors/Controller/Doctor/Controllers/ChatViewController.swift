//
//  ChatListViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 27/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class ChatViewController: JSQMessagesViewController {
    
    //JSQMessage Array
    var messages = [JSQMessage]()
    var selectedUser: Person?
    var entryType: String = ""
    
    //Change color
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1))
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.gray)
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //set sender id
        senderId = String(describing: AppInstance.shared.user!.id!)
        senderDisplayName = self.selectedUser?.userName ?? ""
        
        self.title = "\(senderDisplayName!)"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)
        if self.entryType == "AssistantSideMenu"{
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "abBackWhite"), style: .plain, target: self, action: #selector(self.actionOnBack))
        }
        inputToolbar.contentView.leftBarButtonItem = nil
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        //get all chat rooms
        getAllChatRoom()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = true
       self.navigationController?.navigationBar.isTranslucent = true
       self.navigationController?.navigationBar.barTintColor = .clear
    }
    
    @objc private func actionOnBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)

//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
//        self.navigationController!.pushViewController(VC, animated: false)
    }
    
    //get all chat room for filter
    func getAllChatRoom() {
        
        let roomType1 = "\(String(describing: AppInstance.shared.user?.id ?? 0))_\(String(describing: selectedUser?.fcmUserId ?? ""))"
        let roomType2 = "\(String(describing: selectedUser?.fcmUserId ?? ""))_\(String(describing: AppInstance.shared.user?.id ?? 0))"
        self.showLoader(with: "Loading...".localize())
        if CommonClass.isLiveServer() {
            Database.database().reference().child("Chat_Room").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                
                self.hideLoader()
                if snapshot.hasChild(roomType1){
                    
                    Database.database().reference().child("Chat_Room").child(roomType1).observe(.value) { (snapshot) in
                        self.messages.removeAll()
                        for child in snapshot.children {
                            
                            let snap = child as! DataSnapshot
                            let val = Message.init(dictionary: snap.value! as! NSDictionary)
                            if let message = JSQMessage(senderId: val?.senderUid, displayName: "", text: val?.message){
                                self.messages.append(message)
                                self.finishReceivingMessage()
                            }
                        }
                    }
                }else if snapshot.hasChild(roomType2){
                    
                    Database.database().reference().child("Chat_Room").child(roomType2).observe(.value) { (snapshot) in
                        self.messages.removeAll()
                        for child in snapshot.children {
                            let snap = child as! DataSnapshot
                            let val = Message.init(dictionary: snap.value! as! NSDictionary)
                            if let message = JSQMessage(senderId: val?.senderUid, displayName: "", text: val?.message){
                                self.messages.append(message)
                                self.finishReceivingMessage()
                            }
                        }
                    }
                    
                }else{
                    
                }
            }, withCancel: nil)
        } else {
            Database.database().reference().child("Test_Server_Chat_Room").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                
                self.hideLoader()
                if snapshot.hasChild(roomType1){
                    
                    Database.database().reference().child("Test_Server_Chat_Room").child(roomType1).observe(.value) { (snapshot) in
                        self.messages.removeAll()
                        for child in snapshot.children {
                            
                            let snap = child as! DataSnapshot
                            let val = Message.init(dictionary: snap.value! as! NSDictionary)
                            if let message = JSQMessage(senderId: val?.senderUid, displayName: "", text: val?.message){
                                self.messages.append(message)
                                self.finishReceivingMessage()
                            }
                        }
                    }
                }else if snapshot.hasChild(roomType2){
                    
                    Database.database().reference().child("Test_Server_Chat_Room").child(roomType2).observe(.value) { (snapshot) in
                        self.messages.removeAll()
                        for child in snapshot.children {
                            let snap = child as! DataSnapshot
                            let val = Message.init(dictionary: snap.value! as! NSDictionary)
                            if let message = JSQMessage(senderId: val?.senderUid, displayName: "", text: val?.message){
                                self.messages.append(message)
                                self.finishReceivingMessage()
                            }
                        }
                    }
                    
                }else{
                    
                }
            }, withCancel: nil)
        }
    }
    
    func sendMessage(text: String) {
        
        let currentTimeStamp = String(Date().toMillis())
        let name = "\(String(describing: AppInstance.shared.user!.firstname!)) \(String(describing: AppInstance.shared.user!.lastname!))"
//        let message = ["sender": name,"receiver": selectedUser!.userName!,"senderUid": String(describing: AppInstance.shared.user!.id!),"receiverUid": selectedUser!.fcmUserId!,"message": text,"timestamp": currentTimeStamp] as [String : Any]
//        var ref = Constants.refs.databaseChats.childByAutoId()
//        if !CommonClass.isLiveServer() {
//            ref = Constants.refs.testDatabaseChats.childByAutoId()
//        }
//        ref.setValue(message)
        self.messages.append(JSQMessage.init(senderId: "\(AppInstance.shared.user!.id!)", displayName: name, text: text))
        finishSendingMessage()
        self.updateMessage(text)
    }
    
    private func updateMessage(_ text: String){
        
        DispatchQueue.global(qos: .background).async {
            
            let currentTimeStamp = String(Date().toMillis())
            let roomType1 = "\(String(describing: AppInstance.shared.user!.id!))_\(String(describing: self.selectedUser!.fcmUserId!))"
            let roomType2 = "\(String(describing: self.selectedUser!.fcmUserId!))_\(String(describing: AppInstance.shared.user!.id!))"
            let name = "\(String(describing: AppInstance.shared.user!.firstname!)) \(String(describing: AppInstance.shared.user!.lastname!))"
            let message = ["sender": name,"receiver": self.selectedUser!.userName!,"senderUid": String(describing: AppInstance.shared.user!.id!),"receiverUid": self.selectedUser!.fcmUserId!,"message": text,"timestamp": currentTimeStamp] as [String : Any]
            let userDetailDict = ["email": self.selectedUser!.email!,"fcmToken": self.selectedUser!.fcmToken!,"fcmUserId": self.selectedUser!.fcmUserId!,"image": self.selectedUser!.image!,"lastMessage": self.selectedUser!.lastMessage!,"userName": self.selectedUser!.userName!, "lastMessageTime": currentTimeStamp] as [String : Any]
            let myDetailDict = ["email": AppInstance.shared.user!.email!,"fcmToken": self.selectedUser!.fcmToken!,"fcmUserId": String(describing: AppInstance.shared.user!.id!),"image": AppInstance.shared.user!.image!,"lastMessage": text,"userName": "\(AppInstance.shared.user!.firstname!) \(AppInstance.shared.user!.lastname ?? "")" , "lastMessageTime": currentTimeStamp] as [String : Any]
            
            //get All ChatRoom
            let referenceDatabase = Database.database().reference()
            if CommonClass.isLiveServer() {
                referenceDatabase.child("Chat_Room").observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.value! is NSNull{
                        
                        //if no previous it will set value
                        referenceDatabase.child("Chat_Room").child(roomType1).child(String(describing: currentTimeStamp)).setValue(message)
                        //set new chat in user detail in sende id
                        referenceDatabase.child("UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).setValue(userDetailDict)
                        //set new chat in user detail in reciever Id
                        referenceDatabase.child("UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).setValue(myDetailDict)
                        //                self.getAllChatRoom()
                        //                    self.finishSendingMessage()
                    }
                    
                    //check if chatRoom contain roomType
                    if snapshot.hasChild(roomType1){
                        //add message to chat room with roomType
                        referenceDatabase.child("Chat_Room").child(roomType1).child(String(describing: currentTimeStamp)).setValue(message)
                        //add last message in UserDetails list in sender id
                        
                        referenceDatabase.child("UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessage").setValue(text)
                        //add last message in UserDetails list in reciever id
                        referenceDatabase.child("UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessage").setValue(text)
                        
                        //add lastMessageTime in UserDetails list in reciever id
                        referenceDatabase.child("UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessageTime").setValue(currentTimeStamp)
                        referenceDatabase.child("UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessageTime").setValue(currentTimeStamp)
                        
                        //                    self.finishSendingMessage()
                    }else if snapshot.hasChild(roomType2){
                        
                        referenceDatabase.child("Chat_Room").child(roomType2).child(String(describing: currentTimeStamp)).setValue(message)
                        //add last message in UserDetails list in sender id
                        referenceDatabase.child("UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessage").setValue(text)
                        //add last message in UserDetails list in reciever id
                        referenceDatabase.child("UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessage").setValue(text)
                        
                        //add lastMessageTime in UserDetails list in reciever id
                        referenceDatabase.child("UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessageTime").setValue(currentTimeStamp)
                        referenceDatabase.child("UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessageTime").setValue(currentTimeStamp)
                        //                    self.finishSendingMessage()
                    }else{
                        //if no previous it will set value
                        referenceDatabase.child("Chat_Room").child(roomType1).child(String(describing: currentTimeStamp)).setValue(message)
                        //set new chat in user detail in sende id
                        referenceDatabase.child("UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).setValue(userDetailDict)
                        //set new chat in user detail in reciever Id
                        referenceDatabase.child("UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).setValue(myDetailDict)
                        //                self.getAllChatRoom()
                        //                    self.finishSendingMessage()
                    }
                }, withCancel: nil)
            } else {
                referenceDatabase.child("Test_Server_Chat_Room").observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.value! is NSNull{
                        
                        //if no previous it will set value
                        referenceDatabase.child("Test_Server_Chat_Room").child(roomType1).child(String(describing: currentTimeStamp)).setValue(message)
                        //set new chat in user detail in sende id
                        referenceDatabase.child("Test_Server_UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).setValue(userDetailDict)
                        //set new chat in user detail in reciever Id
                        referenceDatabase.child("Test_Server_UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).setValue(myDetailDict)
                        //                self.getAllChatRoom()
                        //                    self.finishSendingMessage()
                    }
                    
                    //check if chatRoom contain roomType
                    if snapshot.hasChild(roomType1){
                        //add message to chat room with roomType
                        referenceDatabase.child("Test_Server_Chat_Room").child(roomType1).child(String(describing: currentTimeStamp)).setValue(message)
                        //add last message in UserDetails list in sender id
                        
                        referenceDatabase.child("Test_Server_UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessage").setValue(text)
                        //add last message in UserDetails list in reciever id
                        referenceDatabase.child("Test_Server_UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessage").setValue(text)
                        
                        //add lastMessageTime in UserDetails list in reciever id
                        referenceDatabase.child("Test_Server_UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessageTime").setValue(currentTimeStamp)
                        referenceDatabase.child("Test_Server_UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessageTime").setValue(currentTimeStamp)
                        
                        //                    self.finishSendingMessage()
                    }else if snapshot.hasChild(roomType2){
                        
                        referenceDatabase.child("Test_Server_Chat_Room").child(roomType2).child(String(describing: currentTimeStamp)).setValue(message)
                        //add last message in UserDetails list in sender id
                        referenceDatabase.child("Test_Server_UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessage").setValue(text)
                        //add last message in UserDetails list in reciever id
                        referenceDatabase.child("Test_Server_UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessage").setValue(text)
                        
                        //add lastMessageTime in UserDetails list in reciever id
                        referenceDatabase.child("Test_Server_UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).child("lastMessageTime").setValue(currentTimeStamp)
                        referenceDatabase.child("Test_Server_UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).child("lastMessageTime").setValue(currentTimeStamp)
                        //                    self.finishSendingMessage()
                    }else{
                        //if no previous it will set value
                        referenceDatabase.child("Test_Server_Chat_Room").child(roomType1).child(String(describing: currentTimeStamp)).setValue(message)
                        //set new chat in user detail in sende id
                        referenceDatabase.child("Test_Server_UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").child(self.selectedUser!.fcmUserId!).setValue(userDetailDict)
                        //set new chat in user detail in reciever Id
                        referenceDatabase.child("Test_Server_UserDetails").child(self.selectedUser!.fcmUserId!).child("chat_user").child(String(describing: AppInstance.shared.user!.id!)).setValue(myDetailDict)
                        //                self.getAllChatRoom()
                        //                    self.finishSendingMessage()
                    }
                }, withCancel: nil)
            }
            
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        return nil
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        return messages[indexPath.item].senderId == senderId ? nil : NSAttributedString(string: messages[indexPath.item].senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!){
        
        self.sendMessage(text: text)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
