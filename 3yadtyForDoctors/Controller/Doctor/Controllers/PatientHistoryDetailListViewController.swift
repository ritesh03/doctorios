//
//  PatientHistoryDetailListViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PatientHistoryDetailListCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDateTime: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
}

protocol PatientHistoryDetailListViewControllerDelegate: class {
    func setPrescriptionListInParentView(data: [PrescriptionData])
}

class PatientHistoryDetailListViewController: BaseViewControler, IndicatorInfoProvider, UITextViewDelegate {

    var prescriptionData: [PrescriptionData]?
    weak var delegate: PatientHistoryDetailListViewControllerDelegate?
//    var patient_data : DataAppointmentList?
    
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet var errorView: UIView!
    
    @IBOutlet weak var tablePrescription: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        errorDescriptionLabel.text = localizeValue(key: "You don't have any prescriptions")
        errorTitleLabel.text = localizeValue(key: "Prescription list is empty")
//         getPrescriptionDetail()
        // Do any additional setup after loading the view.
        //if AppInstance.shared.selectedDr != nil{getPrescriptionDetail()}
    }
    
    override func viewWillAppear(_ animated: Bool) {
         getPrescriptionDetail()
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "Prescription"))
    }
    
    func getPrescriptionDetail() {
        let info = AppInstance.shared.selectedDr
        let service = BusinessService()
        service.getPrescriptionDetail(with: String(describing: AppInstance.shared.user!.id!), appointment_id: String(describing: info!.id!), patient_id: String(describing: info!.patient_info!.id!), target: self) { (response) in
            if let list = response {
//                print(list)
                self.errorView.isHidden = true
                self.prescriptionData = list.data
                self.delegate?.setPrescriptionListInParentView(data: self.prescriptionData!)
                self.tablePrescription.reloadData()
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetHistory"), object: nil, userInfo: ["casestudy": list.data?.first?.patient_case ?? ""])


            } else {
                self.errorView.isHidden = false
            }
        }
    }
}

extension PatientHistoryDetailListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.prescriptionData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientHistoryDetailListCell") as! PatientHistoryDetailListCell
        
        let dateFormatter = DateFormatter()
        let strDate = self.prescriptionData?[indexPath.row].createdat
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: strDate!)
        dateFormatter.dateFormat = "MMM d, yyyy hh:mm a"
        let str = dateFormatter.string(from: date!)
        cell.labelTitle.text = localizeValue(key: "Prescription created")
        cell.labelDateTime.text = UTCToLocal(date: (self.prescriptionData?[indexPath.row].createdat)!)
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(deletePrescription(_:)), for: UIControlEvents.touchUpInside)
        cell.btnDelete.isHidden = (AppInstance.shared.userType != 0)
        cell.btnDelete.setTitle(localizeValue(key: "Delete"), for: .normal)
        return cell
        
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            dateFormatter.locale = Locale(identifier: "ar")
        }else{
             dateFormatter.locale = Locale(identifier: "en")
        }
        dateFormatter.dateFormat = "d MMM, yyyy | hh:mm a"
        
        return dateFormatter.string(from: dt!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientPrescriptionViewController") as! PatientPrescriptionViewController
//        vc.patient_data = patient_data
        
        if self.prescriptionData?[indexPath.row].prescription_type == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrescriptionImageViewController") as! PrescriptionImageViewController
            vc.imageUrl = (self.prescriptionData?[indexPath.row].image)!
            self.present(vc, animated: true, completion: nil)
        }else{
            vc.prescriptionType = self.prescriptionData?[indexPath.row].prescription_type
            if AppInstance.shared.isShowPlus == true{
                vc.selectedPrescription =  self.prescriptionData?[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                if (self.prescriptionData?[indexPath.row].medicine?.count)! > 0 {
                    vc.selectedPrescription =  self.prescriptionData?[indexPath.row]
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
       
    }
    
    @objc func deletePrescription(_ sender:UIButton) {
        
        let alert = UIAlertController(title: localizeValue(key: "Delete prescription"), message: localizeValue(key: "Do you want to delete this prescription?"), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: localizeValue(key: "No"), style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
        }))
        
        alert.addAction(UIAlertAction(title: localizeValue(key: "Yes"), style: .default , handler:{ (UIAlertAction)in
            
            let pres_id = self.prescriptionData?[sender.tag].id!
            
            let service = BusinessService()
            service.deletePrescription(with: String(describing: AppInstance.shared.user!.id!), pr_id: "\(pres_id!)", target: self) { (response) in
                
                if response {
                    
                    self.prescriptionData = self.prescriptionData?.filter { $0.id != pres_id }
                    self.tablePrescription.reloadData()
                } else {
                    self.errorView.isHidden = false
                }
                
            }
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
}
