//
//  NotificationViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 26/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit


class NotificationViewController: BaseViewControler {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tableNotification: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyDescriptionLabel: UILabel!
    
    
    @IBOutlet weak var errorImageView: UIImageView!
    
    var arrNotification = [NotificationData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
         titleLabel.text = localizeValue(key: "Notifications")
        //menu Button
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.image = UIImage(named: "menu_right")!.withRenderingMode(.alwaysOriginal)
            emptyTitle.text = localizeValue(key: "Notification list is empty")
            emptyDescriptionLabel.text = localizeValue(key: "You don’t have any notifications")
         
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                 menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            else{
                 menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }
            
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
         self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        self.errorView.isHidden = true
//        tableNotification.estimatedRowHeight = YourTableViewHeight
//        tableNotification.rowHeight = UITableViewAutomaticDimension
        getNotification()
    }
    
    func getNotification(){
        let service = BusinessService()
        service.getNotification(with: String(describing: AppInstance.shared.user!.id!), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
            }
            if let list = response {
                self.arrNotification = list.data!
                if (self.arrNotification.count) > 0{
                    self.errorView.isHidden = true
                }else{
                    self.errorView.isHidden = false
                }
            }
            self.tableNotification.reloadData()
        }
    }
}

extension NotificationViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
          if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             cell.labelMessage.text = arrNotification[indexPath.row].message_ar
            cell.labelMessage.textAlignment = .right
          }else{
             cell.labelMessage.text = arrNotification[indexPath.row].message
        }
       
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! NotificationCell
//        return headerView
//
//    }

//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    
}

