//
//  PaymentOptionsViewController.swift
//  3yadtyForDoctors
//
//  Created by Vivek Dogra on 16/10/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

class PaymentOptionsViewController: BaseViewControler {
    
    
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var kioskBtn: UIButton!
    @IBOutlet weak var creditCardbtn: UIButton!
    
    var amount = ""
    var invoiceId = 0
    var paymentType = ""
    var kishokNumber = ""
   
    var onCreditCard : ((String) -> ())?
     var onKiosak : ((String) -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()
        creditCardbtn.setTitle(localizeValue(key: "Credit Card/Debit Card"), for: .normal)
        kioskBtn.setTitle(localizeValue(key: "Masary/Aman Kiosk"), for: .normal)
        
        kioskBtn.layer.cornerRadius = 17
        kioskBtn.layer.borderWidth = 1
        kioskBtn.layer.borderColor = UIColor(red: 40/255.0, green: 170/255.0, blue: 225/255.0, alpha: 1).cgColor
        
        creditCardbtn.layer.cornerRadius = 17
        creditCardbtn.layer.borderWidth = 1
        creditCardbtn.layer.borderColor = UIColor(red: 40/255.0, green: 170/255.0, blue: 225/255.0, alpha: 1).cgColor
        
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
       
      
    }
    
    private func proceedToPayment(complition:@escaping (String) -> ()) {
        let service = BusinessService()
        let parameters = ["user_id":AppInstance.shared.user!.id!, "amount":self.amount, "payment_source": self.paymentType,"invoice_id": self.invoiceId] as [String : Any]
        service.paymentAuthentication(with: parameters, target: self) { (result) in
            if result == nil {
                
                CommonClass.showAlert(self, title: "Something went wrong".localize())
                
                
            } else {
                if self.paymentType == "3"{
                    print(result!)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "KiosakReferenceNumberViewController") as! KiosakReferenceNumberViewController
                    vc.referenceNumber = "Your payment reference no. is \((result!))"
                     vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                }else{
                    complition(result! as! String)
                }
                
            }
        }
    }
    
    @IBAction func creditCardBtnTap(_ sender: Any) {
        self.onCreditCard!("1")
        self.dismiss(animated: true, completion: nil)
        
        self.paymentType = "1"
        self.proceedToPayment() { url in
            let webView =  CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.strHeader = "Payment".localize()
            webView.strUrl = url
            
            //  self.navigationController?.pushViewController(webView, animated: true)
            self.present(webView, animated: true, completion: nil)
            
            
        }
        
    }
    @IBAction func kioskBtnTap(_ sender: Any) {
        self.onKiosak!("3")
         self.dismiss(animated: true, completion: nil)
     
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != dialogView {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
