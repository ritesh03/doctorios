//
//  KiosakReferenceNumberViewController.swift
//  3yadtyForDoctors
//
//  Created by Vivek Dogra on 16/10/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

class KiosakReferenceNumberViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var numberLabel: UILabel!
    var referenceNumber = ""
    var callBack: (() -> Void)? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        numberLabel.text = referenceNumber
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
          callBack!()
    }
    
    @IBAction func closeBtnTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != mainView {
            self.dismiss(animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
