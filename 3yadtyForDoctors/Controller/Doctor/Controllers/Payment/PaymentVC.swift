//
//  PaymentVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 02/04/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit



class PaymentsTableViewCell : UITableViewCell {
    
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var clinicNameLabel: UILabel!
}

class PaymentVC: BaseViewControler {
    
    //MARK:- Outlets
    
   
    @IBOutlet weak var payNowBtn: RoundButton!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var netLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var titleHeaderLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var referenceNumberLabel: UILabel!
    @IBOutlet weak var referenceStackView: UIStackView!
    @IBOutlet weak var payementListTableView: UITableView!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var taxRateLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var netAmountLabel: UILabel!
    @IBOutlet weak var noPaymentView: UIView!
    @IBOutlet weak var constraint_height: NSLayoutConstraint!
    
    //MARK:- Variabeles
    var initialSetupViewController: PTFWInitialSetupViewController!
    private var totalPayableAmount: Int!
    var paymentResponse = PaymentResponseModel()
    var amount = ""
    var kshyokNumber = ""
      var paymentType = ""
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        payementListTableView.tableFooterView = UIView()
        referenceLabel.text = localizeValue(key: "Your payment reference no. is")
        dueLabel.text = localizeValue(key: "Due date")
        netLabel.text = localizeValue(key: "Net amount")
         self.taxRateLabel.text = localizeValue(key: "VAT 14%")
        totalLabel.text = localizeValue(key: "Total amount")
        payNowBtn.setTitle(localizeValue(key: "Pay now"), for: .normal)
        payementListTableView.delegate = self
        payementListTableView.dataSource = self
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.constraint_height.constant += (UIApplication.shared.statusBarFrame.size.height)
           self.getPaymentDetails()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            referenceLabel.textAlignment = .right
            dueLabel.textAlignment = .right
            dueDateLabel.textAlignment = .left
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            totalAmountLabel.textAlignment = .left
            taxLabel.textAlignment = .left
            netAmountLabel.textAlignment = .left
            
        }
        titleHeaderLabel.text = localizeValue(key: "Payment")
        errorLabel.text = localizeValue(key: "No payment due")
       
    }
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    //MARK:- Functions
    
    private func getPaymentDetails() {
        let service = BusinessService()
        service.getPaymentDetails(target: self) { (payments) in
            var totalFees = 0
            if let data = payments!.data {
                if payments!.data!.clinic_amount!.count > 0 {
                    self.paymentResponse = payments!
                    self.payementListTableView.reloadData()
                    self.noPaymentView.isHidden = true
                    let date = payments!.data?.due_date
                    let convertedDate = CommonClass.convertFormatOfDate(date: date!, originalFormat: "yyyy-MM-dd", destinationFormat: "dd MMM, yyyy")
                    self.dueDateLabel.text = convertedDate
                    let amount = Double((self.paymentResponse.data?.amount)!)
                    self.totalAmountLabel.text = String(format: "%.2f", amount!) + " EGP"
                    let amountWithVAT =  Double((self.paymentResponse.data?.amount_with_vat)!)
                    self.amount = String(format: "%.2f", amountWithVAT!)
                    self.netAmountLabel.text = String(format: "%.2f", amountWithVAT!) + " EGP"
                    let vat = Double((self.paymentResponse.data?.vat)!)
                    let vatOnAmount = (amount! * vat!) / 100
                    self.taxLabel.text = String(format: "%.2f", vatOnAmount) + " EGP"
                    if self.paymentResponse.data?.kiosk_scan_code == "" {
                        self.referenceStackView.isHidden = true
                    }else{
                        self.referenceStackView.isHidden = false
                        self.referenceNumberLabel.text = self.paymentResponse.data?.kiosk_scan_code
                    }
                }
            }
           else {
                self.noPaymentView.isHidden = false
            }
        }
    }
    
    private func proceedToPayment(complition:@escaping (String) -> ()) {
        let service = BusinessService()
        
        let newAmount = (Float(self.amount)! * Float(100))
        let parameters = ["user_id":AppInstance.shared.user!.id!, "amount": "\(newAmount)", "payment_source": self.paymentType,"invoice_id": paymentResponse.data!.id!] as [String : Any]
        service.paymentAuthentication(with: parameters, target: self) { (result) in
            if result == nil {
                
                CommonClass.showAlert(self, title: "Something went wrong".localize())
                
                
            } else {
                if self.paymentType == "3"{
                    print(result!)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "KiosakReferenceNumberViewController") as! KiosakReferenceNumberViewController
                    vc.referenceNumber = "Your payment reference no. is \((result!))"
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.callBack = {
                        self.getPaymentDetails()
                    }
                    self.present(vc, animated: true, completion: nil)
                }else{
                    complition(result! as! String)
                }
                
            }
        }
    }
    
    private func setViewFromData(totalFees: Int, date: String) {
        self.totalAmountLabel.text = "\(roundOffToThreePlaces(number: Double(totalFees))) EGP"
        let taxedAmount: Double = ((Double(totalFees) * 14) / 100)
        self.taxLabel.text = "\(roundOffToThreePlaces(number: taxedAmount)) EGP"
        let netAmount: Double = Double(totalFees) + taxedAmount
        self.totalPayableAmount = Int(netAmount)
        self.netAmountLabel.text = "\(roundOffToThreePlaces(number: netAmount))" + " EGP"
       
    }
    
    private func roundOffToThreePlaces(number: Double) -> String {
        //  change the number before f to add decimal places
        let decimalNumber = String(format:"%.2f", number)
        return decimalNumber
    }
    
    // MARK: Close SDK Event
    private func handleBackButtonTapEvent() {
        self.initialSetupViewController.willMove(toParentViewController: self)
        self.initialSetupViewController.view.removeFromSuperview()
        self.initialSetupViewController.removeFromParentViewController()
    }
    
    // MARK: Close Response window Event
    private func handleCloseResponseViewTapEvent() {
        //self.launcherView.responseView.isHidden = true
    }
    
    //MARK:- Action Outlets
    @IBAction func actionOnBack(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    @IBAction func actionOnPay(_ sender: Any) {
        
      
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentOptionsViewController") as! PaymentOptionsViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.amount = self.amount
        vc.invoiceId = paymentResponse.data!.id!
        vc.kishokNumber = paymentResponse.data!.kiosk_scan_code!
      
        vc.onCreditCard = { data in
            print(data)
            self.paymentType = data
            self.proceedToPayment() { url in
                let webView =  CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webView.strHeader = "Payment".localize()
                webView.strUrl = url
                //  self.navigationController?.pushViewController(webView, animated: true)
                self.present(webView, animated: true, completion: nil)
                
                
            }
            
        }
        
        vc.onKiosak = { data in
            
            
            if self.paymentResponse.data!.kiosk_scan_code! == "" {
                
                
                self.paymentType = data
                self.proceedToPayment() { url in
                    let webView =  CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                    webView.strHeader = "Payment".localize()
                    webView.strUrl = url
                    //  self.navigationController?.pushViewController(webView, animated: true)
                    self.present(webView, animated: true, completion: nil)
                    
                }
            }
            else{
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "KiosakReferenceNumberViewController") as! KiosakReferenceNumberViewController
                    vc.referenceNumber = "Your payment reference no. is \(self.paymentResponse.data!.kiosk_scan_code!)"
                    vc.callBack = {
                        self.getPaymentDetails()
                    }
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                }
              
            }
            
        }
        self.present(vc, animated: true, completion: nil)
        
//        self.proceedToPayment() { url in
//            let webView =  CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            webView.strHeader = "Payment".localize()
//            webView.strUrl = url
//            self.navigationController?.pushViewController(webView, animated: true)
//
//        }
    }
    func getMonth() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let date = dateFormatter.date(from: paymentResponse.data!.invoice_date!)
        dateFormatter.dateFormat = "MMMM"
        let result = dateFormatter.string(from: date!)
        print(result)
        return result
    }
}
extension PaymentVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let payment = paymentResponse.data {
            return payment.clinic_amount!.count
        }
        else{return 0}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = payementListTableView.dequeueReusableCell(withIdentifier: "cell") as! PaymentsTableViewCell
        cell.clinicNameLabel.text = paymentResponse.data?.clinic_amount![indexPath.row].clinic_name
        cell.monthLabel.text = getMonth()
        if paymentResponse.data?.payment_type! == 1 { //Percentage Type Clinic
        cell.amountLabel.text = String(format: "%.2f", (paymentResponse.data?.clinic_amount![indexPath.row].clinic_amount)!) + " EGP"
        cell.amount.text = localizeValue(key: "Amount")
        } else {
           cell.amountLabel.text = ""
           cell.amount.text = localizeValue(key: "Flat Fee")
        }
        cell.month.text = localizeValue(key: "Month")
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.month.textAlignment = .right
            cell.amount.textAlignment = .right
            cell.monthLabel.textAlignment = .left
            cell.amountLabel.textAlignment = .left
        }
        return cell
    }
    
    
}
