//
//  SetAppointmentDate&TimeViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import FSCalendar

protocol TimeSelectedDelegate {
    func selectedTime(time: String, date: String, appointmentId: String)
}

class Date_TimeCell: UITableViewCell {
    
    @IBOutlet weak var labelAvailableTime: UILabel!
    @IBAction func backBtn(_ sender: Any) {
    }
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelType: UILabel!
}

class SetAppointmentDate_TimeViewController: BaseViewControler, FSCalendarDataSource, FSCalendarDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var calendar: FSCalendar!
    var clinicId: String?
    let formatter = DateFormatter()
    var timeSlotData: [TimeSlotData]?
    var availability: Availability?
    var arraySlots: [String] = []
    var clin_data : ClinicData?
    var delegate: TimeSelectedDelegate?
    var arrayAvailablity: [Int] = []
    var selectedDate: String = ""
    var isfromReject: Bool?
    var arrayTimesBooked = [String]()
    var clinicType: Int!
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var tableTimeSlot: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
               calendar.calendarHeaderView.calendar.locale =  Locale(identifier: "ar")
        }else{
             calendar.calendarHeaderView.calendar.locale =  Locale(identifier: "en_EN")
        }
        self.headerTitleLabel.text = localizeValue(key: "Set Date and Time")
        errorTitleLabel.text = localizeValue(key: "Doctor is not available for this date.")
        // Do any additional setup after loading the view.
        let date = Date()
        calendar.calendarHeaderView.collectionViewLayout.collectionView?.semanticContentAttribute = .forceLeftToRight
        formatter.dateFormat = "EEEE"
        var day = formatter.string(from: date)
        let INTDAY = self.weekday_to_Int(day: day)
        
        if clin_data != nil {
            calendar.placeholderType = .none
            

            if (clin_data?.availability?.count)! > 0 {
                for obj in (clin_data?.availability)!{
                    let day = obj
                    arrayAvailablity.append(self.weekday_to_Int(day: day.day!))
                }
            }
            let closest = arrayAvailablity.enumerated().min( by: { abs($0.1 - INTDAY) < abs($1.1 - INTDAY)} )!
            let comingintday = closest.element - INTDAY
            
            let comingdate = Calendar.current.date(byAdding: .day, value: comingintday, to: date)
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: comingdate!)
            guard let resultDate = formatter.date(from:result) else { return }
            let currentDateStr = formatter.string(from: Date())
            guard let currentDate = formatter.date(from:currentDateStr) else { return }
            selectedDate = (currentDate > resultDate) ? currentDateStr : result
            appointmentTimeSlots(date: selectedDate, day: self.Int_to_weekday(day: closest.element))

            calendar.select(formatter.date(from: selectedDate))
        } else {
            let comingdate = Calendar.current.date(byAdding: .day, value: 1, to: date)
            calendar.select(comingdate)
            
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: comingdate!)
            selectedDate = result
            
            formatter.dateFormat = "EEEE"
            day = formatter.string(from: comingdate!)
            
            appointmentTimeSlots(date: result, day: day)
        }
        
       //
        
    }
    override func viewWillAppear(_ animated: Bool) {
         calendar.calendarHeaderView.collectionViewLayout.collectionView?.semanticContentAttribute = .forceLeftToRight
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        if isfromReject == true{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    func weekday_to_Int(day :String) -> Int {
        switch day {
        case "Sunday":
            return 1
        case "Monday":
            return 2
        case "Tuesday":
            return 3
        case "Wednesday":
            return 4
        case "Thursday":
            return 5
        case "Friday":
            return 6
        default:
            return 7
        }
    }
    func Int_to_weekday(day :Int) -> String {
        switch day {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        default:
            return "Saturday"
        }
    }
    //FSCalendar
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(date))")
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        selectedDate = result
        formatter.dateFormat = "EEEE"
        let day = formatter.string(from: date)
        
        
        if self.clinicType == 1{
            self.delegate?.selectedTime(time: "", date: result, appointmentId: "")
            self.navigationController?.popViewController(animated: true)
        } else {
            appointmentTimeSlots(date: result, day: day)
            print(result)
        }
        
    }
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(date)")
    }
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        //disable previous date
        let curDate = Date()
        return !(date.addingTimeInterval(60*60*24) < curDate)
    }
    func minimumDate(for calendar: FSCalendar) -> Date {
          return Calendar.current.date(byAdding: .month, value: 0, to: Date())!
    }
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let formatter = "yyyy-MM-dd"
        let stringFromDate = TimeUtils.sharedUtils.stringFromdate(date, with: formatter)
        let currentDate = TimeUtils.sharedUtils.stringFromdate(Date(), with: formatter)
        let date1 = TimeUtils.sharedUtils.dateFromString(stringFromDate, with: formatter)!
        let date2 = TimeUtils.sharedUtils.dateFromString(currentDate, with: formatter)!
        cell.titleLabel.textColor = (monthPosition == .current) ? (date1.addingTimeInterval(60*60*24) > date2) ? UIColor.black : UIColor.lightGray : UIColor.lightGray
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.calendar.isDate(inRange: date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        let weekday = Calendar.current.component(.weekday, from: date)
        for a in arrayAvailablity{
            if a == weekday{
                let currentDate = Date()
                print(currentDate)
                if date>=currentDate{
                return 1
                }
            }
        }
       return 0
    }
   
    func appointmentTimeSlots(date: String, day: String) {
        let service = BusinessService()
        self.arraySlots.removeAll()
        self.arrayTimesBooked.removeAll()
        
        service.appointmentTimeSlots(with: date, clinic_id: clinicId!, day: day, target: self) { (response) in
            if let data = response {
                
               // self.tableTimeSlot.reloadData()
                if data.data != nil{
                     self.timeSlotData = data.data!
                    if (self.timeSlotData?.count)! > 0 {
                        self.arrayTimesBooked = self.timeSlotData!.map{$0.time_slot!}
                    }
                }
                self.availability = data.availability!
                if ((self.availability?.clinic_id) != nil) {
                    self.calculateTimeSlot(startDate: self.availability!.start_time!, endDate: self.availability!.end_time!, timing: Int(self.availability!.clinicScheduleCount!) ?? 10)
                    //self.createArrayfromTimeSlot()
                }
                 self.tableTimeSlot.reloadData()
            }
        
            if self.arraySlots.count == 0 {
                self.errorView.isHidden = false
            } else {
                self.errorView.isHidden = true
            }
            
            if self.clin_data == nil {
                self.errorView.isHidden = true
                self.tableTimeSlot.isHidden = true
            }
        }
        
    }

    func calculateTimeSlot(startDate: String, endDate: String, timing: Int) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "hh:mm a"
        formatter2.amSymbol = "AM"
        formatter2.pmSymbol = "PM"
//        let startDate = "10:00 AM"
//        let endDate = "02:30 PM"
        let date1 = formatter.date(from: startDate)
        let date2 = formatter.date(from: endDate)
        var i = 0
        while true {
            let date = date1?.addingTimeInterval(TimeInterval(i*timing*60))
            let string = formatter2.string(from: date!)
            if date! >= date2! {
                break;
            }
            i += 1
           
             arraySlots.append(string)
//            let x = ["firstname":"","lastname":"","image":""]
//            let xx = Patient_info(dictionary: x as NSDictionary)
//            let a = ["id":"","clinic_id":"","availability_id":"","time_slot":string,"appointment_type":"","patient_id":"","patient_info": xx!] as [String : Any]
//            let aa = TimeSlotData(dictionary: a as NSDictionary)
//            arraySlots.append(aa!)
        }
        print(arraySlots)
        self.tableTimeSlot.reloadData()
    }
    
    func createArrayfromTimeSlot(){
        for a in arraySlots{
            for d in  self.timeSlotData!{
                if d.time_slot == a {
                   
                }else{
                    
                }
            }
        }
    }
    
}

extension SetAppointmentDate_TimeViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.clinicType == 1{
            return 0
        } else {
            return arraySlots.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.arrayTimesBooked.count > 0 {
            
            if self.arrayTimesBooked.contains(arraySlots[indexPath.row]) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Date_TimeCell") as! Date_TimeCell
                print(indexPath.row)
               
                let integer = self.arrayTimesBooked.firstIndex(of: self.arraySlots[indexPath.row])!
                cell.labelName.text = "\(self.timeSlotData![integer].patient_info?.firstname ?? "") \(self.timeSlotData![integer].patient_info?.lastname ?? "")"
                
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                      cell.labelType.text = clin_data?.clinic_name_ar
                }else{
                      cell.labelType.text = clin_data?.clinic_name
                }
              
                
                
                var start_time =  self.timeSlotData![integer].time_slot
                let last = String(start_time!.characters.suffix(2))
                var new_start_time = start_time!.dropLast(2)
                
                if last == "AM" || last == "am" {
                    new_start_time = new_start_time + localizeValue(key: "AM")
                }else if last == "PM" || last == "pm" {
                    new_start_time = new_start_time + localizeValue(key: "PM")
                }

                
                cell.labelTime.text = "\(String(describing: (new_start_time ?? "")))"
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "availableCell") as! AssistantSetDateTimeCell
               
                cell.availableLabel.text = localizeValue(key: "Available")
                cell.bookBtn.setTitle(localizeValue(key: "Book"), for: .normal)
                
                
                var start_time = arraySlots[indexPath.row]
                let last = String(start_time.characters.suffix(2))
                var new_start_time = start_time.dropLast(2)
                if last == "AM" {
                    new_start_time = new_start_time + localizeValue(key: "AM")
                }else{
                    new_start_time = new_start_time + localizeValue(key: "PM")
                }
                 cell.labelAvailableTime.text = "\(String(describing: (new_start_time ?? "")))"
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "availableCell") as! AssistantSetDateTimeCell
            cell.availableLabel.text = localizeValue(key: "Available")
            cell.bookBtn.setTitle(localizeValue(key: "Book"), for: .normal)
            
            if arraySlots.count > 0 {
                var start_time = arraySlots[indexPath.row]
                let last = String(start_time.characters.suffix(2))
                var new_start_time = start_time.dropLast(2)
                if last == "AM" {
                    new_start_time = new_start_time + localizeValue(key: "AM")
                }else{
                    new_start_time = new_start_time + localizeValue(key: "PM")
                }
                
                cell.labelAvailableTime.text = "\(String(describing: (new_start_time ?? "")))"
            }else{
                 self.errorView.isHidden = true
            }
           
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! Date_TimeCell
            return headerView
        }else{
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
            if self.arrayTimesBooked.contains(arraySlots[indexPath.row]) {
            }
        else{
            self.delegate?.selectedTime(time: arraySlots[indexPath.row], date: selectedDate, appointmentId: "\((self.availability?.id)!)")
            if isfromReject == true{
                //            self.dismiss(animated: true, completion: nil)
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
       
    }
}
