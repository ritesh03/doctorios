//
//  EnterPhoneViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 21/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker

class EnterPhoneViewController: BaseViewControler, MRCountryPickerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var backButton2: UIButton!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    @IBOutlet weak var imageViewflag: UIImageView!
    @IBOutlet weak var labelCode: UILabel!
    @IBOutlet weak var buttonSelect: RoundButton!
    @IBOutlet weak var buttonCross: UIButton!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    var phone: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            labelTitle.textAlignment = .right
            descriptionLabel.textAlignment = .right
            textFieldPhone.textAlignment = .left
            backButton2.isHidden = true
            phoneView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            textFieldPhone.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            labelCode.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            imageViewflag.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
        }else{
            textFieldPhone.textAlignment = .left
        }
        if #available(iOS 10.0, *) {
            textFieldPhone.keyboardType = .asciiCapableNumberPad
        } else {
            // Fallback on earlier versions
        }
        
        
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setLocale("EN")
        let countryCode = Locale.current.regionCode
        countryPicker.setCountry(countryCode ?? "")
        textFieldPhone.delegate = self
        buttonSelect.isEnabled = false
        buttonCross.isHidden = true
        
        textFieldPhone.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter phone number", comment: "")
        
        if AppInstance.shared.isFromForgot == true {
            labelTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Forgot Password", comment: "")
            descriptionLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Please enter your phone number, and wait for SMS verification code", comment: "")
        }else{
            labelTitle.text = localizeValue(key: "Sign up")
             descriptionLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Please enter your phone number, and wait for SMS verification code", comment: "")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonSelectCountaryClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    
    @IBAction func buttonCancelClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    @IBAction func buttonCrossClicked(_ sender: Any) {
        
        textFieldPhone.text = ""
    }
    @IBAction func buttonSelectClicked(_ sender: Any) {
        
        guard let validPhone = textFieldPhone.text,!validPhone.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter phone number"), target: self, handler: nil)
            return
        }

        guard let countryCode = labelCode.text,!countryCode.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter country code".localize(), target: self, handler: nil)
            return
        }
        let code = (countryCode as NSString)
        let phoneNumber = CommonClass.checkAndEditPhoneNumberFor(phoneNumber: validPhone)
        
        if AppInstance.shared.isFromForgot == true {
            var type: String?
            if AppInstance.shared.userType == 0{
                type = "2" //Doctor
            }else{
                type = "3" //Assistant
            }
            let service = BusinessService()
            service.forgotPassword(with: phoneNumber, type: type!,countryCode: code as String, target: self) { (response) in
                if response {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as! EnterOTPViewController
                    AppInstance.shared.mobile = self.textFieldPhone.text!
                    print(AppInstance.shared.mobile!)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            if AppInstance.shared.userType == 0{
                // CHANGE IT TO BACK
                let service = BusinessService()
                service.mobileVarification(with: code as String, mobile: phoneNumber, path: Config.mobileVarification, target: self) { (response) in
                    if response == true {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as! EnterOTPViewController
                        AppInstance.shared.mobile = self.textFieldPhone.text!
                        print(AppInstance.shared.mobile!)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }else{
                let service = BusinessService()
                service.mobileVarification(with: code as String, mobile: phoneNumber, path: Config.assistant_mobile_verification, target: self) { (response) in
                    if response == true {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as! EnterOTPViewController
                        AppInstance.shared.mobile = self.textFieldPhone.text!
                        print(AppInstance.shared.mobile!)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            
        }
        
    }
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        imageViewflag.image = flag
        labelCode.text = phoneCode
        AppInstance.shared.selectedFlag = flag
        AppInstance.shared.countryCode = phoneCode
        AppInstance.shared.phoneCode = phoneCode
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text
        let textRange = Range(range, in: text!)
        let updatedText = text!.replacingCharacters(in: textRange!,
                                                    with: string)
        if updatedText.count < 8 {
            //            self.showAlert(with: "Phone number should be more than 8 character")
            buttonSelect.backgroundColor = UIColor(red: 198/255.0, green: 198/255.0, blue: 198/255.0, alpha: 1.0)
            buttonSelect.isEnabled = false
            buttonCross.isHidden = true
        }else {
            buttonSelect.isEnabled = true
            buttonSelect.backgroundColor = UIColor(red: 0/255.0, green: 149/255.0, blue: 188/255.0, alpha: 1.0)
            buttonCross.isHidden = false
        }
        var str: NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        return (((str as String).first == "0") ? str.length <= 12 : str.length <= 11)
        //        let str: NSString = textField.text! as NSString
        //        return str.length <= 11
        // return true
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        
        //        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
     //   if AppInstance.shared.userType == 0{
            
            if let navigationController = self.navigationController{
                
                for vc in navigationController.viewControllers where vc.classForCoder == SignUpViewController.classForCoder(){
                    
                    if let tempVC = vc as? SignUpViewController{
                        
                        tempVC.openLoginController()
                    }
                }
            }
    //    }
    }
}

