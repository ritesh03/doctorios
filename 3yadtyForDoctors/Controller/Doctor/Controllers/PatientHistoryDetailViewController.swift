//
//  PatientHistoryDetailViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class PatientHistoryDetailViewController: ButtonBarPagerTabStripViewController, UITextViewDelegate, PatientHistoryDetailListViewControllerDelegate {
    
    //    MARK:- IBOutlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var textViewEnterDetail: UITextView!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    //    MARK:- Variables
    
    var patient_case: String?
    var prescriptionData: [PrescriptionData]?
    
    //    MARK:- View Life Cycle
    
    override func viewDidLoad() {
        
        // change selected bar color XLPagerTabStrip
        saveButton.setTitle(localizeValue(key: "Save"), for: .normal)
        titleLabel.text = localizeValue(key: "Describe patient case")
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            textViewEnterDetail.textAlignment = .right
        }else{
             textViewEnterDetail.textAlignment = .left
            titleLabel.text = localizeValue(key: "Describe patient case")
        }
        if AppInstance.shared.userType == 1 {
            buttonPlus.isHidden = true
        }
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = UIColor(red: 0/255.0, green: 149/255.0, blue: 188/255.0, alpha: 1.0)
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .gray
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor(red: 0/255.0, green: 149/255.0, blue: 188/255.0, alpha: 1.0)
            newCell?.label.textColor = UIColor(red: 0/255.0, green: 149/255.0, blue: 188/255.0, alpha: 1.0)
        }
        
        super.viewDidLoad()
        if AppInstance.shared.isShowPlus == true{
            
            buttonPlus.isHidden = (AppInstance.shared.userType == 1)
            textViewEnterDetail.isUserInteractionEnabled = (AppInstance.shared.userType == 0)
            textViewEnterDetail.text = localizeValue(key: "Describe patient case…")
            textViewEnterDetail.textColor = UIColor.gray
        }else{
            textViewEnterDetail.text = localizeValue(key: "Describe patient case…")
           // buttonPlus.isHidden = true
            textViewEnterDetail.isUserInteractionEnabled = true
        }
        
        let dateStr = TimeUtils.sharedUtils.chaneDateFormet(AppInstance.shared.selectedDr?.appointment_date)
        labelTitle.text = "\(dateStr)"
        self.saveButton.isHidden = true
        NotificationCenter.default.addObserver(self,selector:#selector(updateView(_:)),
                                               name:NSNotification.Name(rawValue: "GetHistory"),object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //    MARK:- UITextField Delegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == localizeValue(key: "Describe patient case…"){
            textViewEnterDetail.text = ""
        }
        textViewEnterDetail.textColor = UIColor.black
    }
    
    func textViewDidChange(_ textView: UITextView) {
        patient_case = textView.text
    }

    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        //add child view controller to XLPagerTabStrip
        let child_1 = (self.storyboard?.instantiateViewController(withIdentifier: "PatientHistoryDetailListViewController"))! as! PatientHistoryDetailListViewController
        child_1.delegate = self
        
        let array : [UIViewController] = [child_1]//[child_1,child_2,child_3]
        return array
    }
    
    
    //    MARK: IBActions
    
    @IBAction private func saveButtonTapped(_ sender: Any) {
        let service = BusinessService()
        var param = [String:Any]()
        if self.textViewEnterDetail.text == localizeValue(key: "Describe patient case…") || self.textViewEnterDetail.text == "" {
            CommonClass.showAlert(self, title: localizeValue(key: "Patient case field is required"))
            return
        }
        param = ["prescription_id":self.prescriptionData![0].id!, "patient_case":self.textViewEnterDetail.text!]
        service.updatePatientCase(param: param) { (success) in
            if success {
                CommonClass.showAlert(self, title: self.localizeValue(key: "Prescription updated successfully"))
            } else {
                CommonClass.showAlert(self, title: self.localizeValue(key: "Something went wrong"))
            }
        }
    }
    
    @IBAction private func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func buttonPlusClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PrescriptionViewController") as! PrescriptionViewController
        vc.patient_case = patient_case
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    MARK:- Methods
    
    @objc private func updateView(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            if let caseStudy = dict["casestudy"] as? String{
                textViewEnterDetail.text = caseStudy
                textViewEnterDetail.textColor = UIColor.black
                self.saveButton.isHidden = false
            }
        }
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    
    func setPrescriptionListInParentView(data: [PrescriptionData]) {
        self.prescriptionData = data
    }
}

//    MARK: UITableView DataSources & Delegates

extension PatientHistoryDetailViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientHistoryDetailListCell") as! PatientHistoryDetailListCell
        return cell
    }
}
