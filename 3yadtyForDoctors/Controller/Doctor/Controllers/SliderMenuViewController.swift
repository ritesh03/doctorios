//
//  SliderMenuViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 08/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class SliderMenuViewController: BaseViewControler {

    @IBOutlet weak var logOutBtn: RoundButton!
    @IBOutlet weak var tableMenu: UITableView!
//    @IBOutlet weak var asTimerView: UIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labePosition: UILabel!
    @IBOutlet weak var imageViewUser: RoundButton!
    @IBOutlet weak var imageViewMenu: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var constraint_height_view: NSLayoutConstraint!
    @IBOutlet weak var assistantDetailView: UIView!
    
    //let arrImage = ["imgStar","imgAssistants","imgAssistants","imgSettings"]
    //let arrTitle = ["Rating & reviews","New Assistant Request's","Assistants","Settings"]
    let arrImage = ["imgAssistants","imgAssistants","resort1","invoice","credit-card","imgPhone","imgSettings"]
    let arr = ["Assistants","New Assistant Request's","Vacations","Invoices","Payment","Contact us","Settings"]
    var arrTitle = [String]()
    
    let arrImage1 = ["imgSettings","imgPhone"]
    let arr1 = ["Settings","Contact us"]
     var arrTitle1 = [String]()
    var mainViewController: UIViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        logOutBtn.setTitle(localizeValue(key: "Logout"), for: .normal)
        for i in 0..<arr.count {
            arrTitle.append(localizeValue(key: arr[i]))
        }
        for i in 0..<arr1.count {
            arrTitle1.append(localizeValue(key: arr1[i]))
        }
        if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
           // UIView.appearance().semanticContentAttribute = .forceLeftToRight
            //UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.view.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        }else{
            //UIView.appearance().semanticContentAttribute = .forceRightToLeft
           // UIView.appearance().semanticContentAttribute = .forceLeftToRight
            self.view.semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AppInstance.shared.userType == 0{
//            asTimerView.isHidden = true
            self.assistantDetailView.isHidden = true
            labePosition.text = AppInstance.shared.user!.dr_position ?? ""
        }else{
//            asTimerView.isHidden = false
            self.assistantDetailView.isHidden = false
        }
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            labelName.text = "\(String(describing: AppInstance.shared.user!.firstname!)) \(String(describing: AppInstance.shared.user!.lastname!))"
        }else{
            labelName.text = "\(String(describing: AppInstance.shared.user!.firstname!)) \(String(describing: AppInstance.shared.user!.lastname!))"
        }
        
        if AppInstance.shared.userType == 1{
            
            if let doctorInfo = AppInstance.shared.user?.doctor_info as? Dictionary<String, Any>{
                
                let doctorObj = UserData.parse(doctorInfo as NSDictionary)
                self.lbl_name.text = "Dr. \(String(describing: doctorObj.firstname ?? "")) \(String(describing: doctorObj.lastname ?? ""))"
//                self.constraint_height_view.constant = 52.0
            }else if let userObj = AppInstance.shared.user{
                
                self.lbl_name.text = "Dr. " + (userObj.doctor_name ?? "")
//                self.constraint_height_view.constant = 52.0
            }
        }
        
//         cell.imageView.sd_setImage(with: URL.init(string: (Config.testClinicImageBaseUrl + self.imageArr[indexPath.row])), placeholderImage: UIImage.init(named: "addDataToPrescription"), options: [], progress: nil, completed: nil)
        if AppInstance.shared.user?.image?.isEmpty != true{
            imageViewMenu.sd_setShowActivityIndicatorView(true)
            imageViewMenu.sd_setIndicatorStyle(.gray)
            guard let url = URL.init(string: Config.imageBaseUrl + AppInstance.shared.user!.image! ) else { return }
            imageViewMenu.sd_setImage(with: url, placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            imageViewMenu.sd_setShowActivityIndicatorView(false)
        }else{
            imageViewMenu.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        
        imageViewMenu.contentMode = UIViewContentMode.scaleAspectFill
        imageViewMenu.clipsToBounds = true
        tableMenu.reloadData()
    }
    
    @IBAction func buttonEditProfileClicked(_ sender: Any) {

        if AppInstance.shared.userType == 0{
            AppInstance.shared.isFromMenu = true
            paramEdit = [String:Any]()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoViewController") as! PersonalInfoViewController
//            self.navigationController?.pushViewController(vc, animated: true)
            let nc = UINavigationController.init(rootViewController: vc)
            nc.isNavigationBarHidden = true
            self.revealViewController().pushFrontViewController(nc, animated: true)
        }else if AppInstance.shared.userType == 1{
            let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantEditProfileViewController") as! AssistantEditProfileViewController
            let nc = UINavigationController.init(rootViewController: vc)
            self.revealViewController().pushFrontViewController(nc, animated: true)
        }
    }
    
    @IBAction func buttonLogoutClicked(_ sender: Any) {
        
        let alertController = UIAlertController(title: "", message: localizeValue(key: "Are you sure you want to logout?"), preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: localizeValue(key: "Ok"), style: UIAlertActionStyle.destructive) {
            UIAlertAction in
            UserDefaults.standard.set(nil, forKey: "isLogIn")
            UserDefaults.standard.set(nil, forKey: "userdata")
            AppInstance.shared.doctorSignUp = nil

            AppInstance.shared.clinicTimming.removeAll()
        
            let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            let nc = UINavigationController.init(rootViewController: vc)
            self.revealViewController().pushFrontViewController(nc, animated: true)
        }
        let exitAction = UIAlertAction(title: localizeValue(key: "Cancel"), style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(exitAction)
        alertController.addAction(confirmAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func actionOnCall(_ sender: Any) {
        
        if let doctorInfo = AppInstance.shared.user?.doctor_info as? Dictionary<String, Any>{
            
            let doctorObj = UserData.parse(doctorInfo as NSDictionary)
            if let url = URL(string: "tel://\(doctorObj.countrycode ?? "")\(doctorObj.mobile ?? "")") {
                let application = UIApplication.shared
                if application.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        application.open(url, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
        }else if let doctorInfo = AppInstance.shared.user{
            
            if let url = URL(string: "tel://\(doctorInfo.countrycode ?? "")\(doctorInfo.mobile ?? "")") {
                let application = UIApplication.shared
                if application.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        application.open(url, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
        }
    }
    
    @IBAction func actionOnMessage(_ sender: Any) {
        
        if let doctorInfo = AppInstance.shared.user?.doctor_info as? Dictionary<String, Any>{
            
            let doctorObj = UserData.parse(doctorInfo as NSDictionary)
            let user = ["email": doctorObj.email ?? "","fcmUserId": String(describing: doctorObj.id ?? 0) ,"image": doctorObj.image ?? "","lastMessage":"","fcmToken":"","userName": "\(doctorObj.firstname ?? "") \(doctorObj.lastname ?? "")"] as [String : Any]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.selectedUser = Person.init(dictionary: user as NSDictionary)
            vc.entryType = "AssistantSideMenu"
            let nc = UINavigationController.init(rootViewController: vc)
            self.revealViewController().pushFrontViewController(nc, animated: true)
        }else if let doctorObj = AppInstance.shared.user{
            
            let user = ["email": doctorObj.email ?? "","fcmUserId": String(describing: doctorObj.id ?? 0) ,"image": doctorObj.image ?? "","lastMessage":"","fcmToken":"","userName": "\(doctorObj.firstname ?? "") \(doctorObj.lastname ?? "")"] as [String : Any]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.entryType = "AssistantSideMenu"
            vc.selectedUser = Person.init(dictionary: user as NSDictionary)
            let nc = UINavigationController.init(rootViewController: vc)
            self.revealViewController().pushFrontViewController(nc, animated: true)
        }
    }
}

extension SliderMenuViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if AppInstance.shared.userType == 0{
            return arrTitle.count
        }
            return arrTitle1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SliderMenuCell", for: indexPath) as! SliderMenuCell
        if AppInstance.shared.userType == 0{
            cell.buttonImage.setImage(UIImage(named: arrImage[indexPath.row]), for: .normal)
            cell.labelTitle.text = arrTitle[indexPath.row]
        }else{
            cell.buttonImage.setImage(UIImage(named: arrImage1[indexPath.row]), for: .normal)
            cell.labelTitle.text = arrTitle1[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44.0
    }
    //FSFriendsProfileViewController
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if AppInstance.shared.userType == 1{
            if indexPath.row == 0{
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
            }else if indexPath.row == 1{
                
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ContactUSViewController") as! ContactUSViewController
                vc.viewtype = localizeValue(key: "Contact us")
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
            }
        }else if AppInstance.shared.userType == 0{
            if indexPath.row == 0{
                
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "AssistantListViewController") as! AssistantListViewController
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
                
            }else if indexPath.row == 1{
               
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantNewReservationsViewController") as! AssistantNewReservationsViewController
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
            }else if indexPath.row == 2{
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "VacationsVC") as! VacationsVC
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)            }else if indexPath.row == 3{
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)

            }else if indexPath.row == 4{
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
            }else if indexPath.row == 5{
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ContactUSViewController") as! ContactUSViewController
                vc.viewtype = localizeValue(key: "Contact us")
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
            }else if indexPath.row == 6{
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                let nc = UINavigationController.init(rootViewController: vc)
                self.revealViewController().pushFrontViewController(nc, animated: true)
            }
        }
    }
}

