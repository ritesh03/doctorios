//
//  AddNewAssistantViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 01/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker
import ActionSheetPicker_3_0

class AddNewAssistantViewController: BaseViewControler,UIImagePickerControllerDelegate,
UINavigationControllerDelegate, MRCountryPickerDelegate{
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    @IBOutlet weak var tableAddAssistant: UITableView!
        
        let myPickerController = UIImagePickerController()
        var selectedFlag: UIImage?
        var clinicName: String?
        var clinicId: Int?
    var clinicType: Int?

        var imagesArray = [File]()
        var fname: String?
        var lname: String?
        var phone: String?
        var code = ""
        var emailAddress: String?
        var password: String?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
            myPickerController.delegate = self
            viewForCountaryPicker.isHidden = true
            countryPicker.countryPickerDelegate = self
            countryPicker.showPhoneNumbers = true
            
            let lat = LocationManager.sharedInstance.latitude
            let lon = LocationManager.sharedInstance.longitude
            let countryCode = Locale.current.regionCode
            countryPicker.setCountry(countryCode!)
            
//            LocationManager.sharedInstance.reverseGeocodeLocationWithLatLon(latitude: lat, longitude: lon) { (dict, place, name) in
//                if let data = dict?.object(forKey: "country") {
//                    let cname = data as! String
//                    let locale = Locale(identifier: LocalizationSystem.sharedInstance.getLanguage())
//
//                    self.countryPicker.setCountry(locale.isoCode(for: cname) ?? "")
//                }
//            }
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            }
            headerTitleLabel.text = localizeValue(key: "Add assistant")
//            let countryCode = Locale.current.regionCode
//            countryPicker.setCountry(countryCode!)
//            let countryName = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode!)
//            countryPicker.setCountryByName(countryName!)
            countryPicker.setLocale("EN")
            

        }
        @objc private func textFieldDidChange(_ textField: UITextField) {
            if textField.tag == 1{
                fname = textField.text
            }else if textField.tag == 2{
                lname = textField.text
            }else if textField.tag == 3{
                phone = textField.text
            }else if textField.tag == 5{
                emailAddress = textField.text
            }else if textField.tag == 6{
                password = textField.text
            }
        }
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
        @IBAction func buttonProfileClicked(_ sender: Any) {
            
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Camera"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    self.myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                    self.myPickerController.allowsEditing = true
                    self.present(self.myPickerController, animated: true, completion: nil)
                }else{
                    UIAlertController.show(title: nil, message: "No camera available.".localize(), target: self, handler: nil)
                }
            }))
            actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Gallery"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                
                self.myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.myPickerController.allowsEditing = true
                self.present(self.myPickerController, animated: true, completion: nil)
            }))
            actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
        }
        //imagePicker Delegeate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let imageName: String?
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageName = "\(currentTimeStamp).jpg"
            imageViewProfile.image = chosenImage
            let file = File(name: imageName!, image: chosenImage, type: 1)
            if (imagesArray.count) > 0 {
                imagesArray.removeLast()
            }
            imagesArray.append(file)
        }
        picker.dismiss(animated: true, completion: nil)
    }
        //countaryPicker Delegate
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.selectedFlag = flag
        self.code = phoneCode
        tableAddAssistant.reloadData()
    }
        @IBAction func buttonCancelClicked(_ sender: Any) {
            viewForCountaryPicker.isHidden = true
        }
        
        @IBAction func buttonDoneClicked(_ sender: Any) {
            viewForCountaryPicker.isHidden = true
            self.tableAddAssistant.reloadData()
        }
        
    @IBAction func buttonCountryClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    
    @IBAction func buttonSubmitClicked(_ sender: Any) {
        //validation
//        if imagesArray.isEmpty == true{
//            UIAlertController.show(title: nil, message: Alert.PleaseUploadImage, target: self, handler: nil)
//            return
//        }
        guard let fname = fname,!fname.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: Alert.FirstName), target: self, handler: nil)
            return
        }
        guard let lname = lname,!lname.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: Alert.LastName) , target: self, handler: nil)
            return
        }
        guard let clenicName = clinicName,!clenicName.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: Alert.clenicName), target: self, handler: nil)
            return
        }
//        guard let emailAddress = emailAddress,!emailAddress.isEmpty else {
//            UIAlertController.show(title: nil, message: Alert.email, target: self, handler: nil)
//            return
//        }
        if emailAddress != nil {
            if !(emailAddress)!.isValidEmail() {
                UIAlertController.show(title: nil, message: localizeValue(key: Alert.validEmail), target: self, handler: nil)
                return
            }
        }
        if password != nil {
            if password!.count < 6 {
                
                UIAlertController.show(title: nil, message: localizeValue(key: localizeValue(key: "Password must contain 6 digits")), target: self, handler: nil)
                return
                
            }
        }
      
        
       
        guard let password = password,!password.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: Alert.password), target: self, handler: nil)
            return
        }
        addNewAssiatant()
    }
    
    func addNewAssiatant() {
        let service = BusinessService()
        let deviceToken = UserDefaults.standard.string(forKey: "token")
        service.addAssistant(with: fname!, lastname: lname!, email: emailAddress ?? "", password: password!, countrycode: String(code.dropFirst()), mobile: phone!, image: imagesArray, devicetype: "2", devicetoken: deviceToken ?? "", dr_id: String(describing: AppInstance.shared.user!.id!), clinic_id: String(describing: clinicId!), user_id: String(describing: AppInstance.shared.user!.id!), target: self) { (response) in
            if let user = response {
                print(user)
                //self.navigationController?.popViewController(animated: true)
                let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "NewAssistantSucessPopUpViewController") as! NewAssistantSucessPopUpViewController
                vc.email = self.emailAddress
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    vc.Phone = " \(self.code) \(self.phone!)"
                }else{
                    vc.Phone =  "\(self.code) \(self.phone!)"
                }
                
                vc.password = self.password
                vc.delegate = self
                vc.modalPresentationStyle = .overCurrentContext
                vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                self.present(vc, animated: false, completion: nil)
            }
        }
    }
}
    
extension AddNewAssistantViewController : UITableViewDataSource, UITableViewDelegate  {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 6
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewAssistantCell
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.labelTitle.textAlignment = .right
                    cell.textFieldName.textAlignment = .right
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.textFieldName.isSecureTextEntry = false
                cell.textFieldName.keyboardType = .default
                 cell.textFieldName.tag = 1
                cell.textFieldName.placeholder = localizeValue(key: "First name")
                cell.labelTitle.text = localizeValue(key: "First name")
                cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewAssistantCell
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.labelTitle.textAlignment = .right
                    cell.textFieldName.textAlignment = .right
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.textFieldName.isSecureTextEntry = false
                cell.textFieldName.keyboardType = .default
                cell.textFieldName.tag = 2
                cell.textFieldName.placeholder = localizeValue(key: "Last name")
                cell.labelTitle.text = localizeValue(key: "Last name")
                cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as! AddNewAssistantCell
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.countryCodeLabel.textAlignment = .right
                    cell.phoneNumberLabel.textAlignment = .right
                   // cell.tfPhone.textAlignment = .right
                    
                    
                    cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                    cell.tfPhone.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                    cell.labelCountaryCode.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                    cell.imageViewFlag.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                }
                cell.countryCodeLabel.text = localizeValue(key: "Country Code")
               cell.phoneNumberLabel.text = localizeValue(key: "Phone Number")
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.imageViewFlag.image = self.selectedFlag
                cell.labelCountaryCode.text = self.code
                cell.tfPhone.placeholder = "100-123-456-789"
                if #available(iOS 10.0, *) {
                    cell.tfPhone.keyboardType = .asciiCapableNumberPad
                } else {
                    // Fallback on earlier versions
                }
                cell.tfPhone.tag = 3
                cell.tfPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewAssistantCell
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.labelTitle.textAlignment = .right
                    cell.textFieldName.textAlignment = .right
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.textFieldName.isSecureTextEntry = false
                cell.textFieldName.keyboardType = .default
                cell.textFieldName.tag = 4
                cell.textFieldName.placeholder = localizeValue(key: "Hospital / Clinic name")
                cell.labelTitle.text = localizeValue(key: "Hospital / Clinic name")
                if clinicName != nil{
                     cell.textFieldName.text = clinicName
                }
                cell.textFieldName.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
                    //Write code for picker view here
                    self.view.endEditing(true)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ClinicListViewController") as! ClinicListViewController
                    vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 4:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewAssistantCell
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.labelTitle.textAlignment = .right
                    cell.textFieldName.textAlignment = .right
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.textFieldName.isSecureTextEntry = false
                cell.textFieldName.keyboardType = .emailAddress
                cell.textFieldName.autocapitalizationType = .none
                cell.textFieldName.tag = 5
                cell.textFieldName.placeholder = "example@email.com".localize()
                cell.labelTitle.text = localizeValue(key: "Email address")
                cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewAssistantCell
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.labelTitle.textAlignment = .right
                    cell.textFieldName.textAlignment = .right
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.textFieldName.keyboardType = .default
                cell.textFieldName.isSecureTextEntry = true
                cell.textFieldName.tag = 6
                cell.textFieldName.placeholder = "********".localize()
                cell.labelTitle.text = localizeValue(key: "Create Password")
                cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
            return 80.0
        }
}

extension AddNewAssistantViewController: selectedClinicDelegate,sucessPopUp{
    func sucess() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func selectedClinic(name: String, id: Int, ClinicData: ClinicData, clinicType: Int) {
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             clinicName = ClinicData.clinic_name_ar
        }else{
             clinicName = name
        }
       
        clinicId = id
        self.clinicType = clinicType
        tableAddAssistant.reloadRows(at: [IndexPath.init(row: 3, section: 0)], with: .none)
    }
}
