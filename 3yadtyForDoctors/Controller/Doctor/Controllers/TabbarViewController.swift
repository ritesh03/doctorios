//
//  TabbarViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setTabBarItems()

        // Do any additional setup after loading the view.
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    
    func setTabBarItems(){
        
        let myTabBarItem1 = (self.tabBar.items?[0])! as UITabBarItem
        myTabBarItem1.title = localizeValue(key: "Appointments")
        let myTabBarItem2 = (self.tabBar.items?[1])! as UITabBarItem
        myTabBarItem2.title = localizeValue(key: "Patients")
        let myTabBarItem3 = (self.tabBar.items?[2])! as UITabBarItem
        myTabBarItem3.title = localizeValue(key: "Messages")
        let myTabBarItem4 = (self.tabBar.items?[3])! as UITabBarItem
        myTabBarItem4.title = localizeValue(key: "Notifications")
       
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
