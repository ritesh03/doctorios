//
//  AddMedicineViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 03/12/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol addMedicineDelegate {
    func selectedMedicine(dict: Medicine, type: Int, editIndex: Int)
}

class AddMedicineViewController: BaseViewControler,UITextViewDelegate {
   
    @IBOutlet weak var textFieldTradeName: CustomTextField!
    @IBOutlet weak var textFieldGenericName: CustomTextField!
    @IBOutlet weak var buttonDoseMinus: UIButton!
    @IBOutlet weak var buttonDosePlus: UIButton!
    @IBOutlet weak var labelDoseCount: UILabel!
    @IBOutlet weak var buttonFreqMinus: UIButton!
    @IBOutlet weak var buttonFreqPlus: UIButton!
    @IBOutlet weak var labelFreqCount: UILabel!
    @IBOutlet weak var buttonDay: RoundButton!
    @IBOutlet weak var buttonWeek: RoundButton!
    @IBOutlet weak var buttonMonth: RoundButton!
    @IBOutlet weak var buttonYear: RoundButton!
    @IBOutlet weak var buttonDurationMinus: UIButton!
    @IBOutlet weak var buttonDurationPlus: UIButton!
    @IBOutlet weak var labelDurationCount: UILabel!
    @IBOutlet weak var buttonDay1: RoundButton!
    @IBOutlet weak var buttonWeek1: RoundButton!
    @IBOutlet weak var buttonMonth1: RoundButton!
    @IBOutlet weak var buttonYear1: RoundButton!
    @IBOutlet weak var textFieldMorning: CustomTextField!
    @IBOutlet weak var buttonRightEye: RoundButton!
    @IBOutlet weak var buttonLeftEye: RoundButton!
    @IBOutlet weak var buttonRightEar: RoundButton!
    @IBOutlet weak var buttonLeftEar: RoundButton!
    @IBOutlet weak var textViewMedicine: UITextView!
    
    var delegate: addMedicineDelegate?
    
    var selectedTrade: TradeData?
    var selectedGeneric: GenericData?
    var selectedType: Int?
    var arrMedicine: Medicine?
    var arrSenses = [String]()
    
    var dose = 1
    var frequency = 1
    var duration = 1
    var note = ""
    
    var dose_time: String?
    var duration_day: String?
    var frequency_day: String?
    var genric_id: Int?
    var genric_name: String?
    var trade_id: Int?
    var trade_name: String?
    var prescriptionType: Int?
    var editIndex = Int()
    var medicineArr = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      
        setUserInterface()
        if medicineArr.count > 0{
            setValue()
        } else {
            self.frequency_day = "Day"
            self.duration_day = "Day"
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
          UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
    }
    
    func setValue(){
        print(medicineArr)
        self.trade_name = medicineArr["trade_name"] as? String
        self.trade_id = medicineArr["trade_id"] as? Int
        textFieldTradeName.text = medicineArr["trade_name"] as? String
        
        self.genric_name = medicineArr["genric_name"] as? String
        self.genric_id = medicineArr["genric_id"] as? Int
        textFieldGenericName.text = medicineArr["genric_name"] as? String

        dose = medicineArr["dose_number"] as? Int ?? 1
        labelDoseCount.text = String(dose)
       
        frequency = medicineArr["frequency"] as! Int
        labelFreqCount.text = String(frequency)
        
        duration = medicineArr["duration"] as! Int
        labelDurationCount.text = String(duration)

        self.dose_time = medicineArr["dose_time"] as? String
        textFieldMorning.text = medicineArr["dose_time"] as? String
        
        note = medicineArr["notes"] as? String ?? ""
        textViewMedicine.text = note
        textViewMedicine.textColor = UIColor.black
        
        for arr in medicineArr["precription_sence"]! as! [String] {//precription_sence
            print(arr)
            if arr == "Right Eye"{
                buttonRightEye.isSelected = true
//                buttonLeftEye.isSelected = false
                self.buttonRightEye.backgroundColor = Color.selected_color
                self.buttonRightEye.setTitleColor(.white, for: .normal)
                arrSenses.append("Right Eye")
//                self.buttonLeftEye.backgroundColor = Color.unSelected_color
//                self.buttonLeftEye.setTitleColor(.black, for: .normal)
            }else if arr == "Left Eye"{
                buttonLeftEye.isSelected = true
//                buttonRightEye.isSelected = false
                self.buttonLeftEye.backgroundColor = Color.selected_color
                self.buttonLeftEye.setTitleColor(.white, for: .normal)
                arrSenses.append("Left Eye")
//                self.buttonRightEye.backgroundColor = Color.unSelected_color
//                self.buttonRightEye.setTitleColor(.black, for: .normal)
            }else if arr == "Right Ear"{
                buttonRightEar.isSelected = true
//                buttonLeftEar.isSelected = false
                self.buttonRightEar.backgroundColor = Color.selected_color
                self.buttonRightEar.setTitleColor(.white, for: .normal)
                arrSenses.append("Right Ear")
//                self.buttonLeftEar.backgroundColor = Color.unSelected_color
//                self.buttonLeftEar.setTitleColor(.black, for: .normal)
            }else if arr == "Left Ear"{
                buttonLeftEar.isSelected = true
//                buttonRightEar.isSelected = false
                self.buttonLeftEar.backgroundColor = Color.selected_color
                self.buttonLeftEar.setTitleColor(.white, for: .normal)
                arrSenses.append("Left Ear")
//                self.buttonRightEar.backgroundColor = Color.unSelected_color
//                self.buttonRightEar.setTitleColor(.black, for: .normal)
            }
        }
        
        if String(describing: medicineArr["frequency_day"]!) == "Day"{
            buttonDay.isSelected = true
            buttonWeek.isSelected = false
            buttonMonth.isSelected = false
            buttonYear.isSelected = false
            self.buttonDay.backgroundColor = Color.selected_color
            self.buttonDay.setTitleColor(.white, for: .normal)
            self.frequency_day = "Day"
            self.buttonWeek.backgroundColor = Color.unSelected_color
            self.buttonWeek.setTitleColor(.black, for: .normal)
            self.buttonMonth.backgroundColor = Color.unSelected_color
            self.buttonMonth.setTitleColor(.black, for: .normal)
            self.buttonYear.backgroundColor = Color.unSelected_color
            self.buttonYear.setTitleColor(.black, for: .normal)
        }else if String(describing: medicineArr["frequency_day"]!) == "Week"{
            buttonWeek.isSelected = true
            buttonDay.isSelected = false
            buttonMonth.isSelected = false
            buttonYear.isSelected = false
            self.buttonWeek.backgroundColor = Color.selected_color
            self.buttonWeek.setTitleColor(.white, for: .normal)
            self.frequency_day = "Week"
            self.buttonDay.backgroundColor = Color.unSelected_color
            self.buttonDay.setTitleColor(.black, for: .normal)
            self.buttonMonth.backgroundColor = Color.unSelected_color
            self.buttonMonth.setTitleColor(.black, for: .normal)
            self.buttonYear.backgroundColor = Color.unSelected_color
            self.buttonYear.setTitleColor(.black, for: .normal)
        }else if String(describing: medicineArr["frequency_day"]!) == "Month"{
            buttonMonth.isSelected = true
            buttonWeek.isSelected = false
            buttonDay.isSelected = false
            buttonYear.isSelected = false
            self.buttonMonth.backgroundColor = Color.selected_color
            self.buttonMonth.setTitleColor(.white, for: .normal)
            self.frequency_day = "Month"
            self.buttonDay.backgroundColor = Color.unSelected_color
            self.buttonDay.setTitleColor(.black, for: .normal)
            self.buttonWeek.backgroundColor = Color.unSelected_color
            self.buttonWeek.setTitleColor(.black, for: .normal)
            self.buttonYear.backgroundColor = Color.unSelected_color
            self.buttonYear.setTitleColor(.black, for: .normal)
        }else if String(describing: medicineArr["frequency_day"]!) == "Year"{
            buttonYear.isSelected = true
            buttonMonth.isSelected = false
            buttonWeek.isSelected = false
            buttonDay.isSelected = false
            self.buttonYear.backgroundColor = Color.selected_color
            self.buttonYear.setTitleColor(.white, for: .normal)
            self.frequency_day = "Year"
            self.buttonDay.backgroundColor = Color.unSelected_color
            self.buttonDay.setTitleColor(.black, for: .normal)
            self.buttonWeek.backgroundColor = Color.unSelected_color
            self.buttonWeek.setTitleColor(.black, for: .normal)
            self.buttonMonth.backgroundColor = Color.unSelected_color
            self.buttonMonth.setTitleColor(.black, for: .normal)
        }
        
        if String(describing: medicineArr["duration_day"]!) == "Day"{
            buttonDay1.isSelected = true
            buttonWeek1.isSelected = false
            buttonMonth1.isSelected = false
            buttonYear1.isSelected = false
            self.buttonDay1.backgroundColor = Color.selected_color
            self.buttonDay1.setTitleColor(.white, for: .normal)
            self.duration_day = "Day"
            self.buttonWeek1.backgroundColor = Color.unSelected_color
            self.buttonWeek1.setTitleColor(.black, for: .normal)
            self.buttonMonth1.backgroundColor = Color.unSelected_color
            self.buttonMonth1.setTitleColor(.black, for: .normal)
            self.buttonYear1.backgroundColor = Color.unSelected_color
            self.buttonYear1.setTitleColor(.black, for: .normal)
        }else if String(describing: medicineArr["duration_day"]!) == "Week"{
            buttonWeek1.isSelected = true
            buttonDay1.isSelected = false
            buttonMonth1.isSelected = false
            buttonYear1.isSelected = false
            self.buttonWeek1.backgroundColor = Color.selected_color
            self.buttonWeek1.setTitleColor(.white, for: .normal)
            self.duration_day = "Week"
            self.buttonDay1.backgroundColor = Color.unSelected_color
            self.buttonDay1.setTitleColor(.black, for: .normal)
            self.buttonMonth1.backgroundColor = Color.unSelected_color
            self.buttonMonth1.setTitleColor(.black, for: .normal)
            self.buttonYear1.backgroundColor = Color.unSelected_color
            self.buttonYear1.setTitleColor(.black, for: .normal)
        }else if String(describing: medicineArr["duration_day"]!) == "Month"{
            buttonMonth1.isSelected = true
            buttonWeek1.isSelected = false
            buttonDay1.isSelected = false
            buttonYear1.isSelected = false
            self.buttonMonth1.backgroundColor = Color.selected_color
            self.buttonMonth1.setTitleColor(.white, for: .normal)
            self.duration_day = "Month"
            self.buttonDay1.backgroundColor = Color.unSelected_color
            self.buttonDay1.setTitleColor(.black, for: .normal)
            self.buttonWeek1.backgroundColor = Color.unSelected_color
            self.buttonWeek1.setTitleColor(.black, for: .normal)
            self.buttonYear1.backgroundColor = Color.unSelected_color
            self.buttonYear1.setTitleColor(.black, for: .normal)
        }else if String(describing: medicineArr["duration_day"]!) == "Year"{
            buttonYear1.isSelected = true
            buttonMonth1.isSelected = false
            buttonWeek1.isSelected = false
            buttonDay1.isSelected = false
            self.buttonYear1.backgroundColor = Color.selected_color
            self.buttonYear1.setTitleColor(.white, for: .normal)
            self.duration_day = "Year"
            self.buttonDay1.backgroundColor = Color.unSelected_color
            self.buttonDay1.setTitleColor(.black, for: .normal)
            self.buttonWeek1.backgroundColor = Color.unSelected_color
            self.buttonWeek1.setTitleColor(.black, for: .normal)
            self.buttonMonth1.backgroundColor = Color.unSelected_color
            self.buttonMonth1.setTitleColor(.black, for: .normal)
        }
        
       
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonDoseClicked(_ sender: UIButton) {
        if sender.tag == 2{
               dose = dose+1
        }else{
            if dose > 1 {
               dose = dose-1
            }
        }
        labelDoseCount.text = String(dose)
    }
    
    @IBAction func buttonFrequenceyClicked(_ sender: UIButton) {
        if sender.tag == 4{
              frequency = frequency+1
        }else{
            if frequency > 1 {
              frequency = frequency-1
            }
        }
        labelFreqCount.text = String(frequency)
    }
    
    @IBAction func buttonDurationCountClicked(_ sender: UIButton) {
        if sender.tag == 6{
            duration = duration+1
        }else{
            if duration > 1 {
                duration = duration-1
            }
        }
        labelDurationCount.text = String(duration)
    }
    
    @IBAction func buttonEveryClicked(_ sender: UIButton) {
        if sender.tag == 11{
            if buttonDay.isSelected != true{
                buttonDay.isSelected = true
                buttonWeek.isSelected = false
                buttonMonth.isSelected = false
                buttonYear.isSelected = false
                self.buttonDay.backgroundColor = Color.selected_color
                self.buttonDay.setTitleColor(.white, for: .normal)
                self.frequency_day = "Day"
            }else{
                buttonDay.isSelected = false
                self.buttonDay.backgroundColor = Color.unSelected_color
                self.buttonDay.setTitleColor(.black, for: .normal)
                 self.frequency_day = ""
            }
            self.buttonWeek.backgroundColor = Color.unSelected_color
            self.buttonWeek.setTitleColor(.black, for: .normal)
            self.buttonMonth.backgroundColor = Color.unSelected_color
            self.buttonMonth.setTitleColor(.black, for: .normal)
            self.buttonYear.backgroundColor = Color.unSelected_color
            self.buttonYear.setTitleColor(.black, for: .normal)
            
        }else if sender.tag == 12{
            if buttonWeek.isSelected != true{
                buttonWeek.isSelected = true
                buttonDay.isSelected = false
                buttonMonth.isSelected = false
                buttonYear.isSelected = false
                self.buttonWeek.backgroundColor = Color.selected_color
                self.buttonWeek.setTitleColor(.white, for: .normal)
                self.frequency_day = "Week"
            }else{
                buttonWeek.isSelected = false
                self.buttonWeek.backgroundColor = Color.unSelected_color
                self.buttonWeek.setTitleColor(.black, for: .normal)
                self.frequency_day = ""
            }
            self.buttonDay.backgroundColor = Color.unSelected_color
            self.buttonDay.setTitleColor(.black, for: .normal)
            self.buttonMonth.backgroundColor = Color.unSelected_color
            self.buttonMonth.setTitleColor(.black, for: .normal)
            self.buttonYear.backgroundColor = Color.unSelected_color
            self.buttonYear.setTitleColor(.black, for: .normal)
        }else if sender.tag == 13{
            if buttonMonth.isSelected != true{
                buttonMonth.isSelected = true
                buttonWeek.isSelected = false
                buttonDay.isSelected = false
                buttonYear.isSelected = false
                self.buttonMonth.backgroundColor = Color.selected_color
                self.buttonMonth.setTitleColor(.white, for: .normal)
                self.frequency_day = "Month"
            }else{
                buttonMonth.isSelected = false
                self.buttonMonth.backgroundColor = Color.unSelected_color
                self.buttonMonth.setTitleColor(.black, for: .normal)
                self.frequency_day = ""
            }
            self.buttonDay.backgroundColor = Color.unSelected_color
            self.buttonDay.setTitleColor(.black, for: .normal)
            self.buttonWeek.backgroundColor = Color.unSelected_color
            self.buttonWeek.setTitleColor(.black, for: .normal)
            self.buttonYear.backgroundColor = Color.unSelected_color
            self.buttonYear.setTitleColor(.black, for: .normal)
        }else if sender.tag == 14{
            if buttonYear.isSelected != true{
                buttonYear.isSelected = true
                buttonMonth.isSelected = false
                buttonWeek.isSelected = false
                buttonDay.isSelected = false
                self.buttonYear.backgroundColor = Color.selected_color
                self.buttonYear.setTitleColor(.white, for: .normal)
                self.frequency_day = "Year"
            }else{
                buttonYear.isSelected = false
                self.buttonYear.backgroundColor = Color.unSelected_color
                self.buttonYear.setTitleColor(.black, for: .normal)
                self.frequency_day = ""
            }
            self.buttonDay.backgroundColor = Color.unSelected_color
            self.buttonDay.setTitleColor(.black, for: .normal)
            self.buttonWeek.backgroundColor = Color.unSelected_color
            self.buttonWeek.setTitleColor(.black, for: .normal)
            self.buttonMonth.backgroundColor = Color.unSelected_color
            self.buttonMonth.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBAction func buttonDurationClicked(_ sender: UIButton) {
        
        if sender.tag == 15 {
            
            if buttonDay1.isSelected != true{
                buttonDay1.isSelected = true
                buttonWeek1.isSelected = false
                buttonMonth1.isSelected = false
                buttonYear1.isSelected = false
                self.buttonDay1.backgroundColor = Color.selected_color
                self.buttonDay1.setTitleColor(.white, for: .normal)
                self.duration_day = "Day"
            } else {
                buttonDay1.isSelected = false
                self.buttonDay1.backgroundColor = Color.unSelected_color
                self.buttonDay1.setTitleColor(.black, for: .normal)
                self.duration_day = ""
            }
            self.buttonWeek1.backgroundColor = Color.unSelected_color
            self.buttonWeek1.setTitleColor(.black, for: .normal)
            self.buttonMonth1.backgroundColor = Color.unSelected_color
            self.buttonMonth1.setTitleColor(.black, for: .normal)
            self.buttonYear1.backgroundColor = Color.unSelected_color
            self.buttonYear1.setTitleColor(.black, for: .normal)
      
        } else if sender.tag == 16 {
            
            if buttonWeek1.isSelected != true{
                buttonWeek1.isSelected = true
                buttonDay1.isSelected = false
                buttonMonth1.isSelected = false
                buttonYear1.isSelected = false
                self.buttonWeek1.backgroundColor = Color.selected_color
                self.buttonWeek1.setTitleColor(.white, for: .normal)
                self.duration_day = "Week"
            } else {
                buttonWeek1.isSelected = false
                self.buttonWeek1.backgroundColor = Color.unSelected_color
                self.buttonWeek1.setTitleColor(.black, for: .normal)
                self.duration_day = ""
            }
            self.buttonDay1.backgroundColor = Color.unSelected_color
            self.buttonDay1.setTitleColor(.black, for: .normal)
            self.buttonMonth1.backgroundColor = Color.unSelected_color
            self.buttonMonth1.setTitleColor(.black, for: .normal)
            self.buttonYear1.backgroundColor = Color.unSelected_color
            self.buttonYear1.setTitleColor(.black, for: .normal)
        
        } else if sender.tag == 17 {
            
            if buttonMonth1.isSelected != true{
                buttonMonth1.isSelected = true
                buttonWeek1.isSelected = false
                buttonDay1.isSelected = false
                buttonYear1.isSelected = false
                self.buttonMonth1.backgroundColor = Color.selected_color
                self.buttonMonth1.setTitleColor(.white, for: .normal)
                self.duration_day = "Month"
            } else {
                buttonMonth1.isSelected = false
                self.buttonMonth1.backgroundColor = Color.unSelected_color
                self.buttonMonth1.setTitleColor(.black, for: .normal)
                self.duration_day = ""
            }
            self.buttonDay1.backgroundColor = Color.unSelected_color
            self.buttonDay1.setTitleColor(.black, for: .normal)
            self.buttonWeek1.backgroundColor = Color.unSelected_color
            self.buttonWeek1.setTitleColor(.black, for: .normal)
            self.buttonYear1.backgroundColor = Color.unSelected_color
            self.buttonYear1.setTitleColor(.black, for: .normal)
      
        } else if sender.tag == 18 {
            
            if buttonYear1.isSelected != true{
                buttonYear1.isSelected = true
                buttonMonth1.isSelected = false
                buttonWeek1.isSelected = false
                buttonDay1.isSelected = false
                self.buttonYear1.backgroundColor = Color.selected_color
                self.buttonYear1.setTitleColor(.white, for: .normal)
                self.duration_day = "Year"
            } else {
                buttonYear1.isSelected = false
                self.buttonYear1.backgroundColor = Color.unSelected_color
                self.buttonYear1.setTitleColor(.black, for: .normal)
                self.duration_day = ""
            }
            self.buttonDay1.backgroundColor = Color.unSelected_color
            self.buttonDay1.setTitleColor(.black, for: .normal)
            self.buttonWeek1.backgroundColor = Color.unSelected_color
            self.buttonWeek1.setTitleColor(.black, for: .normal)
            self.buttonMonth1.backgroundColor = Color.unSelected_color
            self.buttonMonth1.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBAction func buttonSpecialSenses(_ sender: UIButton) {
        
        if sender.tag == 21{
            if buttonRightEye.isSelected != true{
                buttonRightEye.isSelected = true
//                buttonLeftEye.isSelected = false
                self.buttonRightEye.backgroundColor = Color.selected_color
                self.buttonRightEye.setTitleColor(.white, for: .normal)
                arrSenses.append("Right Eye")
//                arrSenses.removeAll { $0 == "Left Eye" }
            }else{
                buttonRightEye.isSelected = false
                self.buttonRightEye.backgroundColor = Color.unSelected_color
                self.buttonRightEye.setTitleColor(.black, for: .normal)
                arrSenses.removeAll { $0 == "Right Eye" }
            }
//            self.buttonLeftEye.backgroundColor = Color.unSelected_color
//            self.buttonLeftEye.setTitleColor(.black, for: .normal)
        }else if sender.tag == 22{
            if buttonLeftEye.isSelected != true{
                buttonLeftEye.isSelected = true
//                buttonRightEye.isSelected = false
                self.buttonLeftEye.backgroundColor = Color.selected_color
                self.buttonLeftEye.setTitleColor(.white, for: .normal)
                arrSenses.append("Left Eye")
//                arrSenses.removeAll { $0 == "Right Eye" }
            }else{
                buttonLeftEye.isSelected = false
                self.buttonLeftEye.backgroundColor = Color.unSelected_color
                self.buttonLeftEye.setTitleColor(.black, for: .normal)
                arrSenses.removeAll { $0 == "Left Eye" }
            }
//            self.buttonRightEye.backgroundColor = Color.unSelected_color
//            self.buttonRightEye.setTitleColor(.black, for: .normal)
        }else if sender.tag == 23{
            if buttonRightEar.isSelected != true{
                buttonRightEar.isSelected = true
//                buttonLeftEar.isSelected = false
                self.buttonRightEar.backgroundColor = Color.selected_color
                self.buttonRightEar.setTitleColor(.white, for: .normal)
                arrSenses.append("Right Ear")
//                arrSenses.removeAll { $0 == "Left Ear" }
            }else{
                buttonRightEar.isSelected = false
                self.buttonRightEar.backgroundColor = Color.unSelected_color
                self.buttonRightEar.setTitleColor(.black, for: .normal)
                arrSenses.removeAll { $0 == "Right Ear" }
            }
//            self.buttonLeftEar.backgroundColor = Color.unSelected_color
//            self.buttonLeftEar.setTitleColor(.black, for: .normal)
        }else if sender.tag == 24{
           
            if buttonLeftEar.isSelected != true{
                buttonLeftEar.isSelected = true
//                buttonRightEar.isSelected = false
                self.buttonLeftEar.backgroundColor = Color.selected_color
                self.buttonLeftEar.setTitleColor(.white, for: .normal)
                arrSenses.append("Left Ear")
//                arrSenses.removeAll { $0 == "Right Ear" }
            }else{
                buttonLeftEar.isSelected = false
                self.buttonLeftEar.backgroundColor = Color.unSelected_color
                self.buttonLeftEar.setTitleColor(.black, for: .normal)
                arrSenses.removeAll { $0 == "Left Ear" }
            }
//            self.buttonRightEar.backgroundColor = Color.unSelected_color
//            self.buttonRightEar.setTitleColor(.black, for: .normal)
        }
        print(arrSenses)
    }
    
    @IBAction func buttonSubmitClicked(_ sender: Any) {
        
        if trade_id == nil && (textViewMedicine.text == nil || textViewMedicine.text == "Write here…"){
            self.showAlert(with: "Please select trade name or write a note")
            return
        }
        
        
        let medicine:Medicine? = Medicine()
        medicine!.dose_number = String(dose)
        medicine!.dose_time = dose_time ?? ""
        medicine!.duration = duration
        medicine!.duration_day = duration_day ?? ""
        medicine!.frequency = frequency
        medicine!.frequency_day = frequency_day ?? ""
        medicine!.genric_id = genric_id ?? 0
        medicine!.genric_name = genric_name ?? ""
        medicine!.notes = note
        medicine!.precription_sence = arrSenses
        medicine!.trade_id = trade_id ?? 0
        medicine!.trade_name = trade_name ?? ""

        self.delegate?.selectedMedicine(dict: medicine!, type: prescriptionType!, editIndex: editIndex)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUserInterface(){
        textFieldTradeName.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            self.trade_id = nil
            self.selectedType = 2
            self.textFieldAction()
        }
        textFieldGenericName.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            if self.trade_id == nil {
//                self.trade_id = 0
//                self.showAlert(with: "Please select trade")
                self.selectedType = 1
                self.textFieldAction()
//                return
            }
            
        }
        textFieldMorning.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
             self.selectedType = 3
             self.textFieldAction()
        }
    }
    
    func textFieldAction(){
        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "CategoryListViewController") as! CategoryListViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        if selectedType == 1 {
            if self.trade_id == nil {
                vc.tradeId = ""
            } else {
                vc.tradeId = "\(self.trade_id!)"
            }
            
        }
        
        vc.listType = selectedType
        vc.view.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        self.present(vc, animated: false, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write here…"{
            textViewMedicine.text = ""
        }
        textViewMedicine.textColor = UIColor.black
    }
    
    func textViewDidChange(_ textView: UITextView) {
         note = textView.text
    }
}

extension AddMedicineViewController: categorySelectedDelegate{
    
    func selectedGeneric(selectedGeneric: GenericData) {
        print(selectedGeneric)
        self.selectedGeneric = selectedGeneric
        self.genric_name = selectedGeneric.name
        self.genric_id = selectedGeneric.id
        textFieldGenericName.text = selectedGeneric.name
    }
    
    func selectedTrade(selectedTrade: TradeData) {
        print(selectedTrade)
        self.selectedTrade = selectedTrade
        self.trade_name = selectedTrade.name
        self.trade_id = selectedTrade.id
        textFieldTradeName.text = selectedTrade.name
    }
    
    func selectedTime(selectedTime: String) {
        print(selectedTime)
        self.dose_time = selectedTime
        textFieldMorning.text = selectedTime
    }
    
    func selectedCategory(selectedCategory: CategoryList) {
        print(selectedCategory)
    }
    
}
