//
//  PatientHistoryViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PatientHistoryViewController: BaseViewControler, IndicatorInfoProvider {

    @IBOutlet weak var errorDescription: UILabel!
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var tablePatientHistory: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    var userType: String?
    var appointments = [DataAppointmentList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getPatientHostory()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        errorTitleLabel.text = localizeValue(key: "History list is empty")
        errorDescription.text = localizeValue(key: "You don't have any history")
     }

    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "History"))
    }
    
    func getPatientHostory() {
        if AppInstance.shared.userType == 0{userType = "1"}else{ userType = "2"}
        let info: Patient_info?
        if AppInstance.shared.isShowHistoryHeaderCell == false{
            info = AppInstance.shared.patientListSelectedPatient
        }else{
            info = AppInstance.shared.selectedPatient?.patient_info
        }
       
        let service = BusinessService()
        service.patientHistory(with: String(describing: ((AppInstance.shared.userType == 0) ? (AppInstance.shared.user!.id ?? 0) : (AppInstance.shared.user?.clinic_id ?? 0))), patient_id: String(describing: info!.id!), current_page: "1", type: userType!, target: self) { (response) in
            
            if response == nil{
                self.errorView.isHidden = false
            }
            
            if let list = response {
                print(list)
                self.appointments.removeAll()
                
//                let date = Date()
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateFormat = "yyyy-MM-dd"
//                let result = dateFormatter.string(from: date)
//                let dateToday = dateFormatter.date(from: result)
                
                if (list.data?.data?.count)! > 0 {
                    for element in list.data!.data! {
                       // let date1 = dateFormatter.date(from: element.appointment_date!)
                        self.appointments.append(element)
                    }
                    
                    self.errorView.isHidden = true
                } else {
                    self.errorView.isHidden = false
                }
            }
            
            self.tablePatientHistory.reloadData()
        }
    }
}

extension PatientHistoryViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
//        if AppInstance.shared.isShowHistoryHeaderCell == false{
//            return appointments.count
//        }
//        return appointments.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if AppInstance.shared.isShowHistoryHeaderCell == false{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PatientHistoryCell", for: indexPath) as! PatientHistoryCell
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelAppointmrntDate.textAlignment = .left
                cell.labelDr.textAlignment = .right
                cell.labelDrName.textAlignment = .right
            }
            cell.buttonType.setTitle(localizeValue(key: "Details"), for: .normal)
            let data = self.appointments[indexPath.row]
            if data.patient_info!.image!.isEmpty != true{
                cell.imageViewProfile.sd_setShowActivityIndicatorView(true)
                cell.imageViewProfile.sd_setIndicatorStyle(.gray)
                cell.imageViewProfile.sd_setImage(with: URL(string: (data.patient_info!.image!)), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                cell.imageViewProfile.sd_setShowActivityIndicatorView(false)
            }else{
                cell.imageViewProfile.image = UIImage(named: "imgUserPlaceholderSideMenu")
            }
            if data.appointment_type == 1{
                cell.labelAppointmentType.text = "New Visit".localize()
            }else if data.appointment_type == 2{
                cell.labelAppointmentType.text = "Consultation".localize()
            }
            cell.labelDrName.text = "\(String(describing: data.patient_info!.firstname!)) \(String(describing: data.patient_info!.lastname!))"
            let dateStr = TimeUtils.sharedUtils.chaneDateFormet(data.appointment_date)
            if data.set_time == "" {
                cell.labelAppointmrntDate.text = "\(dateStr)"
            } else {
                
                var start_time = data.set_time
                let last = String(start_time!.characters.suffix(2))
                var new_start_time = start_time!.dropLast(2)
                if last == "AM" {
                    new_start_time = new_start_time + localizeValue(key: "AM")
                }else{
                    new_start_time = new_start_time + localizeValue(key: "PM")
                }
                
                 cell.labelAppointmrntDate.text = "\(dateStr) | \(String(describing: (new_start_time ?? "")))"
               
            }
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                 cell.labelDr.text = data.clinic?.clinic_name_ar
                
            }else{
                 cell.labelDr.text = data.clinic?.clinic_name
            }
           
//            data.doctor_info?.about//data.clinic?.clinic_name
            
            return cell
        }else{
            switch indexPath.row {
            case 0:
                
                if AppInstance.shared.userType == 0{
                    
                    let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! PatientHistoryCell
                   
                    
                    let data = AppInstance.shared.selectedPatient
                    if data?.patient_info!.image!.isEmpty != true{
                        headerView.imageViewProfileHeader.sd_setShowActivityIndicatorView(true)
                        headerView.imageViewProfileHeader.sd_setIndicatorStyle(.gray)
                        headerView.imageViewProfileHeader.sd_setImage(with: URL(string: (data!.patient_info!.image!)), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                        headerView.imageViewProfileHeader.sd_setShowActivityIndicatorView(false)
                    }else{
                        headerView.imageViewProfileHeader.image = UIImage(named: "imgUserPlaceholderSideMenu")
                    }
                    if data?.appointment_type == 1{
                        headerView.labelAppointmentTypeHeader.text = "New Visit".localize()
                    }else if data?.appointment_type == 2{
                        headerView.labelAppointmentTypeHeader.text = "Consultation".localize()
                    }
                    headerView.labelDrNameHeader.text = "\(String(describing: data!.patient_info!.firstname!)) \(String(describing: data!.patient_info!.lastname!))"
                    let dateStr = TimeUtils.sharedUtils.chaneDateFormet(data!.appointment_date)
                    
                    if data?.set_time == "" {
                        headerView.labelAppointmrntDateHeader.text = "\(dateStr)"
                        
                        
                        
                        /////////
                        var start_time = data!.set_time
                        let last = String(start_time!.characters.suffix(2))
                        var new_start_time = start_time!.dropLast(2)
                        if last == "AM" {
                            new_start_time = new_start_time + localizeValue(key: "AM")
                        }else{
                            new_start_time = new_start_time + localizeValue(key: "PM")
                        }
                        
                        headerView.labelAppointmrntDateHeader.text = "\(dateStr) | \(String(describing: (new_start_time ?? "")))"
                        
                        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                            headerView.labelDrHeader.text = data?.clinic?.clinic_name_ar
                            headerView.labelAppointmrntDateHeader.textAlignment = .left
                            headerView.labelDrNameHeader.textAlignment = .right
                            headerView.labelDrHeader.textAlignment = .right
                        }else{
                            headerView.labelDrHeader.text = data?.clinic?.clinic_name
                        }
                        ///////
                    } else {
                        
                        
                        var start_time = data!.set_time
                        let last = String(start_time!.characters.suffix(2))
                        var new_start_time = start_time!.dropLast(2)
                        if last == "AM" {
                            new_start_time = new_start_time + localizeValue(key: "AM")
                        }else{
                            new_start_time = new_start_time + localizeValue(key: "PM")
                        }
                        
                         headerView.labelAppointmrntDateHeader.text = "\(dateStr) | \(String(describing: (new_start_time ?? "")))"
                        
                        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                             headerView.labelDrHeader.text = data?.clinic?.clinic_name_ar
                              headerView.labelAppointmrntDateHeader.textAlignment = .left
                                                    headerView.labelDrNameHeader.textAlignment = .right
                                                    headerView.labelDrHeader.textAlignment = .right
                        }else{
                             headerView.labelDrHeader.text = data?.clinic?.clinic_name
                        }
                    }
                    headerView.buttonTypeHeader.setTitle(localizeValue(key: "Check Up"), for: .normal)
                   
                    //                data?.doctor_info?.about//data.clinic?.clinic_name
                    return headerView
                }else{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PatientHistoryCell", for: indexPath) as! PatientHistoryCell
                    if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                        cell.labelAppointmrntDate.textAlignment = .left
                        cell.labelDr.textAlignment = .right
                        cell.labelDrName.textAlignment = .right
                    }
                 
                    guard let data = AppInstance.shared.selectedPatient else { return cell }
                    if data.patient_info!.image!.isEmpty != true{
                        cell.imageViewProfile.sd_setShowActivityIndicatorView(true)
                        cell.imageViewProfile.sd_setIndicatorStyle(.gray)
                        cell.imageViewProfile.sd_setImage(with: URL(string: (data.patient_info!.image!)), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                        cell.imageViewProfile.sd_setShowActivityIndicatorView(false)
                    }else{
                        cell.imageViewProfile.image = UIImage(named: "imgUserPlaceholderSideMenu")
                    }
                    if data.appointment_type == 1{
                        cell.labelAppointmentType.text = "New Visit".localize()
                    }else if data.appointment_type == 2{
                        cell.labelAppointmentType.text = "Consultation".localize()
                    }
                    cell.labelDrName.text = "\(String(describing: data.patient_info!.firstname!)) \(String(describing: data.patient_info!.lastname!))"
                    let dateStr = TimeUtils.sharedUtils.chaneDateFormet(data.appointment_date)
                    
                    if data.set_time == "" {
                        cell.labelAppointmrntDate.text = "\(dateStr)"
                    } else {
                        var start_time = data.set_time
                        let last = String(start_time!.characters.suffix(2))
                        var new_start_time = start_time!.dropLast(2)
                        if last == "AM" {
                            new_start_time = new_start_time + localizeValue(key: "AM")
                        }else{
                            new_start_time = new_start_time + localizeValue(key: "PM")
                        }
                       cell.labelAppointmrntDate.text = "\(dateStr) | \(String(describing: (new_start_time ?? "")))"
                    }
                    
                    cell.labelDr.text = data.clinic?.clinic_name
                    //                data.doctor_info?.about//data.clinic?.clinic_name
                    
                    return cell
                }
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PatientHistoryCell", for: indexPath) as! PatientHistoryCell
                
             
                let data = self.appointments[indexPath.row-1]
                if data.patient_info!.image!.isEmpty != true{
                    cell.imageViewProfile.sd_setShowActivityIndicatorView(true)
                    cell.imageViewProfile.sd_setIndicatorStyle(.gray)
                    cell.imageViewProfile.sd_setImage(with: URL(string: (data.patient_info!.image!)), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                    cell.imageViewProfile.sd_setShowActivityIndicatorView(false)
                }else{
                    cell.imageViewProfile.image = UIImage(named: "imgUserPlaceholderSideMenu")
                }
                if data.appointment_type == 1{
                    cell.labelAppointmentType.text = "New Visit".localize()
                }else if data.appointment_type == 2{
                    cell.labelAppointmentType.text = "Consultation".localize()
                }
                cell.labelDrName.text = "\(String(describing: data.patient_info!.firstname!)) \(String(describing: data.patient_info!.lastname!))"
                let dateStr = TimeUtils.sharedUtils.chaneDateFormet(data.appointment_date)
                
                if data.set_time == "" {
                    cell.labelAppointmrntDate.text = "\(dateStr)"
                } else {
                    
                    
                    var start_time = data.set_time
                    let last = String(start_time!.characters.suffix(2))
                    var new_start_time = start_time!.dropLast(2)
                    if last == "AM" {
                        new_start_time = new_start_time + localizeValue(key: "AM")
                    }else{
                        new_start_time = new_start_time + localizeValue(key: "PM")
                    }
                    
                    cell.labelAppointmrntDate.text = "\(dateStr) | \(String(describing: (new_start_time ?? "")))"
                }
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.labelAppointmrntDate.textAlignment = .left
                    cell.labelDr.textAlignment = .right
                    cell.labelDrName.textAlignment = .right
                    cell.labelDr.text = data.clinic?.clinic_name_ar
                }else{
                    cell.labelDr.text = data.clinic?.clinic_name
                }
               
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PatientHistoryDetailViewController") as! PatientHistoryDetailViewController
        
        if AppInstance.shared.isShowHistoryHeaderCell == false{
            AppInstance.shared.isShowPlus = false
            AppInstance.shared.selectedDr =  self.appointments[indexPath.row]
            AppInstance.shared.patientName = "\(self.appointments[indexPath.row].patient_info?.firstname ?? "") \(self.appointments[indexPath.row].patient_info?.lastname ?? "")"
        }else{
            if indexPath.row == 0 {
                AppInstance.shared.isShowPlus = true
                AppInstance.shared.selectedDr = AppInstance.shared.selectedPatient
                
                AppInstance.shared.patientName = "\(AppInstance.shared.selectedPatient?.patient_info?.firstname ?? "") \(AppInstance.shared.selectedPatient?.patient_info?.lastname ?? "")"
            } else {
                
                AppInstance.shared.patientName = "\(self.appointments[indexPath.row-1].patient_info?.firstname ?? "") \(self.appointments[indexPath.row-1].patient_info?.lastname ?? "")"
                AppInstance.shared.isShowPlus = true
                AppInstance.shared.selectedDr =  self.appointments[indexPath.row-1]
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
