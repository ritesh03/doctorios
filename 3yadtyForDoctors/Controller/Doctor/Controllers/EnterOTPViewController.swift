//
//  EnterOTPViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 21/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class EnterOTPViewController: BaseViewControler, CountdownTimerDelegate {
    
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var countdownBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lbl_resend: UILabel!
    @IBOutlet weak var btn_resend: RoundButton!
    @IBOutlet weak var imageViewFlag: UIImageView!
    @IBOutlet weak var labelCountaryCode: UILabel!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var codeInputView: UIView!
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var buttonSelect: RoundButton!
    @IBOutlet weak var tf_first_digit: UITextField!
    @IBOutlet weak var tf_second_digit: UITextField!
    @IBOutlet weak var tf_third_digit: UITextField!
    @IBOutlet weak var tf_fourth_digit: UITextField!

    var otp: String?
    var userResponse: User?

    lazy var countdownTimer: CountdownTimer = {
        let countdownTimer = CountdownTimer()
        return countdownTimer
    }()
    // Test, for dev
    let selectedSecs:Int = 120
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        // Do any additional setup after loading the view.
        if #available(iOS 10.0, *) {
            tf_first_digit.keyboardType = .asciiCapableNumberPad
            tf_second_digit.keyboardType = .asciiCapableNumberPad
            tf_third_digit.keyboardType = .asciiCapableNumberPad
            tf_fourth_digit.keyboardType = .asciiCapableNumberPad
        } else {
            // Fallback on earlier versions
        }
       
        super.viewDidLoad()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            countdownBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            backbtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            titleLabel.textAlignment = .right
            textFieldPhone.textAlignment = .right
            codeInputView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_first_digit.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_second_digit.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_third_digit.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_fourth_digit.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        
        btn_resend.setTitle(localizeValue(key: "Resend SMS"), for: .normal)
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "You will receive SMS within 2 mins", comment: "")
        lbl_resend.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "If you didn't get code tap on resend!", comment: "")
       
        
        buttonSelect.isEnabled = false
        textFieldPhone.delegate = self
        btn_resend.isHidden = true
        lbl_resend.isHidden = true
        
        imageViewFlag.image = AppInstance.shared.selectedFlag
        labelCountaryCode.text = "\(String(describing: AppInstance.shared.phoneCode ?? ""))"
        textFieldPhone.text = AppInstance.shared.mobile
        
        //timer
        countdownTimer.delegate = self
        self.startTimer()
        self.tf_first_digit.becomeFirstResponder()
        
        self.tf_first_digit.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        self.tf_second_digit.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        self.tf_third_digit.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        self.tf_fourth_digit.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        countdownTimer.stop()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private  func startTimer(){
        
        countdownTimer.setTimer(hours: 0, minutes: 0, seconds: selectedSecs)
        countdownTimer.start()
    }
    

    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.codeInputView.becomeFirstResponder()
    }
    
    func codeInputView(_ codeInputView: CodeInputView, didFinishWithCode code: String) {
        self.codeInputView.endEditing(true)
        buttonSelect.isEnabled = true
        buttonSelect.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
        otp = code
    }
    
    func codeInputView(_ codeInputView: CodeInputView, didDeleteCode code: Int) {
        buttonSelect.isEnabled = false
        buttonSelect.backgroundColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
    }
    
    //MARK: - Countdown Timer Delegate
    func countdownTime(time: (hours: String, minutes: String, seconds: String)) {
        labelTimer.text = "\(time.minutes):\(time.seconds)"
        if "\(time.minutes):\(time.seconds)" == "00:00"{
            self.btn_resend.isHidden = false
            lbl_resend.isHidden = false
        }
    }
    
    private func saveDataToFirebaseRefrence(userData: UserData?) {
         
         let token = UserDefaults.standard.value(forKey: "token")
         if CommonClass.isLiveServer() {
             Constants.refs.databaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
         } else {
             Constants.refs.testDatabaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
         }
         
     }
    
    private func getOTP() -> String{
        
        self.otp = "\(self.tf_first_digit.text!)\(self.tf_second_digit.text!)\(self.tf_third_digit.text!)\(self.tf_fourth_digit.text!)"
        return self.otp!
    }
    
    fileprivate func post(OTP otp: String){
        
        //ForgotPasswordViewController
        if AppInstance.shared.isFromForgot == true {
            var type: String?
            if AppInstance.shared.userType == 0{
                type = "2"  //Doctor
            }else{
                type = "3"  //Assistant
            }
            
            let service = BusinessService()
            service.forgotOtpVerification(with: CommonClass.checkAndEditPhoneNumberFor(phoneNumber: AppInstance.shared.mobile!), otp:  otp, type: type!, target: self) { (response) in
                if response == true {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            if AppInstance.shared.userType == 0 {
                let service = BusinessService()
                service.otpVarify(with: CommonClass.checkAndEditPhoneNumberFor(phoneNumber: AppInstance.shared.mobile!), otp: otp, path: Config.otpVarify, target: self) { (response) in
                    if response == true {
                        AppInstance.shared.isFromMenu = false
                        AppInstance.shared.isOtpVerified = true
                        paramEdit = [String:Any]()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorProfileVC") as! DoctorProfileVC
                        ///////
                        self.saveDataToFirebaseRefrence(userData: self.userResponse?.data!)
                        AppInstance.shared.user = self.userResponse?.data!
                        CustomObjects.archive(Object: self.userResponse!.data!.dictionaryRepresentation() as AnyObject, WithKey: "isLogIn")
                        if let signUpDict = AppInstance.shared.doctorSignUp{
                            CustomObjects.archive(Object: signUpDict as AnyObject, WithKey: "userdata")
                        }
                        ////
                        vc.fromOtp = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }else{
                let service = BusinessService()
                service.otpVarify(with: CommonClass.checkAndEditPhoneNumberFor(phoneNumber: AppInstance.shared.mobile!), otp: otp, path: Config.assistant_otpverify, target: self) { (response) in
                    if response == true {
                        AppInstance.shared.isFromMenu = false
                        AppInstance.shared.isOtpVerified = true
                        paramEdit = [String:Any]()
                        let storyboard = UIStoryboard(name: "Assistant", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "AssistantSignUpViewController") as! AssistantSignUpViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func countdownTimerDone() {
        print("countdownTimerDone")
    }
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnResend(_ sender: Any) {
        
        let phoneCode = ((AppInstance.shared.phoneCode ?? "") as NSString)
        var apiEndPoint = ""
        if AppInstance.shared.isFromForgot == true {
            apiEndPoint = Config.forgotPassword
            var type: String?
            if AppInstance.shared.userType == 0{
                type = "2" //Doctor
            }else{
                type = "3" //Assistant
            }
            let service = BusinessService()
            service.forgotPassword(with: AppInstance.shared.mobile ?? "", type: type!,countryCode: phoneCode as String, target: self) { (response) in
                if response {
                    self.btn_resend.isHidden = true
                    self.lbl_resend.isHidden = true
                    self.countdownTimer.setTimer(hours: 0, minutes: 0, seconds: self.selectedSecs)
                    self.countdownTimer.start()
                }
            }
        } else {
            if AppInstance.shared.userType == 0 { //Doctor
            apiEndPoint = Config.doctorResend
            } else {
              apiEndPoint = Config.assistantResend  //Assistant
            }
            let service = BusinessService()
            
            service.resenOTP(with: AppInstance.shared.mobile ?? "", countryCode: phoneCode as String,endPoint:apiEndPoint, target: self) { (response) in
                if response{
                    self.btn_resend.isHidden = true
                    self.lbl_resend.isHidden = true
                    self.countdownTimer.setTimer(hours: 0, minutes: 0, seconds: self.selectedSecs)
                    self.countdownTimer.start()
                }
            }
        }

    }
    
    @IBAction func buttonSelectClicked(_ sender: Any) {
    
    }
}


extension EnterOTPViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        var OTPStr: String = ""
        if self.tf_first_digit == textField{
            
            OTPStr = "\((str as String))\(self.tf_second_digit.text!)\(self.tf_third_digit.text!)\(self.tf_fourth_digit.text!)"
        }
        if self.tf_second_digit == textField{
            
            OTPStr = "\(self.tf_first_digit.text!)\(str as String)\(self.tf_third_digit.text!)\(self.tf_fourth_digit.text!)"
        }
        if self.tf_third_digit == textField{
            
            OTPStr = "\(self.tf_first_digit.text!)\(self.tf_second_digit.text!)\(str as String)\(self.tf_fourth_digit.text!)"
        }
        if self.tf_fourth_digit == textField{
            
            OTPStr = "\(self.tf_first_digit.text!)\(self.tf_second_digit.text!)\(self.tf_third_digit.text!)\(str as String)"
        }
        
        //        self.buttonSelect.isEnabled = isOTPFiled
        //        self.buttonSelect.backgroundColor = isOTPFiled ? UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1) : UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
        if (OTPStr.replacingOccurrences(of: " ", with: "")).count == 4{
            
            self.post(OTP: OTPStr)
        }
        let isFieldValid = (str.length <= 1)
        if !isFieldValid{
            
            switch textField{
            case self.tf_first_digit:
                self.tf_second_digit.becomeFirstResponder()
            case self.tf_second_digit:
                self.tf_third_digit.becomeFirstResponder()
            case self.tf_third_digit:
                self.tf_fourth_digit.becomeFirstResponder()
            case self.tf_fourth_digit:
                self.tf_fourth_digit.resignFirstResponder()
            default:
                break
            }
        }
        return isFieldValid
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        if text!.utf16.count == 1{
            switch textField{
            case self.tf_first_digit:
                self.tf_second_digit.becomeFirstResponder()
            case self.tf_second_digit:
                self.tf_third_digit.becomeFirstResponder()
            case self.tf_third_digit:
                self.tf_fourth_digit.becomeFirstResponder()
            case self.tf_fourth_digit:
                self.tf_fourth_digit.resignFirstResponder()
            default:
                break
            }
        }
        if text!.utf16.count == 0{
            switch textField{
            case self.tf_first_digit:
                self.tf_first_digit.becomeFirstResponder()
            case self.tf_second_digit:
                self.tf_first_digit.becomeFirstResponder()
            case self.tf_third_digit:
                self.tf_second_digit.becomeFirstResponder()
            case self.tf_fourth_digit:
                self.tf_third_digit.becomeFirstResponder()
            default:
                break
            }
        }
    }
}
