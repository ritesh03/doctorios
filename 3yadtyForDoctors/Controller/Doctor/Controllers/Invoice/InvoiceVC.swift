//
//  InvoiceVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 30/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol ClinicDelegate {
    
    func didSelect(clinic: Clinic)
}

class InvoiceVC: BaseViewControler {

    //MARK:- Outlets
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var tf_clinic: CustomTextField!
    @IBOutlet weak var constraint_height: NSLayoutConstraint!
    @IBOutlet weak var tf_month: CustomTextField!
    @IBOutlet weak var tf_year: CustomTextField!
    @IBOutlet weak var tbl_invoice: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    //MARK:- Variabeles
    var invoiceArr:[[String:AnyObject]] = []
    var clinicArr: [Clinic] = []
    var commision = ""
    var payment_type = 0

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.errorView.isHidden = false
        self.tbl_invoice.isHidden = true
        headerTitleLabel.text = localizeValue(key: "Invoices")
        errorTitleLabel.text = localizeValue(key: "No invoice")
        errorDescriptionLabel.text = localizeValue(key: "You don't have any invoice")
        
        self.constraint_height.constant += (UIApplication.shared.statusBarFrame.size.height)
        self.textfieldsSetupUI()
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_clinic.textAlignment = .right
            tf_month.textAlignment = .right
            tf_year.textAlignment = .right
        }
    }
    
    //MARK:- Functions
    private func textfieldsSetupUI(){
        tf_clinic.text = localizeValue(key: "Select Clinic")
        guard let clinics = AppInstance.shared.user?.clinic else {
            return
        }
        self.clinicArr = clinics
        var clinicNamesArr = [String]()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             clinicNamesArr = self.clinicArr.map({($0.clinic_name_ar ?? "")})
        }
        else{
             clinicNamesArr = self.clinicArr.map({($0.clinic_name ?? "")})
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM"
        //current month number
        let strMonth = formatter.string(from: Date())
        let monthsArr:[MonthFormat] = [.jan,.feb,.mar,.apr,.may,.jun,.jul,.aug,.sep,.oct,.nov,.dec]
        //Current Month Index from Months numbers
        guard let currentMonth = monthsArr.map({$0.inMM}).index(of: strMonth) else { return }

        //Months Array with Names
        let tempMonthsArr:[String] = monthsArr.map({localizeValue(key: $0.inMMM)})
        
        formatter.dateFormat = "yyyy"
        let strYear = formatter.string(from: Date())
        let yearArr = self.getYears()
        guard let currentYear = yearArr.index(of: strYear)  else { return }
        
        let tempCurrentMonthArr = monthsArr.filter({$0.inMM == strMonth})
        var selectedMonth = ""
        
       //let tempCurrentMonthStr = localizeValue(key: tempCurrentMonthArr.last!.inMMM)
        
        self.tf_month.text = NSLocalizedString(self.localizeValue(key: "Select Month"), comment: "")
        self.tf_year.text = NSLocalizedString(self.localizeValue(key: "Select Year"), comment: "")

        self.tf_clinic.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            self.view.endEditing(true)
            if clinicNamesArr.count == 0{
                UIAlertController.showToast(title: "No Clinics Found.", message: nil, target: self, handler: nil)
                return
            }
            
            ActionSheetStringPicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "Select Clinic"), comment: ""), rows: clinicNamesArr as [Any], initialSelection: 0, doneBlock: {
                picker, value, index in
                self.tf_clinic.text = clinicNamesArr[value]
                self.getInvoiceList(by: selectedMonth, and: (self.tf_year.text! == "") ? "\(currentYear)" : self.tf_year.text!, and: self.tf_clinic.text!)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
        
        self.tf_year.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            self.view.endEditing(true)
            ActionSheetStringPicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "Select Year"), comment: ""), rows: yearArr as [Any], initialSelection: currentYear, doneBlock: {
                picker, value, index in
                self.tf_year.text = yearArr[value]
                self.getInvoiceList(by: selectedMonth, and: yearArr[value], and: ((self.tf_clinic.text! == "Select Clinic".localize()) ? (clinicNamesArr.last ?? "") : self.tf_clinic.text!))
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
        
        self.tf_month.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            
            ActionSheetStringPicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "Select Month"), comment: ""), rows: tempMonthsArr as [Any], initialSelection: currentMonth, doneBlock: {
                picker, value, index in
                self.tf_month.text = self.localizeValue(key: monthsArr[value].inMMM)
                selectedMonth = monthsArr[value].inMM
                self.getInvoiceList(by: monthsArr[value].inMM, and: (self.tf_year.text! == "") ? "\(currentYear)" : self.tf_year.text!, and: ((self.tf_clinic.text! == self.localizeValue(key: "Select Clinic")) ? (clinicNamesArr.last ?? "") : self.tf_clinic.text!))
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }


    }

    private func getYears() -> [String]{
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en")
        formatter.dateFormat = "yyyy"
        let strYear = formatter.string(from: Date())
        guard let currentYear = Int.init(strYear) else { return []}
        var strArr: [String] = []
        for i in 2019...currentYear {
            strArr.append("\(i)")
        }
        return strArr
    }
    
    private func getInvoiceList(by month: String,and year: String,and clinic: String) {
        
        guard let userObj = AppInstance.shared.user else { return }
        
        var clinic_id = 0
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             clinic_id = self.clinicArr.filter({$0.clinic_name_ar == self.tf_clinic.text}).last?.id ?? 0
        }
        else{
             clinic_id = self.clinicArr.filter({$0.clinic_name == self.tf_clinic.text}).last?.id ?? 0
        }
        
        let service = BusinessService()
        service.getInvoices(with: ["user_id":userObj.id!,"month":month,"year":year,"current_page":1,"clinic_id":clinic_id], target: self) { (response) in
            
            if let response = response as? [String:AnyObject]{
                
                if let dataArr = response["data"] as? [String:AnyObject]{
                    
                    if let dictArr = dataArr["data"] as? [[String:AnyObject]]{
                        
                        if dictArr.count > 0{
                            self.commision = response["commision"] as! String
                            self.payment_type = response["payment_type"] as! Int
                            self.invoiceArr = dictArr
                            self.errorView.isHidden = true
                            self.tbl_invoice.isHidden = false
                            if self.payment_type == 2 {
                            self.totalAmountLabel.text = "Flat Fee"
                            } else {
                            self.totalAmountLabel.text = "\(self.localizeValue(key: "Total amount")): \(response["amount"] as! Double) EGP"
                            }
                           
                            self.tbl_invoice.reloadData()
                        }else{
                            self.tbl_invoice.isHidden = true
                            self.errorView.isHidden = false
                        }
                        
                    }else{
                        self.tbl_invoice.isHidden = true
                        self.errorView.isHidden = false
                    }
                }else{
                    self.tbl_invoice.isHidden = true
                    self.errorView.isHidden = false
                }
            }else{
                self.tbl_invoice.isHidden = true
                self.errorView.isHidden = false
            }
        }
    }

    
    //MARK:- Action Outlets
    @IBAction func actionOnBack(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController?.pushViewController(VC, animated: false)
    }
}

extension InvoiceVC : UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.invoiceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceTVC", for: indexPath) as! InvoiceTVC

        let invoiceObj = self.invoiceArr[indexPath.row]
        guard let appointmentDate = TimeUtils.sharedUtils.dateFromString((invoiceObj["appointment_date"] as? String ?? ""), with: TimeUtilsFormatter.serverDate) else { return cell}
        let appointment_date = TimeUtils.sharedUtils.stringFromdate(appointmentDate, with: TimeUtilsFormatter.strDate)
        
        cell.feesLabel.text = localizeValue(key: "Appointment Fees :")
        cell.idLabel.text = localizeValue(key: "Appointment Id :")
        cell.statusLabel.text = localizeValue(key: "Booking Status :")
        cell.commisionTItleLabel.text = localizeValue(key: "Commission :")
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.feesLabel.textAlignment = .right
            cell.idLabel.textAlignment = .right
            cell.statusLabel.textAlignment = .right
             cell.commisionTItleLabel.textAlignment = .right
            cell.lbl_name.textAlignment = .right
            
            cell.lbl_date.textAlignment = .left
            cell.lbl_edp.textAlignment = .left
            cell.lbl_appoint_id.textAlignment = .left
            cell.bookingStatusLbl.textAlignment = .left
            cell.commisionLbl.textAlignment = .left
        }
        cell.lbl_appoint_id.text = "# \((invoiceObj["id"] as? Int ?? 0))"
        var patientInfo = invoiceObj["patient_info"]
        cell.lbl_name.text = "\(patientInfo!["firstname"] as? String ?? "") \(patientInfo!["firstname"] as? String ?? "")"
        cell.lbl_date.text = appointment_date
        cell.lbl_edp.text = "\(invoiceObj["amount"] as? Int ?? 0) EGP"
        
       
        if payment_type == 1 {
            let amount = ((invoiceObj["amount"] as? Int ?? 0) * Int(self.commision)!) / 100
            cell.commisionLbl.text = "\(amount) EGP"
        }else{
            cell.commisionLbl.isHidden = true
            cell.commisionTItleLabel.isHidden = true
        }
        if invoiceObj["appointment_status"] as! Int == 1 {
            cell.bookingStatusLbl.text = localizeValue(key: "Success")
            
        }else if invoiceObj["appointment_status"] as! Int == 2{
            cell.bookingStatusLbl.text = localizeValue(key: "Repeated")
            cell.commisionLbl.text = "0.0 EGP"
        }else{
            cell.bookingStatusLbl.text = localizeValue(key: "Closed Failure")
            cell.commisionLbl.text = "0.0 EGP"
        }
        
        return cell
    }
}
