//
//  InvoiceTVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 30/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

class InvoiceTVC: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var lbl_appoint_id: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_edp: UILabel!
    @IBOutlet weak var commisionLbl: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var commisionTItleLabel: UILabel!
    @IBOutlet weak var bookingStatusLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
