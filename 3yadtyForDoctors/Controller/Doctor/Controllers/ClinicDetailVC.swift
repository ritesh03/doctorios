//
//  ClinicDetailVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 14/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import ActionSheetPicker_3_0

class ClinicDetailVC: BaseViewControler {
    
    //MARK:- Outlets
    @IBOutlet weak var phoneCodeLabel: UILabel!
    @IBOutlet weak var tf_clinicName_arabic: UITextField!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var nextButton: RoundButton!
    @IBOutlet weak var clinicImagesLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    @IBOutlet weak var completeAddressLabel: UILabel!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var setOnMapButton: UIButton!
    @IBOutlet weak var hospitalClinicNameLabel: UILabel!
    @IBOutlet weak var titleHeaderLabel: UILabel!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var tf_address_arabic: UITextField!
    @IBOutlet weak var activity_area: UIActivityIndicatorView!
    @IBOutlet weak var tf_area: CustomTextField!
    @IBOutlet weak var view_header_height: NSLayoutConstraint!
    @IBOutlet weak var cv_photos: UICollectionView!
    @IBOutlet weak var btn_country: UIButton!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var view_country_picker: UIView!
    @IBOutlet weak var tf_clinic_name: UITextField!
    @IBOutlet weak var tf_location: UITextField!
    @IBOutlet weak var tf_city: CustomTextField!
    @IBOutlet weak var tf_address: UITextField!
    @IBOutlet weak var tf_fees: EnglishTextField!
    @IBOutlet weak var tf_phone_number: EnglishTextField!
    @IBOutlet weak var tbl_clinic: UITableView!
    
    //MARK:- Variables
    fileprivate var imageArr: [String] = ["addDataToPrescription"]
    fileprivate var selectedImage: Int!
    fileprivate var phoneCode: String!
    fileprivate var arrCityList = [CityList]()
    fileprivate var arrAreaList = [CityList]()
    fileprivate var parameters: [String:Any] = [:]
    
    var clinicTimming: [String:Any]?
    var selectedClinic:Int?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        titleHeaderLabel.text = localizeValue(key: "Clinic Information")
        hospitalClinicNameLabel.text = localizeValue(key: "Hospital / Clinic name(Eng)")
        cityLabel.text = localizeValue(key: "City")
        areaLabel.text = localizeValue(key: "Area")
        completeAddressLabel.text = localizeValue(key: "Address")
        feesLabel.text = localizeValue(key: "Fees (EGP)")
        clinicImagesLabel.text = localizeValue(key: "Clinic Images")
        tf_clinic_name.placeholder = localizeValue(key: "Hospital / Clinic name(Eng)")
        tf_city.placeholder = localizeValue(key: "Select city")
        tf_area.placeholder = localizeValue(key: "Select area")
        tf_fees.placeholder = localizeValue(key: "Fees (EGP)")
        if #available(iOS 10.0, *) {
            tf_fees.keyboardType = .asciiCapableNumberPad
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 10.0, *) {
            tf_phone_number.keyboardType = .asciiCapableNumberPad
        } else {
            // Fallback on earlier versions
        }
        tf_phone_number.placeholder = localizeValue(key: "Clinic phone no.")
        tf_location.placeholder = localizeValue(key: "Address / Location")
        tf_address.placeholder = localizeValue(key: "Address")
        setOnMapButton.setTitle(localizeValue(key: "Set on map"), for: .normal)
        nextButton.setTitle(localizeValue(key: "NEXT"), for: .normal)
        self.viewDidLoadSetupUI()
        self.btn_country.isEnabled = true
        self.btn_country.isHidden = false
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            
            
           phoneView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_phone_number.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            phoneCodeLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            btn_country.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_clinic_name.textAlignment = .right
            hospitalClinicNameLabel.textAlignment = .right
            cityLabel.textAlignment = .right
            areaLabel.textAlignment = .right
            completeAddressLabel.textAlignment = .right
            feesLabel.textAlignment = .right
            clinicImagesLabel.textAlignment = .right
            
            tf_location.textAlignment = .right
            tf_city.textAlignment = .right
            tf_area.textAlignment = .right
            tf_address.textAlignment = .right
            tf_fees.textAlignment = .right
            tf_phone_number.textAlignment = .left
            tf_fees.languageCode = "en"
            tf_phone_number.languageCode = "en"
        }
    }
    
    //MARK:- Functions
    private func viewDidLoadSetupUI(){
        
        self.btn_back.automaticallyRotateOnLanguageConversion()
        self.view_header_height.constant += (UIApplication.shared.statusBarFrame.size.height)
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        self.getCityList() //get all cities
        self.isCountryPicker(Show: false)
        self.parameters["clinic_image"] = []
        self.updateClinicDetail()
    }
    
    private func updateClinicDetail(){
        
        if let selectedClinic = self.selectedClinic{
            
            guard let userObj = AppInstance.shared.user else {return}
            guard let clinics = userObj.clinic else {return}
            let clinic = clinics[selectedClinic]
            if AppLanguage.currentAppleLanguage() ==  LanguageType.arabic {
                self.tf_city.text = clinic.clinic_city_name_ar ?? ""
                self.tf_area.text = clinic.clinic_area_name_ar ?? ""
            } else {
                self.tf_city.text = clinic.clinic_city_name ?? ""
                self.tf_area.text = clinic.clinic_area_name ?? ""
            }
            self.tf_clinic_name.text = clinic.clinic_name ?? ""
            self.tf_clinicName_arabic.text = clinic.clinic_name_ar ?? ""
            self.tf_address.text = clinic.clinic_address ?? ""
            self.tf_location.text = clinic.google_address ?? ""
            self.tf_address_arabic.text = clinic.clinic_address_ar ?? ""
            self.tf_phone_number.text = clinic.clinic_phone ?? ""
            
            
            self.tf_fees.text = "\(clinic.clinic_fees ?? 0)"
            
            
            let phoneCode = "\(CommonClass.checkAndEditCountryCodeFor(countryCode: clinic.clinic_code ?? "91"))"
            self.parameters["clinic_image"] = clinic.clinic_image
            countryPicker.setCountryByPhoneCode(phoneCode)
            if let clinicImages = clinic.clinic_image {
                self.imageArr = (clinicImages.map({($0.image ?? "")})).reversed()
                self.imageArr.append("addDataToPrescription")
                self.cv_photos.reloadData()
            }
            let cityID = String(describing: clinic.clinic_city!)
            self.getAreaList(city_id:cityID)
        }else{
            
            let lat = LocationManager.sharedInstance.latitude
            let lon = LocationManager.sharedInstance.longitude
            let countryCode = Locale.current.regionCode
            countryPicker.setCountry(countryCode!)
//            LocationManager.sharedInstance.reverseGeocodeLocationWithLatLon(latitude: lat, longitude: lon) { (dict, place, name) in
//                if let data = dict?.object(forKey: "country") {
//                    let cname = data as! String
//                    let locale = Locale(identifier: LocalizationSystem.sharedInstance.getLanguage())
//
//                    self.countryPicker.setCountry(locale.isoCode(for: cname)!)
//                }
//            }
            //            let countryCode = (Locale.current as NSLocale).countryCode
            //            countryPicker.setCountry(countryCode!)
        }
    }
    
    private func setCityTextFieldUI(){
        
        var data = [String]()
        for city in arrCityList {
            
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                data.append(city.arabic_name!)
            }else{
                data.append(city.name!)
            }
            
        }
        self.tf_city.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            //Write code for picker view here
            self.view.endEditing(true)
            ActionSheetStringPicker.show(withTitle: self.localizeValue(key: "Select city"), rows: data, initialSelection: 0, doneBlock: {
                picker, value, index in
                if data.count > 0 {
                    self.tf_city.text = data[value]
                    self.tf_area.text = ""
                    let cityID = String(describing: self.arrCityList[value].id!)
                    //API get arealist
                    self.parameters["clinic_city"] = cityID
                    self.parameters["clinic_city_name"] = data[value]
                    self.parameters["clinic_city_name_ar"] = self.arrCityList[value].arabic_name!
                    self.getAreaList(city_id: cityID)
                }
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
    }
    private func setAreaTextFieldUI(){
        
        var data = [String]()
        for city in arrAreaList {
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                data.append(city.arabic_name!)
            }else{
                data.append(city.name!)
            }
            
        }
        self.tf_area.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            //Write code for picker view here
            self.view.endEditing(true)
            ActionSheetStringPicker.show(withTitle: self.localizeValue(key: "Select area"), rows: data, initialSelection: 0, doneBlock: {
                picker, value, index in
                if data.count > 0 {
                    self.tf_area.text = data[value]
                    self.parameters["clinic_area_name"] = data[value]
                    self.parameters["clinic_area_name_ar"] = self.arrAreaList[value].arabic_name!
                    self.parameters["clinic_area"] = String(describing: self.arrAreaList[value].id!)
                }
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
    }
    
    fileprivate func actionOnImage(){
        
        let actionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: localizeValue(key: "Camera"), style: .default, handler: { (_) in
            
            self.openImage(Source: .camera)
        }))
        actionSheet.addAction(UIAlertAction.init(title: localizeValue(key: "Gallery"), style: .default, handler: { (_) in
            
            self.openImage(Source: .photoLibrary)
        }))
        actionSheet.addAction(UIAlertAction.init(title: localizeValue(key: "Dismiss"), style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    private func openImage(Source sourceType: UIImagePickerControllerSourceType){
        
        let picker = UIImagePickerController.init()
        picker.delegate = self
        picker.sourceType = sourceType
        self.present(picker, animated: true, completion: nil)
    }
    
    
    fileprivate func updateClinic(Photo photo: String){
        
        if self.imageArr.count == 1{
            
            self.imageArr.insert(photo, at: 0)
        }else if self.selectedImage != nil{
            
            self.imageArr.remove(at: self.selectedImage)
            self.imageArr.insert(photo, at: self.selectedImage)
        }else{
            
            if(self.imageArr.count == 15){
                
                self.imageArr.remove(at: 14)
                self.imageArr.append(photo)
            }else{
                
                self.imageArr.insert(photo, at: self.imageArr.count - 1)
            }
        }
        self.cv_photos.reloadData()
    }
    
    fileprivate func upload(Image image: UIImage){
        
        let imageFile: Array<File> = [File.init(name: "\(arc4random()).jpg", image: image, type: 3)]
        let service = BusinessService()
        service.uploadClinicImages(with: Config.addClinicImages, And: [:], image: imageFile, target: self) { (response) in
            
            if let response = response as? [String]{
                
                var tempImageArr: [[String:Any]] = []
                
                if let clinicImages = self.parameters["clinic_image"] as? [Any]{
                    
                    if clinicImages.count > 0{
                        
                        for value in clinicImages{
                            
                            if let tempValue = value as? ClinicImages{
                                
                                tempImageArr.append(tempValue.dictionaryRepresentation() as! [String : Any])
                            }else{
                                
                                tempImageArr.append(value as! [String : Any])
                            }
                        }
                        
                        self.updateClinic(Photo: (response.last ?? ""))
                        
                        if self.selectedImage != nil{
                            
                            if tempImageArr.count > self.selectedImage{
                                
                                tempImageArr.remove(at: self.selectedImage)
                                tempImageArr.insert(self.parse(image: (response.last ?? "")), at: self.selectedImage)
                            }else{
                                
                                tempImageArr.append(self.parse(image: (response.last ?? "")))
                            }
                        }else{
                            
                            tempImageArr.append(self.parse(image: (response.last ?? "")))
                        }
                        self.parameters["clinic_image"] = tempImageArr
                    }else{
                        
                        self.updateClinic(Photo: (response.last ?? ""))
                        self.parameters["clinic_image"] = [self.parse(image: (response.last!))]
                    }
                }else {
                    
                    self.updateClinic(Photo: (response.last ?? ""))
                    self.parameters["clinic_image"] = [self.parse(image: (response.last ?? ""))]
                }
            }else{
                
                if let response = response{
                    self.updateClinic(Photo: (response as! String))
                    self.parameters["clinic_image"] = [self.parse(image: (response as! String))]
                }
            }
        }
    }
    
    fileprivate func deleteImage() {
        
    }
    
    private func parse(image: String) -> [String:String]{
        
        return ["image":image]
    }
    
    private func isCountryPicker(Show show: Bool){
        
        self.view.endEditing(true)
        if show {
            self.view.addSubview(view_country_picker)
        }
        self.view_country_picker.isHidden = !show
    }
    
    private func isArea(Loading: Bool){
        
        self.activity_area.isHidden = !Loading
        Loading ? self.activity_area.startAnimating() : self.activity_area.stopAnimating()
    }
    
    func getCityList() {
        
        let service = BusinessService()
        service.getCityList(target: self) { (response) in
            if let city = response {
                self.arrCityList = city.data!
                self.setCityTextFieldUI()
                
            }
        }
    }
    
    private func getAreaList(city_id: String) {
        
        self.tf_area.isUserInteractionEnabled = false
        self.isArea(Loading: true)
        let service = BusinessService()
        service.getAreaList(with: city_id, target: self) { (response) in
            self.tf_area.isUserInteractionEnabled = true
            self.isArea(Loading: false)
            if let area = response {
                self.arrAreaList = area.data!
                self.setAreaTextFieldUI()
            }
        }
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if textField == self.tf_location{
            
            self.showMap()
        }
    }
    
    
    
    
    private func isFieldsValid() -> String{
        if self.tf_clinic_name.text!.isEmpty{
            return NSLocalizedString(localizeValue(key: "Enter clinic name"), comment: "")
        }else  if self.tf_clinicName_arabic.text!.isEmpty{
            
            return NSLocalizedString(localizeValue(key: "Enter clinic name"), comment: "")
        }
        else if self.tf_location.text!.isEmpty{
            
            return NSLocalizedString(localizeValue(key: "Select address/location"), comment: "")
        }else if self.tf_city.text!.isEmpty{
            
            return NSLocalizedString(localizeValue(key: "Please select city"), comment: "")
        }else if self.tf_area.text!.isEmpty{
            
            return NSLocalizedString(localizeValue(key: "Please select area"), comment: "")
        }else if self.tf_address.text!.isEmpty{
            
            return NSLocalizedString(localizeValue(key: "Please enter complete address"), comment: "")
        }else if self.tf_fees.text!.isEmpty{
            
            return NSLocalizedString(localizeValue(key: "Please enter clinic fees"), comment: "")
        }
        return ""
    }
    
    //MARK:- Action Outlets
    @IBAction private func actionOnBack(_ sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func actionOnSkip(_ sender: UIButton){
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController!.pushViewController(VC, animated: true)
    }
    
    @IBAction func actionOnCancel(_ sender: Any) {
        
        self.isCountryPicker(Show: false)
    }
    
    @IBAction func actionOnDone(_ sender: Any) {
        
        self.isCountryPicker(Show: false)
    }
    
    @IBAction func actionOnShowPicker(_ sender: Any) {
        
        self.isCountryPicker(Show: true)
    }
    
    @IBAction func actionOnMap(_ sender: Any) {
        
        self.showMap()
    }
    
    
    @IBAction func actionOnNext(_ sender: Any) {
        
        let validation = self.isFieldsValid()
        
        if !validation.isEmpty{
            
            UIAlertController.showToast(title: validation, message: nil, target: self, handler: nil)
            return
        }
        
        let phoneCode = (((self.phoneCode != nil) ? self.phoneCode : "") as NSString)
        
        self.parameters["clinic_country_code"] = String(self.phoneCode.dropFirst())
       
        if var phone = self.tf_phone_number.text {
            phone = CommonClass.checkAndEditPhoneNumberFor(phoneNumber: phone)
            self.parameters["clinic_mobile" ] = phone
        }
        
        self.parameters["clinic_name"] = self.tf_clinic_name.text ?? ""
        self.parameters["clinic_name_ar"] = self.tf_clinicName_arabic.text ?? ""
        self.parameters["clinic_address"] = self.tf_address.text ?? ""
        self.parameters["clinic_fees"] = self.tf_fees.text ?? ""
        self.parameters["clinic_address_ar"] = self.tf_address_arabic.text ?? ""
        
        if let selectedClinic = self.selectedClinic{
            
            guard let userObj = AppInstance.shared.user else {return}
            guard let clinics = userObj.clinic else {return}
            let clinic = clinics[selectedClinic]
            self.parameters["clinic_lat"] = (clinic.clinic_lat == nil) ? [] : (self.parameters["clinic_lat"] == nil) ? (clinic.clinic_lat ?? "") : (self.parameters["clinic_lat"] ?? "")
            self.parameters["clinic_long"] = (clinic.clinic_long == nil) ? [] : (self.parameters["clinic_long"] == nil) ? (clinic.clinic_long ?? "") : (self.parameters["clinic_long"] ?? "")
            self.parameters["google_address"] = (clinic.google_address == nil) ? [] : (self.parameters["google_address"] == nil) ? (clinic.google_address ?? "") : (self.parameters["google_address"] ?? "")
            self.parameters["clinic_image"] = (clinic.clinic_image == nil) ? [] : (self.parameters["clinic_image"] == nil) ? (clinic.clinic_image ?? []) : (self.parameters["clinic_image"] ?? [])
            self.parameters["clinic_area_name"] = (clinic.clinic_area_name == nil) ? [] : (self.parameters["clinic_area_name"] == nil) ? (clinic.clinic_area_name ?? "") : (self.parameters["clinic_area_name"] ?? "")
            self.parameters["clinic_area"] = (clinic.clinic_area == nil) ? [] : (self.parameters["clinic_area"] == nil) ? (clinic.clinic_area ?? "") : (self.parameters["clinic_area"] ?? "")
            self.parameters["clinic_city"] =  (clinic.clinic_city == nil) ? [] : (self.parameters["clinic_city"] == nil) ? (clinic.clinic_city ?? "") : (self.parameters["clinic_city"] ?? "")
            self.parameters["clinic_city_name"] = (clinic.clinic_city_name == nil) ? [] : (self.parameters["clinic_city_name"] == nil) ? (clinic.clinic_city_name ?? "") : (self.parameters["clinic_city_name"] ?? "")
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorAvailabiltyVC") as! DoctorAvailabiltyVC
        vc.parameters = parameters
        vc.selectedClinic = self.selectedClinic
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ClinicDetailVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClinicDetailPhotosCVC", for: indexPath) as! ClinicDetailPhotosCVC
        
        cell.imageView.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        cell.imageView.sd_addActivityIndicator()
        cell.imageView.sd_showActivityIndicatorView()
        cell.imageView.tag = indexPath.row
        cell.crossButton.tag = indexPath.row
        cell.crossImageView.tag = indexPath.row
        cell.delegate = self
        if CommonClass.isLiveServer() {
            cell.imageView.sd_setImage(with: URL.init(string: (Config.clinicImageBaseUrl + self.imageArr[indexPath.row])), placeholderImage: UIImage.init(named: "addDataToPrescription"), options: [], progress: nil, completed: nil)
        } else {
            cell.imageView.sd_setImage(with: URL.init(string: (Config.clinicImageBaseUrl + self.imageArr[indexPath.row])), placeholderImage: UIImage.init(named: "addDataToPrescription"), options: [], progress: nil, completed: nil)
        }
        cell.crossButton.isHidden = (indexPath.row == self.imageArr.count - 1)
        cell.crossImageView.isHidden = (indexPath.row == self.imageArr.count - 1)
        return cell
    }
}

extension ClinicDetailVC: UICollectionViewDelegate, ClinicImageDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == self.imageArr.count - 1 {
            self.selectedImage = (self.imageArr.count - 1 != indexPath.row) ? indexPath.row : nil
            self.actionOnImage()
        }
    }
    
    func performActionToRemoveImage(tag: Int) {
        CommonClass.showAlert(self, title: "", message: localizeValue(key: "Are you sure you want to delete this image?"), cancelButton: localizeValue(key: "No"), buttons: [localizeValue(key: "Yes")]) { (alertAction, index) in
            self.imageArr.remove(at: tag)
            self.cv_photos.reloadData()
        }
    }
}

extension ClinicDetailVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            self.upload(Image: image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ClinicDetailVC: MRCountryPickerDelegate{
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        self.phoneCode = phoneCode
        self.btn_country.setImage(flag, for: .normal)
       // self.btn_country.setTitle("\(phoneCode)", for: .normal)
        self.phoneCodeLabel.text = phoneCode
    }
}


extension ClinicDetailVC : GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.tf_location.text = place.formattedAddress
        self.parameters["clinic_lat"] = "\(place.coordinate.latitude)"
        self.parameters["clinic_long"] = "\(place.coordinate.longitude)"
        self.parameters["google_address"] = place.formattedAddress
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func showMap(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.autocompleteFilter?.country = Locale.current.regionCode
        
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    
    
    
}


extension ClinicDetailVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (self.tf_city == textField){
            
            if self.tf_city.text!.isEmpty{
                
                CommonClass.showAlert(view: self, title: localizeValue(key: "Please select city"), message: "") { (_) in
                    
                }
                return false
            }
        }else if self.tf_phone_number == textField{
            
            var str: NSString = textField.text! as NSString
            str = str.replacingCharacters(in: range, with: string) as NSString
            return ((((str as String).first == "0") ? str.length <= 12 : str.length <= 11) && textField.isSelectedLanguageEnglish())
        }else if (textField == self.tf_address_arabic){
            return textField.isSelectedLanguageArabic()
        }else if (textField == self.tf_address){
            
            return textField.isSelectedLanguageEnglish()
        }else if (textField == self.tf_clinic_name){
            
            return textField.isSelectedLanguageEnglish()
        }else if (textField == self.tf_fees){
            
            return textField.isSelectedLanguageEnglish()
        }
        else if (textField == self.tf_clinicName_arabic){
            
            return textField.isSelectedLanguageArabic()
        }
        return true
    }
}

class EnglishTextField: UITextField {
    
    // ru, en, ....
    var languageCode:String?{
        didSet{
            if self.isFirstResponder{
                self.resignFirstResponder();
                self.becomeFirstResponder();
            }
        }
    }
    
    override var textInputMode: UITextInputMode?{
        if let language_code = self.languageCode{
            for keyboard in UITextInputMode.activeInputModes{
                if let language = keyboard.primaryLanguage{
                    let locale = Locale.init(identifier: language);
                    if locale.languageCode == language_code{
                        return keyboard;
                    }
                }
            }
        }
        return super.textInputMode;
    }
}
