//
//  VacationsVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 30/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//
import UIKit

class VacationsVC: BaseViewControler, VacationsTableViewCellDelegate {

    //MARK:- Outles
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var errorTitleLabel: UILabel!
    
    @IBOutlet weak var addVacationBtn: RoundButton!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var constraint_height: NSLayoutConstraint!
    @IBOutlet weak var tblVacations: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    //MARK:- Variables
    var vacationsArr: [Vacations] = []
  
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        constraint_height.constant += (UIApplication.shared.statusBarFrame.size.height)
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backbtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        errorDescriptionLabel.text = localizeValue(key: "You don't have any vacation")
        errorTitleLabel.text = localizeValue(key: "No vacations")
        headerTitleLabel.text = localizeValue(key: "Vacations")
        addVacationBtn.setTitle(localizeValue(key: "Add Vacation") , for: .normal)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getVacations()
    }
    
    //MARK:- Functions
    private func getVacations(){
        
        guard let userObj = AppInstance.shared.user else { return }
        let param:[String:Any] = ["dr_id":userObj.id ?? 0]
        let service = BusinessService()
        service.getVacationList(with: param, target: self) { (response) in
            
            if let response = response as? [String:Any]{
                
                if let vacationsArr = response["vacation"] as? [[String:Any]]{
                    
                    var tempVacationArr:[Vacations] = []
                    if vacationsArr.count > 0{
                        
                        for vacation in vacationsArr{
                            
                            let vacationObj = Vacations.init(id: (vacation["id"] as? Int ?? 0), user_id: (vacation["user_id"] as? Int ?? 0), start_on: (vacation["start_on"] as? String ?? ""), end_on: (vacation["end_on"] as? String ?? ""))
                            tempVacationArr.append(vacationObj)
                        }
                        self.vacationsArr = tempVacationArr
                        self.tblVacations.reloadData()
                        self.errorView.isHidden = true
                    }else{
                        
                        self.errorView.isHidden = false
                    }
                }else{
                    
                    self.errorView.isHidden = false
                }
            }else{
                
                self.errorView.isHidden = false
            }
        }
    }

    func performActionWhenEditButtonIsTapped(tag: Int) {
        let addVacationsController = self.storyboard?.instantiateViewController(withIdentifier: "AddVacationsVC") as! AddVacationsVC
        addVacationsController.vacationID = self.vacationsArr[tag].id
        addVacationsController.startDate = self.vacationsArr[tag].start_on
        addVacationsController.endDate = self.vacationsArr[tag].end_on
        self.navigationController?.pushViewController(addVacationsController, animated: true)
    }
    
    func performActionWhenDeleteButtonIsTapped(tag: Int) {
        CommonClass.showAlert(self, title: localizeValue(key: "Delete vacation"), message: "\(localizeValue(key: "Do you want to")) \(localizeValue(key: "Delete vacation"))?", cancelButton: localizeValue(key: "Cancel"), buttons: [localizeValue(key: "Ok")]) { (alertAction, int) in
            let param:[String:Any] = ["id": self.vacationsArr[tag].id]
            let service = BusinessService()
            service.deleteVacation(with: param, target: self) { (success) in
                if success {
                    self.vacationsArr.remove(at: tag)
                    self.tblVacations.reloadData()
                    if self.vacationsArr.count == 0 {
                        self.errorView.isHidden = false
                    }
                } else {
                    CommonClass.showAlert(view: self, title: "Error!", message: "Something went wrong!", completion: { (alertAction) in
                        
                    })
                }
            }
        }
    }
    
    //MARK:- Action Outlets
    @IBAction func actionOnCancel(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    @IBAction func actionOnAddVacation(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddVacationsVC")
        self.navigationController?.pushViewController(VC!, animated: true)
    }
}

extension VacationsVC : UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.vacationsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VacationsTVC", for: indexPath) as! VacationsTVC
        
        let vacationObj = self.vacationsArr[indexPath.row]
        guard let startDate = TimeUtils.sharedUtils.dateFromString(vacationObj.start_on, with: TimeUtilsFormatter.serverDate) else { return cell}
        guard let endDate = TimeUtils.sharedUtils.dateFromString(vacationObj.end_on, with: TimeUtilsFormatter.serverDate) else { return cell}
        let start_on = TimeUtils.sharedUtils.stringFromdate(startDate, with: TimeUtilsFormatter.strDate)
        let end_on = TimeUtils.sharedUtils.stringFromdate(endDate, with: TimeUtilsFormatter.strDate)
        cell.lbl_start_date.text = start_on
        cell.lbl_end_date.text = end_on
        cell.editButton.tag = indexPath.row
        cell.deleteButton.tag = indexPath.row
        cell.startDateLabel.text = localizeValue(key: "Start date:")
        cell.endDateLabel.text = localizeValue(key: "End date:")
        cell.delegate = self
        return cell
    }
}
