//
//  AddVacationsVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 30/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddVacationsVC: BaseViewControler {
    
    //MARK:- Outlets
    @IBOutlet weak var tf_end_date: CustomTextField!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var tf_start_date: CustomTextField!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var constraint_height: NSLayoutConstraint!
    
    @IBOutlet weak var addBtn: RoundButton!
    //MARK:- Variables
    var numberOfVacations: Int = 0
    var vacationID: Int?
    var startDate = ""
    var endDate = ""
    var startTimeTF = ""
    var endTimeTF = ""
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let strtDate = TimeUtils.sharedUtils.dateFromString(startDate, with: TimeUtilsFormatter.serverDate)
        let start_on = TimeUtils.sharedUtils.stringFromdate(strtDate, with: TimeUtilsFormatter.withFullMonthDate)
        let edDate = TimeUtils.sharedUtils.dateFromString(endDate, with: TimeUtilsFormatter.serverDate)
        let end_on = TimeUtils.sharedUtils.stringFromdate(edDate, with: TimeUtilsFormatter.withFullMonthDate)
        self.startTimeTF = self.stringFromdate(strtDate, with: TimeUtilsFormatter.withFullMonthDate)
        self.endTimeTF = self.stringFromdate(edDate, with: TimeUtilsFormatter.withFullMonthDate)
        tf_start_date.text = start_on
        tf_end_date.text = end_on
        startDateLabel.text = localizeValue(key: "Start date:")
        endDateLabel.text = localizeValue(key: "End date:")
        headerTitleLabel.text = localizeValue(key: "Add Vacation")
        addBtn.setTitle(localizeValue(key: "Add"), for: .normal)
        constraint_height.constant += (UIApplication.shared.statusBarFrame.size.height)
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        self.textfieldsSetupUI()
    }
    
    //MARK:- Functions
    private func textfieldsSetupUI(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone.current
        let currentDateInStr = formatter.string(from: Date())
        guard let currentDate = formatter.date(from: currentDateInStr) else { return }
        //        let endDate = currentDate.addingTimeInterval((60*60*24))
        self.tf_start_date.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            
            self.view.endEditing(true)
            ActionSheetDatePicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "Start date"), comment: ""), datePickerMode: UIDatePickerMode.date, selectedDate: currentDate, minimumDate: currentDate, maximumDate: Date.init().addingTimeInterval(60*60*24*365*100),doneBlock: {
                picker, value, index in
                
                guard let selectedDate = self.convertDate(From: value as? Date) else { return }
                if let endDate = self.getDate(From: self.endTimeTF){
                    
                    if endDate < selectedDate{
                        
                        UIAlertController.showToast(title: self.localizeValue(key: "Start date could not be greater than end date."), message: nil, target: self, handler: nil)
                        return
                    }
                }
                
                let time = self.stringFromdate(selectedDate, with: TimeUtilsFormatter.withFullMonthDate)
                self.tf_start_date.text = TimeUtils.sharedUtils.stringFromdate(selectedDate, with: TimeUtilsFormatter.withFullMonthDate)
                self.startTimeTF = time
                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
        
        self.tf_end_date.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            
            self.view.endEditing(true)
            ActionSheetDatePicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "End date"), comment: ""), datePickerMode: UIDatePickerMode.date, selectedDate: currentDate, minimumDate: currentDate, maximumDate: Date.init().addingTimeInterval(60*60*24*365*100),doneBlock: {
                picker, value, index in
                
                guard let selectedDate = self.convertDate(From: value as? Date) else { return }
                if let startDate = self.getDate(From: self.startTimeTF){
                    
                    if startDate > selectedDate{
                        
                        UIAlertController.showToast(title: self.localizeValue(key: "Start date could not be greater than end date."), message: nil, target: self, handler: nil)
                        return
                    }
                }
                let time = self.stringFromdate(selectedDate, with: TimeUtilsFormatter.withFullMonthDate)
                self.tf_end_date.text = TimeUtils.sharedUtils.stringFromdate(selectedDate, with: TimeUtilsFormatter.withFullMonthDate)
                self.endTimeTF = time
                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
    }
    func stringFromdate(_ date:Date?, with format:String) -> String {
        guard let newDate = date else {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: newDate)
    }
    
    
    private func checkParamaters() -> String{
        
        if self.tf_start_date.text!.isEmpty{
            
            return localizeValue(key: "Please select dates")
        }else if self.tf_end_date.text!.isEmpty{
            
            return localizeValue(key: "Please select dates")
        }
        return ""
    }
    
    private func checkVacations(){
        
        let validation = self.checkParamaters()
        if !validation.isEmpty{
            
            UIAlertController.showToast(title: validation, message: nil, target: self, handler: nil)
            return
        }
        let formatter = DateFormatter()
        
        formatter.dateFormat = TimeUtilsFormatter.withFullMonthDate
        guard let startDate = formatter.date(from: self.startTimeTF) else { return }
        guard let endDate = formatter.date(from: self.endTimeTF) else { return }
        formatter.dateFormat = TimeUtilsFormatter.serverDate
        let startDateInStr = formatter.string(from: startDate)
        let endDateInStr = formatter.string(from: endDate)
        guard let userObj = AppInstance.shared.user else { return }
        let param:[String:Any] = ["dr_id":"\(userObj.id ?? 0)","start_on":startDateInStr,"end_on":endDateInStr,"id":"\(self.vacationID ?? 0)"]
        let service = BusinessService()
        service.checkVacationList(with: param, target: self) { (response) in
            
            if let response = response as? [String:Any]{
                
                if let booked_appointments = response["booked_appointments"] as? Int{
                    
                    if booked_appointments > 0 {
                        
                        let alert = UIAlertController.init(title: "\(self.localizeValue(key: "Dear Doctor you have")) \(booked_appointments) \(self.localizeValue(key: "no of booking in this date if you proceed you will cancel these booking automatically"))", message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: self.localizeValue(key: "Proceed"), style: .default, handler: { (_) in
                            
                            self.addVacation()
                        }))
                        alert.addAction(UIAlertAction.init(title: self.localizeValue(key: "Cancel"), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        
                        UIAlertController.showToast(title: ((response["return"] as? Int) == 1) ? self.localizeValue(key: "Vacation added successfully.") : (response["message"] as? String ?? ""), message: nil, target: self, handler: nil)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
        }
    }
    
    private func addVacation(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = TimeUtilsFormatter.withFullMonthDate
        guard let startDate = formatter.date(from: self.tf_start_date.text!) else { return }
        guard let endDate = formatter.date(from: self.tf_end_date.text!) else { return }
        formatter.dateFormat = TimeUtilsFormatter.serverDate
        let startDateInStr = formatter.string(from: startDate)
        let endDateInStr = formatter.string(from: endDate)
        guard let userObj = AppInstance.shared.user else { return }
        var param:[String:Any] = ["dr_id":userObj.id ?? 0,"start_on":startDateInStr,"end_on":endDateInStr]
        if self.vacationID != nil {
            param = ["dr_id":userObj.id ?? 0,"start_on":startDateInStr,"end_on":endDateInStr,"id":self.vacationID!]
        }
        let service = BusinessService()
        service.addVacation(with: param, target: self) { (response) in
            
            if let response = response as? [String:Any]{
                
                if ((response["result"] as? String) == "Success"){
                    
                    self.navigationController?.popViewController(animated: true)
                } else {
                    UIAlertController.showToast(title: response["message"] as? String, message: nil, target: self, handler: nil)
                }
            }
        }
    }
    
    private func getDate(From dateStr: String) -> Date?{
        
        if (dateStr.isEmpty) { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = TimeUtilsFormatter.withFullMonthDate
        return formatter.date(from: dateStr)
    }
    
    private func convertDate(From date: Date?) -> Date?{
        
        guard let date = date else { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = TimeUtilsFormatter.withFullMonthDate
        let dateStr = formatter.string(from: date)
        return formatter.date(from: dateStr)
    }
    
    //MARK:- Actions Outlets
    @IBAction func actionOnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnAdd(_ sender: Any) {
        
        self.checkVacations()
    }
}
