//
//  VacationsTVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 30/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

protocol VacationsTableViewCellDelegate {
    func performActionWhenEditButtonIsTapped(tag: Int)
    func performActionWhenDeleteButtonIsTapped(tag: Int)
}

class VacationsTVC: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var lbl_start_date: UILabel!
    @IBOutlet weak var lbl_end_date: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    var delegate: VacationsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        self.delegate?.performActionWhenDeleteButtonIsTapped(tag: sender.tag)
    }
    @IBAction func editButtonTapped(_ sender: UIButton) {
        self.delegate?.performActionWhenEditButtonIsTapped(tag: sender.tag)
    }
}
