//
//  AddNewPatientViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 08/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker

class AddNewPatientViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, MRCountryPickerDelegate{

    @IBOutlet weak var imageViewProfile: RoundButton!
    @IBOutlet weak var btnMale: RoundButton!
    @IBOutlet weak var btmFemale: RoundButton!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    @IBOutlet weak var tableAddPatient: UITableView!
    
    let myPickerController = UIImagePickerController()
    
    var selectedFlag: UIImage?
    var countryCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        myPickerController.delegate = self
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
      
    }
    @objc private func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 0{
            
        }
    }

    @IBAction func buttonCrossClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonAddPatientClicked(_ sender: Any) {
        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "NewPatientSucessPopUpViewController") as! NewAssistantSucessPopUpViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        self.present(vc, animated: false, completion: nil)
        
    }
    @IBAction func buttonCountryCodeClicked(_ sender: Any) {
        
        self.view.endEditing(true)
         viewForCountaryPicker.isHidden = false
    }
    
    @IBAction func buttonGenderClicked(_ sender: UIButton) {
        if sender.titleLabel?.text == "Male".localize(){
            self.btnMale.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
            self.btmFemale.backgroundColor = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1.0)
            self.btmFemale.setTitleColor(.black, for: .normal)
            self.btnMale.setTitleColor(.white, for: .normal)
        }else{
            self.btmFemale.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1.0)
            self.btnMale.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
            self.btnMale.setTitleColor(.black, for: .normal)
            self.btmFemale.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBAction func buttonProfileClicked(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".localize(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                self.myPickerController.allowsEditing = true
                self.present(self.myPickerController, animated: true, completion: nil)
            }else{
                UIAlertController.show(title: nil, message: "No camera available.".localize(), target: self, handler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".localize(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.myPickerController.allowsEditing = true
            self.present(self.myPickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".localize(), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    //imagePicker Delegeate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageViewProfile.setImage(pickedImage, for: .normal)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    //countaryPicker Delegate
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        self.countryCode = phoneCode
        self.selectedFlag = flag
    }
    @IBAction func buttonCancelClicked(_ sender: Any) {
         viewForCountaryPicker.isHidden = true
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
         viewForCountaryPicker.isHidden = true
        self.tableAddPatient.reloadData()
    }
    
}

extension AddNewPatientViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewPatientCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.placeholder = "Enter name here.".localize()
            cell.labelTitle.text = "Patient name".localize()
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as! AddNewPatientCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.imageViewFlag.image = self.selectedFlag
            cell.labelCountaryCode.text = self.countryCode
            cell.tfPhone.placeholder = "100-123-456-789"
            cell.tfPhone.keyboardType = .phonePad
            cell.tfPhone.tag = 15
            cell.tfPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
       
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewPatientCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.placeholder = "Enter email address".localize()
            cell.labelTitle.text = "Email Address".localize()
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewPatientCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.placeholder = "Enter address".localize()
            cell.labelTitle.text = "Address".localize()
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AddNewPatientCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.placeholder = "Enter patient age".localize()
            cell.labelTitle.text = "Age".localize()
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80.0
    }
    
}
