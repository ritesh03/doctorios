//
//  AssistantListViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 31/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit


class AssistantListViewController: BaseViewControler {

     var drPendingAppointment: [DataDoctorRequest]?
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var errorDescription: UILabel!
    @IBOutlet weak var errorTitle: UILabel!
    @IBOutlet weak var tableAssistant: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    @IBOutlet weak var backBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      

        self.errorView.isHidden = false
        errorTitle.text = localizeValue(key: "Assistant list is empty")
        errorDescription.text = localizeValue(key: "You don't  have any assistants")
        headerTitleLabel.text = localizeValue(key: "Assistants")
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
//       drPendingAppointments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.setNavigationBarHidden(true, animated: false)
         drPendingAppointments()
    }
    @IBAction func buttonBackClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    //MARK: drPendingAppointments assistant
    func drPendingAppointments() {
        let service = BusinessService()
        service.drPendingAppointments(with: String(describing: AppInstance.shared.user!.id!), type: "2", target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
                self.drPendingAppointment = nil
            }
            if let list = response {
                print(list)
                self.drPendingAppointment = nil
                self.errorView.isHidden = true
                self.drPendingAppointment = list.data!.reversed()
                for (index,_) in self.drPendingAppointment!.enumerated(){
                    
                    var countryCode = self.drPendingAppointment![index].countrycode
                    countryCode = countryCode!.contains("++") ? countryCode!.replacingOccurrences(of: "++", with: "+") : !countryCode!.contains("+") ? "+\(countryCode!)" : countryCode!
                    self.drPendingAppointment![index].countrycode = countryCode
                }
            }
            self.tableAssistant.reloadData()
         }
    }
    
    @IBAction func buttonAddAssistantClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewAssistantViewController") as! AddNewAssistantViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func buttonCallClicked(_ sender: UIButton) {
        let phoneNumber = CommonClass.checkAndEditPhoneNumberFor(phoneNumber: "\(String(describing: drPendingAppointment![sender.tag].mobile!))")
        let mobile = "\(String(describing: drPendingAppointment![sender.tag].countrycode!))\(String(describing: phoneNumber))"
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    func buttonMessageClicked(_ sender: UIButton) {
        let data = drPendingAppointment![sender.tag]
        let user = ["email": data.email ?? "","fcmUserId": String(describing: data.id!) ,"image": data.image ?? "","lastMessage":"","fcmToken":"","userName": "\(String(describing: data.firstname!)) \(String(describing: data.lastname!))" ] as [String : Any]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.selectedUser = Person.init(dictionary: user as NSDictionary)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}

extension AssistantListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drPendingAppointment?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssistantListCell", for: indexPath) as! AssistantListCell
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.labelName.textAlignment = .right
            cell.labelDate.textAlignment = .right
            cell.labelPhone.textAlignment = .right
        }
            cell.buttonCall.tag = indexPath.row
            cell.call_block = {
                let phoneNumber = CommonClass.checkAndEditPhoneNumberFor(phoneNumber: "\(String(describing: self.drPendingAppointment![indexPath.row].mobile!))")
                var mobile = ""
                if self.drPendingAppointment![indexPath.row].countrycode?.first == "+"{
                    mobile =  "\(String(describing: self.drPendingAppointment![indexPath.row].countrycode!))\(String(describing: phoneNumber))"
                }else{
                    mobile =  "+\(String(describing: self.drPendingAppointment![indexPath.row].countrycode!))\(String(describing: phoneNumber))"
                }
               
                if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
                    let application = UIApplication.shared
                    if application.canOpenURL(mobileCallUrl) {
                        if #available(iOS 10.0, *) {
                            application.open(mobileCallUrl, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                }
            }
            cell.message_block = {
                let data = self.drPendingAppointment![indexPath.row]
                let user = ["email": data.email ?? "","fcmUserId": String(describing: data.id!) ,"image": data.image ?? "","lastMessage":"","fcmToken":"","userName": "\(String(describing: data.firstname!)) \(String(describing: data.lastname!))" ] as [String : Any]
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                vc.selectedUser = Person.init(dictionary: user as NSDictionary)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            cell.buttonMessage.tag = indexPath.row

        let data = drPendingAppointment![indexPath.row]
        if data.image?.isEmpty != true{
            cell.imageViewAssistant.sd_setShowActivityIndicatorView(true)
            cell.imageViewAssistant.sd_setIndicatorStyle(.gray)
            cell.imageViewAssistant.sd_setImage(with: URL(string: Config.imageBaseUrl + data.image!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewAssistant.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewAssistant.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        cell.labelName.text = "\(data.firstname!) \(data.lastname!)"
        let phoneNumber = CommonClass.checkAndEditPhoneNumberFor(phoneNumber: "\(String(describing: data.mobile!))")
        
        if data.countrycode?.first == "+"{
            cell.labelPhone.text = "\(String(describing: data.countrycode!)) \(String(describing: phoneNumber))"
        }else{
            cell.labelPhone.text = "+\(String(describing: data.countrycode!)) \(String(describing: phoneNumber))"
        }
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.labelDate.text = data.clinic?.clinic_name_ar ?? ""
        }else{
            cell.labelDate.text = data.clinic?.clinic_name!//"Last login \(drPendingAppointment![indexPath.row].cl)"
        }
        cell.blockedLabel.text = localizeValue(key: "Blocked")
        if data.blockstatus == "1"{
            //cell.BlockStatus.text = "Blocked"
            cell.blockedLabel.isHidden = false
        }else{
            //cell.BlockStatus.text = "Unblocked"
            cell.blockedLabel.isHidden = true
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssistantDetailViewController") as! AssistantDetailViewController
        vc.selectedAssistant = drPendingAppointment![indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
