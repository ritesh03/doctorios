//
//  InformationOneViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 12/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit


class InformationOneViewController: UIViewController {
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "welcome to Doctor account", comment: "")
        
        descriptionLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "CliniDo will help you to manage your clinic in easy way", comment: "")
      
        pointsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bulletsText", comment: "")
        skipButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Skip", comment: ""), for: .normal)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InformationTwoViewController") as! InformationTwoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonSkipClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
