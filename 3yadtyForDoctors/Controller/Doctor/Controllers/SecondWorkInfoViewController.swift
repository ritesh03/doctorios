//
//  SecondWorkInfoViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 25/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase

class SecondWorkInfoViewController: BaseViewControler, UIImagePickerControllerDelegate,UINavigationControllerDelegate, IndicatorInfoProvider {
    
    let myPickerController = UIImagePickerController()
    
    @IBOutlet weak var buttonPic: UIButton!
    @IBOutlet weak var buttonUpload: UIButton!
    @IBOutlet weak var imageViewLisance: UIImageView!
    @IBOutlet weak var tableWorkInfo: UITableView!
    @IBOutlet weak var headerView: NSLayoutConstraint!
    @IBOutlet weak var labelUploadText: UILabel!
    @IBOutlet weak var viewImageBg: UIView!
    @IBOutlet weak var buttonSave: RoundButton!
    
    var imagesArray = [File]()
    var catName: String?
    var catId: String?
    var position: String?
    var positionArabic: String?
    var about: String?
    var education: String?
    var certifiLisance: String?
    var selectedCat = CategoryList()
    //    var params = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get previous data
        paramEdit = AppInstance.shared.doctorSignUp!
        myPickerController.delegate = self
        // Do any additional setup after loading the view.
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self,selector:#selector(self.openDashboard),
                                               name:NSNotification.Name(rawValue: "openDashboard"),object: nil)
        
        labelUploadText.isHidden = false
        viewImageBg.backgroundColor = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        if let categoryId = paramEdit["dr_category"]{
            catId = categoryId as? String
            catName = paramEdit["catName"] as? String
        }
        if let pos = paramEdit["dr_position"]{
            position = pos as? String
        }
        if let posArabic = paramEdit["arabic_dr_postion"]{
            positionArabic = posArabic as? String
        }
        if let licence = paramEdit["dr_certificate_number"]{
            certifiLisance = licence as? String
        }
        if let edu = paramEdit["education"]{
            education = edu as? String
        }
        if let abt = paramEdit["about"]{
            about = abt as? String
        }
        if AppInstance.shared.isFromMenu == true {
            if let category = paramEdit["dr_category_name"]{
                catName = category as? String
            }
            if let licenceImage = paramEdit["dr_certificate"]{
                labelUploadText.isHidden = true
                viewImageBg.backgroundColor = UIColor.white
                imageViewLisance.sd_setShowActivityIndicatorView(true)
                imageViewLisance.sd_setIndicatorStyle(.gray)
                imageViewLisance.sd_setImage(with: URL(string: licenceImage as! String), placeholderImage: UIImage(named: ""))
                imageViewLisance.sd_setShowActivityIndicatorView(false)
                labelUploadText.isHidden = true
                viewImageBg.backgroundColor = UIColor.white
            }else{
                imageViewLisance.image = UIImage(named: "")
            }
        }else{
            if let category = paramEdit["catName"]{
                catName = category as? String
            }
            if let licenceImage = paramEdit["file"]{
                imagesArray = licenceImage as! [File]
                let file = imagesArray[0]
                imageViewLisance.image = file.image!
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AppInstance.shared.isFromMenu == true{
            headerView.constant = 0
            buttonSave.isHidden = true
        }else{
            headerView.constant = 140
            buttonSave.isHidden = false
        }
        tableWorkInfo.reloadData()
        super.viewWillAppear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        //MARK: validation
        guard let catName = catName,!catName.isEmpty else {
            UIAlertController.show(title: nil, message: "Please select category".localize(), target: self, handler: nil)
            return
        }
        guard let position = position,!position.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter position".localize(), target: self, handler: nil)
            return
        }
        guard let about = about,!about.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter about".localize(), target: self, handler: nil)
            return
        }
        guard let education = education,!education.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter your education".localize(), target: self, handler: nil)
            return
        }
        guard let certifiLisance = certifiLisance,!certifiLisance.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter certificate/licence number".localize(), target: self, handler: nil)
            return
        }
        if imagesArray.isEmpty == true{
            UIAlertController.show(title: nil, message: "Please upload licence Image".localize(), target: self, handler: nil)
            return
        }
        if AppInstance.shared.isFromMenu == true{
            
        }else{
            drSignup()
        }
    }
    
    @IBAction func buttonUploadClicked(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".localize(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                //self.myPickerController.allowsEditing = true
                self.present(self.myPickerController, animated: true, completion: nil)
            }else{
                UIAlertController.show(title: nil, message: "No camera available.".localize(), target: self, handler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".localize(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            //self.myPickerController.allowsEditing = true
            self.present(self.myPickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".localize(), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            labelUploadText.isHidden = true
            viewImageBg.backgroundColor = UIColor.white
            imageViewLisance.image = chosenImage
            let file = File(name: "\(currentTimeStamp).jpg", image: chosenImage, type: 2)
            if (imagesArray.count) > 0 {
                imagesArray.removeLast()
            }
            imagesArray.append(file)
            paramEdit["file"] = imagesArray
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        AppInstance.shared.doctorSignUp = paramEdit
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func openDashboard(notification: NSNotification){
        //Mark: TODO
        //        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "TabbarController")
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "About me".localize())
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 1{
            position = textField.text
            paramEdit["dr_position"] = position
        }else if textField.tag == 2{
            positionArabic = textField.text
            paramEdit["arabic_dr_postion"] = positionArabic
        }else if textField.tag == 3{
            about = textField.text
            paramEdit["about"] = about
        }else if textField.tag == 4{
            education = textField.text
            paramEdit["education"] = education
        }else if textField.tag == 5{
            certifiLisance = textField.text
            paramEdit["dr_certificate_number"] = certifiLisance
        }
    }
    
    func drSignup(){
        //Append Profile image in imagesArray
        if let profileImage =  paramEdit["profile"] as? Array<File> {
            imagesArray.append(profileImage.last!)
            //            print(imagesArray)
        }
        
        paramEdit.removeValue(forKey: "profile")
        paramEdit.removeValue(forKey: "file")
        paramEdit.removeValue(forKey: "catName")
        paramEdit["countrycode"] = AppInstance.shared.countryCode!
        paramEdit["mobile"] = AppInstance.shared.mobile!
        
        //Final Data of all screens
        AppInstance.shared.doctorSignUp = paramEdit
        /*
         let service = BusinessService()
         service.drSignup(with: AppInstance.shared.doctorSignUp!, image: imagesArray, target: self) { (response) in
         if let user = response {
         print(user)
         //saveDataToFirebaseRefrence
         self.saveDataToFirebaseRefrence(userData: user.data!)
         //                AppInstance.shared.user = user.data!
         //UserDefaults.standard.set(user.data!.dictionaryRepresentation() , forKey: "isLogIn")
         //UserDefaults.standard.set(AppInstance.shared.doctorSignUp , forKey: "userdata")
         AppInstance.shared.doctorSignUp = nil
         let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "SucessPopUpViewController") as! SucessPopUpViewController
         vc.modalPresentationStyle = .overCurrentContext
         vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
         self.present(vc, animated: false, completion: nil)
         }
         }
         */
    }
    
    func saveDataToFirebaseRefrence(userData: UserData?) {
        print(String(describing: userData?.id))
        let token = UserDefaults.standard.value(forKey: "token")
        if CommonClass.isLiveServer() {
            Constants.refs.databaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
        } else {
            Constants.refs.testDatabaseChats.child(String(describing: userData!.id!)).setValue(["fcmUserId": String(describing: userData!.id!),"email": userData?.email,"image": userData?.image,"fcmToken": token,"userName": "\(String(describing: userData!.firstname!)) \(String(describing: userData!.lastname!))"])
        }
        
        
    }
}
//MARK:- Category Delegate
extension SecondWorkInfoViewController : categorySelectedDelegate{
    func selectedCategory(selectedCategory: CategoryList) {
        selectedCat = selectedCategory
        catName = selectedCat.name!
        catId = "\(String(describing: selectedCat.id!))"
        paramEdit["dr_category"] = catId!
        paramEdit["catName"] = catName!
        tableWorkInfo.reloadData()
    }
    func selectedGeneric(selectedGeneric: GenericData) {}
    func selectedTrade(selectedTrade: TradeData) {}
    func selectedTime(selectedTime selectedTrade: String) {}
}

extension SecondWorkInfoViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! secondWorkInfoCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                if catName != nil{
                    cell.textFieldCatName.text = catName!
                }
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "secondWorkInfoCell", for: indexPath) as! secondWorkInfoCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.labelTitle.text = "Position".localize()
                cell.textFieldPosition.placeholder = "Write your current Position…".localize()
                cell.labelTitle.textAlignment = .left
                cell.textFieldPosition.textAlignment = .left
                cell.textFieldPosition.tag = 1
                if position != nil{
                    cell.textFieldPosition.text = position!
                }
                cell.textFieldPosition.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            }
        }else{
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "secondWorkInfoCell", for: indexPath) as! secondWorkInfoCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.labelTitle.text = "Doctor’s Services".localize()
                cell.textFieldPosition.placeholder = "Write about your services".localize()
                cell.labelTitle.textAlignment = .left
                cell.textFieldPosition.textAlignment = .left
                cell.textFieldPosition.tag = 3
                if about != nil{
                    cell.textFieldPosition.text = about!
                }
                cell.textFieldPosition.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "secondWorkInfoCell", for: indexPath) as! secondWorkInfoCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.labelTitle.text = "Education".localize()
                cell.textFieldPosition.placeholder = "Enter your education".localize()
                cell.labelTitle.textAlignment = .left
                cell.textFieldPosition.textAlignment = .left
                cell.textFieldPosition.tag = 4
                if education != nil{
                    cell.textFieldPosition.text = education!
                }
                cell.textFieldPosition.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "secondWorkInfoCell", for: indexPath) as! secondWorkInfoCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.labelTitle.text = "Certificate / licence ( number / code ) ".localize()
                cell.textFieldPosition.placeholder = "ex 5113165616".localize()
                cell.labelTitle.textAlignment = .left
                cell.textFieldPosition.textAlignment = .left
                cell.textFieldPosition.tag = 5
                if certifiLisance != nil{
                    cell.textFieldPosition.text = certifiLisance!
                }
                cell.textFieldPosition.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            switch indexPath.row {
            case 0:
                let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "CategoryListViewController") as! CategoryListViewController
                vc.modalPresentationStyle = .overCurrentContext
                vc.delegate = self
                vc.listType = 0
                vc.view.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                self.present(vc, animated: false, completion: nil)
            default: break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! secondWorkInfoCell
            return headerView
        }else{
            return nil
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}

extension Collection where Iterator.Element == [String:AnyObject] {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:AnyObject]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}
