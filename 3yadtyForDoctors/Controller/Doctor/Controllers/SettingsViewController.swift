//
//  SettingsViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 24/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var labelImage: UILabel!
    @IBOutlet weak var buttonSwitch: UISwitch!
    var call_block: (() -> Void)? = nil
    
    @IBAction func didTap_notification(sender: UIButton) {
        call_block?()
    }
}

class SettingsViewController: BaseViewControler,UIGestureRecognizerDelegate {
    
    var arrSection0 = [String]() //"Invite Friends"
    var arrSection1 = [String]()
    var arrSection2 = [String]()
    var arrSection3 = [String]()
    let arrImageSection3 = ["icFacebook-1","icTwitter","icGoogle-1"]
    
    @IBOutlet weak var tableSetting: UITableView!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrSection0.append(localizeValue(key: "Share App"))
        arrSection0.append(localizeValue(key: "Rate App"))
        arrSection1.append(localizeValue(key: "Push Notification"))
        arrSection2.append(localizeValue(key: "Write Feedback"))
        arrSection2.append(localizeValue(key: "Privacy Policy"))
        arrSection2.append(localizeValue(key: "Terms and Conditions"))
        arrSection2.append(localizeValue(key: "Change Password"))
        arrSection3.append(localizeValue(key: "Like us on Facebook"))
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        tableSetting.tableFooterView = UIView()
        titleLabel.text = localizeValue(key: "Settings")
        languageBtn.setTitle(localizeValue(key: "Language"), for: .normal)
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            languageBtn.contentHorizontalAlignment = .right

        }
    }
 
    
    @IBAction func buttonLogoutClicked(_ sender: Any) {
        UserDefaults.standard.set(nil, forKey: "isLogIn")
        AppInstance.shared.isOtpVerified = false
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func languageButtonClicked(_ sender: Any) {
        let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
//        vc.viewtype = "About Us".localize()
        vc.fromSettings = true
        AppInstance.shared.isFromMenu = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        
        if AppInstance.shared.userType == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = storyboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController?.pushViewController(VC, animated: true)
        } else {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController!.pushViewController(VC, animated: false)
        }
    }
}

extension SettingsViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        case 2:
            return 4
        case 3:
            return 1
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pushNotificationCell", for: indexPath) as! SettingsCell
            cell.buttonSwitch.isHidden = true
            cell.notificationLabel.text = arrSection0[indexPath.row]
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pushNotificationCell", for: indexPath) as! SettingsCell
            cell.buttonSwitch.isHidden = false
            let isnoti = UserDefaults.standard.bool(forKey: "is_notified")
            if isnoti == true{
                cell.buttonSwitch.isOn = true
            }else{
                cell.buttonSwitch.isOn = false
            }
            
            cell.call_block = {
                if cell.buttonSwitch.isOn {
                    UserDefaults.standard.setValue(true, forKey: "is_notified")
                    
                    UIApplication.shared.registerForRemoteNotifications()
                }else{
                    UserDefaults.standard.setValue(false, forKey: "is_notified")
                    UIApplication.shared.unregisterForRemoteNotifications()
                }
            }
            cell.notificationLabel.text = arrSection1[indexPath.row]
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pushNotificationCell", for: indexPath) as! SettingsCell
            cell.buttonSwitch.isHidden = true
            cell.notificationLabel.text = arrSection2[indexPath.row]
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "findUsCell", for: indexPath) as! SettingsCell
            cell.buttonImage.setImage(UIImage(named: arrImageSection3[indexPath.row]), for: .normal)
            cell.labelImage.text = arrSection3[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 34.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(origin: CGPoint.init(x: 0.0, y: 0.0), size: CGSize.init(width: self.view.frame.size.width, height: 34.0)))
        let titleLabel = UILabel.init(frame: CGRect.init(x: 12.0, y: 0.0, width: (self.view.frame.size.width - 24.0), height: 22.0))
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: 18.0)
        titleLabel.text = (section == 0) ? "" : (section == 1) ? localizeValue(key: "Notifications") : (section == 2) ? localizeValue(key: "About Us") : localizeValue(key: "Find Us")
        if section == 2 {
           
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            headerView.addGestureRecognizer(tapRecognizer)

        }
        titleLabel.center.y = headerView.center.y
        headerView.addSubview(titleLabel)
        headerView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        return (section == 0) ? nil : headerView
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ContactUSViewController") as! ContactUSViewController
        vc.viewtype = localizeValue(key: "About Us")
        self.navigationController?.pushViewController(vc, animated: true)
      
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = .white
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                
                let text = "https://www.google.co.in/"
                
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                self.present(activityViewController, animated: true, completion: nil)
            } else {
                if let url = URL(string: "https://www.google.co.in/") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:])
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            
            break
        case 1: break
            
        case 2:
            if indexPath.row == 0 {
                let webView =  CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "FeedbackViewViewController") as! FeedbackViewViewController
                self.navigationController?.pushViewController(webView, animated: true)
            } else if indexPath.row == 1 {
                let webView =  CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webView.strHeader = localizeValue(key: "Privacy Policy")
                webView.fromPayement = false
                webView.strUrl = Config.privacyPolicy
                self.navigationController?.pushViewController(webView, animated: true)
                
            } else if indexPath.row == 2 {
                let webView =  CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webView.strHeader = localizeValue(key: "Terms and Conditions")
                webView.strUrl = Config.termsOfServices
                webView.fromPayement = false
                self.navigationController?.pushViewController(webView, animated: true)
                
            }else {
                let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
               vc.cells = 3
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            break
        case 3:
            if let url = URL(string: "https://www.facebook.com/CliniDo-353601638750906/") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:])
                } else {
                    // Fallback on earlier versions
                }
            }
            break
        default: break
            
        }
    }
}
