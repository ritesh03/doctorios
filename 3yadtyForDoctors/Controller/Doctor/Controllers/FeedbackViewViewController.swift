//
//  FeedbackViewViewController.swift
//  3yadtyForDoctors
//
//  Created by Gagan Arora on 1/16/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

class FeedbackViewViewController: BaseViewControler {
    @IBOutlet var txtfeedback: UITextField!
    @IBOutlet var submitBtn: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var feedbackTitle: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = localizeValue(key: "Write feedback")
        feedbackTitle.text = localizeValue(key: "Help us to know your feedback")
        txtfeedback.placeholder = localizeValue(key: "Write your feedback…")
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             txtfeedback.textAlignment = .right
             backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func click_uploadfeedback(_ sender: Any) {
        guard let txtfeedbacks = txtfeedback.text,!txtfeedbacks.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Write your feedback…"), target: self, handler: nil)
            return
        }
        let service = BusinessService()
        service.upload_feedback(with: String(describing: AppInstance.shared.user!.id!), message: txtfeedback.text ?? "", type:"\(AppInstance.shared.userType!)", target: self) { (response) in
            
            print(response)
            if response  == "success" {
                self.txtfeedback.text = ""
                self.showAlert(with: self.localizeValue(key: "Feedback submitted successfully"))
            }
        }
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if textField.text?.isEmpty != true {
            submitBtn.isEnabled = true
            submitBtn.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
        }else{
            submitBtn.isEnabled = false
            submitBtn.backgroundColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
        }
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
