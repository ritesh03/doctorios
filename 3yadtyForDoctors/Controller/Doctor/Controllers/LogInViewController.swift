//
//  LogInViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 20/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKCoreKit
import GoogleSignIn
import MRCountryPicker

protocol logInSucessDelegate {
    func userLogInType(object: Int)
    func isForgotClicked()
}

class LogInViewController: BaseViewControler, GIDSignInUIDelegate,MRCountryPickerDelegate {
    
    var loginValue: String?
    var email = String()
    var phone = String()
    var password: String?
    var showPwd = false
    var flagImage: UIImage?
    var code: String?
    var loginDelegate: logInSucessDelegate?
    
    @IBOutlet weak var forgotPwdBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonLogin: RoundButton!
    @IBOutlet weak var tableLogin: UITableView!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Sign in with your Email address or Phone number", comment: "")
        forgotPwdBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Forgot Password?", comment: ""), for: .normal)
        
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setLocale("EN")
        let countryCode = Locale.current.regionCode
        countryPicker.setCountry(countryCode!)
        buttonLogin.isEnabled = false
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        flagImage = flag
        code = phoneCode
        tableLogin.reloadData()
    }
    
    @IBAction func buttonCountryClicked(_ sender: Any) {
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    @IBAction func buttonCancelClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    @IBAction func buttonDoneClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @IBAction func ButtonHideClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonForgotPasswordClicked(_ sender: Any) {
        
        AppInstance.shared.isFromForgot = true
        self.loginDelegate?.isForgotClicked()
        self.dismiss(animated: false, completion: nil)
        
    }
    @IBAction func buttonFacebookClicked(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    @IBAction func buttonGoogleClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func buttonLogInClicked(_ sender: Any) {
        
        if email.isEmpty == true && phone.isEmpty == true || email.isEmpty != true && phone.isEmpty != true {
            UIAlertController.show(title: nil, message: "Please enter phone or email".localize(), target: self, handler: nil)
            return
        }
        
        //        if  email.isEmpty != true{
        //            if !(email).isValidEmail() {
        //                UIAlertController.show(title: nil, message: "Please enter valid email".localize(), target: self, handler: nil)
        //                return
        //            }
        //        }
        
        guard let password = password,!password.isEmpty else {
            UIAlertController.show(title: nil, message: "Please enter country code".localize(), target: self, handler: nil)
            return
        }
        
        if phone.isEmpty != true{
            self.loginValue = CommonClass.checkAndEditPhoneNumberFor(phoneNumber: phone)
        }else{
            self.loginValue = email
        }
        let deviceToken = UserDefaults.standard.string(forKey: "token")
        if AppInstance.shared.userType == 0{
            let service = BusinessService()
            service.logIn(with: loginValue!, password: password, devicetoken: deviceToken ??  "ctMkrb4CD5A", path: Config.doctor_login, target: self) { (response) in
                if let user = response {
                    print(user)
                    AppInstance.shared.user = user.data!
                    CustomObjects.archive(Object: user.data!.dictionaryRepresentation(), WithKey: "isLogIn")
                    CustomObjects.archive(Object: AppInstance.shared.doctorSignUp as AnyObject, WithKey: "userdata")
                    self.loginDelegate?.userLogInType(object: 1)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }else{
            let service = BusinessService()
             let deviceToken = UserDefaults.standard.string(forKey: "token")
            service.logIn(with: loginValue!, password: password, devicetoken: deviceToken ?? "", path: Config.assistant_login, target: self) { (response) in
                if let user = response {
                    print(user)
                    AppInstance.shared.user = user.data!
                    CustomObjects.archive(Object: user.data!.dictionaryRepresentation(), WithKey: "isLogIn")
                    //                    UserDefaults.standard.set(user.data!.dictionaryRepresentation() , forKey: "isLogIn")
                    self.loginDelegate?.userLogInType(object: 2)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if textField.tag == 0{
            phone = textField.text!
        }else if textField.tag == 1{
            email = textField.text!
        }else if textField.tag == 2{
            password = textField.text!
        }
        
        if password != nil && (password?.count)! > 5 {
            if email.isEmpty != true || phone.isEmpty != true{
                buttonLogin.isEnabled = true
                buttonLogin.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
            }else{
                buttonLogin.isEnabled = false
                buttonLogin.backgroundColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
            }
            
        }else{
            buttonLogin.isEnabled = false
            buttonLogin.backgroundColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
        }
    }
    @objc private func buttonShowClicked() {
        if showPwd == false {
            showPwd = true
        }else{
            showPwd = false
        }
        tableLogin.reloadData()
    }
    
    //Facebook
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result!)
                    let fbID = (result as! NSDictionary)["id"]!
                    let email = (result as! NSDictionary)["email"]!
                    print("\(fbID)\(email)")
                    //                    let service = BusinessService()
                    //                    service.doLoginFacebook(with: fbID as! String, email: email as! String, device_token: "123", target: self, complition: { (response) in
                    //                        if let user = response {
                    //                            print(user)
                    //                            AppInstance.shared.user = user
                    //                            UserDefaults.standard.set(user.dictionaryRepresentation() , forKey: "isLogIn")
                    //                            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "TabbarController")
                    //                            self.navigationController?.pushViewController(vc!, animated: true)
                    //                        }
                    //                    })
                }
            })
        }
    }
}

extension LogInViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.row {
        case 0:
            //             let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as! LogInCell
            //            cell.selectionStyle = UITableViewCellSelectionStyle.none
            //            cell.imageViewFlag.image = flagImage
            //            cell.labelCountaryCode.text = code
            //            cell.textFieldPhone.keyboardType = .phonePad
            //            cell.textFieldPhone.tag = 0
            //            cell.textFieldPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            //            return cell
            //        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LogInCell", for: indexPath) as! LogInCell
            cell.textField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Email or Phone", comment: "")
            //            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            //                cell.textField.textAlignment = .right
            //            }else{
            //                 cell.textField.textAlignment = .left
            //            }
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.bgView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                cell.textField.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                cell.countryCode.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                cell.iconsImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            }
            cell.textField.textAlignment = .left
            cell.textField.keyboardType = .emailAddress
            cell.textField.isSecureTextEntry = false
            cell.textField.tag = 1
            cell.buttonImage.isEnabled = true
            cell.iconsImageView.image = flagImage
            cell.buttonImage.backgroundColor = .clear
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            cell.countryCode.text = code
            cell.countryCode.textAlignment = .left
            //cell.countryCode.isHidden = true
            
            //  cell.emailPhoneTFLeading.constant = 8
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "passwordCell", for: indexPath) as! LogInCell
            cell.textField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password", comment: "")
            //  cell.emailPhoneTFLeading.constant = -31
            cell.textField.keyboardType = .default
            if showPwd == true{
                cell.buttonShow.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "HIDE", comment: "").localize(), for: .normal)
                cell.textField.isSecureTextEntry = false
            }else{
                cell.buttonShow.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "SHOW", comment: ""), for: .normal)
                cell.textField.isSecureTextEntry = true
            }
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.textField.textAlignment = .right
            }else{
                cell.textField.textAlignment = .left
            }
            cell.countryCode.isHidden = true
            cell.buttonImage.isEnabled = false
            cell.textField.tag = 2
            cell.buttonImage.setImage(UIImage(named: "icPassword"), for: .normal)
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            cell.buttonShow.isHidden = false
            cell.buttonShow.addTarget(self, action: #selector(buttonShowClicked), for: .primaryActionTriggered)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        //        if indexPath.row == 0{
        //            return 110.0
        //        }
        return 70.0
    }
    
}


//extension LogInViewController: UITextFieldDelegate{
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        let str: NSString = textField.text! as NSString
//        return str.length <= 11
//    }
//}
extension Locale {
    func isoCode(for countryName: String) -> String? {
        return Locale.isoRegionCodes.first(where: { (code) -> Bool in
            localizedString(forRegionCode: code)?.compare(countryName, options: [.caseInsensitive, .diacriticInsensitive]) == .orderedSame
        })
    }
}
