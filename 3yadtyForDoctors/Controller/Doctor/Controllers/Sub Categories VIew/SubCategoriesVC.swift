//
//  SubCategoriesVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 28/03/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SubCategoriesVC: BaseViewControler, IndicatorInfoProvider {
    
    //MARK:- Outlets
    @IBOutlet weak var constraint_height: NSLayoutConstraint!
    @IBOutlet weak var tblSubCat: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    //MARK:- Variables
    var subCategoriesArr = [SubCategories]()
    var selectedCategoriesArr : [SubCategories] = []
    var selectedItem: [IndexPath] = []
    var delegate: SubCategoryDelegate!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        constraint_height.constant += (UIApplication.shared.statusBarFrame.size.height)
        doneBtn.setTitle(localizeValue(key: "Done"), for: .normal)
        cancelBtn.setTitle(localizeValue(key: "Cancel"), for: .normal)
        headerTitleLabel.text = localizeValue(key: "Doctor services")
        // Do any additional setup after loading the view.
        self.getCategoryList()
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Sub Categories".localize())
    }
    
    //MARK:- Functions
    private func getCategoryList() {
        
        guard let userObj = AppInstance.shared.user else { return }
        let service = BusinessService() //userObj.dr_category ?? 1
        service.getSubCategories(with: ["category_id":userObj.dr_category ?? 1], target: self) { (response) in
            
            if let response = response as? [String:AnyObject]{
                
                if let dictArr = response["data"] as? [[String:AnyObject]]{
                    
                    if dictArr.count > 0{
                        for index in 0...dictArr.count-1{
                            let category_id = dictArr[index]["category_id"] as? Int ?? 0
                            let id = dictArr[index]["id"] as? Int ?? 0
                            var name = String()

                            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                                name = dictArr[index]["arabic_name"] as? String ?? ""
                            }else{
                                name = dictArr[index]["name"] as? String ?? ""
                            }
                            
                            let subCategoriesObj = SubCategories(category_id: category_id, id: id, name: name)
                            self.subCategoriesArr.append(subCategoriesObj)
                            if self.selectedCategoriesArr.count > 0 {
                                if self.selectedCategoriesArr.contains(where: { $0.id == id }){
                                    let indexPath = IndexPath(row: index, section: 0)
                                    self.selectedItem.append(indexPath)
                                }
                            }
                            self.errorView.isHidden = true
                            self.tblSubCat.reloadData()
                        }
                    }else{
                        
                        self.errorView.isHidden = false
                    }
                    
                }else{
                    
                    self.errorView.isHidden = false
                }
            }else{
                
                self.errorView.isHidden = false
            }
        }
    }
    
    //MARK:- Action Outlets
    @IBAction func actionOnDone(_ sender: Any) {
        
        var tempArr: [SubCategories] = []
        for index in self.selectedItem.map({$0.row}){
            tempArr.append(self.subCategoriesArr[index])
        }
        self.delegate.didSelect(items: tempArr)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOnCancel(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension SubCategoriesVC : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return subCategoriesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoriesTVC", for: indexPath) as! SubCategoriesTVC
        
        cell.lbl_name.text = self.subCategoriesArr[indexPath.row].name
        cell.btn_icon.setImage(UIImage.init(named: (self.selectedItem.contains(indexPath) ? "checkboxActive" : "checkboxDisactive")), for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCat = self.subCategoriesArr[indexPath.row]
        
        if !self.selectedItem.contains(indexPath){
            //self.selectedCategoriesArr.append(selectedCat)
            self.selectedItem.append(indexPath)
        }else{
            let index = self.selectedItem.lastIndex(of: indexPath)
            self.selectedItem.remove(at: index!)
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}
