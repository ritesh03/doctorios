//
//  PatientDetailViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PatientDetailViewController: ButtonBarPagerTabStripViewController{
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var btn_message: UIButton!
    
    var isMessageButtonEnabled: Bool!
    
    override func viewDidLoad() {
        
        // change selected bar color XLPagerTabStrip
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .white
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = .white
        }
        
        super.viewDidLoad()
        
        if AppInstance.shared.isShowHistoryHeaderCell == false{
            labelTitle.text = "\(String(describing: AppInstance.shared.patientListSelectedPatient!.firstname!)) \("\(String(describing: AppInstance.shared.patientListSelectedPatient!.lastname!))")"
        } else {
            labelTitle.text = "\(String(describing: AppInstance.shared.selectedPatient!.patient_info!.firstname!)) \("\(String(describing: AppInstance.shared.selectedPatient!.patient_info!.lastname!))")"
        }
        self.messageButtonInteraction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        //add child view controller to XLPagerTabStrip
        let child_1 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "PatientHistoryViewController"))!
        let child_2 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "PatientInfoViewController"))!
        let array :  [UIViewController] = [child_1,child_2]
        return array
    }
    
    // MARK: - IBActions
    
    @IBAction private func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func buttonCallClicked(_ sender: Any) {
//        let mobile = "\(String(describing: CommonClass.checkAndEditCountryCodeFor(countryCode: self.appointments[sender.tag].patient_info!.countrycode!)))\(String(describing: self.appointments[sender.tag].patient_info!.mobile!))"
//        print(mobile)
        if AppInstance.shared.isShowHistoryHeaderCell == false{
            let mobile = "\(String(describing: CommonClass.checkAndEditCountryCodeFor(countryCode: AppInstance.shared.patientListSelectedPatient!.countrycode!)))\(String(describing: AppInstance.shared.patientListSelectedPatient!.mobile!))"
            print(mobile)
            if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
                let application = UIApplication.shared
                if application.canOpenURL(mobileCallUrl) {
                    if #available(iOS 10.0, *) {
                        application.open(mobileCallUrl, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
        } else {
            let mobile = "\(String(describing: CommonClass.checkAndEditCountryCodeFor(countryCode: AppInstance.shared.selectedPatient!.patient_info!.countrycode!)))\(String(describing: AppInstance.shared.selectedPatient!.patient_info!.mobile!))"
            print(mobile)
            if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
                let application = UIApplication.shared
                if application.canOpenURL(mobileCallUrl) {
                    if #available(iOS 10.0, *) {
                        application.open(mobileCallUrl, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
        }
        

    }
    
    @IBAction private func buttonMessageClicked(_ sender: Any) {
       
        if let value = AppInstance.shared.selectedPatient {
            let data = AppInstance.shared.selectedPatient
            let user = ["email": data?.patient_info?.email ?? "","fcmUserId": String(describing: data!.patient_info!.id!) ,"image": data?.patient_info?.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data?.patient_info?.firstname ?? "") \(data?.patient_info?.lastname ?? "")"] as [String : Any]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.selectedUser = Person.init(dictionary: user as NSDictionary)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let data =  AppInstance.shared.patientListSelectedPatient
            let user = ["email": data?.email ?? "","fcmUserId": String(describing: data!.id!) ,"image": data?.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data?.firstname ?? "") \(data?.lastname ?? "")"] as [String : Any]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.selectedUser = Person.init(dictionary: user as NSDictionary)
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
    // MARK: - Methods
    
    private func messageButtonInteraction() {
        if self.isMessageButtonEnabled {
            self.btn_message.isEnabled = true
            self.btn_message.setImage(UIImage.init(named: "btnMessage"), for: .normal)
        } else {
            self.btn_message.isEnabled = false
            self.btn_message.setImage(UIImage.init(named: "messageIconGray"), for: .normal)
        }
    }
}
