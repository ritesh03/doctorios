//
//  BaseViewControler.swift
//  iOSArchitecture
//
//  Created by Amit on 24/02/18.
//  Copyright © 2018 smartData. All rights reserved.
//

import Foundation
import UIKit


class BaseViewControler: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
   
    /*
    func showLoader(with text:String) {
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 100
        config.backgroundColor = UIColor.black
        config.spinnerColor = UIColor.white
        config.titleTextColor = UIColor.white
        config.spinnerLineWidth = 3.0
        config.foregroundColor = UIColor.clear
        SwiftLoader.setConfig(config: config)
        SwiftLoader.show(title: text, animated: false)
    }

    func hideLoader() {
        SwiftLoader.hide()
    }
    
    func showAlert(with message:String?) {
        UIAlertController.show(title: nil, message: message, target: self, handler: nil)
    }
    
    func openDashboard(target:UIViewController,animation:Bool = true) {
  target.navigationController?.pushViewController(CustomObjects.getStoryBoard(name:"Main").instantiateViewController(withIdentifier: "tabbarView"), animated: animation)
        
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
   
    func logOut() {
        let alertController = UIAlertController(title: "Logout", message: "Your session is expired please login again", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) {
            UIAlertAction in
            UserDefaults.standard.set(nil, forKey: "isLogIn")
            UserDefaults.standard.set(nil, forKey: "userdata")
            AppInstance.shared.doctorSignUp = nil
            let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            let nc = UINavigationController.init(rootViewController: vc)
            self.revealViewController().pushFrontViewController(nc, animated: true)
        }
        alertController.addAction(confirmAction)
        self.present(alertController, animated: true, completion: nil)
    }
    */
}
