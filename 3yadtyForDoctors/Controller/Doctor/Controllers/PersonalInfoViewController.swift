//
//  PersonalInfoViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 25/09/18.
//  Copyright Â© 2018 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker
import XLPagerTabStrip
import ActionSheetPicker_3_0
import NaturalLanguage

var paramEdit = [String:Any]()

class PersonalInfoViewController: BaseViewControler, MRCountryPickerDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, IndicatorInfoProvider {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var dropDownImageView: CustomTextField!
    @IBOutlet weak var chooseGenderLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet private weak var btn_back: UIButton!
    @IBOutlet private weak var btn_next: RoundButton!
    @IBOutlet private weak var btn_female: RoundButton!
    @IBOutlet private weak var btn_male: RoundButton!
    @IBOutlet private weak var view_properties_parent: UIView!
    @IBOutlet private weak var constraint_confirm_password_bottom: NSLayoutConstraint!
    @IBOutlet private weak var constraint_confirm_password_top: NSLayoutConstraint!
    @IBOutlet private weak var constraint_password_bottom: NSLayoutConstraint!
    @IBOutlet private weak var constraint_password_top: NSLayoutConstraint!
    @IBOutlet private weak var constraint_confirm_password_footer_height: NSLayoutConstraint!
    @IBOutlet private weak var constaint_password_foorter_height: NSLayoutConstraint!
    @IBOutlet private weak var constraint_height_confirm_password: NSLayoutConstraint!
    @IBOutlet private weak var constraint_height_password: NSLayoutConstraint!
    @IBOutlet private weak var lbl_confirm_password: UILabel!
    @IBOutlet private weak var lbl_password: UILabel!
    @IBOutlet private weak var lbl_title: UILabel!
    @IBOutlet private weak var lbl_privacy_policy: UILabel!
    @IBOutlet private weak var lbl_licence: UILabel!
    @IBOutlet private weak var btn_country: UIButton!
    @IBOutlet private weak var img_licence: UIImageView!
    @IBOutlet private weak var tf_confirm_password: UITextField!
    @IBOutlet private weak var tf_password: UITextField!
    @IBOutlet private weak var tf_specialty: CustomTextField!
    @IBOutlet private weak var tf_email: UITextField!
    @IBOutlet private weak var tf_mobile_no: UITextField!
    @IBOutlet private weak var tf_last_name: UITextField!
    @IBOutlet private weak var tf_first_name: UITextField!
    @IBOutlet private weak var countryPicker: MRCountryPicker!
    @IBOutlet private weak var viewForCountaryPicker: UIView!
    @IBOutlet private weak var tablePersonalInfo: UITableView!
    @IBOutlet private weak var headerView: NSLayoutConstraint!
    @IBOutlet private weak var btn_skip: UIButton!
    
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var specialtyDropDownIcon: UIImageView!
    
    
    // MARK: - Variables
    private let myPickerController = UIImagePickerController()
    //for storing image as file
    private var imagesArray = [File]()
    private var profileImageUser : String?
    private var flagImage: UIImage?
    private var countryCode: String?
    private var strGender: String?
    
    private var arbicFirstName: String?
    private var arabicLastName: String?
    private var firstName: String?
    private var lastName: String?
    private var email: String?
    private var password: String?
    private var confirmPassword: String?
    private var birthday: String?
    private var phone: String?
    private var countaryCode: String?
    private var specialtyArr = [CategoryList]()
    private var gender: String = ""
    private var cname: String?
    
    var nextBlock : (() -> Void)?
    
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_privacy_policy.text = "\(localizeValue(key: "By signing up, you agree to our")) \(localizeValue(key: "Terms and Conditions")) \(localizeValue(key: "and")) \(localizeValue(key: "Privacy Policy"))"
            
        tf_first_name.delegate = self
        tf_last_name.delegate = self
        firstNameLabel.text = localizeValue(key: "First name")
        tf_first_name.placeholder = localizeValue(key: "First name")
        lastNameLabel.text = localizeValue(key: "Last name")
        tf_last_name.placeholder = localizeValue(key: "Last name")
        tf_mobile_no.placeholder = localizeValue(key: "Enter phone number")
        if #available(iOS 10.0, *) {
            tf_mobile_no.keyboardType = .asciiCapableNumberPad
        } else {
            // Fallback on earlier versions
        }
        emailLabel.text = localizeValue(key: "Email address")
        tf_email.placeholder = localizeValue(key: "Email address")
        specialityLabel.text = localizeValue(key: "Speciality")
        tf_specialty.placeholder = localizeValue(key: "Select your speciality")
        chooseGenderLabel.text = localizeValue(key: "Gender")
        btn_male.setTitle(localizeValue(key: "Male"), for: .normal)
        btn_female.setTitle(localizeValue(key: "Female"), for: .normal)
        lbl_password.text = localizeValue(key: "Password")
        tf_password.placeholder = localizeValue(key: "Password")
        lbl_confirm_password.text = localizeValue(key: "Confirm password")
        tf_confirm_password.placeholder = localizeValue(key: "Confirm password")
        lbl_licence.text = localizeValue(key: "Upload Certificate / Licence")
        self.btn_next.setTitle(localizeValue(key: "NEXT"), for: .normal)
        self.btn_skip.setTitle(localizeValue(key: "Skip"), for: .normal)
        
   
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            mobileView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_mobile_no.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            codeLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            btn_country.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            tf_first_name.textAlignment = .right
            firstNameLabel.textAlignment = .right
            lastNameLabel.textAlignment = .right
            tf_last_name.textAlignment = .right
            emailLabel.textAlignment = . right
            tf_email.textAlignment = .right
            specialityLabel.textAlignment = .right
            tf_specialty.textAlignment = .right
            lbl_password.textAlignment = .right
            tf_password.textAlignment = .right
            lbl_confirm_password.textAlignment = .right
            tf_confirm_password.textAlignment = .right
            chooseGenderLabel.textAlignment = .right
        }
        self.btn_back.automaticallyRotateOnLanguageConversion()
        myPickerController.delegate = self
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        let countryCode = Locale.current.regionCode
        countryPicker.setCountry(countryCode ?? "")
     
        self.addAttributesOnLabel()
        headerView.constant += (UIApplication.shared.statusBarFrame.size.height)
        
        if AppInstance.shared.isFromMenu ?? false {
            specialtyDropDownIcon.isHidden = true
          
            self.lbl_title.text =  localizeValue(key: "Edit Profile")
            self.btn_skip.isHidden = false
            self.btn_next.setTitle(localizeValue(key: "NEXT"), for: .normal)
            guard let userObj = AppInstance.shared.user else { return }
            
            paramEdit = userObj.dictionaryRepresentation() as! [String : Any]
            
            if let dr_certificate = userObj.dr_certificate {
                self.lbl_licence.isHidden = true
                self.img_licence.sd_addActivityIndicator()
                self.img_licence.sd_showActivityIndicatorView()
                self.img_licence.sd_setImage(with: URL(string: Config.doctorCertificateBaseUrl + dr_certificate), placeholderImage: nil, options: [], progress: nil, completed: nil)
            }
            
            self.tf_first_name.text = userObj.firstname ?? ""
            self.tf_last_name.text = userObj.lastname ?? ""
            self.tf_mobile_no.text = userObj.mobile ?? ""
            self.tf_email.text = userObj.email ?? ""
            
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                 self.tf_specialty.text = userObj.dr_category_name_arabic ?? ""
            } else{
                 self.tf_specialty.text = userObj.dr_category_name ?? ""
            }
           
            if ((userObj.dr_category_name ?? "") == ""){
                
                self.tf_specialty.placeholder = "Non Editable".localize()
            }
            
            self.updateGender((userObj.gendor == "Male".localize()) ? self.btn_male : self.btn_female)
            self.lbl_password.isHidden = true
            self.lbl_confirm_password.isHidden = true
            self.constraint_height_password.constant = 0
            self.constraint_height_confirm_password.constant = 0
            self.constaint_password_foorter_height.constant = 0
            self.constraint_confirm_password_footer_height.constant = 0
            self.constraint_password_top.constant = 0
            self.constraint_password_bottom.constant = 0
            self.constraint_confirm_password_top.constant = 0
            self.constraint_confirm_password_bottom.constant = 0
            self.lbl_privacy_policy.isHidden = true
            
            self.view_properties_parent.frame = CGRect.init(x: 0.0, y: self.view_properties_parent.frame.origin.y, width: self.view_properties_parent.frame.size.width, height: 656.0)
            
            AppInstance.shared.countryCode =  userObj.countrycode ?? ""
            countryPicker.setCountryByPhoneCode(AppInstance.shared.countryCode!)
            self.tf_mobile_no.isUserInteractionEnabled = false
            self.btn_country.isUserInteractionEnabled = false
            self.tf_specialty.isUserInteractionEnabled = false
            self.tf_email.isUserInteractionEnabled = false
            self.tf_email.textColor = UIColor.lightGray
            self.btn_country.setTitleColor(UIColor.lightGray, for: .normal)
            self.tf_mobile_no.textColor = UIColor.lightGray
            self.tf_email.textColor = UIColor.lightGray
        } else {
            self.lbl_title.text = localizeValue(key: "Create New Account")
            let countryCode = Locale.current.regionCode
            countryPicker.setCountryByPhoneCode(countryCode ?? "")
            self.textFieldsSetup()
        }
        
        self.getCategoryList()
        
        NotificationCenter.default.removeObserver(self)
        if #available(iOS 12.0, *) {
            NotificationCenter.default.addObserver(self,selector:#selector(self.actionSaveEditProfile),
                                                   name:NSNotification.Name(rawValue: "actionSaveEditProfile"),object: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if AppInstance.shared.isOtpVerified ?? true{
            guard let userObj = AppInstance.shared.user else { return }
            
            paramEdit = userObj.dictionaryRepresentation() as! [String : Any]
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo{
        return IndicatorInfo(title: "Personal info".localize())
    }
    
    // MARK: - Methods
    
    private func textFieldsSetup(){
        
        self.tf_specialty.addDropDown(ddImage: UIImage(named: "icDownArrow")!) { (button) in
            
            self.view.endEditing(true)
            var tempSpecialtyArr = [String]()

            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                tempSpecialtyArr = self.specialtyArr.map({$0.arabic_name!})
            }else{
                tempSpecialtyArr = self.specialtyArr.map({$0.name!})
            }
            
            ActionSheetStringPicker.show(withTitle: NSLocalizedString(self.localizeValue(key: "Select your speciality"), comment: ""), rows: tempSpecialtyArr as [Any], initialSelection: 0, doneBlock: {
                picker, value, index in
                
                if self.specialtyArr.count > 0{
                    if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                         self.tf_specialty.text = self.specialtyArr[value].arabic_name
                    }else{
                        self.tf_specialty.text = self.specialtyArr[value].name
                    }
                }
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: button)
        }
    }
    
    private func addAttributesOnLabel(){
        
        let termsOfServicesRange = (self.lbl_privacy_policy.text! as NSString).range(of: localizeValue(key: "Terms and Conditions"))
        let privacyPolicyRange = (self.lbl_privacy_policy.text! as NSString).range(of: localizeValue(key: "Privacy Policy"))
        let attributes: NSMutableAttributedString = NSMutableAttributedString.init(string: self.lbl_privacy_policy.text!, attributes: nil)
        attributes.setAttributes([NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 16.0),NSAttributedStringKey.foregroundColor: UIColor.black], range: termsOfServicesRange)
        attributes.setAttributes([NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 16.0),NSAttributedStringKey.foregroundColor: UIColor.black], range: privacyPolicyRange)
        self.lbl_privacy_policy.attributedText = attributes
        self.lbl_privacy_policy.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.actionOnPrivacyPolicy(_:))))
    }
    
    @objc private func actionOnPrivacyPolicy(_ gesture: UITapGestureRecognizer){
        
        let termsOfServicesRange = (self.lbl_privacy_policy.text! as NSString).range(of: localizeValue(key: "Terms and Conditions"))
        let privacyPolicyRange = (self.lbl_privacy_policy.text! as NSString).range(of: localizeValue(key: "Privacy Policy"))
        if gesture.didTapAttributedTextInLabel(label: self.lbl_privacy_policy, inRange: termsOfServicesRange){
            
            guard let url = URL.init(string: Config.termsOfServices) else {return}
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }else if gesture.didTapAttributedTextInLabel(label: self.lbl_privacy_policy, inRange: privacyPolicyRange){
            
            guard let url = URL.init(string: Config.privacyPolicy) else {return}
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
 
    
    private func validate(Firstname firstName: String,Lastname lastName: String,PhoneNumber number: String,Email email: String,Specialty specialty: String,Password password: String,ConfirmPassword confirmPassword: String,Gender gender: String) -> String{
        
        if firstName.isEmpty{
            
            return localizeValue(key: "Please enter first name")
        }else if lastName.isEmpty{
            
            return localizeValue(key: "Please enter last name")
        }else if number.isEmpty{
            
            return localizeValue(key: "Please enter phone number")
        }else if email.isEmpty{
            
            return localizeValue(key: "Please enter email")
        }else if !email.isValidEmail(){
            
            return localizeValue(key: "Invaild email")
        }else if specialty.isEmpty{
            
            return localizeValue(key: "Please select doctor specialty")
        }else if gender.isEmpty{
            
            return localizeValue(key: "Please select gender")
        }else if password.isEmpty{
            
            return localizeValue(key: "Please enter password")
        }else if password.count < 6{
            
            return localizeValue(key: "Password must contain 6 digits")
        }else if confirmPassword.isEmpty{
            
            return localizeValue(key: "Please enter confirm password")
        }else if password != confirmPassword{
            
            return localizeValue(key: "Password doesn't match")
        }else if self.imagesArray.count == 0{
            
            return localizeValue(key: "Upload Certificate / Licence.")
        }else if self.tf_mobile_no.text!.count < 7 {
            return localizeValue(key: "Invaild Number")
        }
        
        return ""
    }
    
    
    
    
    private func validateForEdit(Firstname firstName: String,Lastname lastName: String) -> String{
        
        if firstName.isEmpty{
             return localizeValue(key: "Please enter first name")
        }else if lastName.isEmpty{
            
            return localizeValue(key: "Please enter last name")
        }
        return ""
    }
    
    private func updateGender(_ sender: UIButton){
        if (sender.tag == self.btn_male.tag) && (self.btn_male.backgroundColor == #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)) {
            self.btn_male.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
            self.btn_male.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.gender = ""
        } else if (sender.tag == self.btn_female.tag) && (self.btn_female.backgroundColor == #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)) {
            self.btn_female.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
            self.btn_female.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.gender = ""
        } else {
            self.btn_male.backgroundColor = (sender.tag != self.btn_male.tag) ? #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)
            self.btn_female.backgroundColor = (sender.tag != self.btn_female.tag) ? #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)
            self.btn_male.setTitleColor((sender.tag != self.btn_male.tag) ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            self.btn_female.setTitleColor((sender.tag != self.btn_female.tag) ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            self.gender = (sender.tag == self.btn_male.tag) ? "Male" : "Female"
                 
        }
    }
    
    //countary picker
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        self.btn_country.setImage(flag, for: .normal)
      //  self.btn_country.setTitle("\(phoneCode)", for: .normal)
        self.codeLabel.text = phoneCode
        AppInstance.shared.countryCode = countryCode
        AppInstance.shared.selectedFlag = flag
        AppInstance.shared.phoneCode = phoneCode
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.img_licence.image = chosenImage
            self.lbl_licence.isHidden = true
            self.imagesArray = [File.init(name: "\(arc4random()).jpg", image: chosenImage, type: 2)]
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Action Outlets
    @IBAction private func buttonCancelClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @IBAction private func buttonDoneClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    @IBAction private func buttonCountaryClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    
    @IBAction private func actionOnUploadLicence(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".localize(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self.myPickerController, animated: true, completion: nil)
            }else{
                UIAlertController.show(title: nil, message: "No camera available.".localize(), target: self, handler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".localize(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(self.myPickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".localize(), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction private func actionOnFemale(_ sender: Any) {
        
        self.updateGender(sender as! UIButton)
    }
    
    @IBAction private func actionOnMale(_ sender: Any) {
        
        self.updateGender(sender as! UIButton)
    }
    
    @available(iOS 12.0, *)
    @IBAction private func buttonNextClicked(_ sender: Any) {
        
        var validationStr = self.validate(Firstname: self.tf_first_name.text!, Lastname: self.tf_last_name.text!, PhoneNumber: self.tf_mobile_no.text!, Email: self.tf_email.text!, Specialty: self.tf_specialty.text!, Password: self.tf_password.text!, ConfirmPassword: self.tf_confirm_password.text!, Gender: self.gender).localize()
        
        if AppInstance.shared.isFromMenu ?? false{
            validationStr = self.validateForEdit(Firstname: self.tf_first_name.text!, Lastname: self.tf_last_name.text!).localize()
        }else{
            validationStr = self.validate(Firstname: self.tf_first_name.text!, Lastname: self.tf_last_name.text!, PhoneNumber: self.tf_mobile_no.text!, Email: self.tf_email.text!, Specialty: self.tf_specialty.text!, Password: self.tf_password.text!, ConfirmPassword: self.tf_confirm_password.text!, Gender: self.gender).localize()
        }
        if !validationStr.isEmpty{
            
            UIAlertController.show(title: nil, message: validationStr, target: self, handler: nil)
            return
        }
        
        let deviceToken = UserDefaults.standard.string(forKey: "token")
        
        var dr_category = Int()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            if tf_specialty.text != "" {
                dr_category = (self.specialtyArr.filter({$0.arabic_name == self.tf_specialty.text})).last!.id!
            }
          
        }else{
            dr_category = (self.specialtyArr.filter({$0.name == self.tf_specialty.text})).last!.id!
        }

     
        
        let phoneCode = ((AppInstance.shared.phoneCode ?? "") as NSString)
        
        let phoneNumber = CommonClass.checkAndEditPhoneNumberFor(phoneNumber: self.tf_mobile_no.text!)
        
        let service = BusinessService()
        
        //            Parameters for english are firstname & lastname
        //            Parameters for arabic are arabicfname & arabilname
        let code = "\(phoneCode)"
        var params:[String:Any] = ["countrycode":String(code.dropFirst())    , "mobile":phoneNumber, "devicetoken":deviceToken ?? "1234", "devicetype":"2", "email":self.tf_email.text!, "password":self.tf_password.text!,"dr_category":dr_category ?? "","gendor":self.gender]
        
        //        if AppLanguage.currentAppleLanguage() ==  LanguageType.english {
        //            params["firstname"] = self.tf_first_name.text!
        //            params["lastname"] = self.tf_last_name.text!
        //        } else if AppLanguage.currentAppleLanguage() ==  LanguageType.arabic {
        //            params["arabicfname"] = self.tf_first_name.text!
        //            params["arabilname"] = self.tf_last_name.text!
        //        }
        
        
        if self.detectedLangauge(for: tf_first_name.text!) == "Arabic"{
            params["arabicfname"] = self.tf_first_name.text!
        }else{
            params["firstname"] = self.tf_first_name.text!
        }
        
        if self.detectedLangauge(for: tf_last_name.text!) == "Arabic"{
            params["arabilname"] = self.tf_last_name.text!
        }else{
            params["lastname"] = self.tf_last_name.text!
        }
        
       
        
        
        if AppInstance.shared.isFromMenu ?? false{
            self.EditProfileDoctor()
        } else {
            //print(AppInstance.shared.isOtpVerified!)
            if AppInstance.shared.isOtpVerified ?? true{
                self.EditProfileDoctor()
            }else{
                service.drSignup(with: Config.drsignup,And: params, image: self.imagesArray, target: self) { (response) in
                    
                    if let user = response {
                        
                        
                        
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as! EnterOTPViewController
                        vc.userResponse = response
                        AppInstance.shared.mobile = self.tf_mobile_no.text
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    
    
    @available(iOS 12.0, *)
    func detectedLangauge(for string: String) -> String? {
        let recognizer = NLLanguageRecognizer()
        recognizer.processString(string)
        guard let languageCode = recognizer.dominantLanguage?.rawValue else { return nil }
        let detectedLangauge = Locale.current.localizedString(forIdentifier: languageCode)
        return detectedLangauge
    }
    @IBAction private func actionOnDone(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController!.pushViewController(VC, animated: true)
    }
    
    @IBAction private func buttonBackClicked(_ sender: Any) {
        
        if (AppInstance.shared.isFromMenu ?? false) {
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController!.pushViewController(VC, animated: false)
        }else{
            
            AppInstance.shared.doctorSignUp = nil
            AppInstance.shared.clinicTimming.removeAll()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- APIs
    private func getCategoryList() {
        let service = BusinessService()
        service.getDrCategoryList(with: self) { (response) in
            if let cat = response {
                self.specialtyArr = cat
            }
        }
    }
    
    //actionSaveEditProfile
    @available(iOS 12.0, *)
    @objc private func actionSaveEditProfile(notification: NSNotification){
        EditProfileDoctor()
    }
    
    @available(iOS 12.0, *)
    private func EditProfileDoctor() {
        
        let dr_category = (self.specialtyArr.filter({$0.name == self.tf_specialty.text})).last?.id
        var pam = [String:Any]()
        pam["user_id"] = paramEdit["id"]
        //        pam["firstname"] = self.tf_first_name.text!
        //        pam["lastname"] = self.tf_last_name.text!
        pam["email"] = self.tf_email.text!
        pam["dr_category"] = dr_category ?? ""
        pam["gendor"] = self.gender
        
        if self.detectedLangauge(for: tf_first_name.text!) == "Arabic"{
            pam["arabicfname"] = self.tf_first_name.text!
        }else{
            pam["firstname"] = self.tf_first_name.text!
        }
        
        if self.detectedLangauge(for: tf_last_name.text!) == "Arabic"{
            pam["arabilname"] = self.tf_last_name.text!
        }else{
            pam["lastname"] = self.tf_last_name.text!
        }
        
        let service = BusinessService()
        service.editProfileDoctor(with: pam, image: imagesArray, target: self) { (response) in
            if let user = response {
                AppInstance.shared.user = user.data!
                CustomObjects.archive(Object: user.data!.dictionaryRepresentation(), WithKey: "isLogIn")
                if let signUpDict = AppInstance.shared.doctorSignUp{
                    
                    CustomObjects.archive(Object: signUpDict as AnyObject, WithKey: "userdata")
                }
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorProfileVC") as! DoctorProfileVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension PersonalInfoViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField == self.tf_first_name) || (textField == tf_last_name)){
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                  return textField.isSelectedLanguageArabic()
            }else{
                 return textField.isSelectedLanguageEnglish()
            }
           
        }
        
        var str: NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        return (((str as String).first == "0") ? str.length <= 12 : str.length <= 11)
    }
    

}



