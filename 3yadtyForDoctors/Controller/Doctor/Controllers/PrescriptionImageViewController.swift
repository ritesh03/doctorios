//
//  PrescriptionImageViewViewController.swift
//  3yadtyForDoctors
//
//  Created by Vivek Dogra on 06/10/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit
import SDWebImage

class PrescriptionImageViewController: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var prescriptionImageView: UIImageView!
    var imageUrl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        prescriptionImageView.sd_setShowActivityIndicatorView(true)
        prescriptionImageView.sd_setIndicatorStyle(.gray)
        prescriptionImageView.sd_setImage(with: URL(string: Config.imageBaseUrl + imageUrl as! String), placeholderImage: UIImage(named: ""))
        prescriptionImageView.sd_setShowActivityIndicatorView(false)

       
    }
    

    @IBAction func backBtnTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
