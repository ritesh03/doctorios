//
//  AppointmentsViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 05/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage.UIButton_WebCache

class AppointmentsViewController: ButtonBarPagerTabStripViewController {

    @IBOutlet weak var btn_profile: UIBarButtonItem!
    @IBOutlet weak var menuButton: UIBarButtonItem!
   
    override func viewDidLoad() {
        // change selected bar color XLPagerTabStrip
       
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .white
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        containerView.isScrollEnabled = false
        self.navigationItem.title = localizeValue(key: "Appointments")

        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = .white
       }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.image = UIImage(named: "menu_right")!.withRenderingMode(.alwaysOriginal)
            if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
    
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }else{
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            
           // self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            //self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let nc = NotificationCenter.default
        nc.post(name: NSNotification.Name("loadAppointmentList"), object: nil)
         self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        //add child view controller to XLPagerTabStrip
       
        let child_1 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "AppointmentListViewController"))!
        let child_2 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "AppointmentCalenderViewController"))!
        let array :  [UIViewController] = [child_1,child_2]
        return array
        
        
    }

    @IBAction func buttonAddAppointmentClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewAppointmientsViewController") as! AddNewAppointmientsViewController
        vc.hidesBottomBarWhenPushed = true

        self.navigationController?.pushViewController(vc, animated: true)
    }//AssistantNewAppointmentsViewController
}
