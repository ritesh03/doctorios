//
//  PatientInfoViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PatientInfoCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var labelDetail: UILabel!
    
}

class PatientInfoViewController: UIViewController,IndicatorInfoProvider {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "Info"))
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    
}

extension PatientInfoViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "PatientInfoCell") as! PatientInfoCell
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.titleLabel.textAlignment = .right
            cell.labelDetail.textAlignment = .right
        }
     
        if AppInstance.shared.isShowHistoryHeaderCell == false{
          
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = localizeValue(key: "Phone Number")
                if AppInstance.shared.patientListSelectedPatient!.countrycode?.first == "+"{
                     cell.labelDetail.text = "\(String(describing: AppInstance.shared.patientListSelectedPatient!.countrycode!)) \(String(describing: AppInstance.shared.patientListSelectedPatient!.mobile!))"
                }else{
                     cell.labelDetail.text = "+\(String(describing: AppInstance.shared.patientListSelectedPatient!.countrycode!)) \(String(describing: AppInstance.shared.patientListSelectedPatient!.mobile!))"
                }
               
                cell.buttonImage.setImage(UIImage(named: "imgPhone"), for: .normal)
            case 1:
                cell.titleLabel.text = localizeValue(key: "Email")
                cell.labelDetail.text = AppInstance.shared.patientListSelectedPatient!.email
                cell.buttonImage.setImage(UIImage(named: "imgEmail"), for: .normal)
            case 2:
                cell.titleLabel.text = localizeValue(key: "Address")
                cell.labelDetail.text = AppInstance.shared.patientListSelectedPatient!.address
                cell.buttonImage.setImage(UIImage(named: "imgAddress"), for: .normal)
            default:
                cell.titleLabel.text = localizeValue(key: "Gender")
                cell.labelDetail.text = AppInstance.shared.patientListSelectedPatient!.gendor
                cell.buttonImage.setImage(UIImage(named: "genderIcon"), for: .normal)
            }
        }else{
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = localizeValue(key: "Phone Number")
                if AppInstance.shared.selectedPatient!.patient_info!.countrycode?.first == "+"{
                    cell.labelDetail.text = "\(String(describing: AppInstance.shared.selectedPatient!.patient_info!.countrycode!)) \(String(describing: AppInstance.shared.selectedPatient!.patient_info!.mobile!))"
                }
                else{
                    cell.labelDetail.text = "+\(String(describing: AppInstance.shared.selectedPatient!.patient_info!.countrycode!)) \(String(describing: AppInstance.shared.selectedPatient!.patient_info!.mobile!))"
                }
                cell.buttonImage.setImage(UIImage(named: "imgPhone"), for: .normal)
            case 1:
                cell.titleLabel.text = localizeValue(key: "Email")
                cell.labelDetail.text = AppInstance.shared.selectedPatient?.patient_info?.email
                cell.buttonImage.setImage(UIImage(named: "imgEmail"), for: .normal)
            case 2:
                cell.titleLabel.text = localizeValue(key: "Address")
                cell.labelDetail.text = AppInstance.shared.selectedPatient?.patient_info?.address
                cell.buttonImage.setImage(UIImage(named: "imgAddress"), for: .normal)
            default:
                cell.titleLabel.text = localizeValue(key: "Gender")
                cell.labelDetail.text = AppInstance.shared.selectedPatient?.patient_info?.gendor
                cell.buttonImage.setImage(UIImage(named: "genderIcon"), for: .normal)
            }
        }
      
        return cell
    }
    
}
