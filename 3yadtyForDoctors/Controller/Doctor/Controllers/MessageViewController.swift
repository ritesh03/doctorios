//
//  MessageViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 31/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class MessageViewController: BaseViewControler {
    
    
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyDescriptionLabel: UILabel!
    
    
    @IBOutlet weak var errorImageView: UIImageView!
    

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tableChatList: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    var users = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = localizeValue(key: "Messages")
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //menu Button
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.image = UIImage(named: "menu_right")!.withRenderingMode(.alwaysOriginal)
            emptyTitle.text = localizeValue(key: "Message list is empty")
            emptyDescriptionLabel.text = localizeValue(key: "You don’t have any message")
            
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            else{
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }
            
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
           self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }

    override func viewWillAppear(_ animated: Bool) {
         fetchUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func fetchUser() {
        self.showLoader(with: "Loading...")
        if CommonClass.isLiveServer() {
            Database.database().reference().child("UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").observeSingleEvent(of: .value, with: { (snapshot) in
                self.hideLoader()
                self.users.removeAll()
                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    let val = Person.init(dictionary: snap.value! as! NSDictionary)
                    self.users.append(val!)
                    print(self.users)
                }
                if self.users.count > 0{
                    self.errorView.isHidden = true
                }else{
                    self.errorView.isHidden = false
                }
                self.tableChatList.reloadData()
            }, withCancel: nil)
        } else {
            Database.database().reference().child("Test_Server_UserDetails").child(String(describing: AppInstance.shared.user!.id!)).child("chat_user").observeSingleEvent(of: .value, with: { (snapshot) in
                self.hideLoader()
                self.users.removeAll()
                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    let val = Person.init(dictionary: snap.value! as! NSDictionary)
                    self.users.append(val!)
                    print(self.users)
                }
                if self.users.count > 0{
                    self.errorView.isHidden = true
                }else{
                    self.errorView.isHidden = false
                }
                self.tableChatList.reloadData()
            }, withCancel: nil)
        }
    }
}

extension MessageViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        let userData = users[indexPath.row]
        if (userData.image != nil){
            cell.imageViewProfile.sd_setShowActivityIndicatorView(true)
            cell.imageViewProfile.sd_setIndicatorStyle(.gray)
            cell.imageViewProfile.sd_setImage(with: URL(string: (userData.image)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewProfile.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewProfile.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        
        cell.labelName.text = userData.userName
        cell.labellastMessage.text = userData.lastMessage
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.selectedUser = users[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
