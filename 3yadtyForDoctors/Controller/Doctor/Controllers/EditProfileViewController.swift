//
//  EditProfileViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 10/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class EditProfileViewController: ButtonBarPagerTabStripViewController {

    override func viewDidLoad() {
        // change selected bar color
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .white
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = .white
        }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoViewController"))! as! PersonalInfoViewController
        (child_1 as! PersonalInfoViewController).nextBlock = {
            self.moveToViewController(at: 1)
        }
        let child_2 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "WorkInfoViewController"))! as! WorkInfoViewController
        (child_2 as! WorkInfoViewController).nextBlockWork = {
            self.moveToViewController(at: 2)
        }
        let child_3 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "SecondWorkInfoViewController"))!
        let array :  [UIViewController] = [child_1,child_2,child_3]
        return array
    }
    @IBAction func buttonSaveClicked(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "actionSaveEditProfile"), object: nil)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        
        if AppInstance.shared.isFromMenu == true{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! UITabBarController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}

