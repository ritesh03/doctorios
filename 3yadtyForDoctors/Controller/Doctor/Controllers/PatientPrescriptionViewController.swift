//
//  PatientPrescriptionViewViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 17/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class PatientPrescriptionCell: UITableViewCell{
    
    @IBOutlet weak var LableTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonDelete: UIButton!
}

class PatientPrescriptionViewController: BaseViewControler {
    
    @IBOutlet weak var addViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var heightConstrantFooter: NSLayoutConstraint!
    @IBOutlet private weak var heightConstraintHeader: NSLayoutConstraint!
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var viewForWithoutHeader: UIView!
    @IBOutlet private weak var tablePatientPriscription: UITableView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var labelDrName: UILabel!
    @IBOutlet private weak var labelDrProfession: UILabel!
    @IBOutlet private weak var labelPhone: UILabel!
    @IBOutlet private weak var labelAddress: UILabel!
    @IBOutlet private weak var addPatientHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var submitButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var viewFooter: UIView!
    @IBOutlet private weak var footerLogoBackgroundView: UIView!
    
    //    var patient_data : DataAppointmentList?
    var prescriptionType: Int?
    var selectedPrescription: PrescriptionData?
    var patient_case: String?
    
    private var isShowHeaderFooter: Bool?
    private var prescriptionArr = [[String: Any]]()
    private var prescriptionObj = [String: Any]()
    private var medicineArr = [[String: Any]]()
    private var strCase = String()
    private var footerLogoHeight: CGFloat = 30
    
    @IBOutlet private var btnBack: UIButton!
    @IBOutlet private var btnShare: UIButton!
    @IBOutlet private var btnClick: RoundButton!
    @IBOutlet private var viewAdd: UIView!
    
    @IBOutlet var btnAdd: RoundButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        if patient_case != nil{ strCase = patient_case!}
        //if isShowHeaderFooter == true {
        if prescriptionType == 1{
            viewFooter.isHidden = false
            heightConstrantFooter.constant = 80
            heightConstraintHeader.constant = 76
            headerView.isHidden = false
        }else{
            viewFooter.isHidden = true
            viewForWithoutHeader.isHidden = false
            heightConstrantFooter.constant = 0
            heightConstraintHeader.constant = 0
            headerView.isHidden = true
        }
        
        if selectedPrescription?.medicine?.count ?? -1 > 0{viewForWithoutHeader.isHidden = true}else{viewForWithoutHeader.isHidden = false}
        if selectedPrescription?.medicine != nil{
            for arr in selectedPrescription!.medicine!{
                self.medicineArr.append(arr.dictionaryRepresentation())
            }
            self.prescriptionObj["medicine"] = self.medicineArr
            self.prescriptionObj["prescription_type"] = selectedPrescription!.prescription_type!
        }

        self.tablePatientPriscription.reloadData()
//        if AppInstance.shared.isShowPlus == true{
//            submitButtonHeightConstraint.constant = 40
//        }else{
//            submitButtonHeightConstraint.constant = 0
//        }
        //if AppInstance.shared.isShowPlus == true{
            addPatientHeightConstraint.constant = 50//0//50
            submitButtonHeightConstraint.constant = 40
//        }else{
//            addPatientHeightConstraint.constant = 0
//            submitButtonHeightConstraint.constant = 0
//        }
        self.setInfo()
        self.footerLogoBackgroundView.layer.cornerRadius = (self.footerLogoHeight / 2)
    }
    override func viewWillAppear(_ animated: Bool) {
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    override func viewWillDisappear(_ animated: Bool) {
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
    }
    fileprivate func hideViews() {
        
        self.btnBack.isHidden = true
        self.btnShare.isHidden = true
        self.btnClick.isHidden = true
        self.btnAdd.isHidden = true
        
        AppInstance.shared.isShowPlus = false
        self.tablePatientPriscription.reloadData()
        
    }
    
    fileprivate func showViews() {
        
        self.btnBack.isHidden = false
        self.btnShare.isHidden = false
        self.btnClick.isHidden = false
        self.btnAdd.isHidden = false
        
        AppInstance.shared.isShowPlus = true
        self.tablePatientPriscription.reloadData()
    }
    
    
    @IBAction func actionShare(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: localizeValue(key: "Share prescription"), style: .default , handler:{ (UIAlertAction)in
            
            if self.tablePatientPriscription.numberOfRows(inSection: 0) > 0{
                
                DispatchQueue.main.async {
                    
                    self.hideViews()
                    let image = self.takeScreenshot(true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        self.showViews()
                        let imageToShare = [image!]
                        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                        activityViewController.popoverPresentationController?.sourceView = self.view
                        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                        self.present(activityViewController, animated: true, completion: nil)
                    }
                }
            }else{
                
                UIAlertController.showToast(title: self.localizeValue(key: "Please add prescription"), message: nil, target: self, handler: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: localizeValue(key: "Please add prescription"), style: .default , handler:{ (UIAlertAction)in
            
            if self.tablePatientPriscription.numberOfRows(inSection: 0) > 0{
                
                DispatchQueue.main.async {
                    
                    self.hideViews()
                    let image = self.takeScreenshot(true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        self.showViews()
                        let printController = UIPrintInteractionController.shared
                        let printInfo = UIPrintInfo(dictionary:nil)
                        printInfo.outputType = UIPrintInfoOutputType.general
                        printInfo.jobName = self.localizeValue(key: "Print prescription")
                        printController.printInfo = printInfo
                        printController.printingItem = image
                        printController.present(animated: true, completionHandler: nil)
                    }
                }
            }else{
                
                UIAlertController.showToast(title: self.localizeValue(key: "Please add prescription"), message: nil, target: self, handler: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: self.localizeValue(key: "Cancel"), style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
        var screenshotImage :UIImage?
        let scale = UIScreen.main.scale
        let heightO = self.view.frame.size.height
        
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height-self.tablePatientPriscription.frame.height+self.tablePatientPriscription.contentSize.height+self.tablePatientPriscription.frame.origin.y)
        UIGraphicsBeginImageContextWithOptions(self.view!.layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        self.view.layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: heightO)
        
        return screenshotImage
    }
    
    @IBAction func buttonAddClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMedicineViewController")  as! AddMedicineViewController
        vc.delegate = self
        vc.prescriptionType = self.prescriptionType
        vc.editIndex = -1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setInfo() {
        
//        print()
        
        let dateStr = TimeUtils.sharedUtils.chaneDateFormet(AppInstance.shared.selectedDr?.appointment_date)
        self.titleLabel.text = "\(dateStr)"
        self.labelDrName.text = "Dr. \(AppInstance.shared.selectedDr!.doctor_info!.firstname!) \(AppInstance.shared.selectedDr!.doctor_info!.lastname!)"
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             self.labelDrProfession.text = AppInstance.shared.selectedDr?.doctor_info?.dr_category_name_arabic
        }else{
             self.labelDrProfession.text = AppInstance.shared.selectedDr?.doctor_info?.dr_category_name
        }
       
        self.labelPhone.text = "\(AppInstance.shared.selectedDr?.clinic?.clinic_code ?? "") \(AppInstance.shared.selectedDr?.clinic?.clinic_phone ?? "")"
        self.labelPhone.text = !(self.labelPhone.text!.contains("+")) ? "+\(self.labelPhone.text!)" : self.labelPhone.text!
        if prescriptionType == 1 {
            self.heightConstrantFooter.constant = CGFloat(50.0) + CommonClass.getLabelHeight(label: self.labelAddress, text: (AppInstance.shared.selectedDr?.clinic!.clinic_address)!, width: self.labelAddress.bounds.width, font: self.labelAddress.font)
        }
        
        self.labelAddress.text = AppInstance.shared.selectedDr?.clinic?.clinic_address
       // self.viewAdd.isHidden = (AppInstance.shared.userType != 0)
        self.viewAdd.isHidden = true
        self.addViewHeight.constant = 0
        self.btnClick.isHidden = (AppInstance.shared.userType != 0)
       
    }
    
    @IBAction func buttonSubmitClicked(_ sender: UIButton) {
        if selectedPrescription?.medicine != nil{
            editPrescription()
        }else{
            if medicineArr.count == 0 {
                self.showAlert(with: localizeValue(key: "Please add prescription"))
                return
            }
            addPrescription()
        }
    }
    
    func editPrescription() {
        let finalJson = self.json(from:[self.prescriptionObj])
        print(finalJson!)
        let service = BusinessService()
        service.editPrescription(with: finalJson!, prescription_id: String(selectedPrescription!.id!), target: self) { (response) in
            if response == true {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: PatientHistoryDetailViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }
    }
    
    func addPrescription() {
        let finalJson = self.json(from:[self.prescriptionObj])
        print(finalJson!)
        let info = AppInstance.shared.selectedDr
        let service = BusinessService()
        service.addPrescription(with: finalJson!, patient_id: String(info!.patient_id!), doctor_id: String(info!.doctor_info!.id!), appointment_id: String(info!.id!), patient_case: strCase, image: [], target: self) { (response) in
            if response == true {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: PatientHistoryDetailViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }
    }
    @IBAction func buttonDeleteClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: localizeValue(key: "Delete medicine"), message: localizeValue(key: "Do you want to delete this medicine?"), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: localizeValue(key: "No"), style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button".localize())
        }))
        
        alert.addAction(UIAlertAction(title: localizeValue(key: "Yes"), style: .default , handler:{ (UIAlertAction)in
            self.medicineArr.remove(at: sender.tag)
            self.prescriptionObj["medicine"] = self.medicineArr
            if self.medicineArr.count > 0{self.viewForWithoutHeader.isHidden = true}else{self.viewForWithoutHeader.isHidden = false}
            
            self.tablePatientPriscription.reloadData()
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @IBAction func buttonEditClicked(_ sender: UIButton) {
        let arr = medicineArr[sender.tag]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMedicineViewController")  as! AddMedicineViewController
        vc.delegate = self
        vc.prescriptionType = self.prescriptionType
        vc.medicineArr = arr
        vc.editIndex = sender.tag
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PatientPrescriptionViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.btnShare.isHidden = (self.medicineArr.count == 0)
        self.viewForWithoutHeader.isHidden = (self.medicineArr.count > 0)
        return medicineArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientPrescriptionCell", for: indexPath) as! PatientPrescriptionCell
        cell.LableTitle.text = (medicineArr[indexPath.row] as Dictionary)["trade_name"] as? String
        
        cell.buttonDelete.tag = indexPath.row
        cell.buttonEdit.tag = indexPath.row
        
        let prescription = medicineArr[indexPath.row]
        
        if (prescription["notes"] as! String) != "" {
            cell.labelSubTitle.text = prescription["notes"] as? String
        }else{
            cell.labelSubTitle.text = "\(prescription["dose_number"]!)X\(prescription["frequency"]!) Every \(prescription["frequency_day"]!) For \(prescription["duration"]!) \(prescription["duration_day"]!)"
            
            if (prescription["dose_time"] as! String) != "" {
                cell.labelSubTitle.text?.append("\(prescription["dose_time"]!)")
            }
            
            if (prescription["precription_sence"] as! [String]).count > 0 {
                let array = prescription["precription_sence"] as! [String]
                cell.labelSubTitle.text?.append("/\(array.joined(separator: ","))")
            }
        }
        if AppInstance.shared.userType == 1 {
            cell.buttonDelete.isHidden = true
            cell.buttonEdit.isHidden = true
        }else{
            cell.buttonDelete.isHidden = false
            cell.buttonEdit.isHidden = false
        }
        
        /*if AppInstance.shared.isShowPlus == true{
            cell.buttonDelete.isHidden = false
            cell.buttonEdit.isHidden = false
        }else{
            cell.buttonDelete.isHidden = true
            cell.buttonEdit.isHidden = true
        }*/
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footervw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let btnAddMore = UIButton(frame: CGRect(x: 15, y: 10, width: tableView.frame.width-30, height: 30))
        btnAddMore.layer.cornerRadius = 15.0
        btnAddMore.layer.borderColor = #colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1)
        btnAddMore.layer.borderWidth = 1.5
        btnAddMore.setTitle("+ Add More", for: .normal)
        btnAddMore.setTitleColor(#colorLiteral(red: 0, green: 0.6493645906, blue: 0.785187006, alpha: 1), for: .normal)
        btnAddMore.addTarget(self, action: #selector(PatientPrescriptionViewController.buttonAddClicked(_:)) , for: .touchUpInside)
        if AppInstance.shared.userType == 0 {
              footervw.addSubview(btnAddMore)
        }
      
        return footervw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
        
    }
}

extension PatientPrescriptionViewController : addMedicineDelegate{
    func selectedMedicine(dict: Medicine, type: Int, editIndex: Int) {
        print(dict.dictionaryRepresentation()) // to convert medicine object to json
        if editIndex >= 0 {
            self.medicineArr[editIndex] = dict.dictionaryRepresentation()
            self.prescriptionObj["medicine"] = self.medicineArr
            self.prescriptionObj["prescription_type"] = "\(type)"
        }else{
            self.medicineArr.append(dict.dictionaryRepresentation())
            self.prescriptionObj["medicine"] = self.medicineArr
            self.prescriptionObj["prescription_type"] = "\(type)"
        }
        if self.medicineArr.count > 0{viewForWithoutHeader.isHidden = true}else{viewForWithoutHeader.isHidden = false}
        tablePatientPriscription.reloadData()
    }
}
