//
//  SucessPopUpViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 25/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class SucessPopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name("openDashboard"), object: nil)
    }
    
}
