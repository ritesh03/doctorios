//
//  CategoryListViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 25/09/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol categorySelectedDelegate {
    
    func selectedCategory(selectedCategory: CategoryList)
    func selectedGeneric(selectedGeneric: GenericData)
    func selectedTrade(selectedTrade: TradeData)
    func selectedTime(selectedTime: String)
}

class CategoryListViewController: BaseViewControler, UISearchBarDelegate {

    var delegate: categorySelectedDelegate?
    var selectIndex = Int()
    var selectedCategory = CategoryList()
    var selectedTrade: TradeData?
    var selectedGeneric: GenericData?
    var selectedTime: String?
    var selCategory = [CategoryList]()
    
    var tradeData = [TradeData]()
    var genericData = [GenericData]()
    
    var filterTradeData = [TradeData]()
    var filterGenericData = [GenericData]()
    var isSearched = Bool()
    
    var arrMorning = ["In the morning".localize(),"In the evening".localize(),"In the morning & evening".localize(),"Before sleep".localize(),
        "Before breakfast".localize(),"After breakfast".localize(),"Before lunch".localize(),"After lunch".localize(),"Before dinner".localize(),"After dinner".localize(),"Before food".localize(),"During food".localize(),"After food".localize(),"On an empty stomach".localize(),"Should be repeated".localize(),"Shouldn't be repeated".localize(),"For life".localize(),"Gradual withdrawal".localize()]
    
    var listType: Int?
    var tradeId: String?
    
    @IBOutlet weak var heightConstrantSearch: NSLayoutConstraint!
    @IBOutlet weak var searchBarList: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableCategoryList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBarList.delegate = self
        searchBarList.barTintColor = .clear
        let image = UIImage()
        searchBarList.setBackgroundImage(image, for: .any, barMetrics: .default)
        searchBarList.scopeBarBackgroundImage = image
        
        selectIndex = -1
        
        if listType == 0{
            titleLabel.text = "Category List".localize()
            heightConstrantSearch.constant = 50
            getCategoryList()
        }else if listType == 1{
             titleLabel.text = "Generic List".localize()
             heightConstrantSearch.constant = 100
             genericList()
        }else if listType == 2{
            titleLabel.text = "Trade List".localize()
            heightConstrantSearch.constant = 100
            tradeList()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonCrossClicked(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }

    @IBAction func buttonSelectCategory(_ sender: Any) {
       
        if listType == 0{
            if selectedCategory.id != nil{
                delegate?.selectedCategory(selectedCategory: selectedCategory)
                self.dismiss(animated: true, completion: nil)
            }else{
                showAlert(with: "Please select a category".localize())
            }
        }else if listType == 1 {
            if selectedGeneric?.name != nil{
                delegate?.selectedGeneric(selectedGeneric: selectedGeneric!)
                if self.tradeId == "" {
                    self.getLastTradeList()
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                showAlert(with: "Please select a generic".localize())
            }
        }else if listType == 2 {
            if selectedTrade?.name != nil{
                if tradeId == nil {
                    delegate?.selectedTrade(selectedTrade: selectedTrade!)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.genericList()
                }
//
//                delegate?.selectedGeneric(selectedGeneric: selectedGeneric!)
//                self.dismiss(animated: true, completion: nil)
            }else{
                showAlert(with: "Please select a trade".localize())
            }
        }else{
            if selectedTime != nil{
                delegate?.selectedTime(selectedTime: selectedTime!)
                self.dismiss(animated: true, completion: nil)
            }else{
                showAlert(with: "Please select time".localize())
            }
        }
    }
    
    func getCategoryList() {
        let service = BusinessService()
        service.getDrCategoryList(with: self) { (response) in
            if let cat = response {
                self.selCategory = cat
                self.tableCategoryList.reloadData()
            }
        }
    }
    func genericList() {
        let service = BusinessService()
        service.genericList(with: String(AppInstance.shared.user!.id!), trade_id: tradeId ?? "", target: self) { (response) in
            if let generic = response {
                self.genericData = generic.data!
                self.filterGenericData = generic.data!
                
                if self.tradeId == ""  {
                    self.tableCategoryList.reloadData()
                } else {
                    self.delegate?.selectedTrade(selectedTrade: self.selectedTrade!)
                    self.delegate?.selectedGeneric(selectedGeneric: self.genericData.first!)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func tradeList() {
        let service = BusinessService()
        let param:[String : Any] = ["user_id":String(AppInstance.shared.user!.id!)]
        service.tradeList(with: param, target: self) { (response) in
            if let trade = response {
                 self.tradeData = trade.data!
                 self.filterTradeData = trade.data!
                 self.tableCategoryList.reloadData()
            }
        }
    }
    
    func getLastTradeList() {
        let service = BusinessService()
        selectIndex = -1

        let param:[String : Any] = ["user_id":String(AppInstance.shared.user!.id!),"genric_id":String(self.selectedGeneric!.id!)]
        service.tradeList(with: param, target: self) { (response) in
            if let trade = response {
                self.listType = 2
                self.tradeData = trade.data!
                self.filterTradeData = trade.data!
                self.tableCategoryList.reloadData()
            }
        }
    }
    
    // searchbar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         selectIndex = -1
        if listType == 1{
            self.isSearched = true
            filterGenericData = genericData.filter() { ($0.name?.contains(searchText))!}
            if searchText == ""{
                self.isSearched = false
                filterGenericData = genericData
            }
            if filterGenericData.count < 1 {
                //self.errorView.isHidden = false
            }else{
                //self.errorView.isHidden = true
            }
            
        }else if listType == 2{
            self.isSearched = true
            filterTradeData = tradeData.filter() { ($0.name?.contains(searchText))!}
            if searchText == ""{
                self.isSearched = false
                filterTradeData = tradeData
            }
            if filterTradeData.count < 1 {
                //self.errorView.isHidden = false
            }else{
                //self.errorView.isHidden = true
            }
        }
        self.tableCategoryList.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if listType == 1{
            if searchBar.text == nil{
                selectIndex = -1
                filterGenericData = genericData
            }
            if filterGenericData.count < 1 {
                //self.errorView.isHidden = false
            }else{
                //self.errorView.isHidden = true
            }
        }else if listType == 2{
            if searchBar.text == nil{
                selectIndex = -1
                filterTradeData = tradeData
            }
            if filterTradeData.count < 1 {
                //self.errorView.isHidden = false
            }else{
                //self.errorView.isHidden = true
            }
        }
        self.tableCategoryList.reloadData()
    }

}
extension CategoryListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if listType == 0{
            return self.selCategory.count
        }else if listType == 1 {
            //return self.genericData.count
            return self.filterGenericData.count
        }else if listType == 2 {
            //return self.tradeData.count
            return self.filterTradeData.count
        }else{
            return arrMorning.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
          cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if listType == 0{
            cell.labelCategory.text = self.selCategory[indexPath.row].name
        }else if listType == 1 {
            cell.labelCategory.text = self.filterGenericData[indexPath.row].name
        }else if listType == 2 {
            cell.labelCategory.text = self.filterTradeData[indexPath.row].name
        }else{
            cell.labelCategory.text = arrMorning[indexPath.row]
        }
        
        if selectIndex == indexPath.row {
            cell.radioButton.isSelected = true
        }else{
            cell.radioButton.isSelected = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.view.endEditing(true)
        selectIndex = indexPath.row
        if listType == 0{
             selectedCategory = self.selCategory[indexPath.row]
        }else if listType == 1 {
             selectedGeneric = self.filterGenericData[indexPath.row]
        }else if listType == 2 {
            self.tradeId = String(self.filterTradeData[indexPath.row].id!)
            selectedTrade = self.filterTradeData[indexPath.row]
        }else{
             selectedTime = arrMorning[indexPath.row]
        }
        tableCategoryList.reloadData()
    }
}
