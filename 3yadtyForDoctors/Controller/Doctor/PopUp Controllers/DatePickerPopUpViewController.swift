//
//  DatePickerPopUpViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 08/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol AvailabilityDelegate {
    func addAvailability(object:[String:Any], index: Int?)
    func deleteAvailability()
}

class DatePickerPopUpViewController: BaseViewControler {

    @IBOutlet weak var labelSelectedDay: UILabel!
    @IBOutlet weak var datePickerTo: UIDatePicker!
    @IBOutlet weak var datePickerFrom: UIDatePicker!
    
    let dateFormatterGet = DateFormatter()
    var fromTime: String?
    var toTime: String?
    var day: String?
    var delegate:AvailabilityDelegate?
    var salectedIndex: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dateFormatterGet.dateFormat = "hh:mm a"
        dateFormatterGet.timeZone = TimeZone.current
        if fromTime != nil {
            if let date = dateFormatterGet.date(from: fromTime!) {
                datePickerFrom.date = date
            }
        }else{
             fromTime = dateFormatterGet.string(from: datePickerFrom.date)
        }
        if toTime != nil{
            if let date = dateFormatterGet.date(from: toTime!) {
                datePickerTo.date = date
            }
        }else{
             toTime = dateFormatterGet.string(from: datePickerTo.date)
        }
        
        datePickerTo.addTarget(self, action: #selector(datePickerToChanged(_:)), for: .valueChanged)
        datePickerFrom.addTarget(self, action: #selector(datePickerFromChanged(_:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        labelSelectedDay.text = day
    }
    
    @IBAction func buttonCrossClicked(_ sender: Any) {
       // delegate?.deleteAvailability()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonSubmitClicked(_ sender: Any) {
        
        //get time interval
        if fromTime != nil || toTime != nil {
            let startTime = dateFormatterGet.date(from: fromTime!)
            let endTime = dateFormatterGet.date(from: toTime!)
            //You can directly use from here if you have two dates
            if endTime! > startTime!  {
                //create dict for selectedDatetime
                let dict = ["day":day!, "start_time":fromTime!,"end_time":toTime!]
                delegate?.addAvailability(object: dict, index: salectedIndex)
                dismiss(animated: true, completion: nil)
            }else{
                showAlert(with: "End time should be greater than start time".localize())
            }
        }else{
            showAlert(with: "Please select Start and End Time".localize())
        }
    }
    
    @objc func datePickerFromChanged(_ sender: UIDatePicker) {
        fromTime = dateFormatterGet.string(from: datePickerFrom.date)
    }
    
    @objc func  datePickerToChanged(_ sender: UIDatePicker) {
        toTime = dateFormatterGet.string(from: datePickerTo.date)
    }
}


