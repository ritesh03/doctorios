//
//  ForgotSucessPopUpViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol forgotSuccessDelegate {
    func forgotSuccess()
}

class ForgotSucessPopUpViewController: BaseViewControler {
    
    @IBOutlet weak var titleDescriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var cells = 0
    
    var delegate: forgotSuccessDelegate?
    var callback : (() -> Void)? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        titleDescriptionLabel.text = localizeValue(key: "Your password changed successfully")
        titleLabel.text = localizeValue(key: "Congratulations!")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        self.delegate?.forgotSuccess()
        if cells == 3{
            callback!()
        }
        self.dismiss(animated: true, completion: nil)
    }
}
