//
//  NewPatientSucessPopUpViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 13/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol sucessPopUp {
    func sucess()
}

class NewAssistantSucessPopUpViewController: BaseViewControler {

    @IBOutlet weak var shareButton: RoundButton!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelPassword: UILabel!
    
    var delegate: sucessPopUp?
    
    var email: String?
    var Phone: String?
    var password: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
titleLabel.text = localizeValue(key: "Your assistant account created successfully.You can share account info with your assistant")
        emailAddressLabel.text = localizeValue(key: "Email address")
        phoneNumberLabel.text = localizeValue(key: "Phone Number")
        passwordLabel.text = localizeValue(key: "Password")
        shareButton.setTitle(localizeValue(key: "Share"), for: .normal)
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             emailAddressLabel.textAlignment = .right
            phoneNumberLabel.textAlignment = .right
            passwordLabel.textAlignment = .right
            labelEmail.textAlignment = .right
            labelPhone.textAlignment = .right
            labelPassword.textAlignment = .right
        }
        // Do any additional setup after loading the view.
        
        if email == nil {
            self.emailAddressLabel.isHidden = true
            self.labelEmail.isHidden = true
        } else {
            labelEmail.text = email
        }
        
        labelPhone.text = Phone
         labelPassword.text = password
    }
    
    @IBAction func buttonShareClicked(_ sender: Any) {
    
        // text to share
        let text = "Assistant Credentials:\nEmail:\(String(describing: email!))\nPhone No.:\(String(describing: Phone!))\nPassword:\(String(describing: password!))"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.sucess()
    
    }
    
}
