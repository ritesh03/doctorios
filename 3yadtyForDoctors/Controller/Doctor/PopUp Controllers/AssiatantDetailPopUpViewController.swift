//
//  AssiatantDetailPopUpViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 20/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol AssistantDetailDelegate {
    func editProfileClicked()
    func blockAssistantClicked()
    func deleteAssistantClicked()
}

class AssiatantDetailPopUpCell: UITableViewCell{
    @IBOutlet weak var buttonCell: UIButton!
    @IBOutlet weak var labelAssistant: UILabel!
}

class AssiatantDetailPopUpViewController: BaseViewControler {

    @IBOutlet weak var tableOption: UITableView!
    let arrImege: [String] = ["icBlock","icDelete"]//["icEdit","icBlock","icDelete"]
    var arrTitle: [String] = []
    
    var delegate: AssistantDetailDelegate?
    
    var status: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //["Edit Profile","Unblock assistant","Delete assistant access"]
        if status == "1"{
            arrTitle = [localizeValue(key: "Unblock assistant"),localizeValue(key: "Delete assistant access")]
        }else{
           arrTitle = [localizeValue(key: "Block assistant"),localizeValue(key: "Delete assistant access")]
        }
        self.tableOption.reloadData()
    }
    
    @IBAction func buttonCrossClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AssiatantDetailPopUpViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count//3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssiatantDetailPopUpCell", for: indexPath) as! AssiatantDetailPopUpCell
         cell.buttonCell.setImage(UIImage(named: arrImege[indexPath.row]), for: .normal)
         cell.labelAssistant.text = arrTitle[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            self.delegate?.blockAssistantClicked()
        }else if indexPath.row == 1{
            self.delegate?.deleteAssistantClicked()
        }
        self.dismiss(animated: true, completion: nil)
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 65
    }
}
/*
 if indexPath.row == 0{
 self.delegate?.editProfileClicked()
 }else if indexPath.row == 1{
 self.delegate?.blockAssistantClicked()
 }else if indexPath.row == 2{
 self.delegate?.deleteAssistantClicked()
 }
 */
