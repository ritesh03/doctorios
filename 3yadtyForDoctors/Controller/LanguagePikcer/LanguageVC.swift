//
//  LanguageVC.swift
//  3yadtyForDoctors
//
//  Created by apple on 05/04/19.
//  Copyright © 2019 Xperge. All rights reserved.
//

import UIKit

class LanguageVC: BaseViewControler {
    
    //MARK:- Outlets
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var constraint_height: NSLayoutConstraint!
    @IBOutlet weak var view_english: RoundView!
    @IBOutlet weak var view_arabic: RoundView!
    @IBOutlet weak var img_english: UIImageView!
    @IBOutlet weak var img_arabic: UIImageView!
    @IBOutlet weak var languageTitleLabel: UILabel!
    //MARK:- Variables
    private var selectedLanguage: Int = 0
    var fromSettings = false
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.viewDidLoadSetupUI()
        languageTitleLabel.text = localizeValue(key: "Select your language")
        headerTitle.text = localizeValue(key: "Language")
        
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
        }
        if fromSettings{
            backBtn.isHidden = false
        }
        else{
            backBtn.isHidden = true
        }
    }
    @IBAction func backBtnTap(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    func loadWithEnglish() {
          AppLanguage.setAppleLAnguageTo(lang: LanguageType.english)
       
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController")
        self.navigationController!.pushViewController(VC, animated: true)
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewWillAppearSetupUI()
    }
    
    //MARK:- Functions
    private func viewDidLoadSetupUI(){
        
        self.constraint_height.constant += UIApplication.shared.statusBarFrame.size.height
        self.view_english.isUserInteractionEnabled = true
        self.view_arabic.isUserInteractionEnabled = true
        self.view_english.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.actionOnEnglish)))
        self.view_arabic.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.actionOnArabic)))
        self.updateSelected(View: (AppLanguage.currentAppleLanguage() == LanguageType.arabic) ? self.view_arabic : self.view_english)
        if UserDefaults.standard.value(forKey: "isLogIn") != nil{
            AppInstance.shared.user = UserData.parse(CustomObjects.unArchiveObject(WithKey: "isLogIn") as!  Dictionary<String, Any> as NSDictionary)
            if AppInstance.shared.isFromMenu ?? false { return }
            if AppInstance.shared.user?.accounttype == 2{
                
                AppInstance.shared.userType = 0
                if let signupDict = CustomObjects.unArchiveObject(WithKey: "userdata") as?  Dictionary<String, Any>{
                    
                    AppInstance.shared.doctorSignUp = signupDict
                }
            }else if AppInstance.shared.user!.accounttype == 3{
                
                AppInstance.shared.userType = 1
            }
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController!.pushViewController(VC, animated: false)
        }
//        else{
//             loadWithEnglish()
//        }
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.slideMenuController?.allowedLeftSwipe = true
        self.slideMenuController?.allowedRightSwipe = true
    }
    
    private func viewWillAppearSetupUI(){
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func updateSelected(View sender: UIView){
        print(sender.tag)
        self.selectedLanguage = sender.tag
        self.img_english.image = UIImage.init(named: (sender.tag == 1) ? "checkboxActive" : "checkboxDisactive")
        self.img_arabic.image = UIImage.init(named: (sender.tag == 0) ? "checkboxActive" : "checkboxDisactive")
    }
    
    @objc private func actionOnEnglish(){
        
        self.updateSelected(View: self.view_english)
       // AppLanguage.setAppleLAnguageTo(lang: LanguageType.english)
    }
    
    @objc private func actionOnArabic(){
        
        self.updateSelected(View: self.view_arabic)
      //  AppLanguage.setAppleLAnguageTo(lang: LanguageType.arabic)
    }
    
    //MARK:- Action Outlets
    @IBAction func actionOnSelect(_ sender: Any) {
        
        if AppInstance.shared.isFromMenu ?? false {
            if self.selectedLanguage == 0 {
                
                //if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
                // AppLanguage.setAppleLAnguageTo(lang: LanguageType.arabic)
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
                print(AppLanguage.currentAppleLanguage())
                viewDidLoadSetupUI()
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
                self.navigationController!.pushViewController(VC, animated: true)
                let mainwindow = (UIApplication.shared.delegate?.window!)!
                mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
                UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromRight, animations: { () -> Void in
                }) { (finished) -> Void in
                }
                //            }else{
                //
                //                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                let VC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController")
                //                self.navigationController!.pushViewController(VC, animated: true)
                //            }
            }else if self.selectedLanguage == 1 {
                
                //if AppLanguage.currentAppleLanguage() !=  LanguageType.english{
                // AppLanguage.setAppleLAnguageTo(lang: LanguageType.english)
                print(AppLanguage.currentAppleLanguage())
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
                self.navigationController!.pushViewController(VC, animated: true)
                let mainwindow = (UIApplication.shared.delegate?.window!)!
                mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
                UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
                }) { (finished) -> Void in
                }
                //            }else{
                //
                //                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                let VC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController")
                //                self.navigationController!.pushViewController(VC, animated: true)
                //            }
            }
            
            AppInstance.shared.isFromMenu = false
            return
        }
        
        if self.selectedLanguage == 0 {
            
            //if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
               // AppLanguage.setAppleLAnguageTo(lang: LanguageType.arabic)
              LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            print(AppLanguage.currentAppleLanguage())
            viewDidLoadSetupUI()
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let VC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController")
                self.navigationController!.pushViewController(VC, animated: true)
                let mainwindow = (UIApplication.shared.delegate?.window!)!
                mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
                UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromRight, animations: { () -> Void in
                }) { (finished) -> Void in
                }
//            }else{
//
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let VC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController")
//                self.navigationController!.pushViewController(VC, animated: true)
//            }
        }else if self.selectedLanguage == 1 {
            
            //if AppLanguage.currentAppleLanguage() !=  LanguageType.english{
               // AppLanguage.setAppleLAnguageTo(lang: LanguageType.english)
             print(AppLanguage.currentAppleLanguage())
              LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let VC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController")
                self.navigationController!.pushViewController(VC, animated: true)
                let mainwindow = (UIApplication.shared.delegate?.window!)!
                mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
                UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
                }) { (finished) -> Void in
                }
//            }else{
//
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let VC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController")
//                self.navigationController!.pushViewController(VC, animated: true)
//            }
        }
    }
}
