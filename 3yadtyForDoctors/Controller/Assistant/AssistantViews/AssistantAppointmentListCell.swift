//
//  AssistantAppointmentListCell.swift
//  3yadtyForDoctors
//
//  Created by apple on 27/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantAppointmentListCell: UITableViewCell {
    
    @IBOutlet weak var labelPendingReservation: UILabel!
    @IBOutlet weak var labelDrName: UILabel!
    @IBOutlet weak var labelNoOfPatient: UILabel!
    @IBOutlet weak var labelNewVisit: UILabel!
    @IBOutlet weak var labelConsultation: UILabel!
    @IBOutlet weak var imageViewList: UIImageView!
    @IBOutlet weak var labelAsName: UILabel!
    @IBOutlet weak var labelAsDate: UILabel!
    @IBOutlet weak var labelAppointmentType: UILabel!
    @IBOutlet weak var labelNo: UILabel!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonMessage: UIButton!
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelPatientHeader: UILabel!
    @IBOutlet weak var headerTotalPatient: UILabel!
    @IBOutlet weak var labelOnSight: UILabel!
    @IBOutlet weak var labelClinicName: UILabel!
    @IBOutlet weak var buttonLogo: RoundButton!
    @IBOutlet weak var labelTokenNo: UILabel!
    
    @IBOutlet weak var buttonSelectedCell: UIButton!
    @IBOutlet weak var buttonCheckedIn: RoundButton!
    @IBOutlet weak var lineVartical: UILabel!
    @IBOutlet weak var lineHorizontal: UILabel!
    @IBOutlet weak var lineToken: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
