//
//  AssiatantNewReservationCell.swift
//  3yadtyForDoctors
//
//  Created by apple on 27/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssiatantNewReservationCell: UITableViewCell {

    
    @IBOutlet weak var imageViewRequest: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelReservedDate: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var buttonAccept: RoundButton!
    @IBOutlet weak var buttonReject: RoundButton!
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var imageViewUser: RoundImage!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
