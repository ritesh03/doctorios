//
//  AssistantMessageCell.swift
//  3yadtyForDoctors
//
//  Created by apple on 29/11/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantMessageCell: UITableViewCell {

    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labellastMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
