//
//  AssistantAcceptReservationViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol requestAcceptDelegate {
    func requestAcceptedFunc(time: String, note: String, first_time: String, token_number: String, pendingAppointments: DataAppointmentList?)
}

class AssistantAcceptReservationCell : UITableViewCell {
    
    @IBOutlet weak var buttonRadio: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textFieldnumber: UITextField!
    @IBOutlet weak var textFieldTime: CustomTextField!
}

class AssistantAcceptReservationViewController: BaseViewControler, UITextViewDelegate {

    var selectedIndex = 0
    var arrName = ["First come first serve".localize(),"Set time".localize()]
    
    var delegate: requestAcceptDelegate?
    var pendingAppointments: DataAppointmentList?
    var time: String?
    var note: String?
    var token = ""
    
    @IBOutlet weak var textViewAccept: UITextView!
    @IBOutlet weak var tableAccept: UITableView!
    @IBOutlet weak var imageViewReq: RoundImage!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        get_last_appointment_number()
        // Do any additional setup after loading the view.
        if pendingAppointments!.patient_info?.image!.isEmpty != true{
            imageViewReq.sd_setShowActivityIndicatorView(true)
            imageViewReq.sd_setIndicatorStyle(.gray)
            imageViewReq.sd_setImage(with: URL(string: (pendingAppointments!.patient_info?.image!)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            imageViewReq.sd_setShowActivityIndicatorView(false)
        }else{
            imageViewReq.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        labelName.text = "\(pendingAppointments!.patient_info?.firstname ?? "") \(pendingAppointments!.patient_info?.lastname ?? "")"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: pendingAppointments!.appointment_date!)
        dateFormatter.dateFormat = "MMM d, yyyy"
        let result = dateFormatter.string(from: date!)
        labelDate.text = " Consultation on \(result)"
    }
    func get_last_appointment_number(){
        let service = BusinessService()
        
        
        service.get_last_token(with: String(describing: pendingAppointments!.doctor_id!), clinic_id: String(describing: pendingAppointments!.clinic_id!), date: (pendingAppointments!.appointment_date)!, target: self) { (response) in
            if response != nil {
                self.token = "\(response ?? 0)"
            }
            
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add note here…".localize(){
            textViewAccept.text = ""
        }
        textViewAccept.textColor = UIColor.black
    }
    
    func textViewDidChange(_ textView: UITextView) {
        note = textView.text
    }
    
    @IBAction func buttonCrossClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonDoneClicked(_ sender: Any) {
        
        if selectedIndex == 0 {
//            guard let note = note,!note.isEmpty else {
//                UIAlertController.show(title: nil, message: "Please add note", target: self, handler: nil)
//                return
//            }
            
            delegate?.requestAcceptedFunc(time: "", note: note ?? "", first_time: "1", token_number: "", pendingAppointments: self.pendingAppointments)
        }else{
//            guard let token = token,!token.isEmpty else {
//                UIAlertController.show(title: nil, message: "Please enter number", target: self, handler: nil)
//                return
//            }
            guard let time = time,!time.isEmpty else {
                UIAlertController.show(title: nil, message: "Please select time".localize(), target: self, handler: nil)
                return
            }
            if note == "" || textViewAccept.text == "Add note here…".localize() {
//                UIAlertController.show(title: nil, message: "Please add note", target: self, handler: nil)
                note = ""
//                return
            } else {
                note = textViewAccept.text
            }
        
            delegate?.requestAcceptedFunc(time: time, note: note!, first_time: "0", token_number: token, pendingAppointments: self.pendingAppointments)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonSelectTimeClicked(_ sender: Any) {
        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantSetAppointmentTimeViewController") as! AssistantSetAppointmentTimeViewController
        vc.clinicId = String(describing: pendingAppointments!.clinic_id!)
        vc.date = String(describing: pendingAppointments!.appointment_date!)
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        self.present(vc, animated: false, completion: nil)
    }
    @objc private func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 1{
            token = textField.text!
        }
    }
}

extension AssistantAcceptReservationViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0{
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.row {
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell", for: indexPath) as! AssistantAcceptReservationCell
            cell.textFieldnumber.tag = 1
            if time != nil{
                cell.textFieldTime.text = time
            }
            cell.textFieldnumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            cell.textFieldnumber.text = self.token
            cell.textFieldnumber.isUserInteractionEnabled = false
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! AssistantAcceptReservationCell
            cell.labelName.text = arrName[indexPath.row]
            if selectedIndex == indexPath.row {
                cell.buttonRadio.isSelected = true
            }else{
                cell.buttonRadio.isSelected = false
            }
            return cell
        }
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            self.selectedIndex = 0
        }else{
            self.selectedIndex = 1
        }
        tableAccept.reloadData()
    }
    
}

extension AssistantAcceptReservationViewController: TimeSlotSelectedDelegate{
    func timeSelected(time: String) {
        print(time)
        self.time = time
        tableAccept.reloadData()
    }
}
