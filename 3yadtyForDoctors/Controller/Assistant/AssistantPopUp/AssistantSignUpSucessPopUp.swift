//
//  SignUpSucessPopUp.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol assistantSignUpSucessDelegate {
    func signUpSucess()
}

class AssistantSignUpSucessPopUp: BaseViewControler {

    var delegate: assistantSignUpSucessDelegate?
    var isFromEdit: Bool?
    @IBOutlet weak var labelText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isFromEdit == true{
            labelText.text = localizeValue(key: "Profile edit successfully")
        }else{
            labelText.text = localizeValue(key: "Your assistant account created successfully.You can share account info with your assistant")
        }
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        delegate?.signUpSucess()
    }
}
