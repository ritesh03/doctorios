//
//  AssistantCheckInPopUpViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantCheckInPopUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonCheckIn: RoundButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewAssiartant: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        buttonCheckIn.isEnabled = false
        textFieldPassword.delegate = self 
        self.tabBarController?.tabBar.isHidden = true
        labelName.text = "Hi, \(AppInstance.shared.user?.firstname ?? "") \(AppInstance.shared.user?.lastname ?? "")"
        
        if AppInstance.shared.user?.image!.isEmpty != true{
            imageViewAssiartant.sd_setBackgroundImage(with: URL(string: (AppInstance.shared.user?.image!)!), for: .normal)
        }else{
            imageViewAssiartant.setImage(UIImage(named: "imgUserPlaceholderSideMenu"), for: .normal)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text
        let textRange = Range(range, in: text!)
        let updatedText = text!.replacingCharacters(in: textRange!,
                                                    with: string)
        if updatedText.count < 2 {
            //            self.showAlert(with: "Phone number should be more than 8 character")
            buttonCheckIn.backgroundColor = UIColor(red: 198/255.0, green: 198/255.0, blue: 198/255.0, alpha: 1.0)
            buttonCheckIn.isEnabled = false
        }else {
            buttonCheckIn.isEnabled = true
            buttonCheckIn.backgroundColor = UIColor(red: 0/255.0, green: 149/255.0, blue: 188/255.0, alpha: 1.0)
            
        }
        return true
    }

    @IBAction func buttonDoneClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name("showSucessPopUp"), object: nil)
    }
}
