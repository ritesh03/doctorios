//
//  AssistantSetAppointmentDateViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantSetAppointmentDateCell: UITableViewCell {
    
}

class AssistantSetAppointmentDateViewController: UIViewController {
    
    var isFromAppointment = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        if isFromAppointment == true {
           self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: false, completion: nil)
        }
        
    }
}

extension AssistantSetAppointmentDateViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Date_TimeCell", for: indexPath) as! AssistantSetAppointmentDateCell
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "availableCell", for: indexPath) as! AssistantSetAppointmentDateCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! AssistantSetAppointmentDateCell
            return headerView
        }else{
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 35
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.popViewController(animated: true)
    }
}
