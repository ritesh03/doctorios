//
//  AssistantRejectReservationViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol requestRejectDelegate {
    func requestRejectFunc(time: String, note: String, first_time: String, pendingAppointments: DataAppointmentList?)
}

class AssistantRejectReservationCell : UITableViewCell {
    
    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textFieldDate: CustomTextField!
}

class AssistantRejectReservationViewController: UIViewController {
    
    var selectedIndex = 0
    var arrName = ["I am sorry doctor not available this time ".localize(),"Sorry doctor couldn’t accept more patients today".localize()]
    var delegate: requestRejectDelegate?
    var pendingAppointments: DataAppointmentList?
    
    var selectedDate: String?
    var dateTime: String?
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewUser: RoundImage!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var tableReject: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if pendingAppointments!.patient_info?.image!.isEmpty != true{
            
            imageViewUser.sd_setShowActivityIndicatorView(true)
            imageViewUser.sd_setIndicatorStyle(.gray)
            imageViewUser.sd_setImage(with: URL(string: (pendingAppointments!.patient_info?.image!)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            imageViewUser.sd_setShowActivityIndicatorView(false)
        }else{
            imageViewUser.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        labelName.text = "\(pendingAppointments!.patient_info?.firstname ?? "") \(pendingAppointments!.patient_info?.lastname ?? "")"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: pendingAppointments!.appointment_date!)
        dateFormatter.dateFormat = "MMM d, yyyy"
        let result = dateFormatter.string(from: date!)
        labelDate.text = " Consultation on \(result)"
    }
    
    @IBAction func buttonCrossClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        
        guard let selectedDate = selectedDate,!selectedDate.isEmpty else {
            UIAlertController.show(title: nil, message: "Please select date".localize(), target: self, handler: nil)
            return
        }
        
        delegate?.requestRejectFunc(time: selectedDate , note: "1", first_time: String(describing: self.selectedIndex), pendingAppointments: self.pendingAppointments)
        self.dismiss(animated: true, completion: nil)
    }
}

extension AssistantRejectReservationViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "dateCell", for: indexPath) as! AssistantRejectReservationCell
            if dateTime != nil{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date = dateFormatter.date(from: self.selectedDate!)
                dateFormatter.dateFormat = "MMM d, yyyy"
                let result = dateFormatter.string(from: date!)
                cell.textFieldDate.text = "\(String(describing: result))"
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! AssistantRejectReservationCell
            cell.labelName.text = arrName[indexPath.row]
            if selectedIndex == indexPath.row {
                cell.btnRadio.isSelected = true
            }else{
                cell.btnRadio.isSelected = false
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if indexPath.row == 2{
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "SetAppointmentDate_TimeViewController") as! SetAppointmentDate_TimeViewController
                vc.clinicId = String(describing: pendingAppointments!.clinic_id!)
                vc.delegate = self
                vc.isfromReject = true
                vc.modalPresentationStyle = .overCurrentContext
                vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                self.present(vc, animated: false, completion: nil)
            
            /*let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantSetAppointmentDateViewController") as! AssistantSetAppointmentDateViewController
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
            self.present(vc, animated: false, completion: nil)*/
        }else{
            self.selectedIndex = indexPath.row
            tableReject.reloadData()
        }
    }
}

extension AssistantRejectReservationViewController: TimeSelectedDelegate{
  
    
    func selectedTime(time: String, date: String, appointmentId: String) {
        selectedDate = date
        dateTime = time
        tableReject.reloadData()
    }
}
