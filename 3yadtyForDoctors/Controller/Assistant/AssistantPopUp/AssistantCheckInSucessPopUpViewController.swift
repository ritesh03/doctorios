//
//  AssistantCheckInSucessPopUpViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantCheckInSucessPopUpViewController: UIViewController {

    @IBOutlet weak var imageViewSignUp: RoundButton!
    @IBOutlet weak var labelName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelName.text = "Hi, \(AppInstance.shared.user?.firstname ?? "") \(AppInstance.shared.user?.lastname ?? "")"
        
        if AppInstance.shared.user?.image!.isEmpty != true{
            imageViewSignUp.sd_setBackgroundImage(with: URL(string: (AppInstance.shared.user?.image!)!), for: .normal)
        }else{
            imageViewSignUp.setImage(UIImage(named: "imgUserPlaceholderSideMenu"), for: .normal)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    @IBAction func buttonDoneClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name("showSucessPopUp"), object: 1)
    }
}
