//
//  AssistantSetAppointmentTimeViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

protocol TimeSlotSelectedDelegate {
    func timeSelected(time: String)
}

class AssistantSetDateTimeCell: UITableViewCell {
    @IBOutlet weak var labelAvailableTime: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelType: UILabel!
    
    @IBOutlet weak var availableLabel: UILabel!
    @IBOutlet weak var bookBtn: RoundButton!
}

class AssistantSetAppointmentTimeViewController: BaseViewControler {
    
    var clinicId: String?
    var timeSlotData: [TimeSlotData]?
    var availability: Availability?
    var arraySlots: [String] = []
    var delegate: TimeSlotSelectedDelegate?
    let formatter = DateFormatter()
    var date: String?
    var arrayTimesBooked : [String] = []
    @IBOutlet weak var tableTimeSlot: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        formatter.dateFormat = "yyyy-MM-dd"
        let a = formatter.date(from: date!)
        let result = formatter.string(from: a!)
        formatter.dateFormat = "EEEE"
        let day = formatter.string(from: a!)
        self.appointmentTimeSlots(date: result, day: day)
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func appointmentTimeSlots(date: String, day: String) {
        let service = BusinessService()
        service.appointmentTimeSlots(with: date, clinic_id: clinicId!, day: day, target: self) { (response) in
            if let data = response {
                if data.data != nil{
                    self.timeSlotData = data.data!
                    if (self.timeSlotData?.count)! > 0 {
                        self.arrayTimesBooked = self.timeSlotData!.map{$0.time_slot!}
                    }
                    
                }
                self.availability = data.availability!
                if ((self.availability?.clinic_id) != nil) {
                    self.calculateTimeSlot(startDate: self.availability!.start_time!, endDate: self.availability!.end_time!, timing: Int(self.availability!.clinicScheduleCount!)!)
                    //self.createArrayfromTimeSlot()
                }
            }
        }
    }
    
    func calculateTimeSlot(startDate: String, endDate: String, timing: Int) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "hh:mm a"
        
        let date1 = formatter.date(from: startDate)
        let date2 = formatter.date(from: endDate)
        var i = 0
        while true {
            let date = date1?.addingTimeInterval(TimeInterval(i*timing*60))
            let string = formatter2.string(from: date!)
            if date! > date2! {
                break;
            }
            i += 1
            
            arraySlots.append(string)
            
        }
        print(arraySlots)
        self.tableTimeSlot.reloadData()
    }

}

extension AssistantSetAppointmentTimeViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arraySlots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.arrayTimesBooked.count > 0 {
            if self.arrayTimesBooked.contains(arraySlots[indexPath.row]) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Date_TimeCell1") as! Date_TimeCell
                let integer = self.arrayTimesBooked.firstIndex(of: self.arraySlots[indexPath.row])!
                cell.labelName.text = "\(self.timeSlotData![integer].patient_info?.firstname ?? "") \(self.timeSlotData![integer].patient_info?.lastname ?? "")"
                cell.labelTime.text = self.timeSlotData![integer].time_slot
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "availableCell") as! AssistantSetDateTimeCell
                cell.labelAvailableTime.text = arraySlots[indexPath.row]
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "availableCell") as! AssistantSetDateTimeCell
            cell.labelAvailableTime.text = arraySlots[indexPath.row]
            return cell
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.timeSelected(time: arraySlots[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}
