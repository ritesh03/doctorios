//
//  AssistantInfoTwoViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 22/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantInfoTwoViewController: BaseViewControler {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
titleLabel.text = localizeValue(key: "welcome to Doctor assistant")
        descriptionLabel.text = localizeValue(key: "● Manage your clinic. ● Connect with your patient.")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buttonSkipClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
