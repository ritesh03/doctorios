//
//  AssistantNotificationViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 31/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantNotificationCell: UITableViewCell {
    
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labeldate: UILabel!
}


class AssistantNotificationViewController: BaseViewControler {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tableNotification: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    var arrNotification = [NotificationData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //menu Button
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.image = UIImage(named: "menu_right")!.withRenderingMode(.alwaysOriginal)
            if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
                
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }else{
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        self.errorView.isHidden = true
        getNotification()
    }
    
    func getNotification(){
        let service = BusinessService()
        service.getNotification(with: String(describing: AppInstance.shared.user!.id!), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
            }
            if let list = response {
                self.arrNotification = list.data!
                if (self.arrNotification.count) > 0{
                    self.errorView.isHidden = true
                }else{
                    self.errorView.isHidden = false
                }
            }
            self.tableNotification.reloadData()
        }
    }
}

extension AssistantNotificationViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssistantNotificationCell", for: indexPath) as! AssistantNotificationCell
        cell.labelMessage.text = arrNotification[indexPath.row].message
        //cell.labeldate.text = arrNotification[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //
    //        let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! NotificationCell
    //        return headerView
    //
    //    }
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return 1
    //    }
    
}
