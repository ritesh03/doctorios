//
//  AssistantPatientInfoViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class AssistantPatientInfoCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var labelDetail: UILabel!
}

class AssistantPatientInfoViewController: UIViewController,IndicatorInfoProvider {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "Info"))
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    
    
}

extension AssistantPatientInfoViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientInfoCell") as! AssistantPatientInfoCell
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "Phone".localize()
            cell.labelDetail.text = "01234567890"
            cell.buttonImage.setImage(UIImage(named: "imgPhone"), for: .normal)
        case 1:
            cell.titleLabel.text = "Email".localize()
            cell.labelDetail.text = "Test@gmail.com"
            cell.buttonImage.setImage(UIImage(named: "imgEmail"), for: .normal)
        default:
            cell.titleLabel.text = "Address".localize()
            cell.labelDetail.text = "Street: 94681 Mockingbird City: La Habra State: CO Zip: 610"
            cell.buttonImage.setImage(UIImage(named: "imgAddress"), for: .normal)
            
        }
        return cell
    }
    
    
}
