//
//  AssistantNewAppointmentsViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantNewAppointmentsCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textFieldName: UITextField!
}

class AssistantNewAppointmentsViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var buttonNew: RoundButton!
    @IBOutlet weak var buttonConsult: RoundButton!
    @IBOutlet weak var textViewAppointment: UITextView!
    @IBOutlet weak var buttonFirstCome: UIButton!
    @IBOutlet weak var buttonSetTime: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        textViewAppointment.delegate = self
        buttonFirstCome.isSelected = true
    }
    
    @IBAction func buttonReservationTypeClicked(_ sender: UIButton) {
        if sender.titleLabel?.text == "New Visit".localize(){
            self.buttonNew.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
            self.buttonConsult.backgroundColor = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1.0)
            self.buttonConsult.setTitleColor(.black, for: .normal)
            self.buttonNew.setTitleColor(.white, for: .normal)
        }else{
            self.buttonConsult.backgroundColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1.0)
            self.buttonNew.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
            self.buttonNew.setTitleColor(.black, for: .normal)
            self.buttonConsult.setTitleColor(.white, for: .normal)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewAppointment.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSelectClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonTypeClicked(_ sender: Any) {
        if (sender as AnyObject).tag == 10{
            self.buttonFirstCome.isSelected = true
            self.buttonSetTime.isSelected = false
        }else{
            self.buttonFirstCome.isSelected = false
            self.buttonSetTime.isSelected = true
        }
    }
}

extension AssistantNewAppointmentsViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAppointmientCell", for: indexPath) as! AssistantNewAppointmentsCell
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "Patient name".localize()
            cell.textFieldName.placeholder = "Patient name".localize()
        default:
            cell.titleLabel.text = "Date & Time".localize()
            cell.textFieldName.placeholder = "Today 12 Jan 2018 : 02:30 PM"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = storyboard?.instantiateViewController(withIdentifier: "AssistantDoctatorListViewController") as! AssistantDoctatorListViewController
            vc.listType = "3"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1{
            let vc = storyboard?.instantiateViewController(withIdentifier: "AssistantSetAppointmentDateViewController") as! AssistantSetAppointmentDateViewController
                vc.isFromAppointment = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
