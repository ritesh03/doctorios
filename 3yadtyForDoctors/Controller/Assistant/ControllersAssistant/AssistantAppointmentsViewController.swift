//
//  AssistantAppointmentsViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class AssistantAppointmentsViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var error_view: UIView!
    
    override func viewDidLoad() {
        // change selected bar color XLPagerTabStrip
        
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .white
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = .white
        }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //menu Button
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.image = UIImage(named: "menu_right")!.withRenderingMode(.alwaysOriginal)
            if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
                
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }else{
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        NotificationCenter.default.removeObserver(self)
    NotificationCenter.default.addObserver(self,selector:#selector(self.showSucessPopUp),
                                               name:NSNotification.Name(rawValue: "showSucessPopUp"),object: nil)
//        if  AppInstance.shared.isAssistantCheckIn != true{
//            //show pop for assistant check in
//            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantCheckInPopUpViewController") as! AssistantCheckInPopUpViewController
//            vc.modalPresentationStyle = .overFullScreen
//            vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
//            self.present(vc, animated: false, completion: nil)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        //add child view controller to XLPagerTabStrip
        let child_1 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "AssistantAppointmentsListViewController"))!
        let child_2 : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "AssistantAppointmentCalenderViewController"))!
        let array :  [UIViewController] = [child_1,child_2]
        return array
    }
    
    @IBAction func buttonAddAppointmentClicked(_ sender: Any) {
        let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "AddNewAppointmientsViewController") as! AddNewAppointmientsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func showSucessPopUp(notification: NSNotification){
        if notification.object == nil{
             AppInstance.shared.isAssistantCheckIn = true
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantCheckInSucessPopUpViewController") as! AssistantCheckInSucessPopUpViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
            self.present(vc, animated: false, completion: nil)
        }else{
            
        }
    }
}
