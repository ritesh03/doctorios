//
//  AssistantInfoOneViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 22/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistantInfoOneViewController: BaseViewControler {

    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var label3: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "welcome to Doctor assistant", comment: "")
        skipButton.setTitle(localizeValue(key: "Skip"), for: .normal)
descriptionLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "CliniDo will help you to manage your clinic in easy way", comment: "")
        label3.text = ""
        pointsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bulletsText", comment: "")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssistantInfoTwoViewController") as! AssistantInfoTwoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buttonSkipClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
