//
//  AssistantNewReservationsViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit


class AssistantNewReservationsViewController: BaseViewControler {

    var appointmentListData: DataAppointment?
    var pendingAppointments: [DataAppointmentList]?
    var drPendingAppointment: [DataDoctorRequest]?
    var isPatientRequest: Bool?
    
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var errorDescription: UILabel!
    @IBOutlet weak var errorTitle: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tablePendingReservation: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorTitle.text = localizeValue(key: "Request list is empty")
        errorDescription.text = localizeValue(key: "You don't have any requests")
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        errorView.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //Api get pending appintment
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            errorImageView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
        }

        if AppInstance.shared.userType == 0 {
            //show list of patient or Assistant(Dr App)
            if isPatientRequest == true{
                labelTitle.text = "New Reservations".localize()
                DrPendingPatientAppointments() // panding appoint list of patient
            }else{
                labelTitle.text = localizeValue(key: "New Requests")
                drPendingAppointments() // panding appoint list of Assistant
            }
        }else if AppInstance.shared.userType == 1{
             labelTitle.text = "New Reservations".localize()
             getPendingAppointment() // panding appoint list of patient(Assistant App)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        if AppInstance.shared.userType == 0 {
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "TabbarController")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if AppInstance.shared.userType == 1{
             self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func AcceptButton(_ sender: UIButton) {
        if AppInstance.shared.userType == 0 {
             if isPatientRequest == true{
                let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantAcceptReservationViewController") as! AssistantAcceptReservationViewController
                vc.delegate = self
                vc.pendingAppointments = self.pendingAppointments![sender.tag]
                vc.modalPresentationStyle = .overCurrentContext
                vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                self.present(vc, animated: false, completion: nil)
             }else{
                drApproveAssistantRequest(id: String(describing: drPendingAppointment![sender.tag].id!), type: "1")
            }
        }else if AppInstance.shared.userType == 1{
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantAcceptReservationViewController") as! AssistantAcceptReservationViewController
            vc.delegate = self
            vc.pendingAppointments = self.pendingAppointments![sender.tag]
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func RejectButton(_ sender: UIButton) {
        
        
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: "\(localizeValue(key: "this Assistant?"))", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: localizeValue(key: "Yes"), style: UIAlertActionStyle.default) {
            UIAlertAction in
           
            if AppInstance.shared.userType == 0 {
                if self.isPatientRequest == true{
                    let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantRejectReservationViewController") as! AssistantRejectReservationViewController
                    vc.delegate = self
                    vc.pendingAppointments = self.pendingAppointments![sender.tag]
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                    self.present(vc, animated: false, completion: nil)
                }else{
                    self.drApproveAssistantRequest(id: String(describing: self.drPendingAppointment![sender.tag].id!), type: "2")
                }
            }else if AppInstance.shared.userType == 1{
                let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantRejectReservationViewController") as! AssistantRejectReservationViewController
                vc.delegate = self
                vc.pendingAppointments = self.pendingAppointments![sender.tag]
                vc.modalPresentationStyle = .overCurrentContext
                vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                self.present(vc, animated: false, completion: nil)
            }
        }
        let cancelAction = UIAlertAction(title: localizeValue(key: "No"), style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    //MARK: Get Pending Appointment of patients
    func getPendingAppointment() {
        let service = BusinessService()
        service.assistantPendingAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: "1", doctor_id: String(describing: AppInstance.shared.user!.dr_id!), clinic_id: String(describing:AppInstance.shared.user!.clinic_id!), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
                self.appointmentListData = nil
                self.pendingAppointments = nil
            }
            if let list = response {
                print(list)
                self.appointmentListData = nil
                self.pendingAppointments = nil
                self.errorView.isHidden = true
                self.appointmentListData = list
                self.pendingAppointments = list.data!
            }
             self.tablePendingReservation.reloadData()
        }
    }
    //MARK: drPendingAppointments assistant
    func drPendingAppointments() {
        let service = BusinessService()
        service.drPendingAppointments(with: String(describing: AppInstance.shared.user!.id!), type: "1", target: self) { (response) in
            if response == nil{
                 self.errorView.isHidden = false
                 self.drPendingAppointment = nil
            }
            if let list = response {
                print(list)
                 self.drPendingAppointment = nil
                 self.errorView.isHidden = true
                self.drPendingAppointment = list.data!
            }
            self.tablePendingReservation.reloadData()
        }
    }
    
    //MARK: accept Assistant Request
    func drApproveAssistantRequest(id : String, type: String)  {
        let service = BusinessService()
        service.approveAssistant(with: id, type: type, doctor_id: String(describing: AppInstance.shared.user!.id!), target: self) { (response) in
            if response == true{
                self.drPendingAppointments()
            }
        }
    }
    
     //MARK: patient pending Appointment
    func DrPendingPatientAppointments() {
        let service = BusinessService()
        service.DrPendingPatientAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: "1", clinic_id: String(describing:AppInstance.shared.user!.remembertoken!), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
                self.appointmentListData = nil
                self.pendingAppointments = nil
            }
            if let list = response {
                print(list)
                self.appointmentListData = nil
                self.pendingAppointments = nil
                self.errorView.isHidden = true
                self.appointmentListData = list
                self.pendingAppointments = list.data!
            }
            self.tablePendingReservation.reloadData()
        }
    }
}

extension AssistantNewReservationsViewController : UITableViewDataSource, UITableViewDelegate  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AppInstance.shared.userType == 0 {
            if isPatientRequest == true{
                return self.pendingAppointments?.count ?? 0
            }
            return self.drPendingAppointment?.count ?? 0
        }else {
            return self.pendingAppointments?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if indexPath.section == 0{
          if AppInstance.shared.userType == 0 {
            if isPatientRequest == true{
                let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as! AssiatantNewReservationCell
                let data = pendingAppointments![indexPath.row]
                cell.labelName.text = "\(String(describing: data.patient_info!.firstname!)) \(String(describing: data.patient_info!.lastname!))"
                let dateStr = TimeUtils.sharedUtils.chaneDateFormet(data.appointment_date!)
                
                if data.set_time == "" {
                    cell.labelDate.text = "\(dateStr)"
                } else {
                    cell.labelDate.text = "\(dateStr) | \(data.set_time ?? "")"
                }
                
                if data.appointment_type == 1{
                    cell.labelReservedDate.text = "Reserved a New Visit on \(String(describing: dateStr))"
                }else{
                    cell.labelReservedDate.text = "Reserved a Consultation on \(String(describing: dateStr))"
                }
                if data.patient_info?.image!.isEmpty != true{
                    
                    cell.imageViewUser.sd_setShowActivityIndicatorView(true)
                    cell.imageViewUser.sd_setIndicatorStyle(.gray)
                    cell.imageViewUser.sd_setImage(with: URL(string: Config.imageBaseUrl + (data.patient_info?.image!)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                    cell.imageViewUser.sd_setShowActivityIndicatorView(false)
                    
                }else{
                    cell.imageViewUser.image = UIImage(named: "imgUserPlaceholderSideMenu")
                }

                cell.buttonAccept.tag = indexPath.row
                cell.buttonReject.tag = indexPath.row
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as! AssiatantNewReservationCell
              
                let data = drPendingAppointment![indexPath.row]
                cell.labelReservedDate.text = "\(String(describing: data.firstname!)) \(String(describing: drPendingAppointment![indexPath.row].lastname!))"
                if data.image! .isEmpty != true{
                    cell.imageViewUser.sd_setShowActivityIndicatorView(true)
                    cell.imageViewUser.sd_setIndicatorStyle(.gray)
                    cell.imageViewUser.sd_setImage(with: URL(string: data.image!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                    cell.imageViewUser.sd_setShowActivityIndicatorView(false)
                }else{
                    cell.imageViewUser.image = UIImage(named: "imgUserPlaceholderSideMenu")
                }

                if data.countrycode?.first == "+" {
                   cell.labelDate.text = "\(String(describing: data.countrycode!)) \(String(describing: data.mobile!))"
                }
                else{
                      cell.labelDate.text = "+\(String(describing: data.countrycode!)) \(String(describing: data.mobile!))"
                }
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.labelName.textAlignment = .right
                     cell.labelDate.textAlignment = .right
                    cell.labelReservedDate.textAlignment = .right
                      cell.labelName.text = data.clinic?.clinic_name_ar
                }else{
                      cell.labelName.text = data.clinic?.clinic_name
                }
                cell.buttonAccept.tag = indexPath.row
                cell.buttonReject.tag = indexPath.row
                cell.buttonAccept.setTitle(localizeValue(key: "Reject"), for: .normal)
                cell.buttonReject.setTitle(localizeValue(key: "Accept"), for: .normal)
                return cell
            }
          }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as! AssiatantNewReservationCell
            let data = pendingAppointments![indexPath.row]
            cell.labelName.text = "\(String(describing: data.patient_info!.firstname!)) \(String(describing: data.patient_info!.lastname!))"
            //change date formet
            let dateStr = TimeUtils.sharedUtils.chaneDateFormet(data.appointment_date!)
            
            if data.set_time == "" {
                cell.labelDate.text = "\(dateStr)"
            } else {
                cell.labelDate.text = "\(dateStr) | \(data.set_time ?? "")"
            }
            
            if data.appointment_type == 1{
                cell.labelReservedDate.text = "Reserved a New Visit on \(String(describing: dateStr))"
            }else{
                cell.labelReservedDate.text = "Reserved a Consultation on \(String(describing: dateStr))"
            }
            if data.patient_info?.image!.isEmpty != true{
                
                cell.imageViewUser.sd_setShowActivityIndicatorView(true)
                cell.imageViewUser.sd_setIndicatorStyle(.gray)
                cell.imageViewUser.sd_setImage(with: URL(string: (data.patient_info?.image!)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                cell.imageViewUser.sd_setShowActivityIndicatorView(false)
            }else{
                cell.imageViewUser.image = UIImage(named: "imgUserPlaceholderSideMenu")
            }

            cell.buttonAccept.tag = indexPath.row
            cell.buttonReject.tag = indexPath.row
            return cell
          }
            
//            switch indexPath.row {
//            case 0:
//
//                return cell
//            default:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "AcceptCell", for: indexPath) as! AssiatantNewReservationCell
//                return cell
//            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as! AssiatantNewReservationCell
            return cell
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! AssiatantNewReservationCell
//         return headerView
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if AppInstance.shared.userType == 0 {
            if drPendingAppointment != nil{
                return 35
            }
            return 0
        }else{
            if pendingAppointments != nil{
                return 35
            }
            return 0
        }
    }
}

extension AssistantNewReservationsViewController : requestAcceptDelegate, requestRejectDelegate{
   
    func requestAcceptedFunc(time: String, note: String, first_time: String, token_number:String, pendingAppointments: DataAppointmentList?) {
        print(pendingAppointments!.id!)
        //MARK: accept appointment
        if AppInstance.shared.userType == 0 {
            let service = BusinessService()
            service.drPendingPatientAcceptAppointment(with: String(pendingAppointments!.id!), user_id: String(describing: AppInstance.shared.user!.id!), set_time: time, first_time: first_time, token_number: token_number, notes: note, doctor_id: String(describing: AppInstance.shared.user!.id!), target: self) { (response) in
                if let list = response {
                    print(list)
                    self.DrPendingPatientAppointments()
                }
            }
        }else if AppInstance.shared.userType == 1 {
            let service = BusinessService()
            service.acceptAppointment(with: String(pendingAppointments!.id!), user_id: String(describing: AppInstance.shared.user!.id!), set_time: time, first_time: first_time, token_number: token_number, notes: note, target: self) { (response) in
                if let list = response {
                    print(list)
                    self.getPendingAppointment()
                }
            }
        }
    }
    
    func requestRejectFunc(time: String, note: String, first_time: String, pendingAppointments: DataAppointmentList?) {
        //MARK: reject appointment
        if AppInstance.shared.userType == 0 {
            let service = BusinessService()
            service.drPendingPatientRejectAppointment(with: String(pendingAppointments!.id!), user_id: String(describing: AppInstance.shared.user!.id!), first_time: first_time, notes: "1", reject_date: time, target: self) { (response) in
                if let list = response {
                    print(list)
                    self.DrPendingPatientAppointments()
                }
            }
        }else  if AppInstance.shared.userType == 1{
            let service = BusinessService()
            service.rejectAppointment(with: String(pendingAppointments!.id!), user_id: String(describing: AppInstance.shared.user!.id!), reject_type: first_time, notes: "2018-10-26", target: self) { (response) in
                if let list = response {
                    print(list)
                    self.getPendingAppointment()
                }
            }
        }
    }
}
