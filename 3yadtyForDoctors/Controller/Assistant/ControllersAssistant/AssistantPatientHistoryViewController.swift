//
//  AssistantPatientHistoryViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class AssistantPatientHistoryCell: UITableViewCell {
    @IBOutlet weak var labelAppointmentType: UILabel!
    @IBOutlet weak var labelAppointmrntDate: UILabel!
    @IBOutlet weak var imageViewDoctator: UIButton!
    @IBOutlet weak var labelDrName: UILabel!
    @IBOutlet weak var labelDr: UILabel!
    @IBOutlet weak var buttonType: RoundButton!
}

class AssistantPatientHistoryViewController:  UIViewController, IndicatorInfoProvider {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "History"))
    }
    func localizeValue(key: String) -> String  {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: "")
    }
    
    
}

extension AssistantPatientHistoryViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! PatientHistoryCell
            return headerView
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PatientHistoryCell", for: indexPath) as! AssistantPatientHistoryCell
            cell.labelAppointmentType.text = "Consult".localize()
            cell.labelAppointmrntDate.text = "20 jan 2018 |  1:30 PM "
            cell.buttonType.backgroundColor = UIColor(red: 126/255, green: 211/255, blue: 33/255, alpha: 1)
            cell.buttonType.titleColor = UIColor.white
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PatientHistoryCell", for: indexPath) as! AssistantPatientHistoryCell
            cell.buttonType.backgroundColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1)
            cell.buttonType.titleColor = UIColor(red: 0/255, green: 149/255, blue: 188/255, alpha: 1)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssistantPatientHistoryDetailViewController") as! AssistantPatientHistoryDetailViewController
                 self.navigationController?.pushViewController(vc, animated: true)
    }
}
