//
//  AssistantSignUpViewController.swift
//  3yadty For Doctors
//
//  Created by apple on 11/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker
import Photos

class AssistantSignUpViewController: BaseViewControler , MRCountryPickerDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    //MARK:- Outlets

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    @IBOutlet weak var tableAssistantSignupInfo: UITableView!
    @IBOutlet weak var imageViewImage: RoundButton!
    
    
    //MARK:- Variables
    let myPickerController = UIImagePickerController()
    var imagesArray = [File]()
    var flagImage: UIImage?
    var countryCode: String?
    var fname: String?
    var lname: String?
    var email: String?
    var password: String?
    var confirmPassword: String?
    var drName: String?
    var drId: Int?
    var clinicName: String?
    var clinicId: Int?
    var arrSelectedDr: UserData?
    var arrSelectedClinic: Clinic?
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        titleLabel.text = localizeValue(key: "Assistant sign up")
        myPickerController.delegate = self
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        let countryCode = Locale.current.regionCode
        countryPicker.setCountry(countryCode ?? "" )
        let countryName = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode ?? "")
        countryPicker.setCountryByName(countryName ?? "")
        countryPicker.setLocale("EN")
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self,selector:#selector(self.drSelected),
                                               name:NSNotification.Name(rawValue: "drSelected"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(self.clinicSelected),
                                               name:NSNotification.Name(rawValue: "clinicSelected"),object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:- Functions
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //self.flagImage = flag
        //self.countryCode = countryCode
        // AppInstance.shared.selectedFlag = flag
        //AppInstance.shared.countryCode = phoneCode
    }
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if textField.tag == 10{
            fname = textField.text
        }else if textField.tag == 11{
            lname = textField.text
        }else if textField.tag == 12{
            email = textField.text
        }else if textField.tag == 13{
            password = textField.text
        }else if textField.tag == 14{
            confirmPassword = textField.text
        }else if textField.tag == 15{
            drName = textField.text
        }else if textField.tag == 16{
            clinicName = textField.text
        }
    }
    func assistantSignUp() {
        let service = BusinessService()
        print(imagesArray.count)
        var countryCode = ""
        if imagesArray.count == 1 {
            countryCode = String(AppInstance.shared.countryCode!.dropFirst())
        }else {
            countryCode = AppInstance.shared.countryCode!
        }
         let deviceToken = UserDefaults.standard.string(forKey: "token")
        service.assistantSignup(with: fname ?? "", lastname: lname ?? "", email: email ?? "", password: password ?? "", countrycode: countryCode , mobile: CommonClass.checkAndEditPhoneNumberFor(phoneNumber: AppInstance.shared.mobile!), image: imagesArray, devicetype: "2", devicetoken: deviceToken ?? "", dr_id: String(describing: drId!), clinic_id: String(describing: clinicId!), target: self) { (response) in
            if let user = response {
                print(user)
                //AppInstance.shared.user = user.data!
                //UserDefaults.standard.set(user.data!.dictionaryRepresentation() , forKey: "isLogIn")
                let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantSignUpSucessPopUp") as! AssistantSignUpSucessPopUp
                vc.delegate = self
                vc.isFromEdit = false
                vc.modalPresentationStyle = .overCurrentContext
                vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                self.present(vc, animated: false, completion: nil)
            }
        }
    }
    
    @objc func drSelected(notification: NSNotification){
        arrSelectedDr = notification.object! as? UserData
        //        print(arrSelectedDr!.firstname!)
        drName = "\(arrSelectedDr!.firstname!) \(arrSelectedDr!.lastname!)"
        drId = arrSelectedDr!.id!
        clinicName = ""
        clinicId = nil
        tableAssistantSignupInfo.reloadData()
    }
    @objc func clinicSelected(notification: NSNotification){
        arrSelectedClinic = notification.object! as? Clinic
        print(arrSelectedClinic!.clinic_name!)
        clinicName = arrSelectedClinic!.clinic_name!
        clinicId = arrSelectedClinic!.id!
        tableAssistantSignupInfo.reloadData()
    }
    
    //MARK:- Actions Outlets
    @IBAction func buttonCancelClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    @IBAction func buttonNextClicked(_ sender: Any) {
        
        //validation
//        if imagesArray.isEmpty == true{
//            UIAlertController.show(title: nil, message: Alert.PleaseUploadImage, target: self, handler: nil)
//            return
//        }
        guard let fname = fname,!fname.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter first name"), target: self, handler: nil)
            return
        }
        guard let lname = lname,!lname.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter last name"), target: self, handler: nil)
            return
        }
        guard let email = email,!email.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter email"), target: self, handler: nil)
            return
        }
        if !(email).isValidEmail() {
            UIAlertController.show(title: nil, message: localizeValue(key: "Invaild email"), target: self, handler: nil)
            return
        }
        guard let password = password,!password.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please enter password"), target: self, handler: nil)
            return
        }
        if password != confirmPassword{
            UIAlertController.show(title: nil, message: localizeValue(key: "Password doesn't match"), target: self, handler: nil)
            return
        }
        guard let drName = drName,!drName.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please select doctor"), target: self, handler: nil)
            return
        }
        guard let clenicName = clinicName,!clenicName.isEmpty else {
            UIAlertController.show(title: nil, message: localizeValue(key: "Please select Clinic"), target: self, handler: nil)
            return
        }
        assistantSignUp()
    }
    @IBAction func buttonCountaryClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonProfileClicked(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Camera"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                self.myPickerController.allowsEditing = true
                self.present(self.myPickerController, animated: true, completion: nil)
            }else{
                UIAlertController.show(title: nil, message: "No camera available.".localize(), target: self, handler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Gallery"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.myPickerController.allowsEditing = true
            self.present(self.myPickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    //imagePicker Delegeate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //let imageName = randomString(length: 30)
        let imageName: String?
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageName = "\(currentTimeStamp).jpg"
            imageViewImage.setImage(chosenImage, for: .normal)
            let file = File(name: imageName!, image: chosenImage, type: 1)
            if (imagesArray.count) > 0 {
                imagesArray.removeLast()
            }
            imagesArray.append(file)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    //generate random string
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
 
    
}

extension AssistantSignUpViewController : assistantSignUpSucessDelegate{
   
    func signUpSucess() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        //        let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantTabbarController")
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AssistantSignUpViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantSignupCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .words
            cell.textFieldName.placeholder = localizeValue(key: "First name")
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.labelTitle.text = localizeValue(key: "First name")
            cell.downArrow.isHidden = true
            cell.textFieldName.tag = 10
            cell.textFieldName.isUserInteractionEnabled = true
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantSignupCell
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .words
            cell.textFieldName.placeholder = localizeValue(key: "Last name")
            cell.labelTitle.text = localizeValue(key: "Last name")
            cell.downArrow.isHidden = true
            cell.textFieldName.tag = 11
            cell.textFieldName.isUserInteractionEnabled = true
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as! AssistantSignupCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.tfPhone.isSecureTextEntry = false
             cell.countryCodeLabel.text = localizeValue(key: "Country Code")
            cell.phoneNumberLabel.text = localizeValue(key: "Phone Number")
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.countryCodeLabel.textAlignment = .right
                cell.phoneNumberLabel.textAlignment = .right
                cell.tfPhone.textAlignment = .right
            }
            cell.tfPhone.isUserInteractionEnabled = false
            cell.imageViewFlag.image = AppInstance.shared.selectedFlag
            cell.labelCountaryCode.text = AppInstance.shared.countryCode
            cell.tfPhone.text = AppInstance.shared.mobile
            cell.tfPhone.keyboardType = .phonePad
            cell.tfPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantSignupCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .none
            cell.textFieldName.placeholder = "example@email.com"
            cell.textFieldName.keyboardType = .emailAddress
            cell.downArrow.isHidden = true
            cell.textFieldName.isUserInteractionEnabled = true
            cell.textFieldName.tag = 12
            cell.labelTitle.text = localizeValue(key: "Email address")
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantSignupCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.isSecureTextEntry = true
            cell.textFieldName.autocapitalizationType = .none
            cell.textFieldName.isUserInteractionEnabled = true
            cell.downArrow.isHidden = true
            cell.textFieldName.placeholder = "･････････"
            cell.textFieldName.tag = 13
            cell.labelTitle.text = localizeValue(key: "Create Password")
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantSignupCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.isSecureTextEntry = true
            cell.textFieldName.autocapitalizationType = .none
            cell.downArrow.isHidden = true
            cell.textFieldName.placeholder = "･････････"
            cell.textFieldName.isUserInteractionEnabled = true
            cell.textFieldName.tag = 14
            cell.labelTitle.text = localizeValue(key: "Confirm password")
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantSignupCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.isUserInteractionEnabled = false
            cell.textFieldName.autocapitalizationType = .words
            cell.downArrow.isHidden = false
            cell.textFieldName.placeholder = localizeValue(key: "Doctor name who is you work with")
            cell.textFieldName.tag = 15
            if let data = drName{
              
                if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    cell.textFieldName.text = "\(String(describing: arrSelectedDr!.arabicfname!)) \(String(describing: arrSelectedDr!.arabilname!))"
                }else{
                    cell.textFieldName.text = "\(String(describing: arrSelectedDr!.firstname!)) \(String(describing: arrSelectedDr!.lastname!))"
                }
               
            }
            cell.labelTitle.text = localizeValue(key: "Doctor name who is you work with")
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantSignupCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.autocapitalizationType = .words
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.isUserInteractionEnabled = false
            cell.downArrow.isHidden = false
            cell.textFieldName.placeholder = localizeValue(key: "Hospital / Clinic name")
            cell.textFieldName.tag = 16
            if let data = arrSelectedClinic{
                
                if clinicName == "" {
                    cell.textFieldName.text = ""
                }else{
                    if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                        cell.textFieldName.text = arrSelectedClinic!.clinic_name_ar!
                    }else{
                        cell.textFieldName.text = arrSelectedClinic!.clinic_name!
                    }
                    
                }
                
             
            }else{
                 cell.textFieldName.text = ""
            }
            cell.labelTitle.text = localizeValue(key: "Hospital / Clinic name")
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 6{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssistantDoctatorListViewController") as!
            AssistantDoctatorListViewController
            vc.listType = "1"
            self.present(vc, animated: true, completion: nil)
        }else if indexPath.row == 7{
            if drName == nil{
                self.showAlert(with: "Please select Doctor".localize())
                return
            }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssistantDoctatorListViewController") as!
            AssistantDoctatorListViewController
            vc.listType = "2"
            vc.arrClenicList = arrSelectedDr!.clinic!
            self.present(vc, animated: true, completion: nil)
        }
    }
}

