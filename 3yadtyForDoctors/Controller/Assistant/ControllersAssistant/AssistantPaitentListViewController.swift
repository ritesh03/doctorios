//
//  AssistantPaitentListViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 16/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit

class AssistentPatientCell: UITableViewCell {
    
    @IBOutlet weak var imageViewPatient: RoundImage!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelday: UILabel!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonMessage: UIButton!
    @IBOutlet weak var labelClinicName: UILabel!
}

class AssistantPaitentListViewController: BaseViewControler, UISearchBarDelegate {
    
    @IBOutlet weak var searchBarPatient: UISearchBar!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tablePatientList: UITableView!
    @IBOutlet weak var errorView: UIView!
    var arrPatientList: [Patient_info]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBarPatient.delegate = self
        searchBarPatient.barTintColor = .clear
        let image = UIImage()
        searchBarPatient.setBackgroundImage(image, for: .any, barMetrics: .default)
        searchBarPatient.scopeBarBackgroundImage = image
        //searchBarPatient.showsCancelButton = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //menu Button
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.image = UIImage(named: "menu_right")!.withRenderingMode(.alwaysOriginal)
            if AppLanguage.currentAppleLanguage() != LanguageType.arabic{
                
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }else{
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
         getPatientList(searchType: "2", searchText: "", pageNo: 1)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        let searchStr = searchBar.text
        self.arrPatientList?.removeAll()
        getPatientList(searchType: "1", searchText: searchStr ?? "", pageNo: 1)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            searchBar.text = ""
            searchBar.showsCancelButton = false
            self.view.endEditing(true)
            self.arrPatientList?.removeAll()
            getPatientList(searchType: "2", searchText: "", pageNo: 1)
        }
    }
    
    func getPatientList(searchType: String, searchText: String, pageNo: Int) {
        
        let service = BusinessService()
        service.patientList(with: String(describing: AppInstance.shared.user!.id!), search_type: searchType, name: searchText, current_page: String(describing: pageNo), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
            }
            if let list = response {
                self.arrPatientList = list
                if ((self.arrPatientList?.count)!) > 0{
                    self.errorView.isHidden = true
                }else{
                    self.errorView.isHidden = false
                }
            }
            self.tablePatientList.reloadData()
        }
    }
    
    @IBAction func buttonCallClicked(_ sender: UIButton) {
        var mobile = ""
        if self.arrPatientList![sender.tag].countrycode?.first == "+"{
            mobile = "\(String(describing: self.arrPatientList![sender.tag].countrycode!))\(String(describing: self.arrPatientList![sender.tag].mobile!))"
        }else{
            mobile = "+\(String(describing: self.arrPatientList![sender.tag].countrycode!))\(String(describing: self.arrPatientList![sender.tag].mobile!))"
        }
        
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @IBAction func buttonMessageClicked(_ sender: UIButton) {
        
        let data = self.arrPatientList?[sender.tag]
        let user = ["email": data?.email ?? "","fcmUserId": String(describing: data!.id!) ,"image": data!.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data?.firstname ?? "") \(data?.lastname ?? "")"] as [String : Any]
         let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.selectedUser = Person.init(dictionary: user as NSDictionary)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AssistantPaitentListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrPatientList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) as! AssistentPatientCell
        if self.arrPatientList?[indexPath.row].image!.isEmpty != true{
            cell.imageViewPatient.sd_setShowActivityIndicatorView(true)
            cell.imageViewPatient.sd_setIndicatorStyle(.gray)
            cell.imageViewPatient.sd_setImage(with: URL(string: (self.arrPatientList?[indexPath.row].image)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewPatient.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewPatient.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        
        cell.buttonCall.tag = indexPath.row
        cell.buttonMessage.tag = indexPath.row
        
        cell.labelName.text = "\(String(describing: self.arrPatientList![indexPath.row].firstname!)) \(String(describing: self.arrPatientList![indexPath.row].lastname!))"
        cell.labelPhone.text = self.arrPatientList?[indexPath.row].mobile!
        //cell.labelday.text = self.arrPatientList?[indexPath.row].age!
        //cell.labelClinicName.text = self.arrPatientList?[indexPath.row].clinic?.last?.clinic_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let vc = storyboard?.instantiateViewController(withIdentifier: "AssistantPatientDetailViewController") as! AssistantPatientDetailViewController
        //self.navigationController?.pushViewController(vc, animated: true)
    }
}


