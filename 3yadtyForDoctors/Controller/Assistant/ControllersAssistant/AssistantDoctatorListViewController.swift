//
//  DoctatorListViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import SDWebImage

class DrCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNo: UILabel!
    @IBOutlet weak var imageViewDoctor: RoundImage!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    
    
}

class AssistantDoctatorListViewController: BaseViewControler, UISearchBarDelegate {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchBarDr: UISearchBar!
    @IBOutlet weak var labelNavTitle: UILabel!
    @IBOutlet weak var tableDrList: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    var listType: String?
    var arrDrList = [UserData]()
    var arrFilterDrList = [UserData]()
    var arrClenicList = [Clinic]()
    var arrFilterClenicList = [Clinic]()
    var isSearched = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBarDr.placeholder = localizeValue(key: "Search name..")
        // Do any additional setup after loading the view.
        searchBarDr.delegate = self
        searchBarDr.barTintColor = .clear
        let image = UIImage()
        searchBarDr.setBackgroundImage(image, for: .any, barMetrics: .default)
        searchBarDr.scopeBarBackgroundImage = image
        self.errorView.isHidden = true
        //searchBarPatient.showsCancelButton = true
        if listType == "1"{
            labelNavTitle.text = localizeValue(key: "Select Doctor")
          //API get DrList
            getDrList()
        }else{
            labelNavTitle.text = localizeValue(key: "Select clinic")
            arrFilterClenicList = arrClenicList
        }
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
  
    @IBAction func buttonBackClicked(_ sender: Any) {
        if listType == "3"{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    //get drList
    func getDrList() {
        let service = BusinessService()
        service.getDrList(with: self) { (response) in
            if response == nil{
                 self.errorView.isHidden = false
            }
            if let user = response {
                print(user)
                self.arrDrList = user
                self.arrFilterDrList = user
                 self.errorView.isHidden = true
                self.tableDrList.reloadData()
            }
        }
    }
    
    // searchbar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
        if listType == "1"{
            self.isSearched = true
            arrFilterDrList = arrDrList.filter() { ($0.firstname?.contains(searchText))! || ($0.lastname?.contains(searchText))! || ($0.mobile?.contains(searchText))!}
            if searchText == ""{
                self.isSearched = false
                arrFilterDrList = arrDrList
            }
            if arrFilterDrList.count < 1 {
                self.errorView.isHidden = false
            }else{
                self.errorView.isHidden = true
            }
           
        }else{
            self.isSearched = true
            arrFilterClenicList = arrClenicList.filter() { ($0.clinic_name?.contains(searchText))! || ($0.clinic_address?.contains(searchText))!}
            if searchText == ""{
                self.isSearched = false
                arrFilterClenicList = arrClenicList
            }
            if arrFilterClenicList.count < 1 {
                self.errorView.isHidden = false
            }else{
                self.errorView.isHidden = true
            }
        }
         self.tableDrList.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if listType == "1"{
            if searchBar.text == nil{
                arrFilterDrList = arrDrList
            }
            if arrFilterDrList.count < 1 {
                self.errorView.isHidden = false
            }else{
                self.errorView.isHidden = true
            }
            
        }else{
            if searchBar.text == nil{
                arrFilterClenicList = arrClenicList
            }
            if arrFilterClenicList.count < 1 {
                self.errorView.isHidden = false
            }else{
                self.errorView.isHidden = true
            }
        }
        self.tableDrList.reloadData()
    }

}

extension AssistantDoctatorListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listType == "1"{
            return arrFilterDrList.count
        }
        return arrFilterClenicList.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrCell", for: indexPath) as! DrCell
         cell.labelNo.isHidden = true
       if listType == "1"{
        let dr = arrFilterDrList[indexPath.row]
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             cell.labelName.text = "\(String(describing: dr.arabicfname!)) \(String(describing: dr.arabilname!))"
        }else{
            cell.labelName.text = "\(String(describing: dr.firstname!)) \(String(describing: dr.lastname!))"
        }
        if dr.countrycode?.first == "+"{
              cell.labelNo.text = "\(dr.countrycode!) \(dr.mobile!)"
        }else{
            cell.labelNo.text = "+\(dr.countrycode!) \(dr.mobile!)"
        }
        
        if dr.image?.isEmpty != true{
            cell.imageViewDoctor.sd_setShowActivityIndicatorView(true)
            cell.imageViewDoctor.sd_setIndicatorStyle(.gray)
            cell.imageViewDoctor.sd_setImage(with: URL(string: Config.imageBaseUrl + dr.image! ?? ""), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewDoctor.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewDoctor.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
            cell.imageViewWidthConstraint.constant = 50
       }else{
         let dr = arrFilterClenicList[indexPath.row]
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
             cell.labelName.text = dr.clinic_name_ar
        }else{
             cell.labelName.text = dr.clinic_name
        }
        
             cell.labelNo.text = dr.clinic_address
             cell.imageViewWidthConstraint.constant = 0
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if listType == "3"{
            self.navigationController?.popViewController(animated: true)
        }else{
            if listType == "1"{
                print(arrFilterDrList[indexPath.row])
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "drSelected"), object: arrFilterDrList[indexPath.row])
            }else{
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "clinicSelected"), object: arrClenicList[indexPath.row])
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 79
    }
}
