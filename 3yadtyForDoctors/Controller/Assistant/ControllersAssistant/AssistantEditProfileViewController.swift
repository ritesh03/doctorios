//
//  AssistantEditProfileViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 25/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import MRCountryPicker

class AssistantEditProfileViewController:  BaseViewControler , MRCountryPickerDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewForCountaryPicker: UIView!
    @IBOutlet weak var tableEditAssistantSignupInfo: UITableView!
    @IBOutlet weak var imageViewImage: RoundButton!
    
    let myPickerController = UIImagePickerController()

    var flagImage: UIImage?
    var countryCode: String?
    var fName: String?
    var lName: String?
    var emailId: String?
    var doctorName: String?
    var clinicName: String?
    var phone: String?
    var imagesArray = [File]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            backButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        saveButton.setTitle(localizeValue(key: "Save"), for: .normal)
        titleLabel.text = localizeValue(key: "Edit Profile")
        myPickerController.delegate = self
        viewForCountaryPicker.isHidden = true
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        let getCountryCode = Locale.current.regionCode
        countryPicker.setCountry(getCountryCode!)
        
        let countryName = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: getCountryCode!)
        countryPicker.setCountryByName(countryName!)
        countryPicker.setLocale("EN")
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        imageViewImage.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        imageViewImage.sd_addActivityIndicator()
        imageViewImage.sd_showActivityIndicatorView()
        imageViewImage.sd_setImage(with: URL(string: Config.imageBaseUrl + (AppInstance.shared.user?.image ?? "")), for: .normal, placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"), options: [], completed: nil)
        
        fName = AppInstance.shared.user?.firstname ?? ""
        lName = AppInstance.shared.user?.lastname ?? ""
        emailId = AppInstance.shared.user?.email ?? ""
        doctorName = AppInstance.shared.user?.doctor_name ?? ""
        clinicName = AppInstance.shared.user?.clinic_name ?? ""
        phone = AppInstance.shared.user?.mobile ?? ""
        countryCode = AppInstance.shared.user?.countrycode ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonCancelClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        viewForCountaryPicker.isHidden = true
    }
    
    @IBAction func buttonSaveClicked(_ sender: Any) {
        assistantEditProfile()
    }
    
    @IBAction func buttonCountaryClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        viewForCountaryPicker.isHidden = false
    }
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.flagImage = flag
        self.countryCode = countryCode
        //AppInstance.shared.selectedFlag = flag
        //AppInstance.shared.countryCode = phoneCode
    }
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if textField.tag == 10{
            fName = textField.text
        }else if textField.tag == 11{
            lName = textField.text
        }
    }
 
    @IBAction func buttonBackClicked(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController!.pushViewController(VC, animated: false)
    }
    @IBAction func buttonProfileClicked(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Camera"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                self.myPickerController.allowsEditing = true
                self.present(self.myPickerController, animated: true, completion: nil)
            }else{
                UIAlertController.show(title: nil, message: "No camera available.", target: self, handler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Gallery"), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.myPickerController.allowsEditing = true
            self.present(self.myPickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: localizeValue(key: "Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let imageName: String?
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageName = "\(currentTimeStamp).jpg" //to make imagename
            imageViewImage.setImage(chosenImage, for: .normal)
            let file = File(name: imageName!, image: chosenImage, type: 1)
            if  imagesArray.count > 0 {
                imagesArray.removeLast()
            }
            imagesArray.append(file)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func assistantEditProfile() {
        
        let service = BusinessService()
        service.assistantEditProfile(with: String(describing: AppInstance.shared.user!.id!), firstname: fName!, lastname: lName!, image: imagesArray, target: self) { (response) in
            if let user = response {
                print(user)
                AppInstance.shared.user = user.data!
                UserDefaults.standard.set(user.data!.dictionaryRepresentation() , forKey: "isLogIn")
                //let storyboard = UIStoryboard(name: "Assistant", bundle: nil)
                //let vc = storyboard.instantiateViewController(withIdentifier: "AssistantTabbarController") as! UITabBarController
                //self.navigationController?.pushViewController(vc, animated: true)
                let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Assistant")).instantiateViewController(withIdentifier: "AssistantSignUpSucessPopUp") as! AssistantSignUpSucessPopUp
                vc.delegate = self
                vc.isFromEdit = true
                vc.modalPresentationStyle = .overCurrentContext
                vc.view.backgroundColor =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
                self.present(vc, animated: false, completion: nil)
            }
        }
    }
}

extension AssistantEditProfileViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantEditProfileCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .words
            cell.textFieldName.placeholder = localizeValue(key: "First name")
            if fName?.isEmpty != true{
                 cell.textFieldName.text = fName
            }
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.textColor = UIColor.black
            cell.textFieldName.isUserInteractionEnabled = true
            cell.labelTitle.text = localizeValue(key: "First name")
            cell.downArrow.isHidden = true
            cell.textFieldName.tag = 10
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantEditProfileCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .words
            cell.textFieldName.placeholder = localizeValue(key: "Last name")
            if lName?.isEmpty != true{
                cell.textFieldName.text = lName
            }
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.textColor = UIColor.black
            cell.textFieldName.isUserInteractionEnabled = true
            cell.labelTitle.text = localizeValue(key: "Last name")
            cell.downArrow.isHidden = true
            cell.textFieldName.tag = 11
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as! AssistantEditProfileCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.tfPhone.isSecureTextEntry = false
            cell.tfPhone.isUserInteractionEnabled = true
            cell.imageViewFlag.image = self.flagImage
            if self.countryCode?.first == "+"{
                cell.labelCountaryCode.text = self.countryCode
            }else{
                cell.labelCountaryCode.text = "+\(self.countryCode!)"
            }
            
            cell.tfPhone.placeholder = localizeValue(key: "Enter phone number")
            if #available(iOS 10.0, *) {
                cell.tfPhone.keyboardType = .asciiCapableNumberPad
            } else {
                // Fallback on earlier versions
            }
            if phone?.isEmpty != true{
                cell.tfPhone.text = phone
            }
            
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.tfPhone.textAlignment = .right
                
            }
            cell.tfPhone.textColor = !(cell.tfPhone.text!.isEmpty) ? UIColor.lightGray : UIColor.black
            cell.tfPhone.isUserInteractionEnabled = false
            cell.tfPhone.keyboardType = .phonePad
            return cell
          
         case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantEditProfileCell
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.labelTitle.text = localizeValue(key: "Email address")
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .none
            cell.textFieldName.placeholder = "example@email.com"
            if emailId?.isEmpty != true{
                cell.textFieldName.text = emailId
            }
            cell.textFieldName.textColor = !(cell.textFieldName.text!.isEmpty) ? UIColor.lightGray : UIColor.black
            cell.textFieldName.keyboardType = .emailAddress
            cell.textFieldName.isUserInteractionEnabled = false
            cell.downArrow.isHidden = true
            cell.textFieldName.tag = 12
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantEditProfileCell
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .words
            if doctorName?.isEmpty != true{
                cell.textFieldName.text = doctorName
            }
            cell.textFieldName.textColor = !(cell.textFieldName.text!.isEmpty) ? UIColor.lightGray : UIColor.black
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.isUserInteractionEnabled = false
            cell.labelTitle.text = localizeValue(key: "Doctor name who is you work with")
            cell.textFieldName.placeholder = localizeValue(key: "Doctor name who is you work with")
            cell.downArrow.isHidden = true
            cell.textFieldName.tag = 10
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! AssistantEditProfileCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            
            if  LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                cell.labelTitle.textAlignment = .right
                cell.textFieldName.textAlignment = .right
            }
            cell.textFieldName.isSecureTextEntry = false
            cell.textFieldName.autocapitalizationType = .words
            if doctorName?.isEmpty != true{
                cell.textFieldName.text = AppInstance.shared.user?.clinic_name
            }
            cell.textFieldName.textColor = !(cell.textFieldName.text!.isEmpty) ? UIColor.lightGray : UIColor.black
            cell.textFieldName.isUserInteractionEnabled = false
            cell.labelTitle.text = localizeValue(key: "Enter clinic name")
            cell.textFieldName.placeholder = localizeValue(key: "Enter clinic name")
            cell.downArrow.isHidden = true
            cell.textFieldName.tag = 10
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension AssistantEditProfileViewController : assistantSignUpSucessDelegate{
    
    func signUpSucess() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = mainStoryboard.instantiateViewController(withIdentifier: "TabbarController")
        self.navigationController!.pushViewController(VC, animated: false)
    }
}

