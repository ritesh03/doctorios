//
//  AssistantAppointmentsListViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class AssistantAppointmentsListViewController: BaseViewControler, IndicatorInfoProvider {
    
    @IBOutlet weak var tableAppointmentList: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    var arrAcceptedAppointment: Appointment?
    var statisticsData: Appointment_data?
    var appointmentListData: DataAppointment?
    var appointments: [DataAppointmentList]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
         self.errorView.isHidden = true
        self.getAcceptedAppointmentList()
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self,selector:#selector(self.AssistantAppointmentSuccess),
                                               name:NSNotification.Name(rawValue: "AssistantAppointmentSuccess"),object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getAcceptedAppointmentList()
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "List"))
    }
    
    func getAcceptedAppointmentList() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        print(result)
        //String(AppInstance.shared.user.id!)
        let clinicId = AppInstance.shared.user?.clinic_id
        let service = BusinessService()
        service.acceptedAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: "1", date: result, type: "2", doctor_id: String(describing: AppInstance.shared.user!.dr_id!), clinic_id: String(describing: clinicId!), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
                self.arrAcceptedAppointment = nil
                self.statisticsData = nil
                self.appointmentListData = nil
                self.appointments = nil
            }
            if let user = response {
                self.arrAcceptedAppointment = user
                self.statisticsData = user.appointment_data!
                self.appointmentListData = user.data!
                self.appointments = user.data!.data!
                if self.statisticsData?.new != 0 || self.statisticsData?.new != 0 || (self.appointments?.count)! > 0 || self.statisticsData?.total_pending_reservation != 0 {
                    self.errorView.isHidden = true
                }
                else{
                    self.errorView.isHidden = false
                }
                self.tableAppointmentList.reloadData()
            }
        }
    }
    @objc func AssistantAppointmentSuccess(notification: NSNotification){
         self.getAcceptedAppointmentList()
    }
    
    @IBAction func buttonCallClicked(_ sender: UIButton) {
        let mobile = self.appointments![sender.tag].patient_info?.mobile
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile!))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @IBAction func buttonMessageClicked(_ sender: UIButton) {
        let data = self.appointments?[sender.tag]
        if  (data!.patient_info?.image?.isEmpty != true){
            let user = ["email": data?.patient_info?.email ?? "","fcmUserId": String(describing: data!.patient_info!.id!) ,"image": data?.patient_info?.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data?.patient_info?.firstname ?? "") \(data?.patient_info?.lastname ?? "")"] as [String : Any]
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.selectedUser = Person.init(dictionary: user as NSDictionary)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func buttonSelectedCell(_ sender: UIButton) {
        let data = self.appointments?[sender.tag]
        if data!.visit_type != 0{
            let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
            AppInstance.shared.patientListSelectedPatient = data?.patient_info
            AppInstance.shared.isShowHistoryHeaderCell = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func buttonCheckInClicked(_ sender: UIButton) {
        let data = self.appointments?[sender.tag]
        let service = BusinessService()
        service.patientCheckinDr(with: String(describing: AppInstance.shared.user!.id!), appointment_id: String(describing: data!.id!), target: self) { (response) in
            if response == true {
                data!.patient_checkin = 1
                let indexPath = NSIndexPath(row: sender.tag, section: 1)
                self.tableAppointmentList.reloadRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.top)
            }
        }
    }
}

extension AssistantAppointmentsListViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if statisticsData?.total_pending_reservation != 0{
                if statisticsData?.new != 0 || statisticsData?.new != 0{
                    return 2
                }
                return 1
            }
            return 0
        }
        return  self.appointments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            if statisticsData?.total_pending_reservation != 0{
                if statisticsData?.new != 0 || statisticsData?.new != 0{
                    switch indexPath.row {
                    case 0:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "reservationCell", for: indexPath) as! AssistantAppointmentListCell
                        if statisticsData?.total_pending_reservation! != nil{
                            cell.labelPendingReservation.text = "You have \(String(describing: statisticsData!.total_pending_reservation!)) reservation pending.. "
                        }
                        return cell
                    default:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "patientCell", for: indexPath) as! AssistantAppointmentListCell
                        //                cell.labelDrName.text =
                        if statisticsData?.total_patient! != nil{
                            cell.labelNoOfPatient.text = "Today you have \(String(describing: statisticsData!.total_patient!)) patients"
                            cell.labelNewVisit.text = "\(String(describing: statisticsData!.new!))"
                            cell.labelConsultation.text = "\(String(describing: statisticsData!.consult!))"
                        }
                        return cell
                    }
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "reservationCell", for: indexPath) as! AssistantAppointmentListCell
                    if statisticsData?.total_pending_reservation! != nil{
                        cell.labelPendingReservation.text = "You have \(String(describing: statisticsData!.total_pending_reservation!)) reservation pending.. "
                    }
                    return cell
                }
            }else{
                if statisticsData?.new != 0 || statisticsData?.new != 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "patientCell", for: indexPath) as! AssistantAppointmentListCell
                    //                cell.labelDrName.text =
                    if statisticsData?.total_patient! != nil{
                        cell.labelNoOfPatient.text = "Today you have \(String(describing: statisticsData!.total_patient!)) patients"
                        cell.labelNewVisit.text = "\(String(describing: statisticsData!.new!))"
                        cell.labelConsultation.text = "\(String(describing: statisticsData!.consult!))"
                    }
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "reservationCell", for: indexPath) as! AssistantAppointmentListCell
                    if statisticsData?.total_pending_reservation! != nil{
                        cell.labelPendingReservation.text = "You have \(String(describing: statisticsData!.total_pending_reservation!)) reservation pending.. "
                    }
                    return cell
                }
            }
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! AssistantAppointmentListCell

            let data = self.appointments?[indexPath.row]
            cell.labelAsName.text = "\(data?.patient_info?.firstname ?? "") \(data?.patient_info?.lastname ?? "")"
            if data?.patient_info?.image!.isEmpty != true{
                cell.imageViewList.sd_setShowActivityIndicatorView(true)
                cell.imageViewList.sd_setIndicatorStyle(.gray)
                cell.imageViewList.sd_setImage(with: URL(string: (data?.patient_info?.image)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
                cell.imageViewList.sd_setShowActivityIndicatorView(false)
            }else{
                cell.imageViewList.image = UIImage(named: "imgUserPlaceholderSideMenu")
            }
            
            cell.labelClinicName.text = data?.clinic?.clinic_name!
            if data?.appointment_type == 1{
                cell.labelAppointmentType.text = localizeValue(key: "New Visit")
            }else if data?.appointment_type == 2{
                cell.labelAppointmentType.text = localizeValue(key: "Consultation")
            }
            cell.buttonCall.tag = indexPath.row
            cell.buttonMessage.tag = indexPath.row
            cell.buttonSelectedCell.tag = indexPath.row
            cell.buttonCheckedIn.tag = indexPath.row
            
            if data!.visit_type == 0{
                let dateStr = TimeUtils.sharedUtils.chaneDateFormet((data?.appointment_date)!)
                cell.labelAsDate.text = "\(dateStr) | \(data?.set_time ?? "")"
                
                cell.labelOnSight.isHidden = false
                cell.buttonLogo.isHidden = true
                cell.labelTokenNo.text = ""
                cell.labelNo.text = ""
                cell.buttonCheckedIn.isHidden = true
                cell.buttonCall.isHidden = true
                cell.buttonMessage.isHidden = true
                cell.lineVartical.isHidden = true
                cell.lineHorizontal.isHidden = true
                cell.lineToken.isHidden = true
            }else{
                let dateStr = TimeUtils.sharedUtils.chaneDateFormet((data?.appointment_date)!)
                cell.labelAsDate.text = "\(dateStr) | \(data?.set_time ?? "")"
                
                cell.labelOnSight.isHidden = true
                cell.buttonLogo.isHidden = false
                cell.labelTokenNo.text = "Token no.".localize()
                cell.labelNo.text = String(describing: data!.token_number!)
                cell.buttonCheckedIn.isHidden = false
                cell.buttonCall.isHidden = false
                cell.buttonMessage.isHidden = false
                cell.lineVartical.isHidden = false
                cell.lineHorizontal.isHidden = false
                cell.lineToken.isHidden = false
                cell.buttonCheckedIn.isHidden = false
                
                if data!.patient_checkin == 0{
                    cell.buttonCheckedIn.setTitle(localizeValue(key: "Check Up"), for: .normal)
                    cell.buttonCheckedIn.borderColor = Color.selected_color
                }else{
                    cell.buttonCheckedIn.setTitle("Checked Up".localize(), for: .normal)
                    cell.buttonCheckedIn.borderColor = Color.unSelected_color
                }
            }
            return cell
        }
    }
     
        
 //       if indexPath.section == 0{
//            switch indexPath.row {
//            case 0:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "reservationCell", for: indexPath) as! AssistantAppointmentListCell
//                if statisticsData?.total_pending_reservation! != nil{
//                    cell.labelPendingReservation.text = "You have \(String(describing: statisticsData!.total_pending_reservation!)) reservation pending.. "
//                }
//                return cell
//            default:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "patientCell", for: indexPath) as! AssistantAppointmentListCell
////                cell.labelDrName.text =
//                if statisticsData?.total_patient! != nil{
//                    cell.labelNoOfPatient.text = "Today you have \(String(describing: statisticsData!.total_patient!)) patients"
//                    cell.labelNewVisit.text = "\(String(describing: statisticsData!.new!))"
//                    cell.labelConsultation.text = "\(String(describing: statisticsData!.consult!))"
//                }
//                return cell
//            }
//        }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! AssistantAppointmentListCell
//            cell.labelAsName.text = self.appointments?[indexPath.row].patient_info?.firstname
////            cell.imageViewAssistant
//            cell.labelAsDate.text = self.appointments?[indexPath.row].appointment_date
//            cell.labelNo.text = String(describing: self.appointments![indexPath.row].token_number!)
//
//            if self.appointments?[indexPath.row].appointment_type == 1{
//                cell.labelAppointmentType.text = "New Visit"
//            }else if self.appointments?[indexPath.row].appointment_type == 2{
//                cell.labelAppointmentType.text = "Consultation"
//            }
//            return cell
//        }
  //  }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssistantNewReservationsViewController") as! AssistantNewReservationsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            if (self.appointments?.count)! > 0{
                let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! AssistantAppointmentListCell
                if self.appointments!.count > 1{
                    headerView.headerTotalPatient.text = "\(String(describing: self.appointments!.count)) Patient's"
                }else{
                    headerView.headerTotalPatient.text = "\(String(describing: self.appointments!.count)) Patient"
                }
                return headerView
            }else{
                return nil
            }
          
        }else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            if (self.appointments?.count)! > 0{
                return 35
            }
            return 0
        }else{
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if statisticsData != nil{
            return 2
        }
            return 0
    }
    
}

