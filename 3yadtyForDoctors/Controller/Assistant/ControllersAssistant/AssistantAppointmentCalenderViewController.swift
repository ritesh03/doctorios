//
//  AssistantAppointmentCalenderViewController.swift
//  3yadtyForDoctors
//
//  Created by apple on 15/10/18.
//  Copyright © 2018 Xperge. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import FSCalendar


class AssistantAppointmentCalenderViewController: BaseViewControler, IndicatorInfoProvider, FSCalendarDataSource, FSCalendarDelegate {
    
    fileprivate weak var calendar: FSCalendar!
    let formatter = DateFormatter()
    
    var arrAcceptedAppointment: Appointment?
    var statisticsData: Appointment_data?
    var appointmentListData: DataAppointment?
    var appointments: [DataAppointmentList]?
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var tableAssitantList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        errorView.isHidden = true
//        let date = Date()
//        formatter.dateFormat = "yyyy-MM-dd"
//        let result = formatter.string(from: date)
//        print(result)
//        getAcceptedAppointmentList(date: result)
    }
    override func viewWillAppear(_ animated: Bool) {
        let date = Date()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        print(result)
        getAcceptedAppointmentList(date: result)
    }
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: localizeValue(key: "Calendar"))
    }
    //FSCalendar
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(date))")
        let result = formatter.string(from: date)
        print(result)
        getAcceptedAppointmentList(date: result)
    }
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(date)")
    }
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        //disable previous date
        let curDate = Date().addingTimeInterval(-24*60*60)
        if date < curDate {
            return false
        } else {
            return true
        }
    }
//    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//
//        for arr in (AppInstance.shared.user?.clinic)! {
//            for dateStr in arr.availability! {
//                if(dateFormatter.string(from: date) == dateStr.day)
//                {
//                    return 1
//                }
//            }
//        }
//        return 0
//    }
    
    func getAcceptedAppointmentList(date: String) {
        //String(AppInstance.shared.user.id!)
        let clinicId = AppInstance.shared.user?.clinic_id
        let service = BusinessService()
        service.acceptedAppointments(with: String(describing: AppInstance.shared.user!.id!), current_page: "1", date: date, type: "1", doctor_id: String(describing: AppInstance.shared.user!.dr_id!), clinic_id: String(describing: clinicId!), target: self) { (response) in
            if response == nil{
                self.errorView.isHidden = false
                self.arrAcceptedAppointment = nil
                self.statisticsData = nil
                self.appointmentListData = nil
                self.appointments = nil
            }
            if let user = response {
                self.arrAcceptedAppointment = user
                self.statisticsData = user.appointment_data!
                self.appointmentListData = user.data!
                self.appointments = user.data!.data!
                if self.statisticsData?.new != 0 || self.statisticsData?.new != 0
                    || (self.appointments?.count)! > 0 ||
                    self.statisticsData?.total_pending_reservation != 0 {
                    self.errorView.isHidden = true
                }
                else{
                    self.errorView.isHidden = false
                }
                self.tableAssitantList.reloadData()
            }
        }
    }
    
    @IBAction func buttonCallClicked(_ sender: UIButton) {
        let mobile = self.appointments![sender.tag].patient_info?.mobile
        if let mobileCallUrl = URL(string: "tel://\(String(describing: mobile!))") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    @IBAction func buttonMessageClicked(_ sender: UIButton) {
        let data = self.appointments?[sender.tag]
        if  (data!.patient_info?.image?.isEmpty != true){
            let user = ["email": data?.patient_info?.email ?? "","fcmUserId": String(describing: data!.patient_info!.id!) ,"image": data?.patient_info?.image ?? "","lastMessage":"","fcmToken":"","userName": "\(data?.patient_info?.firstname ?? "") \(data?.patient_info?.lastname ?? "")"] as [String : Any]
            let vc  = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.selectedUser = Person.init(dictionary: user as NSDictionary)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func buttonSelectedCell(_ sender: UIButton) {
        let data = self.appointments?[sender.tag]
        if data!.visit_type != 0{
            let vc = CustomObjects.getStoryBoard(name:Helper.getStoryboardName(for: "Main")).instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
            AppInstance.shared.patientListSelectedPatient = data?.patient_info
            AppInstance.shared.isShowHistoryHeaderCell = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func buttonCheckInClicked(_ sender: UIButton) {
        let data = self.appointments?[sender.tag]
        let service = BusinessService()
        service.patientCheckinDr(with: String(describing: AppInstance.shared.user!.id!), appointment_id: String(describing: data!.id!), target: self) { (response) in
            if response == true {
                data!.patient_checkin = 1
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                self.tableAssitantList.reloadRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.top)
            }
        }
    }
}

extension AssistantAppointmentCalenderViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appointments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! AssistantAppointmentCalanderCell
        let data = self.appointments?[indexPath.row]
        cell.labelAsName.text = "\(data?.patient_info?.firstname ?? "") \(data?.patient_info?.lastname ?? "")"
        if data?.patient_info?.image!.isEmpty != true{
        
            cell.imageViewCalander.sd_setShowActivityIndicatorView(true)
            cell.imageViewCalander.sd_setIndicatorStyle(.gray)
            cell.imageViewCalander.sd_setImage(with: URL(string: (data?.patient_info?.image)!), placeholderImage: UIImage(named: "imgUserPlaceholderSideMenu"))
            cell.imageViewCalander.sd_setShowActivityIndicatorView(false)
        }else{
            cell.imageViewCalander.image = UIImage(named: "imgUserPlaceholderSideMenu")
        }
        
        cell.labelClinicName.text = data?.clinic?.clinic_name!
        if data?.appointment_type == 1{
            cell.labelAppointmentType.text = localizeValue(key: "New Visit")
        }else if data?.appointment_type == 2{
            cell.labelAppointmentType.text = localizeValue(key: "Consultation")
        }
        cell.buttonCall.tag = indexPath.row
        cell.buttonMessage.tag = indexPath.row
        cell.buttonSelectedCell.tag = indexPath.row
        cell.buttonCheckedIn.tag = indexPath.row
        
        if data!.visit_type == 0{
            let dateStr = TimeUtils.sharedUtils.chaneDateFormet((data?.appointment_date)!)
            cell.labelAsDate.text = "\(dateStr) | \(data?.set_time ?? "")"
            
            cell.labelOnSight.isHidden = false
            cell.buttonLogo.isHidden = true
            cell.labelTokenNo.text = ""
            cell.labelNo.text = ""
            cell.buttonCheckedIn.isHidden = true
            cell.buttonCall.isHidden = true
            cell.buttonMessage.isHidden = true
            cell.lineVartical.isHidden = true
            cell.lineHorizontal.isHidden = true
            cell.lineToken.isHidden = true
        }else{
            let dateStr = TimeUtils.sharedUtils.chaneDateFormet((data?.appointment_date)!)
            cell.labelAsDate.text = "\(dateStr) | \(data?.set_time ?? "")"
            
            cell.labelOnSight.isHidden = true
            cell.buttonLogo.isHidden = false
            cell.labelTokenNo.text = "Token no.".localize()
            cell.labelNo.text = String(describing: data!.token_number!)
            cell.buttonCheckedIn.isHidden = false
            cell.buttonCall.isHidden = false
            cell.buttonMessage.isHidden = false
            cell.lineVartical.isHidden = false
            cell.lineHorizontal.isHidden = false
            cell.lineToken.isHidden = false
            cell.buttonCheckedIn.isHidden = false
            
            if data!.patient_checkin == 0{
                cell.buttonCheckedIn.setTitle("Check Up".localize(), for: .normal)
                cell.buttonCheckedIn.borderColor = Color.selected_color
            }else{
                cell.buttonCheckedIn.setTitle("Checked Up".localize(), for: .normal)
                cell.buttonCheckedIn.borderColor = Color.unSelected_color
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! AssistantAppointmentCalanderCell
            if self.appointments!.count > 1{
                headerView.headerTotalPatient.text = "\(String(describing: self.appointments!.count)) Patient's"
            }else{
                headerView.headerTotalPatient.text = "\(String(describing: self.appointments!.count)) Patient"
            }
            return headerView
        }else{
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            if (self.appointments?.count)! > 0{
                return 35
            }
            return 0
        }else{
            return 0
        }
    }
    
}

